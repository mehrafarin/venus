<?php
/* Template Name:  برگه - بلاگ */
get_header(); ?>
<?php if (iwp_Shield::is_activated() === true) : ?>
    <div class="container cont-page mt-5 pt-3">
<!--        <h1 class="title-page-shop text-center">--><?php //the_title(); ?><!--</h1>-->
        <div class="row post-categories-page">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="col-md-4 category-post-style">
                    <a href="<?php the_permalink(); ?>">
                        <figure>
                            <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                 alt="<?php echo the_title(); ?>">
                        </figure>
                        <div class="blog-posts-inner">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>

