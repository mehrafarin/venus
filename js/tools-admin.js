jQuery(document).ready(function ($) {
    var uploader;
    $(document).on('click', '#ve_up_bg_single_product', function (e) {
        e.preventDefault();

        if (uploader) {
            uploader.open();
            return;
        }

        uploader = wp.media({
            title: 'بکگراند محصول',
            button: {
                text: 'انتخاب تصویر',
            },
            multiple: false
        });
        uploader.on('select', function () {
            var att = uploader.state().get('selection').first().toJSON();

            $('#ve_show_bg_product').attr('src', att.url);
            $('#ve-link-bg-product').val(att.url);
        })
    })
})
