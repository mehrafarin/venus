jQuery(document).ready(function () {

    jQuery('#right-menu').sidr({
        name: 'sidr-r',
        side: 'right'
    });

    jQuery(document.body).click(function (e) {
        // If a sidr is open.
        if (jQuery.sidr('status').opened) {
            var isBlur = true;
            // If the event is not coming from within the sidr.
            if (jQuery(e.target).closest('.sidr').length !== 0) {
                isBlur = false;
            }
            // If the event is not coming from within a trigger.
            if (jQuery(e.target).closest('.js-sidr-trigger').length !== 0) {
                isBlur = false;
            }
            // Close sidr is unfocused.
            if (isBlur) {
                jQuery.sidr('close', jQuery.sidr('status').opened);
            }
        }
    });

});

/* Scroll top */
jQuery(document).ready(function () {

    var scrollTop = jQuery("#scrollTop");

    jQuery(window).scroll(function () {

        var topPos = jQuery(this).scrollTop();


        if (topPos > 500) {

            jQuery(scrollTop).css("opacity", "1");

        } else {
            jQuery(scrollTop).css("opacity", "0");
        }

    });

    jQuery(scrollTop).click(function () {
        jQuery('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;

    });

});

/* Header Fixed */
jQuery(document).ready(function () {

    var scrollTop = jQuery(".venus-header-fixed");

    jQuery(window).scroll(function () {

        var topPos = jQuery(this).scrollTop();


        if (topPos > 500) {

            jQuery(scrollTop).addClass('fixed-active');

        } else {
            jQuery(scrollTop).removeClass('fixed-active');
        }

    });

});


/* Menu Phone */
var venusTheme;
(function ($) {
    'use strict';
    venusTheme = (function () {
        var body = $('body');
        return {
            init: function () {
                this.dropdwon_arrow_mobile();
                this.animatedCounter();
                this.sticky_sidebar();
                this.dropdwon_arrow_sidebar();
                this.countDownTimer();

            },
            /**
             * Mobile Navigation DropDown
             */
            dropdwon_arrow_mobile: function () {
                $(".menu-mob ul.sub-menu").before("<i class='sub-menu-arrow fa fa-angle-left'></a>");
                $(".menu-mob .sub-menu-arrow").click(function () {
                    if ($(this).hasClass("fa-angle-left")) {
                        $(this).next("ul.sub-menu").show(500);
                        $(this).removeClass("fa-angle-left").addClass("fa-angle-down");
                    } else {
                        $(this).next("ul.sub-menu").hide(500);
                        $(this).removeClass("fa-angle-down").addClass("fa-angle-left");
                    }
                });
            },

            dropdwon_arrow_sidebar: function () {

                $(".sidebar-widgets-wrapper .widget_nav_menu ul>li").click(function () {
                    if ($(this).hasClass("menu-item-has-children")) {
                        $(this).toggleClass("opened");
                    }
                });
            },
            /**
             * Count Down Timer
             */
            countDownTimer: function () {
                $('.countdown-item').each(function () {
                    $(this).countdown($(this).data('date'), function (event) {
                        $(this).html(event.strftime(''
                            + '<div class="countvenus-col"><span class="countdown-unit countdown-days"><span class="number">%-D </span><span class="text">' + venus_sell_name.countdown_days + '</span></span></div> '
                            + '<div class="countvenus-col"><span class="countdown-unit countdown-hours"><span class="number">%H </span><span class="text">' + venus_sell_name.countdown_hours + '</span></span></div> '
                            + '<div class="countvenus-col"><span class="countdown-unit countdown-min"><span class="number">%M </span><span class="text">' + venus_sell_name.countdown_mins + '</span></span></div> '
                            + '<div class="countvenus-col"><span class="countdown-unit countdown-sec"><span class="number">%S </span><span class="text">' + venus_sell_name.countdown_sec + '</span></span></div>'));
                    });
                })
            },
            /**
             * Animated Counter
             */
            animatedCounter: function () {

                var counters = $('.counter-number');

                if (counters.length) {
                    counters.each(function () {
                        var counter = $(this);
                        counter.appear(function () {
                            counter.parent().css({'opacity': 1});

                            //Counter zero type
                            var max = parseFloat(counter.text());
                            counter.countTo({
                                from: 0,
                                to: max,
                                speed: 1500,
                                refreshInterval: 100
                            });

                        }, {accX: 0, accY: 0});
                    });
                }
            },
            /**
             * Sticky Sidebar
             */
            sticky_sidebar: function () {

                var offsetTop = 30;
                var headerstick = 50;


                if ($("body.fixed-body").length) {
                    offsetTop += headerstick + $("#wpadminbar").outerHeight();
                }


                if ($('.sticky-sidebar').length > 0) {
                    $(".sticky-sidebar").theiaStickySidebar({
                        "containerSelector": "",
                        "additionalMarginTop": offsetTop,
                        "additionalMarginBottom": "0",
                        "updateSidebarHeight": false,
                        "minWidth": "768",
                        "sidebarBehavior": "modern"
                    });
                }

            },
        }
    }());
})(jQuery);
jQuery(document).ready(function () {
    venusTheme.init();
});
(function ($) {
    'use strict';

    jQuery(document).ready(function ($) {

        $(document).on('click', 'ul.dl-list li a.title', function (event) {
            event.preventDefault();
            $('ul.dl-list').toggleClass('d-none');
            $('.ve-notify-title').toggleClass('d-none');
            $('.ve-loader-js').toggleClass('d-block');
            $(this).next('.ul.dl-list').slideToggle(300);
            setTimeout(function () {
                $('#ve-loader-js').removeClass('d-block');
                $('#ve-loader-js').toggleClass('d-none');
            }, 1400);
        });

        function SigmaReturnSwal(num) {
            let Toast;
            switch (num) {
                case 1:
                    Toast = Swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 2000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer), toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    });
                    break;
                case 2:
                    Toast = Swal.mixin({
                        toast: true, position: 'center', showConfirmButton: false, timer: 2000,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer), toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    });
                    break;
                default:
                    Toast = Swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 10000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer), toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    });
                    break;
            }
            return Toast;
        }

        $(document).on('click', 'a.btn--back', function (event) {
            event.preventDefault();
            $('ul.dl-list').removeClass('d-none');
            $('.ve-notify-title').removeClass('d-none');
            $('.ve-loader-js').toggleClass('d-block');
            setTimeout(function () {
                $('#ve-loader-js').removeClass('d-block');
                $('#ve-loader-js').toggleClass('d-none');
            }, 1000);
        });

        $(document).on('click', '.my-account-section-title', function (event) {
            event.preventDefault();
            $('.ve-panel-navigation').toggleClass('mas-rot');
            $('.ve-panel-navigation').toggleClass('show');
            $('.my-account-section-title').toggleClass('my-account-section-title-r');
        });

        var element = document.querySelector('.yith-wcdp'); // returns only first match

        if (element) {
            document.querySelector('.price').setAttribute('hidden', true);
        }

    });
})(jQuery);

jQuery(document).ready(function ($) {

    $('.video-button a').click(function (event) {
        event.preventDefault();
        var post_id = $(this).attr('data-post-id');
        var url = $(this).attr('href');

        $('.video_popup_wrrapper').fadeIn();
        $('body').find('.video_popup_wrrapper .video_popup_inner').html('<div class="spinner"> <div class="double-bounce1"></div> <div class="double-bounce2"></div></div>');

        $.ajax({
            type: 'post',
            url: woocommerce_params['ajax_url'],
            data: {'action': 'get_video_ajax', 'post_id': post_id, 'url': url},

            success: function (response) {
                $('body').find('.video_popup_wrrapper .video_popup_inner').html('<video controls="" autoplay="" name="media"><source src="' + response + '" type="video/mp4"></video>');
                $('.video_popup_wrrapper').fadeIn();
            }


        });
    });
    $('.video_popup_overlay').click(function (event) {
        $('.video_popup_wrrapper').fadeOut();
        $('body').find('.video_popup_wrrapper .video_popup_inner video').remove();
    });
});


jQuery(document).ready(function ($) {

    $('.info-lesson-venus a.preview-button').click(function (event) {
        event.preventDefault();
        var post_id = $(this).attr('data-lesson-id');
        var url = $(this).attr('href');

        $('.video_popup_wrrapper').fadeIn();
        $('body').find('.video_popup_wrrapper .video_popup_inner').html('<div class="spinner"> <div class="double-bounce1"></div> <div class="double-bounce2"></div></div>');

        $.ajax({
            type: 'post',
            url: woocommerce_params['ajax_url'],
            data: {'action': 'get_lesson_video_ajax', 'post_id': post_id, 'url': url},

            success: function (response) {
                $('body').find('.video_popup_wrrapper .video_popup_inner').html('<video controls="" autoplay="" name="media"><source src="' + response + '" type="video/mp4"></video>');
                $('.video_popup_wrrapper').fadeIn();
            }


        });
    });
    $('.video_popup_overlay').click(function (event) {
        $('.video_popup_wrrapper').fadeOut();
        $('body').find('.video_popup_wrrapper .video_popup_inner video').remove();
    });
});

jQuery(window).on("elementor/frontend/init", function (t) {
    elementorFrontend.hooks.addAction("frontend/element_ready/digikala-products.default", function (t) {
        jQuery(".owl-carousel-product").each(function () {
            var i = jQuery(this).data("itemplay"), s = jQuery(this).data("itemtime"),
                n = jQuery(this).data("itemloop");
            jQuery(this).owlCarousel({
                rtl: true,
                nav: true,
                navText: ["<i class='fas fa-chevron-right'></i>", "<i class='fas fa-chevron-left'></i>"],
                autoplay: i,
                autoplayTimeout: s,
                autoplayHoverPause: 1,
                lazyLoad: !0,
                dots: false,
                loop: n,
                margin: 10,
                responsiveClass: !0,
                responsive: {
                    0: {
                        items: 1
                    },
                    576: {
                        items: 2
                    },
                    830: {
                        items: 3
                    },
                    992: {
                        items: 3
                    },
                    1200: {
                        items: 4
                    },
                }
            })
        })
    }),
        elementorFrontend.hooks.addAction("frontend/element_ready/venus-instant-offer.default", function (t) {
            jQuery(".owl-amazing-carousel").each(function () {
                var t = jQuery(this).data("itemtime");

                function e() {
                    jQuery(".slide-progress").css({width: "100%", transition: "width 5000ms"})
                }

                jQuery(this).owlCarousel({
                    rtl: !0,
                    nav: !1,
                    navText: ["<i class='fas fa-chevron-right'></i>", "<i class='fas fa-chevron-left'></i>"],
                    autoplay: !0,
                    autoplayTimeout: t,
                    mouseDrag: !1,
                    touchDrag: !1,
                    dots: !1,
                    loop: !0,
                    margin: 10,
                    onInitialized: e,
                    onTranslate: function () {
                        jQuery(".slide-progress").css({width: 0, transition: "width 0s"})
                    },
                    onTranslated: e,
                    responsiveClass: !0,
                    responsive: {0: {items: 1}}
                })
            })

        }),
        elementorFrontend.hooks.addAction("frontend/element_ready/digikala-amazing-products.default", function (t) {
            jQuery(".owl-carousel-amazing-products").each(function () {
                var i = jQuery(this).data("itemplay"), s = jQuery(this).data("itemtime"),
                    n = jQuery(this).data("itemloop");
                jQuery(this).owlCarousel({
                    rtl: true,
                    nav: true,
                    navText: ["<i class='fas fa-chevron-right'></i>", "<i class='fas fa-chevron-left'></i>"],
                    autoplay: i,
                    autoplayTimeout: s,
                    autoplayHoverPause: 1,
                    lazyLoad: !0,
                    dots: false,
                    loop: n,
                    margin: 10,
                    responsiveClass: !0,
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        830: {
                            items: 3
                        },
                        992: {
                            items: 3
                        },
                        1200: {
                            items: 4
                        },
                    }
                })
            })
        }),
        elementorFrontend.hooks.addAction("frontend/element_ready/venus-menu-phone.default", function (t) {
            jQuery(document).ready(function () {
                jQuery('#right-menu').sidr({
                    name: 'sidr-r',
                    side: 'right'
                });
            });
        }),
        elementorFrontend.hooks.addAction("frontend/element_ready/widget-iron-willed.default", function (t) {

            function owl_iron_willed(owl_size) {
                if (owl_size.matches) { // If media query matches

                    jQuery(".owl-iron-willed-el").each(function () {
                        var i = jQuery(this).data("itemplay"), s = jQuery(this).data("itemtime"),
                            n = jQuery(this).data("itemloop");
                        jQuery(this).owlCarousel({
                            rtl: true,
                            nav: false,
                            autoplay: i,
                            autoplayTimeout: s,
                            autoplayHoverPause: 1,
                            onRefreshed: show_slide_fade(),
                            lazyLoad: !0,
                            dots: true,
                            loop: n,
                            margin: 10,
                            responsiveClass: !0,
                            responsive: {
                                0: {
                                    items: 1
                                },
                                576: {
                                    items: 1
                                },
                                830: {
                                    items: 1
                                },
                                992: {
                                    items: 2
                                },
                                1200: {
                                    items: 2
                                },
                            }
                        })
                    })

                    function show_slide_fade() {
                        jQuery('.slider-fade-out').show();
                    }

                } else {

                    jQuery(".owl-iron-willed-el").each(function () {
                        var i = jQuery(this).data("itemplay"), s = jQuery(this).data("itemtime"),
                            n = jQuery(this).data("itemloop");
                        jQuery(this).owlCarousel({
                            stagePadding: 120,
                            rtl: true,
                            nav: false,
                            autoplay: i,
                            autoplayTimeout: s,
                            autoplayHoverPause: 1,
                            onRefreshed: show_slide_fade(),
                            lazyLoad: !0,
                            dots: true,
                            loop: n,
                            margin: 10,
                            responsiveClass: !0,
                            responsive: {
                                0: {
                                    items: 1
                                },
                                576: {
                                    items: 1
                                },
                                830: {
                                    items: 1
                                },
                                992: {
                                    items: 2
                                },
                                1200: {
                                    items: 2
                                },
                            }
                        })
                    })

                    function show_slide_fade() {
                        jQuery('.slider-fade-out').show();
                    }
                }
            }

            var owl_size = window.matchMedia("(max-width: 600px)");

            owl_iron_willed(owl_size);

            owl_size.addListener(owl_iron_willed);

        }),
        elementorFrontend.hooks.addAction("frontend/element_ready/venus_blog.default", function (t) {

            var mySwiper = new Swiper('.swiper-blog', {
                slidesPerView: 1,
                spaceBetween: 20,
                slidesPerGroup: 1,
                loop: true,
                autoplay: {
                    delay: 9000,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 20,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 40,
                    },
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 20,
                    },
                }
            });

        }),
        elementorFrontend.hooks.addAction("frontend/element_ready/widget-venus-landing-product.default", function (t) {

            var mySwiper = new Swiper('.swiper-product', {
                slidesPerView: 1,
                spaceBetween: 20,
                slidesPerGroup: 1,
                loop: true,
                autoplay: {
                    delay: 9000,
                    disableOnInteraction: false,
                },
                // If we need pagination
                pagination: {
                    el: '.swiper-pagination',
                },

                // Navigation arrows
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 20,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 40,
                    },
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 20,
                    },
                }
            })

        })


});





// jQuery(".owl-carousel-related").each(function () {
//     jQuery(this).owlCarousel({
//         rtl: true,
//         nav: true,
//         navText: ["<i class='fas fa-chevron-right'></i>", "<i class='fas fa-chevron-left'></i>"],
//         dots: false,
//         margin: 10,
//         responsiveClass: !0,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             576: {
//                 items: 2
//             },
//             830: {
//                 items: 3
//             },
//             992: {
//                 items: 3
//             },
//             1200: {
//                 items: 4
//             },
//         }
//     })
// })
