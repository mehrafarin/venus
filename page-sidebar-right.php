<?php
/* Template Name:  برگه - سایدبار راست */
get_header();
?>

<?php if (iwp_Shield::is_activated() === true) : ?>
   <div class="venus-home-sidebar">
       <div class="container">
           <div class="blog-data-main has-sidebar d-block d-lg-flex sidebar-right">
               <div class="main-home-page blog-data mt-2 mt-md-0 p-0 border-0 shadow-none blog-data-style-one">
                   <?php if (have_posts()) : while (have_posts()) : the_post();
                       the_content();
                   endwhile;
                   endif;
                   ?>
               </div>
               <aside class="sidebar-home blog-sidebar d-none d-lg-block sticky-sidebar">
                   <div class="theiaStickySidebar">
                       <?php get_sidebar('page'); ?>
                   </div>
               </aside>
           </div>
       </div>
   </div>
<?php endif; ?>

<?php get_footer(); ?>

