<?php global $venus_options ; ?>
<div class="search search-box">
    <form method="get" class="text-left" action="<?php echo home_url( '/' ); ?>">
        <input type="search" class="text-color-search-header" placeholder="<?php echo $venus_options['text_box_search_main']; ?>" value="<?php echo get_search_query() ?>" name="s">
        <button class="btn pt-1 pl-0">
            <i class="fal fa-search color-btn-icon-search-header" style="font-size: 30px"></i>
        </button>
    </form>
</div>
