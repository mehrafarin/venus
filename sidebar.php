<?php
/**
 * The sidebar containing the main widget area
 *
 */

if ( ! is_active_sidebar( 'sidebar_blog' ) ) {
	return;
}
?>
<div class="sidebar-widgets-wrapper">
	<?php dynamic_sidebar( 'sidebar_blog' ); ?>
</div>
