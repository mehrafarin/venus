<?php
$home_url = home_url();
$active_class = ' class="active"'
?>

<div class="dokan-dash-sidebar col-sm-12 col-md-4 col-lg-3">
    <div class="ve-dokan-profile">
        <div class="ve-dokan-avatar">
            <?php
            global $current_user;
            echo get_avatar($current_user->ID, 80);
            ?>
        </div>
        <div class="ve-dokan-user-name">
            <?php
            global $current_user;
            echo $current_user->display_name
            ?>
        </div>
        <div class="ve-dokan-nav-profile">
            <a href="<?php echo dokan_get_navigation_url('announcement'); ?>" class="ve-dokan-nav-a ve-dokan-nav-message">پیام‌ها</a>
            <a href="<?php echo dokan_get_navigation_url('settings/store'); ?>" class="ve-dokan-nav-a ve-dokan-nav-shop-link">فروشگاه</a>
            <a href="<?php echo dokan_get_navigation_url('edit-account'); ?>" class="ve-dokan-nav-a ve-dokan-nav-profile-link">پروفایل</a>
        </div>
    </div>
    <?php
        global $allowedposttags;

        // These are required for the hamburger menu.
        if ( is_array( $allowedposttags ) ) {
            $allowedposttags['input'] = [
                'id'      => [],
                'type'    => [],
                'checked' => []
            ];
        }

        echo wp_kses( dokan_dashboard_nav( $active_menu ), $allowedposttags );
    ?>
</div>
