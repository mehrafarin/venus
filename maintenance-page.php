<!DOCTYPE html>
<?php
// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}
global $venus_options;

if (isset($venus_options['favicon']) && strlen($venus_options['favicon']['url']) > 0) {
    $favicon_href = $venus_options['favicon']['url'];
} else {
    $favicon_href = get_template_directory_uri() . '/images/favicon.png';
}

if (isset($venus_options['venus_maintenance_img']) && strlen($venus_options['venus_maintenance_img']['url']) > 0) {
    $venus_maintenance_img_href = $venus_options['venus_maintenance_img']['url'];
} else {
    $venus_maintenance_img_href = get_template_directory_uri() . '/images/maintenance.svg';
}
wp_head();

?>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo $favicon_href; ?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" media="screen" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>"/>
</head>

<body class="venus-maintenance" <?php  $venus_options['venus_maintenance_background']?> >
<div class="container-fluid">
    <img src="<?php echo $venus_maintenance_img_href; ?>" class="w-50 d-block mx-auto my-5" alt="<?php bloginfo('name'); ?> | <?php bloginfo('description'); ?>">
    <h2 class="text-center mb-4"><?php echo $venus_options['venus_maintenance_title']; ?></h2>
    <h3 class="text-center"><?php echo $venus_options['venus_maintenance_editor']; ?></h3>
</div>
</body>

</html>