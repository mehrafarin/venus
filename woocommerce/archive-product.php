<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

global $venus_options;

$blog_sidebar = 'left';

if (class_exists('Redux')) {
    $blog_sidebar = $venus_options['sidebar_position_shop'];
}

$sidebar_position_single = isset($_GET['sidebar']) ? $_GET['sidebar'] : $blog_sidebar;

$blog_container_classes = array();

if ($sidebar_position_single == 'left' || $sidebar_position_single == 'right') {
    $blog_container_classes[] = 'has-sidebar';
    $blog_container_classes[] = 'd-flex';
}

if ($sidebar_position_single == 'left') {
    $blog_container_classes[] = 'sidebar-left';
} elseif ($sidebar_position_single == 'right') {
    $blog_container_classes[] = 'sidebar-right';
}


get_header('shop');

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );

global $venus_options;

$content_products = 'container';
$none_content_products = '';

switch ($venus_options['style_archive_products']) {
    case 'archive-product-style-one':
        $content_products = 'container';
        break;
    case 'archive-product-style-two':
        $content_products = 'container container-digikala';
        $none_content_products = ' d-none';
        break;
    default:
        $content_products = 'container';
}


?>
    <header class="woocommerce-products-header <?php echo $content_products . $none_content_products; ?> category">
        <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
            <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
        <?php endif; ?>

        <?php
        /**
         * Hook: woocommerce_archive_description.
         *
         * @hooked woocommerce_taxonomy_archive_description - 10
         * @hooked woocommerce_product_archive_description - 10
         */
        do_action('woocommerce_archive_description');
        ?>
    </header>
    <div class="<?php echo $content_products; ?>">
        <div class="<?php echo esc_attr(implode(' ', $blog_container_classes)); ?>">
            <div class="archive-shop-sidebar h-fit digikala-holder-bg-section">
                <div class="breadcrumbs digikala-breadcrumbs text-right d-none">
                    <?php
                    $args = array(
                        'delimiter' => '<i class="fas fa-chevron-left"></i>',
                    );
                    woocommerce_breadcrumb($args); ?>
                </div>
                <div class="digikala-bg-section">
                    <?php
                    if (woocommerce_product_loop()) {

                        /**
                         * Hook: woocommerce_before_shop_loop.
                         *
                         * @hooked woocommerce_output_all_notices - 10
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        do_action('woocommerce_before_shop_loop');

                        woocommerce_product_loop_start();

                        if (wc_get_loop_prop('total')) {
                            while (have_posts()) {
                                the_post();

                                /**
                                 * Hook: woocommerce_shop_loop.
                                 */
                                do_action('woocommerce_shop_loop');

                                wc_get_template_part('content', 'product');
                            }
                        }

                        woocommerce_product_loop_end();

                        /**
                         * Hook: woocommerce_after_shop_loop.
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action('woocommerce_after_shop_loop');
                    } else {
                        /**
                         * Hook: woocommerce_no_products_found.
                         *
                         * @hooked wc_no_products_found - 10
                         */
                        do_action('woocommerce_no_products_found');
                    }
                    ?>
                </div>
            </div>
            <?php if ($sidebar_position_single !== 'none') : ?>

                <aside class="blog-sidebar d-none d-lg-block sticky-sidebar">
                    <div class="theiaStickySidebar">
                        <?php get_sidebar('product'); ?>
                    </div>
                </aside>
            <?php
            endif;
            /**
             * Hook: woocommerce_after_main_content.
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
            do_action('woocommerce_after_main_content');
            ?>
        </div>
    </div>
<?php

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */

//                do_action('woocommerce_sidebar');

get_footer('shop');
