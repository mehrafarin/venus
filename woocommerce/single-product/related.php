<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/related/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.9.0
 */
global $venus_options;
if (!defined('ABSPATH')) {
    exit;
}

if ($related_products) :

    global $venus_options;

    $pro_style = get_post_meta(get_the_ID(), 'select_style_page_products', true);

    if ($pro_style == 'default' || empty($pro_style)) {
        switch ($venus_options['style_page_products']) {
            case 'product-style-one': ?>
                <section class="related products mt-5">

                    <h2 class="section-title"><?php echo $venus_options['show_related_text']; ?></h2>
                    <!--		<h2 class="section-title">-->
                    <?php //esc_html_e( 'Related products', 'woocommerce' ); ?><!--</h2>-->

                    <?php woocommerce_product_loop_start(); ?>

                    <!-- Slider main container -->
                    <div class="swiper-container swiper-related w-100 d-none d-md-block">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php foreach ($related_products as $related_product) : ?>
                                <?php
                                $post_object = get_post($related_product->get_id());

                                setup_postdata($GLOBALS['post'] =& $post_object);
                                ?>
                                <div class="col-sm-12 col-md-6 col-xl-6 swiper-slide course">
                                    <div class="course-inner">
                                        <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                            <?php if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                <div class="course-item-sale">
                                                    <?php
                                                    global $product;

                                                    if ($product->is_on_sale()) {

                                                        if (!$product->is_type('variable')) {

                                                            $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                        } else {

                                                            $max_percentage = 0;

                                                            foreach ($product->get_children() as $child_id) {
                                                                $variation = wc_get_product($child_id);
                                                                $price = $variation->get_regular_price();
                                                                $sale = $variation->get_sale_price();
                                                                if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                if ($percentage > $max_percentage) {
                                                                    $max_percentage = $percentage;
                                                                }
                                                            }

                                                        }
                                                        echo "<div class='sale-perc-badge'>";
                                                        echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                        echo "<div class='sale-badge-text'>تخفیف</div>";
                                                        echo "</div>";
                                                    }
                                                    ?>
                                                </div>
                                            <?php } ?>
                                            <a href="<?php the_permalink(); ?>">
                                                <?php if (isset($venus_options['waves_product_show_related']) && $venus_options['waves_product_show_related']) : ?>
                                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         viewBox="0 24 150 28"
                                                         preserveAspectRatio="none" shape-rendering="auto">
                                                        <defs>
                                                            <path id="gentle-wave"
                                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                        </defs>
                                                        <g class="parallax">
                                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                 fill="rgba(255,255,255,0.7"></use>
                                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                                 fill="rgba(255,255,255,0.5)"></use>
                                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                                 fill="rgba(255,255,255,0.3)"></use>
                                                            <use xlink:href="#gentle-wave" x="90" y="7"
                                                                 fill="#fff"></use>
                                                        </g>
                                                    </svg>
                                                <?php endif; ?>
                                            </a>
                                            <?php
                                            $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                            if ($link_show_course_intro_video) : ?>
                                                <div class="video-button">
                                                    <a data-post-id="<?php echo get_the_ID(); ?>"
                                                       href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                       class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                </div>
                                            <?php endif; ?>
                                        </header>
                                        <div class="course-detail">
                                            <div class="course-top-content">
                                                <a href="<?php the_permalink(); ?>" class="title">
                                                    <h2><?php the_title(); ?></h2></a>
                                                <div class="course-description">
                                                    <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                </div>
                                            </div>
                                            <?php if (isset($venus_options['info_product_bottom_show_related']) && $venus_options['info_product_bottom_show_related']) { ?>
                                                <div class="course-information position-relative d-flex">
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">سطح</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">دوره</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                    <div class="col p-0 text-center price-course m-auto">
                                                        <?php
                                                        global $product;
                                                        echo $product->get_price_html(); ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev btn-prev-related"></div>
                        <div class="swiper-button-next btn-next-related"></div>
                    </div>
                    <?php foreach ($related_products as $related_product) : ?>
                        <?php
                        $post_object = get_post($related_product->get_id());
                        setup_postdata($GLOBALS['post'] =& $post_object);
                        ?>
                        <div class="col-sm-12 col-md-6 col-xl-6 swiper-slide course d-md-none">
                            <div class="course-inner">
                                <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                    <?php if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                        <div class="course-item-sale">
                                            <?php
                                            global $product;

                                            if ($product->is_on_sale()) {

                                                if (!$product->is_type('variable')) {

                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                } else {

                                                    $max_percentage = 0;

                                                    foreach ($product->get_children() as $child_id) {
                                                        $variation = wc_get_product($child_id);
                                                        $price = $variation->get_regular_price();
                                                        $sale = $variation->get_sale_price();
                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                        if ($percentage > $max_percentage) {
                                                            $max_percentage = $percentage;
                                                        }
                                                    }

                                                }
                                                echo "<div class='sale-perc-badge'>";
                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                echo "</div>";
                                            }
                                            ?>
                                        </div>
                                    <?php } ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if (isset($venus_options['waves_product_show_related']) && $venus_options['waves_product_show_related']) : ?>
                                            <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                 preserveAspectRatio="none" shape-rendering="auto">
                                                <defs>
                                                    <path id="gentle-wave"
                                                          d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                </defs>
                                                <g class="parallax">
                                                    <use xlink:href="#gentle-wave" x="-12" y="11"
                                                         fill="rgba(255,255,255,0.7"></use>
                                                    <use xlink:href="#gentle-wave" x="25" y="13"
                                                         fill="rgba(255,255,255,0.5)"></use>
                                                    <use xlink:href="#gentle-wave" x="100" y="5"
                                                         fill="rgba(255,255,255,0.3)"></use>
                                                    <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                </g>
                                            </svg>
                                        <?php endif; ?>
                                    </a>
                                    <?php
                                    $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                    if ($link_show_course_intro_video) : ?>
                                        <div class="video-button">
                                            <a data-post-id="<?php echo get_the_ID(); ?>"
                                               href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                               class="intro-play-icon"><i class="fal fa-play"></i></a>
                                        </div>
                                    <?php endif; ?>
                                </header>
                                <div class="course-detail">
                                    <div class="course-top-content">
                                        <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2>
                                        </a>
                                        <div class="course-description">
                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                        </div>
                                    </div>
                                    <?php if (isset($venus_options['info_product_bottom_show_related']) && $venus_options['info_product_bottom_show_related']) { ?>
                                        <div class="course-information position-relative d-flex">
                                            <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">سطح</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                            <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">دوره</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                            <div class="col p-0 text-center price-course m-auto">
                                                <?php echo $product->get_price_html(); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <?php woocommerce_product_loop_end(); ?>

                </section>
                <?php
                break;
            case 'product-style-two':
                ?>
                <section class="related products">

                    <?php //woocommerce_product_loop_start();
                    ?>

                    <div class="full-box-row-show-product">
                        <div class="main-full-box-row-show-product py-2 px-4">
                            <div class="head-sec">
                                <a href="#">محصولات مرتبط</a>
                            </div>
                            <div class="index-full-box-row-show-product">
                                <div class="index-left-box-show-product">
                                    <?php include Venus_DIR . "/template/products/relateds/related-digikala.php";; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php //woocommerce_product_loop_end();
                    ?>

                </section>
                <?php
                break;
            default: ?>
                <section class="related products mt-5">

                    <h2 class="section-title"><?php echo $venus_options['show_related_text']; ?></h2>
                    <!--		<h2 class="section-title">-->
                    <?php //esc_html_e( 'Related products', 'woocommerce' ); ?><!--</h2>-->

                    <?php woocommerce_product_loop_start(); ?>

                    <!-- Slider main container -->
                    <div class="swiper-container swiper-related w-100 d-none d-md-block">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php foreach ($related_products as $related_product) : ?>
                                <?php
                                $post_object = get_post($related_product->get_id());

                                setup_postdata($GLOBALS['post'] =& $post_object);
                                ?>
                                <div class="col-sm-12 col-md-6 col-xl-6 swiper-slide course">
                                    <div class="course-inner">
                                        <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                            <?php if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                <div class="course-item-sale">
                                                    <?php
                                                    global $product;

                                                    if ($product->is_on_sale()) {

                                                        if (!$product->is_type('variable')) {

                                                            $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                        } else {

                                                            $max_percentage = 0;

                                                            foreach ($product->get_children() as $child_id) {
                                                                $variation = wc_get_product($child_id);
                                                                $price = $variation->get_regular_price();
                                                                $sale = $variation->get_sale_price();
                                                                if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                if ($percentage > $max_percentage) {
                                                                    $max_percentage = $percentage;
                                                                }
                                                            }

                                                        }
                                                        echo "<div class='sale-perc-badge'>";
                                                        echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                        echo "<div class='sale-badge-text'>تخفیف</div>";
                                                        echo "</div>";
                                                    }
                                                    ?>
                                                </div>
                                            <?php } ?>
                                            <a href="<?php the_permalink(); ?>">
                                                <?php if (isset($venus_options['waves_product_show_related']) && $venus_options['waves_product_show_related']) : ?>
                                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         viewBox="0 24 150 28"
                                                         preserveAspectRatio="none" shape-rendering="auto">
                                                        <defs>
                                                            <path id="gentle-wave"
                                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                        </defs>
                                                        <g class="parallax">
                                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                 fill="rgba(255,255,255,0.7"></use>
                                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                                 fill="rgba(255,255,255,0.5)"></use>
                                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                                 fill="rgba(255,255,255,0.3)"></use>
                                                            <use xlink:href="#gentle-wave" x="90" y="7"
                                                                 fill="#fff"></use>
                                                        </g>
                                                    </svg>
                                                <?php endif; ?>
                                            </a>
                                            <?php
                                            $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                            if ($link_show_course_intro_video) : ?>
                                                <div class="video-button">
                                                    <a data-post-id="<?php echo get_the_ID(); ?>"
                                                       href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                       class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                </div>
                                            <?php endif; ?>
                                        </header>
                                        <div class="course-detail">
                                            <div class="course-top-content">
                                                <a href="<?php the_permalink(); ?>" class="title">
                                                    <h2><?php the_title(); ?></h2></a>
                                                <div class="course-description">
                                                    <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                </div>
                                            </div>
                                            <?php if (isset($venus_options['info_product_bottom_show_related']) && $venus_options['info_product_bottom_show_related']) { ?>
                                                <div class="course-information position-relative d-flex">
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">سطح</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">دوره</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                    <div class="col p-0 text-center price-course m-auto">
                                                        <?php
                                                        global $product;
                                                        echo $product->get_price_html(); ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev btn-prev-related"></div>
                        <div class="swiper-button-next btn-next-related"></div>
                    </div>
                    <?php foreach ($related_products as $related_product) : ?>
                        <?php
                        $post_object = get_post($related_product->get_id());
                        setup_postdata($GLOBALS['post'] =& $post_object);
                        ?>
                        <div class="col-sm-12 col-md-6 col-xl-6 swiper-slide course d-md-none">
                            <div class="course-inner">
                                <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                    <?php if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                        <div class="course-item-sale">
                                            <?php
                                            global $product;

                                            if ($product->is_on_sale()) {

                                                if (!$product->is_type('variable')) {

                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                } else {

                                                    $max_percentage = 0;

                                                    foreach ($product->get_children() as $child_id) {
                                                        $variation = wc_get_product($child_id);
                                                        $price = $variation->get_regular_price();
                                                        $sale = $variation->get_sale_price();
                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                        if ($percentage > $max_percentage) {
                                                            $max_percentage = $percentage;
                                                        }
                                                    }

                                                }
                                                echo "<div class='sale-perc-badge'>";
                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                echo "</div>";
                                            }
                                            ?>
                                        </div>
                                    <?php } ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if (isset($venus_options['waves_product_show_related']) && $venus_options['waves_product_show_related']) : ?>
                                            <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                 preserveAspectRatio="none" shape-rendering="auto">
                                                <defs>
                                                    <path id="gentle-wave"
                                                          d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                </defs>
                                                <g class="parallax">
                                                    <use xlink:href="#gentle-wave" x="-12" y="11"
                                                         fill="rgba(255,255,255,0.7"></use>
                                                    <use xlink:href="#gentle-wave" x="25" y="13"
                                                         fill="rgba(255,255,255,0.5)"></use>
                                                    <use xlink:href="#gentle-wave" x="100" y="5"
                                                         fill="rgba(255,255,255,0.3)"></use>
                                                    <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                </g>
                                            </svg>
                                        <?php endif; ?>
                                    </a>
                                    <?php
                                    $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                    if ($link_show_course_intro_video) : ?>
                                        <div class="video-button">
                                            <a data-post-id="<?php echo get_the_ID(); ?>"
                                               href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                               class="intro-play-icon"><i class="fal fa-play"></i></a>
                                        </div>
                                    <?php endif; ?>
                                </header>
                                <div class="course-detail">
                                    <div class="course-top-content">
                                        <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2>
                                        </a>
                                        <div class="course-description">
                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                        </div>
                                    </div>
                                    <?php if (isset($venus_options['info_product_bottom_show_related']) && $venus_options['info_product_bottom_show_related']) { ?>
                                        <div class="course-information position-relative d-flex">
                                            <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">سطح</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                            <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">دوره</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                            <div class="col p-0 text-center price-course m-auto">
                                                <?php echo $product->get_price_html(); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <?php woocommerce_product_loop_end(); ?>

                </section>
            <?php
        }
    } elseif ($pro_style == 'hamyarco') { ?>
        <section class="related products mt-5">

            <h2 class="section-title"><?php echo $venus_options['show_related_text']; ?></h2>
            <!--		<h2 class="section-title">-->
            <?php //esc_html_e( 'Related products', 'woocommerce' ); ?><!--</h2>-->

            <?php woocommerce_product_loop_start(); ?>

            <!-- Slider main container -->
            <div class="swiper-container swiper-related w-100 d-none d-md-block">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <?php foreach ($related_products as $related_product) : ?>
                        <?php
                        $post_object = get_post($related_product->get_id());

                        setup_postdata($GLOBALS['post'] =& $post_object);
                        ?>
                        <div class="col-sm-12 col-md-6 col-xl-6 swiper-slide course">
                            <div class="course-inner">
                                <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                    <?php if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                        <div class="course-item-sale">
                                            <?php
                                            global $product;

                                            if ($product->is_on_sale()) {

                                                if (!$product->is_type('variable')) {

                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                } else {

                                                    $max_percentage = 0;

                                                    foreach ($product->get_children() as $child_id) {
                                                        $variation = wc_get_product($child_id);
                                                        $price = $variation->get_regular_price();
                                                        $sale = $variation->get_sale_price();
                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                        if ($percentage > $max_percentage) {
                                                            $max_percentage = $percentage;
                                                        }
                                                    }

                                                }
                                                echo "<div class='sale-perc-badge'>";
                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                echo "</div>";
                                            }
                                            ?>
                                        </div>
                                    <?php } ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if (isset($venus_options['waves_product_show_related']) && $venus_options['waves_product_show_related']) : ?>
                                            <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                 preserveAspectRatio="none" shape-rendering="auto">
                                                <defs>
                                                    <path id="gentle-wave"
                                                          d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                </defs>
                                                <g class="parallax">
                                                    <use xlink:href="#gentle-wave" x="-12" y="11"
                                                         fill="rgba(255,255,255,0.7"></use>
                                                    <use xlink:href="#gentle-wave" x="25" y="13"
                                                         fill="rgba(255,255,255,0.5)"></use>
                                                    <use xlink:href="#gentle-wave" x="100" y="5"
                                                         fill="rgba(255,255,255,0.3)"></use>
                                                    <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                </g>
                                            </svg>
                                        <?php endif; ?>
                                    </a>
                                    <?php
                                    $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                    if ($link_show_course_intro_video) : ?>
                                        <div class="video-button">
                                            <a data-post-id="<?php echo get_the_ID(); ?>"
                                               href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                               class="intro-play-icon"><i class="fal fa-play"></i></a>
                                        </div>
                                    <?php endif; ?>
                                </header>
                                <div class="course-detail">
                                    <div class="course-top-content">
                                        <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2>
                                        </a>
                                        <div class="course-description">
                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                        </div>
                                    </div>
                                    <?php if (isset($venus_options['info_product_bottom_show_related']) && $venus_options['info_product_bottom_show_related']) { ?>
                                        <div class="course-information position-relative d-flex">
                                            <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">سطح</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                            <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">دوره</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                            <div class="col p-0 text-center price-course m-auto">
                                                <?php echo $product->get_price_html(); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev btn-prev-related"></div>
                <div class="swiper-button-next btn-next-related"></div>
            </div>
            <?php foreach ($related_products as $related_product) : ?>
                <?php
                $post_object = get_post($related_product->get_id());
                setup_postdata($GLOBALS['post'] =& $post_object);
                ?>
                <div class="col-sm-12 col-md-6 col-xl-6 swiper-slide course d-md-none">
                    <div class="course-inner">
                        <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                            <?php if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                <div class="course-item-sale">
                                    <?php
                                    global $product;

                                    if ($product->is_on_sale()) {

                                        if (!$product->is_type('variable')) {

                                            $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                        } else {

                                            $max_percentage = 0;

                                            foreach ($product->get_children() as $child_id) {
                                                $variation = wc_get_product($child_id);
                                                $price = $variation->get_regular_price();
                                                $sale = $variation->get_sale_price();
                                                if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                if ($percentage > $max_percentage) {
                                                    $max_percentage = $percentage;
                                                }
                                            }

                                        }
                                        echo "<div class='sale-perc-badge'>";
                                        echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                        echo "<div class='sale-badge-text'>تخفیف</div>";
                                        echo "</div>";
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                            <a href="<?php the_permalink(); ?>">
                                <?php if (isset($venus_options['waves_product_show_related']) && $venus_options['waves_product_show_related']) : ?>
                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                         preserveAspectRatio="none" shape-rendering="auto">
                                        <defs>
                                            <path id="gentle-wave"
                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                        </defs>
                                        <g class="parallax">
                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                 fill="rgba(255,255,255,0.7"></use>
                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                 fill="rgba(255,255,255,0.5)"></use>
                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                 fill="rgba(255,255,255,0.3)"></use>
                                            <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                        </g>
                                    </svg>
                                <?php endif; ?>
                            </a>
                            <?php
                            $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                            if ($link_show_course_intro_video) : ?>
                                <div class="video-button">
                                    <a data-post-id="<?php echo get_the_ID(); ?>"
                                       href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                       class="intro-play-icon"><i class="fal fa-play"></i></a>
                                </div>
                            <?php endif; ?>
                        </header>
                        <div class="course-detail">
                            <div class="course-top-content">
                                <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2></a>
                                <div class="course-description">
                                    <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                </div>
                            </div>
                            <?php if (isset($venus_options['info_product_bottom_show_related']) && $venus_options['info_product_bottom_show_related']) { ?>
                                <div class="course-information position-relative d-flex">
                                    <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                        <div class="col p-0 text-right">
                                            <span class="level-course">سطح</span>
                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                        </div>
                                    <?php } else {
                                    } ?>
                                    <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                        <div class="col p-0 text-right">
                                            <span class="level-course">دوره</span>
                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                        </div>
                                    <?php } else {
                                    } ?>
                                    <div class="col p-0 text-center price-course m-auto">
                                        <?php echo $product->get_price_html(); ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php woocommerce_product_loop_end(); ?>

        </section>
        <?php
    } elseif ($pro_style == 'digikala') {
        ?>
        <section class="related products">

            <?php //woocommerce_product_loop_start(); ?>

            <div class="full-box-row-show-product">
                <div class="main-full-box-row-show-product py-2 px-4">
                    <div class="head-sec">
                        <a href="#"><?php echo $venus_options['show_related_text']; ?></a>
                    </div>
                    <div class="index-full-box-row-show-product">
                        <div class="index-left-box-show-product">
                            <?php include Venus_DIR . "/template/products/relateds/related-digikala.php";; ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php //woocommerce_product_loop_end(); ?>

        </section>
        <?php
    }

endif;

wp_reset_postdata();
