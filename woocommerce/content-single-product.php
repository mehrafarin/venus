<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

global $venus_options;

$pro_style = get_post_meta(get_the_ID(), 'select_style_page_products', true);

if ($pro_style == 'default' || empty($pro_style)) {
    switch ($venus_options['style_page_products']) {
        case 'product-style-one':
            get_template_part('template/products/single-product/product-style-one');
            break;
        case 'product-style-two':
            get_template_part('template/products/single-product/product-style-two');
            break;
        default:
            get_template_part('template/products/single-product/product-style-one');
    }
} elseif ($pro_style == 'hamyarco') {

    get_template_part('template/products/single-product/product-style-one');

} elseif ($pro_style == 'digikala') {

    get_template_part('template/products/single-product/product-style-two');

}
