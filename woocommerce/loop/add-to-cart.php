<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

global $product, $venus_options;
$buy_less = $venus_options['buy_less'];

$name_btn_buy = '';
if (empty(get_post_meta(get_the_ID(), 'register_btn_center', true))) {
    $name_btn_buy = $venus_options['text_register_btn_product_page'];
} else {
    $name_btn_buy = get_post_meta(get_the_ID(), 'register_btn_center', true);
}
$css_top_add_cart = '';
$link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
if ($link_show_course_intro_video) :
    $css_top_add_cart = 'add-to-cart-ten';
endif;

$current_user = wp_get_current_user();
if ($venus_options['add_cart_course_screen']) {
    if (wc_customer_bought_product($current_user->user_email, $current_user->ID, $product->get_id()) && ($buy_less)) {

    } else {
        echo apply_filters('woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
            sprintf('<a href="%s" data-quantity="%s" class="%s add-to-cart-loop-course ' . $css_top_add_cart . '" %s><i class="fal fa-cart-plus"></i></a>',
                esc_url($product->add_to_cart_url()),
                esc_attr(isset($args['quantity']) ? $args['quantity'] : 1),
                esc_attr(isset($args['class']) ? $args['class'] : 'button'),
                isset($args['attributes']) ? wc_implode_html_attributes($args['attributes']) : '',
                esc_html($name_btn_buy)
            ),
            $product, $args);
    }
}
