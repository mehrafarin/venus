<?php
/**
 * Downloads
 *
 * Shows downloads on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */

if (!defined('ABSPATH')) {
    exit;
}

$woo_downloads = WC()->customer->get_downloadable_products();
$has_downloads = (bool)$woo_downloads;

if ($has_downloads) {
    $downloads = array();
    foreach ($woo_downloads as $download) {
        $downloads[$download['product_id']][$download['order_id']][] = $download;
    }
}

$prefix = '_venus_';

do_action('woocommerce_before_account_downloads', $has_downloads); ?>

<?php if ($has_downloads) : ?>
    <div class="ve-loader-js" id="ve-loader-js" style="display: none;">
        <img src="<?php echo Venus_URI  ?>/images/loader.gif" alt="">
    </div>
    <div class="holder-download">
        <?php do_action('woocommerce_before_available_downloads'); ?>
        <ul class="dl-list">
            <?php foreach ($downloads as $product_id => $orders_download): ?>
                <li>
                    <a class="title d-sm-flex justify-content-between ve-main-product-dashboard" data-toggle="collapse"
                       href="#Collapse<?php echo $product_id; ?>" role="button" aria-expanded="false"
                       aria-controls="multiCollapseExample1">
                        <span class="ve-download-product-dashboard" style="background: url(<?php echo get_the_post_thumbnail_url($product_id, 'product-size'); ?>)center no-repeat;width: 175px;height: 120px;background-size: cover;border-radius: 7px;margin-left: 10px;"></span>
                        <span class="tt-name-course col"><?php echo get_the_title($product_id) ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php foreach ($downloads as $product_id => $orders_download): ?>
            <div class="sub_items collapse multi-collapse" id="Collapse<?php echo $product_id; ?>">
                <div class="head-inner-box-download mb-5 d-flex justify-content-between">
                    <h2 class="my-auto"><?php echo get_the_title($product_id) ?></h2>
                    <a class="btn--back" data-toggle="collapse" href="#Collapse<?php echo $product_id; ?>" role="button"
                       aria-expanded="false" aria-controls="multiCollapseExample1">
                        <i class="far fa-chevron-left"></i>
                    </a>
                </div>
                <div class="accordion" id="accordion<?php echo $product_id; ?>">
                    <?php $course = get_post_meta($product_id, 'feture_group', true);
                    if ($course){
                    ?>
                    <div class="btn-accordion d-flex mb-4">
                        <button class="btn btn-link btn-block text-center" type="button" data-toggle="collapse"
                                data-target="#collapse<?php echo $product_id; ?>" aria-expanded="true"
                                aria-controls="collapse<?php echo $product_id; ?>">
                            دوره
                        </button>
                        <?php
                        if ($course) {
                            $a = 0;
                            foreach ($course as $key => $entry) {
                                $a++;
                                ?>
                                <button class="btn btn-link btn-block text-center collapsed" type="button" data-toggle="collapse" data-target="#collapse<?php echo $product_id . $a; ?>" aria-expanded="true" aria-controls="collapse<?php echo $product_id . $a; ?>">
                                    <?php echo $entry[ $prefix . 'feture_title']; ?>
                                </button>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <?php }?>
                    <div id="collapse<?php echo $product_id; ?>" class="collapse show" aria-labelledby="headingOne"
                         data-parent="#accordion<?php echo $product_id; ?>">
                        <?php foreach ($orders_download as $order_id => $download_items): ?>
                            <ul class="dl_order">

                                <?php if (!empty($download_items)): ?>
                                    <table class="w-100 table border-0">
                                        <thead class="d-block">
                                        <tr class="d-flex w-100">
                                            <th class="col border-0"><?php _e('File', 'woocommerce'); ?></th>
                                            <th class="col-3  border-0 ve-table-download"><?php _e('Downloads remaining', 'woocommerce'); ?></th>
                                            <th class="col-3  border-0 text-center ve-table-download"><?php _e('Expires', 'woocommerce'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($download_items as $key => $download): ?>
                                            <tr class="d-flex w-100">
                                                <td class="col"><a
                                                            href="<?php echo esc_url($download['download_url']); ?>"
                                                            class="download-btns"><i
                                                                class="fal fa-download pl-2"></i><?php echo $download['file']['name'] ?>
                                                    </a></td>
                                                <td class="col-3 ve-table-download"><?php echo is_numeric($download['downloads_remaining']) ? esc_html($download['downloads_remaining']) : __('&infin;', 'woocommerce'); ?></td>
                                                <td class="col-3 text-center ve-table-download">
                                                    <?php if (!empty($download['access_expires'])) : ?>
                                                        <time datetime="<?php echo date('Y-m-d', strtotime($download['access_expires'])); ?>"
                                                              title="<?php echo esc_attr(strtotime($download['access_expires'])); ?>"><?php echo date_i18n(get_option('date_format'), strtotime($download['access_expires'])); ?></time>
                                                    <?php else : ?>
                                                        <?php _e('Never', 'woocommerce'); ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php endif; ?>
                            </ul>
                        <?php endforeach; ?>
                    </div>

                    <?php
                    if ($course) {
                        $a = 0;
                        foreach ($course as $key => $entry) {
                            $a++;
                            ?>
                            <div id="collapse<?php echo $product_id . $a; ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion<?php echo $product_id; ?>">
                                <div class="card-body">
                                    <?php
                                    if(!empty($entry[ $prefix . 'feture_input'])):
                                    echo do_shortcode($entry[ $prefix . 'feture_input']);
                                    endif;
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
    <?php do_action('woocommerce_after_available_downloads'); ?>
<?php else : ?>
    <div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
        <a class="woocommerce-Button button"
           href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
            <?php esc_html_e('Go shop', 'woocommerce') ?>
        </a>
        <?php esc_html_e('No downloads available yet.', 'woocommerce'); ?>
    </div>
<?php endif; ?>

<?php do_action('woocommerce_after_account_downloads', $has_downloads); ?>
