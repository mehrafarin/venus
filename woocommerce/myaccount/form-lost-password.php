<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

global $venus_options;

if ( isset( $venus_options['img_lost_password'] ) && strlen( $venus_options['img_lost_password']['url'] ) > 0 ) {
    $img_pass = $venus_options['img_lost_password']['url'];
} else {
    $img_pass = get_template_directory_uri().'/images/password.jpg';
}

do_action( 'woocommerce_before_lost_password_form' );
?>

<div class="form-lost-venus">
    <div class="text-center">
        <img class="lost-pass-image" src="<?php echo $img_pass; ?>">
    </div>
    <form method="post" class="woocommerce-ResetPassword lost_reset_password">

        <p><?php echo apply_filters( 'woocommerce_lost_password_message', esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p><?php // @codingStandardsIgnoreLine ?>

        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first w-100">
            <label class="mt-3" for="user_login"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?></label>
            <input class="woocommerce-Input my-4 woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" autocomplete="username" />
        </p>

        <div class="clear"></div>

        <?php do_action( 'woocommerce_lostpassword_form' ); ?>

        <p class="woocommerce-form-row coupon-venus form-row">
            <input type="hidden" name="wc_reset_password" value="true" />
            <button type="submit" class="woocommerce-Button button" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?></button>
        </p>

        <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>

    </form>
</div>
<?php
do_action( 'woocommerce_after_lost_password_form' );
