<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 */
global $venus_options;
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
if ( isset( $venus_options['user_login_icon_img'] ) && strlen( $venus_options['user_login_icon_img']['url'] ) > 0 ) {
    $img_login = $venus_options['user_login_icon_img']['url'];
} else {
    $img_login = get_template_directory_uri().'/images/user-login.png';
}
do_action('woocommerce_before_customer_login_form'); ?>

<?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

<div class="u-columns col2-set" id="customer_login">
    <div class="login-user-back position-relative">
        <style>
            .login-user:before{
                background-color: <?php echo $venus_options['user_login_icon_bg_color'] ?> !important;
                background: url(<?php echo $img_login; ?>) no-repeat 0 0;
            }
        </style>
        <div class="row login-user">
            <div class="col-12 col-lg-6">
                <div class="tab-content" id="nav-tabContent">
                    <!--Start-->
                    <div class="tab-pane fade show active" id="nav-wc-login" role="tabpanel"
                         aria-labelledby="nav-wc-login-tab">
                        <div class="u-column1">
                            <?php endif; ?>
<!--                            <h2>--><?php //esc_html_e('Login', 'woocommerce'); ?><!--</h2>-->
                            <form class="woocommerce-form woocommerce-form-login login m-0 p-0 border-0" method="post">

                                <?php do_action('woocommerce_login_form_start'); ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="username"><?php esc_html_e('Username or email address', 'woocommerce'); ?>
                                        &nbsp;<span class="required">*</span></label>
                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="username"
                                           id="username" autocomplete="username"
                                           value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                                </p>
                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="password"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
                                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password"/>
                                </p>

                                <?php do_action('woocommerce_login_form'); ?>

                                <p class="form-row">
                                    <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
                                    <button type="submit" class="woocommerce-button button w-100 ml-0 my-3 woocommerce-form-login__submit" name="login" value="<?php esc_attr_e('Log in', 'woocommerce'); ?>"><?php esc_html_e('Log in', 'woocommerce'); ?></button>
                                </p>
                                <p class="woocommerce-LostPassword lost_password my-3 d-flex justify-content-between">
                                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                                        <input class="woocommerce-form__input woocommerce-form__input-checkbox"
                                               name="rememberme"
                                               type="checkbox" id="rememberme" value="forever"/>
                                        <span><?php esc_html_e('Remember me', 'woocommerce'); ?></span>
                                    </label>
                                    <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'woocommerce'); ?></a>
                                </p>

                                <?php do_action('woocommerce_login_form_end'); ?>

                            </form>
                            <?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>
                        </div>
                    </div>
                    <!--End-->
                    <div class="tab-pane fade" id="nav-wc-reg" role="tabpanel" aria-labelledby="nav-wc-reg-tab">
                        <div class="u-column2">

<!--                            <h2>--><?php //esc_html_e('Register', 'woocommerce'); ?><!--</h2>-->

                            <form method="post" class="woocommerce-form woocommerce-form-register register-wc-venus m-0 p-0 border-0" <?php do_action('woocommerce_register_form_tag'); ?> >

                                <?php do_action('woocommerce_register_form_start'); ?>

                                <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_username"><?php esc_html_e('Username', 'woocommerce'); ?>
                                            &nbsp;<span class="required">*</span></label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                               name="username" id="reg_username" autocomplete="username"
                                               value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                                    </p>

                                <?php endif; ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_email"><?php esc_html_e('Email address', 'woocommerce'); ?>
                                        &nbsp;<span
                                                class="required">*</span></label>
                                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="email"
                                           id="reg_email" autocomplete="email"
                                           value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                                </p>

                                <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_password"><?php esc_html_e('Password', 'woocommerce'); ?>
                                            &nbsp;<span class="required">*</span></label>
                                        <input type="password"
                                               class="woocommerce-Input woocommerce-Input--text input-text"
                                               name="password" id="reg_password" autocomplete="new-password"/>
                                    </p>

                                <?php else : ?>

                                    <p><?php esc_html_e('A password will be sent to your email address.', 'woocommerce'); ?></p>

                                <?php endif; ?>

                                <?php do_action('woocommerce_register_form'); ?>

                                <p class="woocommerce-FormRow form-row">
                                    <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                                    <button type="submit" class="woocommerce-Button button w-100 ml-0 my-3 woocommerce-form-login__submit" name="register" value="<?php esc_attr_e('Register', 'woocommerce'); ?>"><?php esc_html_e('Register', 'woocommerce'); ?></button>
                                </p>

                                <?php do_action('woocommerce_register_form_end'); ?>

                            </form>

                        </div>
                    </div>
                </div>
                <div class="nav nav-tabs d-block border-0" id="nav-tab" role="tablist">
                    <a class="leading w-100 d-flex justify-content-between nav-item nav-link active" id="nav-wc-login-tab" data-toggle="tab" href="#nav-wc-login" role="tab" aria-controls="nav-wc-login" aria-selected="true"><span
                                class="text-dark">قبلا عضو شده اید؟</span>ورود به
                        سایت</a>
                    <a class="leading w-100 d-flex justify-content-between nav-item nav-link" id="nav-wc-reg-tab" data-toggle="tab" href="#nav-wc-reg" role="tab"
                       aria-controls="nav-wc-reg" aria-selected="false"><span class="text-dark">کاربر جدید هستید؟</span>ثبت
                        نام
                        در
                        سایت</a>
                </div>
            </div>
            <div class="d-none d-lg-block col-6">
                <div class="login-user-dis">
                    <?php if ( isset($venus_options['text_login_form_user_woo']) && $venus_options['text_login_form_user_woo'] ) { ?>
                        <?php echo $venus_options['text_login_form_user_woo']; ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php do_action('woocommerce_after_customer_login_form'); ?>
