<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_account_navigation');

global $venus_options;

$style_profile = $venus_options['select_dashboard'];

switch ($style_profile) {
    case 'default':
        ?>
        <nav class="col-12 col-lg-3 woocommerce-MyAccount-navigation">
            <div class="upanel-sidebar-inner">
                <div class="user-info">
                    <div class="avatar">
                        <?php
                        global $current_user;
                        echo get_avatar($current_user->ID, 250);
                        ?>
                    </div>
                    <div class="user-name"><?php global $current_user;
                        echo $current_user->display_name ?></div>
                    <div class="user-position"><?php
                        $user = wp_get_current_user();
                        $roles = ( array )$user->roles;
                        if ($roles[0] == 'administrator') {
                            echo 'مدیر کل';
                        } elseif ($roles[0] == 'editor') {
                            echo 'ویرایشگر';
                        } elseif ($roles[0] == 'author') {
                            echo 'نویسنده';
                        } elseif ($roles[0] == 'contributor') {
                            echo 'Contributor';
                        } elseif ($roles[0] == 'subscriber') {
                            echo 'دانشجو';
                        } else {
                            echo '<strong>' . $roles[0] . '</strong>';
                        } ?></div>
                </div>
                <?php
                $endpoint_ve = WC()->query->get_current_endpoint();
                $ve_class_endpoint = wc_get_account_menu_item_classes($endpoint_ve);
                ?>
                <label class="my-account-section-title <?php echo $ve_class_endpoint; ?>">
                    <?php
                    $string = wc_page_endpoint_title('title');

                    if ($string == 'title') {
                        echo get_the_title();
                    } else {
                        echo $string;
                    }
                    ?>
                </label>
                <aside class="panel-navigation ve-panel-navigation" id="panel-navigation">
                    <ul>
                        <?php foreach (wc_get_account_menu_items() as $endpoint => $label) : ?>
                            <li class="<?php echo wc_get_account_menu_item_classes($endpoint); ?> position-relative">
                                <a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>"><?php echo esc_html($label); ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </aside>
            </div>
        </nav>
        <?php
        break;
    case 'style_one_dashboard': ?>
        <nav class="col-12 col-lg-3 woocommerce-MyAccount-navigation woo-digikala-dashboard">
            <div class="digikala-sidebar-inner">
                <div class="digikala-info-user">
                    <div class="avatar">
                        <?php
                        global $current_user;
                        echo get_avatar($current_user->ID, 250);
                        ?>
                    </div>
                    <div class="digikala-user-name">
                        <span class="name-user">
                        <?php
                        global $current_user;
                        echo $current_user->display_name
                        ?>
                        </span>
                        <span class="phone-user">
                        <?php echo get_user_meta(get_current_user_id(), 'billing_phone', true); ?>
                        </span>
                    </div>
                </div>
                <?php
                $endpoint_ve = WC()->query->get_current_endpoint();
                $ve_class_endpoint = wc_get_account_menu_item_classes($endpoint_ve);
                ?>
                <label class="my-account-section-title mt-2 <?php echo $ve_class_endpoint; ?>">
                    <?php
                    $string = wc_page_endpoint_title('title');

                    if ($string == 'title') {
                        echo get_the_title();
                    } else {
                        echo $string;
                    }
                    ?>
                </label>
                <aside class="panel-navigation ve-panel-navigation">
                    <ul>
                        <?php foreach (wc_get_account_menu_items() as $endpoint => $label) : ?>
                            <li class="<?php echo wc_get_account_menu_item_classes($endpoint); ?> position-relative">
                                <a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>"><?php echo esc_html($label); ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </aside>
        </nav>
        <?php
        break;
    default:
        echo 'استایل پنل کاربری را از تنظیمات قالب انتخاب نمایید.';
}
do_action('woocommerce_after_account_navigation'); ?>
