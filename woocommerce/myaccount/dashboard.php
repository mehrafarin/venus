<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.4.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
global $venus_options;

$style_profile = $venus_options['select_dashboard'];

switch ($style_profile) {
    case 'default':
        ?>
        <?php
        if (isset($venus_options['user_panel_icon_bg_top_color']) && $venus_options['user_panel_icon_bg_top_color']) { ?>
            <style>
                .status-user-widget ul li .key_wrapper .icon i {
                    background: linear-gradient(324deg, <?php echo $venus_options['user_panel_icon_bg_top_color']['from']; ?> 0%, <?php echo $venus_options['user_panel_icon_bg_top_color']['to']; ?> 100%);
                    box-shadow: 0 2px 12px <?php echo $venus_options['user_panel_icon_bg_top_color']['from']; ?>99;
                }

                .notifications-box .notifications-icon i {
                    background: linear-gradient(324deg, <?php echo $venus_options['user_panel_icon_bg_top_color']['from']; ?> 0%, <?php echo $venus_options['user_panel_icon_bg_top_color']['to']; ?> 100%);
                    box-shadow: 0 2px 12px <?php echo $venus_options['user_panel_icon_bg_top_color']['from']; ?>99;
                }

                .notif-content:before {
                    background: <?php echo $venus_options['user_panel_icon_bg_top_color']['from']; ?>;
                    background-image: linear-gradient(270deg, <?php echo $venus_options['user_panel_icon_bg_top_color']['from']; ?> 0%, <?php echo $venus_options['user_panel_icon_bg_top_color']['to']; ?> 100%);
                }

                .notif-content span {
                    color: <?php echo $venus_options['user_panel_icon_bg_top_color']['from']; ?>;
                }
            </style>
        <?php } ?>
        <!--    <p>--><?php
        //        /* translators: 1: user display name 2: logout url */
        //        printf(
        //            __('Hello %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'woocommerce'),
        //            '<strong>' . esc_html($current_user->display_name) . '</strong>',
        //            esc_url(wc_logout_url(wc_get_page_permalink('myaccount')))
        //        );
        //
        ?><!--</p>-->
        <div class="status-user-widget">
            <ul class="row m-0 justify-content-between">
                <li class="all_course col-12 col-lg ml-2">
                    <div class="key_wrapper">
                        <span class="icon"><i class="fal fa-university"></i></span>
                        <a href="<?php echo get_permalink(get_option('woocommerce_shop_page_id')); ?>"
                           class="text-dark">
                            <span class="wc-amount">
                                <?php echo do_shortcode('[product_count]'); ?>
                                <span class="woocommerce-Price-currencySymbol"><?php echo $venus_options['ve_info_dashboard_title_box_one']; ?></span>
                            </span>
                            <span class="title"><?php echo $venus_options['ve_info_dashboard_sub_text_box_one']; ?></span>
                        </a>
                    </div>
                </li>
                <li class="all_course col-12 col-lg ml-2">
                    <div class="key_wrapper">
                        <span class="icon"><i class="fal fa-user-graduate"></i></span>
                        <a href="<?php echo esc_url(wc_get_account_endpoint_url('downloads')); ?>" class="text-dark">
                        <span class="wc-amount">
                        <?php
                        // GET USER ORDERS (COMPLETED + PROCESSING)
                        $customer_orders = get_posts(array(
                            'numberposts' => -1,
                            'meta_key' => '_customer_user',
                            'meta_value' => $current_user->ID,
                            'post_type' => wc_get_order_types(),
                            'post_status' => array('wc-completed'),
                        ));


                        // LOOP THROUGH ORDERS AND GET PRODUCT IDS
                        $product_ids = array();
                        foreach ($customer_orders as $customer_order) {
                            $order = new WC_Order($customer_order->ID);
                            $items = $order->get_items();
                            foreach ($items as $item) {
                                $product_id = $item->get_product_id();
                                $product_ids[] = $product_id;
                            }
                        }
                        $product_ids = array_unique($product_ids);

                        // QUERY PRODUCTS
                        $args = array(
                            'post_type' => 'product',
                            'post__in' => $product_ids,
                        );

                        echo count($product_ids);
                        ?>
				        <span class="wc-Symbol"><?php echo $venus_options['ve_info_dashboard_title_box_two']; ?></span></span><span class="title"><?php echo $venus_options['ve_info_dashboard_sub_text_box_two']; ?></span></a>
                    </div>
                </li>
                <li class="all_course col-12 col-lg ml-2">
                    <div class="key_wrapper">
                        <span class="icon"><i class="fal fa-cart-plus"></i></span>
                        <a href="<?php echo get_permalink(get_option('woocommerce_checkout_page_id')); ?>"
                           class="text-dark">
                            <span class="wc-amount">
                            <?php echo WC()->cart->get_cart_contents_count(); ?>
                            <span class="woocommerce-Price-currencySymbol"><?php echo $venus_options['ve_info_dashboard_title_box_three']; ?></span>
                            </span>
                            <span class="title"><?php echo $venus_options['ve_info_dashboard_sub_text_box_three']; ?></span>
                        </a>
                    </div>
                </li>
                <?php if (is_plugin_active('woo-wallet/woo-wallet.php')) { ?>
                    <li class="all_course col-12 col-lg ml-2">
                        <div class="key_wrapper">
                            <span class="icon"><i class="fal fa-wallet"></i></span>
                            <span class="wc-amount">
				<?php
                if (is_plugin_active('woo-wallet/woo-wallet.php')) {

                    $title = __('Current wallet balance', 'woo-wallet');
                    $menu_item = '<a class="woo-wallet-menu-contents" href="' . esc_url(wc_get_account_endpoint_url(get_option('woocommerce_woo_wallet_endpoint', 'woo-wallet'))) . '" title="' . $title . '">';
                    $menu_item .= woo_wallet()->wallet->get_wallet_balance(get_current_user_id());
                    $menu_item .= '<span class="title">موجودی شما</span>';
                    $menu_item .= '</a>';

                    echo $menu_item;
                } else {
                    echo '<span class="wc-Symbol">0 تومان</span>';
                    echo '<span class="title">موجودی شما</span>';
                }
                ?>
				</span>
                        </div>
                    </li>
                <?php } else { ?>
                    <style>
                        .status-user-widget ul li .key_wrapper .icon i {
                            right: 32% !important;
                        }
                    </style>
                <?php } ?>
            </ul>
        </div>
        <?php if (isset($venus_options['account_notify']) && $venus_options['account_notify']) { ?>

        <div class="notifications-box">
            <span class="notifications-icon"><i class="fal fa-bell"></i></span>
            <h4><?php echo $venus_options['notify_title_text_new']; ?></h4>
            <ul class="list-unstyled p-0 mt-4">
                <?php
                $args = array(
                    'post_type' => 'notifications',
                    'posts_per_page' => $venus_options['repetitive_notify_count'],
                );
                $my_query = new WP_Query($args);
                while ($my_query->have_posts()) : $my_query->the_post();
                    ?>
                    <li class="announce-read mx-2">
                        <a class="text-dark" data-toggle="collapse" href="#collapse<?php echo get_the_ID(); ?>"
                           role="button" aria-expanded="false" aria-controls="collapse<?php echo get_the_ID(); ?>">
                            <div class="notif-content py-2 px-3 bg-light rounded mt-3 d-flex justify-content-between">
                                <h3><?php the_title(); ?></h3>
                                <span class="notif-date"><?php echo get_the_date('j F Y'); ?></span>
                            </div>
                        </a>
                        <div class="collapse" id="collapse<?php echo get_the_ID(); ?>">
                            <div class="card p-con-notif card-body mx-2 border-0">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </li>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </ul>
        </div>
    <?php } ?>
        <?php
        break;
    case 'style_one_dashboard': ?>
        <?php global $current_user; ?>
        <div class="row">
            <div class="col-12 col-sm-6">
                <label class="lbl-hs-panel">نام و نام خانوادگی :</label>
                <p class="form-control"><?php echo get_user_meta(get_current_user_id(), 'first_name', true), get_user_meta(get_current_user_id(), 'last_name', true); ?></p>
            </div>
            <div class="col-12 col-sm-6">
                <label class="lbl-hs-panel">آدرس ایمیل :</label>
                <p class="form-control"><?php echo $current_user->user_email; ?></p>
            </div>
            <div class="col-12 col-sm-6 mt-3">
                <label class="lbl-hs-panel">شماره تلفن همراه:</label>
                <p class="form-control"><?php echo get_user_meta(get_current_user_id(), 'billing_phone', true); ?></p>
            </div>
            <div class="col-12 col-sm-6 mt-3">
                <label class="lbl-hs-panel">تاریخ عضویت :</label>
                <p class="form-control" style="direction: ltr;text-align: right;"><?php echo hs_user_registered(); ?></p>
            </div>
            <div class="col-12 mt-3">
                <a href="<?php echo $string = wc_get_account_endpoint_url('edit-account'); ?>"
                   class="btn btn-edit-digikala d-flex float-left">ویرایش اطلاعات
                </a>
            </div>
        </div>
        <?php
        break;
    default:
        echo 'استایل پنل کاربری را از تنظیمات قالب انتخاب نمایید.';
}
?>
<?php

/**
 * My Account dashboard.
 *
 * @since 2.6.0
 */
do_action('woocommerce_account_dashboard');

/**
 * Deprecated woocommerce_before_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_before_my_account');

/**
 * Deprecated woocommerce_after_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_after_my_account');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
