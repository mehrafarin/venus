<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined('ABSPATH') || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */ ?>
<div class="row ve-res-dashboard">
    <?php do_action('woocommerce_account_navigation'); ?>

    <?php
    global $venus_options;

    $style_profile = $venus_options['select_dashboard'];

    switch ($style_profile) {
        case 'default':
            ?>
            <div class="col-12 col-lg-9 woocommerce-MyAccount-content upanel-content">
                <div class="upanel-content-inner">
                    <?php
                    /**
                     * My Account content.
                     *
                     * @since 2.6.0
                     */
                    do_action('woocommerce_account_content');
                    ?>
                </div>
            </div>
            <?php
            break;
        case 'style_one_dashboard': ?>
            <div class="col-12 col-lg-9 woocommerce-MyAccount-content digikala-panel-content">
                <div class="digikala-content-inner">
                    <?php
                    /**
                     * My Account content.
                     *
                     * @since 2.6.0
                     */
                    do_action('woocommerce_account_content');
                    ?>
                </div>
            </div>
            <?php
            break;
        default:
            echo 'استایل پنل کاربری را از تنظیمات قالب انتخاب نمایید.';
    }
    ?>
</div>
