<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product, $venus_options;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}
switch ($venus_options['style_archive_products']) {
    case 'archive-product-style-one':
            get_template_part('template/products/archive-products/archive-style-one');
        break;
    case 'archive-product-style-two':
        get_template_part('template/products/archive-products/archive-style-two');
    break;
    default:
    get_template_part('template/products/archive-products/archive-style-one');
}

