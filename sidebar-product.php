<?php
/**
 * The sidebar containing the main widget area
 *
 */

if ( ! is_active_sidebar( 'sidebar_product' ) ) {
	return;
}
?>
<div class="sidebar-widgets-wrapper">
	<?php dynamic_sidebar( 'sidebar_product' ); ?>
</div>
