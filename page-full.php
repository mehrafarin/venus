<?php /* Template Name:  برگه - بدون عنوان */ ?>
<?php get_header(); ?>

<?php if( iwp_Shield::is_activated() === true ) : ?>
<div class="container mt-5 pt-3">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>

    <?php endwhile;?>
    <?php endif; ?>
</div>
<?php endif; ?>

<?php get_footer(); ?>

