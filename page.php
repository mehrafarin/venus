<?php get_header(); ?>
<?php if( iwp_Shield::is_activated() === true ) : ?>
<div class="container cont-page mt-5 pt-3">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

       <h1 class="title-page-shop text-center"><?php the_title(); ?></h1>

        <?php the_content(); ?>

    <?php endwhile;?>
    <?php endif; ?>
</div>
<?php endif; ?>

<?php get_footer(); ?>

