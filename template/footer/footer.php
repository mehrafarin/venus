<?php
global $venus_options;
if (isset($venus_options['logo_footer']) && strlen($venus_options['logo_footer']['url']) > 0) {
    $logo_footer_href = $venus_options['logo_footer']['url'];
} else {
    $logo_footer_href = get_template_directory_uri() . '/images/logo-footer.png';
}
if (isset($venus_options['footer_background']) && $venus_options['footer_background']) { ?>
    <style>
        .bgr:before {
            background: <?php echo $venus_options['footer_background']['to']; ?>;
            background: linear-gradient(324deg, <?php echo $venus_options['footer_background']['from']; ?> 0%, <?php echo $venus_options['footer_background']['to']; ?> 100%);
        }

        @media (max-width: 1200px) {
            .footer-back:before {
                background: <?php echo $venus_options['footer_background']['to']; ?>;
            }
        }
    </style>
<?php } ?>
<?php if (isset($venus_options['go_top_bg_color']) && $venus_options['go_top_bg_color']) { ?>
    <style>
        #scrollTop:hover {
            box-shadow: 0 2px 12px<?php echo $venus_options['go_top_bg_color']; ?>;
        }
    </style>
<?php }; ?>
<?php if (isset($venus_options['padding_top_footer_num']) && $venus_options['padding_top_footer_num']) { ?>
    <style>
        .footer-back {
            margin-top: <?php echo $venus_options['padding_top_footer_num']; ?>px !important;
        }
    </style>
<?php }; ?>
<!-- Start Footer -->
<div class="footer-back position-relative">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-9 text-right bgr position-relative pb-2">
                <div class="main-footer">
                    <div class="logo-footer mb-xl-4">
                        <?php if ($venus_options['logo_footer_switch'] == true): ?>
                            <a href="<?php echo home_url(); ?>">
                                <img src="<?php echo $logo_footer_href; ?>" style="margin-right: 183px;" alt="<?php bloginfo('name'); ?>">
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="footer-social pr-0 pr-xl-5 mr-0 mr-xl-5 d-block" style="margin-top: 30px;margin-bottom: 30px;">
                        <div class="footer-social-images m-0 row">
                            <?php if (isset($venus_options['social-facebook']) && $venus_options['social-facebook']) { ?>
                                <div class="footer-social-icon footer-social-facebook">
                                    <a href="<?php echo $venus_options['social-facebook']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($venus_options['social-youtube']) && $venus_options['social-youtube']) { ?>
                                <div class="footer-social-icon footer-social-youtube">
                                    <a href="<?php echo $venus_options['social-youtube']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($venus_options['social-twitter']) && $venus_options['social-twitter']) { ?>
                                <div class="footer-social-icon footer-social-twitter">
                                    <a href="<?php echo $venus_options['social-twitter']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($venus_options['social-googleplus']) && $venus_options['social-googleplus']) { ?>
                                <div class="footer-social-icon footer-social-googleplus">
                                    <a href="<?php echo $venus_options['social-googleplus']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($venus_options['social-instagram']) && $venus_options['social-instagram']) { ?>
                                <div class="footer-social-icon footer-social-instagram">
                                    <a href="<?php echo $venus_options['social-instagram']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($venus_options['social-aparat']) && $venus_options['social-aparat']) { ?>
                                <div class="footer-social-icon footer-social-aparat">
                                    <a href="<?php echo $venus_options['social-aparat']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($venus_options['social-telegram']) && $venus_options['social-telegram']) { ?>
                                <div class="footer-social-icon footer-social-telegram">
                                    <a href="<?php echo $venus_options['social-telegram']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($venus_options['social-pinterest']) && $venus_options['social-pinterest']) { ?>
                                <div class="footer-social-icon footer-social-pinterest">
                                    <a href="<?php echo $venus_options['social-pinterest']; ?>" target="_blank">
                                        <span></span>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <?php
                    if (isset($venus_options['nemad_dis_enb_btn']) && $venus_options['nemad_dis_enb_btn']) {
                        if (isset($venus_options['logo_enamad']) && $venus_options['logo_enamad']) { ?>
                            <div class="d-none nemad-footer">
                                <div class="nemad mt-1">
                                    <?php echo $venus_options['logo_enamad']; ?>
                                </div>
                            </div>
                        <?php }
                    } ?>

                    <div class="menu-footer position-relative">
                        <?php if (has_nav_menu('footer_menu')): ?>
                            <?php wp_nav_menu(array('theme_location' => 'footer_menu')) ?>
                        <?php endif; ?>
                    </div>
                    <p class="prower m-0">
                        <?php echo $venus_options['copyright_footer']; ?>
                    </p>
                </div>
            </div>
            <?php if (isset($venus_options['logo_enamad']) && $venus_options['logo_enamad']) { ?>
                <div class="col-12 col-xl-3 n-logo">
                    <div class="nemad">
                        <?php echo $venus_options['logo_enamad']; ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
