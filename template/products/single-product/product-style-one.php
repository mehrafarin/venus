<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<?php
global $venus_options;
$cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
$buy_less = $venus_options['buy_less'];

if (isset($venus_options['color_bg_btn_register_product_main']) && $venus_options['color_bg_btn_register_product_main']) { ?>
    <style>
        .btns-color-pro {
            background-color: <?php echo $venus_options['color_bg_btn_register_product_main']; ?> !important;
            color: <?php echo $venus_options['color_bg_btn_text_register_product_main']; ?> !important;
            box-shadow: 0 2px 12px <?php echo $venus_options['color_bg_btn_register_product_main']; ?>99 !important;
        }

        .btn-reg {
            background-color: <?php echo $venus_options['color_bg_btn_register_product_main']; ?> !important;
            color: <?php echo $venus_options['color_bg_btn_text_register_product_main']; ?> !important;
            box-shadow: 0 2px 12px <?php echo $venus_options['color_bg_btn_register_product_main']; ?>99 !important;
        }

        .wpcf7-submit {
            background-color: <?php echo $venus_options['color_bg_btn_register_product_main']; ?> !important;
        }

        .style-form .advice .advice-content .form-box .form-contact input[type="submit"] {
            background-color: <?php echo $venus_options['color_bg_btn_register_product_main']; ?> !important;
            box-shadow: 0 2px 12px <?php echo $venus_options['color_bg_btn_register_product_main']; ?>99 !important;
        }

        .phone-add-cats a:before {
            color: <?php echo $venus_options['color_bg_btn_text_register_product_main']; ?> !important;
        }
    </style>
<?php } ?>
<?php if (isset($venus_options['color_btn_info_register_product_main']) && $venus_options['color_btn_info_register_product_main']) { ?>
    <style>
        .sidebar .sidebar-inner .sidebar-buttons a {
            box-shadow: 0 2px 12px <?php echo $venus_options['color_btn_info_register_product_main']; ?>99;
        }

        .sidebar .sidebar-inner .sidebar-buttons a:hover {
            background-color: <?php echo $venus_options['color_btn_info_register_product_main']; ?>;
            box-shadow: 0 2px 12px<?php echo $venus_options['color_btn_info_register_product_main']; ?>;
        }
    </style>
<?php } ?>
<?php if (isset($venus_options['color_btn_advice_product_main']) && $venus_options['color_btn_advice_product_main']) { ?>
    <style>
        .sidebar .sidebar-inner .sidebar-buttons a.requet-course-advice {
            border: 1px solid<?php echo $venus_options['color_btn_advice_product_main']; ?>;
            box-shadow: 0px 2px 11px <?php echo $venus_options['color_btn_advice_product_main']; ?>99;
        }

        .sidebar .sidebar-inner .sidebar-buttons a.requet-course-advice:hover {
            color: <?php echo $venus_options['color_btn_advice_product_main']; ?>;
            box-shadow: 0px 2px 11px<?php echo $venus_options['color_btn_advice_product_main']; ?>;
        }
    </style>
<?php } ?>
<?php if (isset($venus_options['padding_top_product_page']) && $venus_options['padding_top_product_page']) { ?>
    <style>
        .lms-top-header {
            margin-bottom: <?php echo $venus_options['padding_top_product_page']; ?>px !important;
        }
    </style>
<?php }; ?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('venus-single-product-page', $product); ?>>
    <!-- Start Body -->
    <div class="container">
        <div class="row mb-5"></div>
        <div class="lms-top-header lms-top-header-venus">
            <div class="header-bg col-md-2 col-xs-5">
                <div class="header-bg-image header-bg-image-product" style="background: url(<?php echo get_post_meta(get_the_ID(), 'bg_date', true); ?>) no-repeat;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-9 single-product course-content content-product-woo" id="course-intro">

                <div class="breadcrumbs text-right mt-0 m-lg-5 mb-3">
                    <?php
                    $args = array(
                        'delimiter' => '<i class="fas fa-chevron-left"></i>',
                    );
                    woocommerce_breadcrumb($args); ?>
                </div>
                <?php ?>
                <div class="info-top-product d-lg-none d-flex justify-content-between">
                    <div class="right">
                        <?php
                        venus_course_status();

                        if ($cu_status != 'soon') {
                            if (!empty(get_post_meta(get_the_ID(), 'pro_control', true))) : ?>
                                <div id="course_student_info">
                                <span class="student_count">
                                    <?php echo $product->get_total_sales(); ?>
                                </span>
                                    <span class="student_text"><?php echo get_post_meta(get_the_ID(), 'pro_control', true) ?></span>
                                </div>
                            <?php endif;
                        } else {
                            ?>
                            <div class="sidebar-product-soon">
                                <div class="countdown-item"
                                     data-date="<?php echo get_post_meta(get_the_ID(), 'timer_course_status', true); ?>"></div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="left">
                        <?php if (!empty(get_post_meta(get_the_ID(), 'consultation_request', true))) : ?>
                            <a href="#course-advice" class="sidebar-register anchor requet-course-advice">
                                <?php echo get_post_meta(get_the_ID(), 'consultation_request', true) ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>

                <h1 class="product-title mt-2 d-md-flex justify-content-md-between">
                    <?php
                    the_title();

                    if ($cu_status != 'soon') {
                        $current_user = wp_get_current_user();
                        if (wc_customer_bought_product($current_user->user_email, $current_user->ID, $product->get_id()) && ($buy_less)) {
                            echo '';
                        } else {
                            do_action('woocommerce_single_product_countdown');
                        }
                    }
                    ?>
                </h1>

                <?php the_content(); ?>

                <!-- START Course Register -->
                <div id="course-register">
                    <div class="course-register">
                        <?php
                        $ve_pro_type = get_post_meta(get_the_ID(), 'pro_type', true);
                        if (!empty($ve_pro_type)):
                            echo '<p class="title-register">' . $ve_pro_type . '</p>';
                        endif;
                        ?>
                        <div class="row content-register">
                            <div class="col-12 col-lg-7 right-box-register">
                                <?php echo $product->get_short_description(); ?>
                            </div>
                            <div class="col-12 col-lg-5 left-box-register">
                                <div class="price-box text-center mt-3">
                                    <?php
                                    //echo $product->get_price_html();
                                    $current_user = wp_get_current_user();
                                    if ($cu_status != 'soon') {
                                        if (wc_customer_bought_product($current_user->user_email, $current_user->ID, $product->get_id()) && ($buy_less)) {
                                            echo '<span class="btn-reg btns-color-pro mt-4">' . $venus_options['buy_less_text'] . '</span>';
                                        } else {
                                            do_action('woocommerce_single_product_summary');
                                        }
                                    } ?>
                                </div>
                                <div class="phone-add-cats d-lg-none position-fixed">
                                    <?php if ($cu_status != 'soon') {

                                        $style_add_cart_phone = $venus_options['style_add_cart_fix_products'];

                                        if ($venus_options['add_cart_btn_phone_bottom']):
                                            switch ($style_add_cart_phone) {
                                                case 'btn-fix-add-cart-one':
                                                    ?>
                                                    <a class="button product_type_variable add_to_cart_button btn-reg btns-color-pro mt-4"
                                                       href="#course-register">
                                                        <?php
                                                        global $venus_options;

                                                        if (empty(get_post_meta(get_the_ID(), 'register_btn_center', true))) {

                                                            $name_btn_buy = $venus_options['text_register_btn_fixed_product_page'];

                                                        } else {

                                                            $name_btn_buy = get_post_meta(get_the_ID(), 'register_btn_center', true);

                                                        };

                                                        if (wc_customer_bought_product($current_user->user_email, $current_user->ID, $product->get_id()) && ($buy_less)) {

                                                            echo($venus_options['buy_less_text']);

                                                        } else {

                                                            echo $name_btn_buy;

                                                        }
                                                        ?></a>
                                                    <?php
                                                    break;
                                                case 'btn-fix-add-cart-two':
                                                    ?>
                                                    <a class="button product_type_variable add_to_cart_button btn-reg btns-color-pro mt-4 phone-add-cart-v-two"
                                                       href="<?php echo ve_url_add_cart(); ?>">
                                                        <div class="top-phone-add-cart d-flex">
                                                            <?php the_post_thumbnail();

                                                            global $product;

                                                            echo $product->get_price_html();

                                                            global $venus_options;

                                                            if (empty(get_post_meta(get_the_ID(), 'register_btn_center', true))) {
                                                                $name_btn_buy = $venus_options['text_register_btn_fixed_product_page'];
                                                            } else {
                                                                $name_btn_buy = get_post_meta(get_the_ID(), 'register_btn_center', true);
                                                            };

                                                            if (wc_customer_bought_product($current_user->user_email, $current_user->ID, $product->get_id()) && ($buy_less)) {
                                                                echo '<span class="d-block my-auto ve-two-add-cart-phone-btn">' . $venus_options['buy_less_text'] . '</span>';
                                                            } else {

                                                                echo '<span class="d-block my-auto ve-two-add-cart-phone-btn">' . $name_btn_buy . '</span>';
                                                            }
                                                            ?>
                                                        </div>
                                                    </a>
                                                    <?php
                                                    break;
                                                default: ?>
                                                    <a class="button product_type_variable add_to_cart_button btn-reg btns-color-pro mt-4"
                                                       href="#course-register">
                                                        <?php
                                                        global $venus_options;
                                                        if (wc_customer_bought_product($current_user->user_email, $current_user->ID, $product->get_id()) && ($buy_less)) {
                                                            echo($venus_options['buy_less_text']);
                                                        } else {
                                                            echo $venus_options['text_register_btn_fixed_product_page'];
                                                        }
                                                        ?></a>
                                                <?php
                                            }
                                        endif;
                                        ?>

                                    <?php } else {
                                        ?>
                                        <a href="#course-register" class="button badge-info soon-btn-phone">
                                            <div class="sidebar-product-soon">
                                                <div class="countdown-item"
                                                     data-date="<?php echo get_post_meta(get_the_ID(), 'timer_course_status', true); ?>"></div>
                                            </div>
                                        </a>
                                        <?php
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Course Register -->
                <!-- Start related product -->
                <?php if (isset($venus_options['show_related']) && $venus_options['show_related']) { ?>
                    <div class="related-product">
                        <?php woocommerce_output_related_products(); ?>
                    </div>
                <?php } ?>
                <!-- End related product -->
                <!--  START Comment -->
                <div class="mt-5 box-comment" id="comments">
                    <h2 class="section-title"><?php echo $venus_options['reviews_tab_heading']; ?></h2>
                    <?php comments_template(); ?>
                </div>
                <!-- END Comment -->

            </div>
            <aside class="d-none d-lg-block col-3 sidebar">
                <div class="sidebar-inner">
                    <?php
                    venus_course_status();

                    if ($cu_status != 'soon') {
                        if (!empty(get_post_meta(get_the_ID(), 'pro_control', true))) : ?>
                            <div id="course_student_info">
                                <span class="student_count">
                                    <?php echo $product->get_total_sales(); ?>
                                </span>
                                <span class="student_text"><?php echo get_post_meta(get_the_ID(), 'pro_control', true) ?></span>
                            </div>
                        <?php endif;
                    } else {
                        ?>
                        <div class="sidebar-product-soon">
                            <div class="countdown-item"
                                 data-date="<?php echo get_post_meta(get_the_ID(), 'timer_course_status', true); ?>"></div>
                        </div>
                        <?php
                    }
                    ?>

                    <ul id="menu-center">
                        <?php if (!empty(get_post_meta(get_the_ID(), 'course_introduction', true))) : ?>
                            <li>
                                <a href="#course-intro" class="anchor">
                                    <?php echo get_post_meta(get_the_ID(), 'course_introduction', true) ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty(get_post_meta(get_the_ID(), 'session_sessions', true))) : ?>
                            <li>
                                <a href="#course-lessons" class="anchor">
                                    <?php echo get_post_meta(get_the_ID(), 'session_sessions', true) ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty(get_post_meta(get_the_ID(), 'course_requirement', true))) : ?>
                            <li>
                                <a href="#course-requirement" class="anchor">
                                    <?php echo get_post_meta(get_the_ID(), 'course_requirement', true) ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty(get_post_meta(get_the_ID(), 'course_teacher', true))) : ?>
                            <li>
                                <a href="#course-teachers" class="anchor">
                                    <?php echo get_post_meta(get_the_ID(), 'course_teacher', true) ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty(get_post_meta(get_the_ID(), 'questions', true))) : ?>
                            <li>
                                <a href="#course-faq" class="anchor">
                                    <?php echo get_post_meta(get_the_ID(), 'questions', true) ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty(get_post_meta(get_the_ID(), 'opinions', true))) : ?>
                            <li>
                                <a href="#comments" class="anchor">
                                    <?php echo get_post_meta(get_the_ID(), 'opinions', true) ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <div class="sidebar-buttons">
                        <?php if (!empty(get_post_meta(get_the_ID(), 'consultation_request', true))) : ?>
                            <a href="#course-advice" class="sidebar-register anchor requet-course-advice">
                                <?php echo get_post_meta(get_the_ID(), 'consultation_request', true) ?>
                            </a>
                        <?php endif; ?>
                        <?php
                        if ($cu_status != 'soon') {

                            if (!empty(get_post_meta(get_the_ID(), 'register_btn', true))) :
                                if (wc_customer_bought_product($current_user->user_email, $current_user->ID, $product->get_id()) && ($buy_less)) {
                                    echo '<a href="#course-register" class="sidebar-register anchor">';
                                    echo($venus_options['buy_less_text']);
                                    echo '</a>';
                                } else {
                                    ?>
                                    <a href="#course-register" class="sidebar-register anchor">
                                        <?php echo get_post_meta(get_the_ID(), 'register_btn', true) ?>
                                    </a>
                                <?php } endif;
                        } ?>
                    </div>
                    <?php
                    if ($cu_status != 'soon') {

                        if (!empty(get_post_meta(get_the_ID(), 'price_text_sidebar', true))) : ?>
                            <div class="sidebar-price text-center">
                                <div class="side-title text-center">
                                    <span><?php echo get_post_meta(get_the_ID(), 'price_text_sidebar', true) ?></span>
                                </div>
                                <?php echo $product->get_price_html(); ?>
                            </div>
                        <?php endif;
                    } ?>
                </div>
            </aside>
        </div>
    </div>
    <!-- End Body -->
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
