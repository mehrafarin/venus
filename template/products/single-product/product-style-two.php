<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>
<?php
global $venus_options;
$prefix = '_venus_';
$digikala_en_name = get_post_meta(get_the_ID(), $prefix . 'physical_name_en_product', true);
$buy_less = $venus_options['buy_less'];
$in_stock = $product->is_in_stock();
?>
<div id="product-<?php the_ID(); ?> digikala" <?php wc_product_class('', $product); ?>>

    <div class="breadcrumbs digikala-breadcrumbs text-right my-4">
        <?php
        $args = array(
            'delimiter' => '<i class="fas fa-chevron-left"></i>',
        );
        woocommerce_breadcrumb($args); ?>
    </div>
    <?php
    if ($in_stock == false) :
        if (isset($venus_options['show_related']) && $venus_options['show_related']) : ?>
            <div class="digikala-related-product my-3">
                <?php woocommerce_output_related_products(); ?>
            </div>
        <?php endif;
    endif; ?>
    <div class="venus-single-pro">
        <div class="row">
            <div class="col-12 col-md-4 position-relative c-border-left d-block m-auto">
                <div class="digikala-gallery py-3">
                    <?php woocommerce_show_product_images(); ?>
                </div>
            </div>
            <div class="col-12 col-md-8 summary entry-summary left-product">
                <h1 class="title-pro"><?php the_title(); ?>
                    <span class="title-en"><?php echo $digikala_en_name; ?></span>
                </h1>
                <div class="content-pro row m-0">
                    <div class="c-right-box col-12 col-md-6 col-lg-7 pr-md-0">
                        <div class="data-box-digikala d-flex mb-3">
                            <div class="rate-product">
                                <?php
                                $average = $product->get_average_rating();
                                echo '<i class="fas fa-star ml-1" style="color: #FEBA3C"></i>';
                                echo esc_attr($average);
                                ?>
                            </div>
                            <div class="comments-product">
                                <?php echo get_comments_number(); ?>
                                دیدگاه کاربران
                            </div>
                        </div>
                        <div class="c-product-directory">
                            <ul>
                                <li><span>دسته‌بندی :</span>
                                    <?php echo '<ul>' . wc_get_product_category_list(get_the_id(), '</li> ،<li>', '<li>', '</li>') . '</ul>'; ?>
                                </li>
                            </ul>
                        </div>
                        <div class="c-product-params">
                            <?php echo do_shortcode($product->get_short_description()); ?>
                        </div>
                    </div>
                    <div class="c-cart-price col-12 col-md-6 col-lg-5">
                        <div class="c-inner-cart-price">
                            <?php
                            if ($in_stock) {

                                if (isset($venus_options['switch_status_physical_name_seller']) && $venus_options['switch_status_physical_name_seller']):
                                    physical_display_seller_name();
                                endif;
                                if (isset($venus_options['switch_status_physical_guarantee']) && $venus_options['switch_status_physical_guarantee']):
                                    physical_guarantee_product();
                                endif;
                                if (isset($venus_options['switch_status_physical_in_stock']) && $venus_options['switch_status_physical_in_stock']):
                                    physical_stock_product();
                                endif;

                                physical_product_price();

                                woocommerce_template_single_add_to_cart();
                            } else {
                                physical_is_in_stock();
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="digikala-content-product mt-5">
        <?php
        /**
         * Hook: woocommerce_after_single_product_summary.
         *
         * @hooked woocommerce_output_product_data_tabs - 10
         * @hooked woocommerce_upsell_display - 15
         * @hooked woocommerce_output_related_products - 20
         */
        do_action('woocommerce_after_single_product_summary');
        ?>
    </div>
    <!-- Start related product -->
    <?php
    if ($in_stock) :

        if (isset($venus_options['show_related']) && $venus_options['show_related']) : ?>
            <div class="digikala-related-product mt-3">
                <?php woocommerce_output_related_products(); ?>
            </div>
        <?php endif;

    endif; ?>
    <!-- End related product -->
</div>

<?php do_action('woocommerce_after_single_product'); ?>
