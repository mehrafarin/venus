<div class="digikala-product m-0 col-sm-12 col-md-6 col-xl-3">
                                            <a href="<?php the_permalink(); ?>" class="digikala-img">
                                                <?php global $product;

                                                if ($product->is_on_sale()) {

                                                    if (!$product->is_type('variable')) {

                                                        $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                    } else {

                                                        $max_percentage = 0;

                                                        foreach ($product->get_children() as $child_id) {
                                                            $variation = wc_get_product($child_id);
                                                            $price = $variation->get_regular_price();
                                                            $sale = $variation->get_sale_price();
                                                            if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                            if ($percentage > $max_percentage) {
                                                                $max_percentage = $percentage;
                                                            }
                                                        }

                                                    }
                                                    echo "<span class='badge-vip'>" . round($max_percentage) . "% </span>";
                                                }
                                                ?>
                                                <img src="<?php the_post_thumbnail_url(); ?>" class="digikala-inner-img-product" alt="">
                                            </a>
                                            <div class="digikala-title">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </div>
                                            <div class="digikala-price">
                                                <span><?php echo $product->get_price_html(); ?></span>
                                            </div>
                                        </div>
