<?php

global $product, $venus_options;

if ($venus_options['sidebar_position_shop'] == 'none') {
    ?>
    <div class="col-sm-12 col-md-6 col-xl-4 course">
        <div class="course-inner">
            <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">

                <?php
                venus_course_status();

                $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                if ($cu_status != 'soon') {
                    if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                        <div class="course-item-sale">
                            <?php
                            global $product;

                            if ($product->is_on_sale()) {

                                if (!$product->is_type('variable')) {

                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                } else {

                                    $max_percentage = 0;

                                    foreach ($product->get_children() as $child_id) {
                                        $variation = wc_get_product($child_id);
                                        $price = $variation->get_regular_price();
                                        $sale = $variation->get_sale_price();
                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                        if ($percentage > $max_percentage) {
                                            $max_percentage = $percentage;
                                        }
                                    }

                                }
                                echo "<div class='sale-perc-badge'>";
                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                    <?php }
                } ?>
                <a href="<?php the_permalink(); ?>" class="d-block position-relative h-100">
                    <?php if (isset($venus_options['waves_product_show']) && $venus_options['waves_product_show']) : ?>
                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                             preserveAspectRatio="none" shape-rendering="auto">
                            <defs>
                                <path id="gentle-wave"
                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                            </defs>
                            <g class="parallax">
                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                     fill="rgba(255,255,255,0.7"></use>
                                <use xlink:href="#gentle-wave" x="25" y="13"
                                     fill="rgba(255,255,255,0.5)"></use>
                                <use xlink:href="#gentle-wave" x="100" y="5"
                                     fill="rgba(255,255,255,0.3)"></use>
                                <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                            </g>
                        </svg>
                    <?php endif; ?>
                </a>
                <?php
                $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                if ($link_show_course_intro_video) : ?>
                    <div class="video-button">
                        <a data-post-id="<?php echo get_the_ID(); ?>"
                           href="<?php echo esc_url($link_show_course_intro_video); ?>"
                           class="intro-play-icon"><i class="fal fa-play"></i></a>
                    </div>
                <?php endif;
                if ($cu_status != 'soon') {
                    woocommerce_template_loop_add_to_cart();
                }
                ?>
            </header>
            <div class="course-detail">
                <div class="course-top-content">
                    <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2></a>
                    <div class="course-description">
                        <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                    </div>
                </div>
                <?php if (isset($venus_options['info_product_bottom']) && $venus_options['info_product_bottom']) { ?>
                    <div class="course-information position-relative d-flex">
                        <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                            <div class="col p-0 text-right">
                                <span class="level-course">سطح</span>
                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                            </div>
                        <?php } else {
                        } ?>
                        <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                            <div class="col p-0 text-right">
                                <span class="level-course">دوره</span>
                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                            </div>
                        <?php } else {
                        } ?>
                        <div class="col p-0 text-center price-course m-auto">
                            <?php echo $product->get_price_html(); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="col-sm-12 col-md-6 course">
        <div class="course-inner">
            <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">

                <?php
                venus_course_status();

                $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                if ($cu_status != 'soon') {
                    if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                        <div class="course-item-sale">
                            <?php
                            global $product;

                            if ($product->is_on_sale()) {

                                if (!$product->is_type('variable')) {

                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                } else {

                                    $max_percentage = 0;

                                    foreach ($product->get_children() as $child_id) {
                                        $variation = wc_get_product($child_id);
                                        $price = $variation->get_regular_price();
                                        $sale = $variation->get_sale_price();
                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                        if ($percentage > $max_percentage) {
                                            $max_percentage = $percentage;
                                        }
                                    }

                                }
                                echo "<div class='sale-perc-badge'>";
                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                    <?php }
                } ?>
                <a href="<?php the_permalink(); ?>" class="d-block position-relative h-100">
                    <?php if (isset($venus_options['waves_product_show']) && $venus_options['waves_product_show']) : ?>
                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                             preserveAspectRatio="none" shape-rendering="auto">
                            <defs>
                                <path id="gentle-wave"
                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                            </defs>
                            <g class="parallax">
                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                     fill="rgba(255,255,255,0.7"></use>
                                <use xlink:href="#gentle-wave" x="25" y="13"
                                     fill="rgba(255,255,255,0.5)"></use>
                                <use xlink:href="#gentle-wave" x="100" y="5"
                                     fill="rgba(255,255,255,0.3)"></use>
                                <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                            </g>
                        </svg>
                    <?php endif; ?>
                </a>
                <?php
                $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                if ($link_show_course_intro_video) : ?>
                    <div class="video-button">
                        <a data-post-id="<?php echo get_the_ID(); ?>"
                           href="<?php echo esc_url($link_show_course_intro_video); ?>"
                           class="intro-play-icon"><i class="fal fa-play"></i></a>
                    </div>
                <?php endif;
                if ($cu_status != 'soon') {
                    woocommerce_template_loop_add_to_cart();
                }
                ?>
            </header>
            <div class="course-detail">
                <div class="course-top-content">
                    <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2></a>
                    <div class="course-description">
                        <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                    </div>
                </div>
                <?php if (isset($venus_options['info_product_bottom']) && $venus_options['info_product_bottom']) { ?>
                    <div class="course-information position-relative d-flex">
                        <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                            <div class="col p-0 text-right">
                                <span class="level-course">سطح</span>
                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                            </div>
                        <?php } else {
                        } ?>
                        <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                            <div class="col p-0 text-right">
                                <span class="level-course">دوره</span>
                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                            </div>
                        <?php } else {
                        } ?>
                        <div class="col p-0 text-center price-course m-auto">
                            <?php echo $product->get_price_html(); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
