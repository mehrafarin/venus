<?php global $venus_options; ?>

<div class="dropdown ve--search">
    <button class="venus-search-header" data-display="static" data-toggle="dropdown" id="search-header" role="button" aria-expanded="false">
        <i class="color-btn-icon-search-header pt-1 fal fa-search"></i>
    </button>
    <div class="venus-search-header-inner dropdown-menu" aria-labelledby="search-header">
        <form role="search" class="venus-search-form position-relative" action="<?php echo home_url('/'); ?>"
              method="get">
            <input name="s" id="s" value="<?php echo get_search_query() ?>" type="text" placeholder="<?php echo $venus_options['text_box_search_main']; ?>" class="border-0 w-100 shadow-none text-color-search-header" autocomplete="off">
            <button class="search-submit-header">
                <i class="fal fa-search"></i>
            </button>
        </form>
    </div>
</div>
