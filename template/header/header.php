<?php
global $venus_options;

if (isset($venus_options['logo']) && strlen($venus_options['logo']['url']) > 0) {
    $logo_href = $venus_options['logo']['url'];
} else {
    $logo_href = get_template_directory_uri() . '/images/logo_header.png';
}
if (isset($venus_options['logo_mobile_image']) && strlen($venus_options['logo_mobile_image']['url']) > 0) {
    $logo_phone_href = $venus_options['logo_mobile_image']['url'];
}
if (!current_user_can('administrator')) {
    if ($venus_options['switch_maintenance']) {
        get_template_part('maintenance-page');
        die();
    }
}

?>
<div class="header-color main-header position-relative <?php if ($venus_options['header_sticky_menu'] == true) : echo "venus-header-fixed"; endif; ?>">
    <!-- Start Header -->
    <header class="container">
        <div class="row mx-0 py-3">
            <?php if ($venus_options['header_logo_mood']):
                $ve_view = 'col-md-7';
                ?>
                <div class="logo d-flex ml-auto my-auto">
                    <a href="<?php echo site_url() ?>">
                        <img src="<?php echo $logo_href; ?>" alt="<?php bloginfo('name'); ?>">
                    </a>
                </div>
            <?php else:
                $ve_view = 'col-md-9';
            endif; ?>
            <div class="col-xs-8 col-sm-8 <?php echo $ve_view; ?> navigation text-right">
                <?php if (has_nav_menu('top_menu')): ?>
                    <?php wp_nav_menu(array('theme_location' => 'top_menu')) ?>
                <?php endif; ?>
            </div>
            <div class="col-xs-9 col-sm-12 col-md-3 register register-header pt-1 px-0">
                <?php
                if ($venus_options['show_account_user']):
                    get_template_part('template/header-login');
                endif;
                ?>
                <!-- Start -->
                <?php if (isset($venus_options['show_cart']) && $venus_options['show_cart']) { ?>
                    <div class="hwp-mini-cart-area cart">
                        <i class="fal fa-shopping-cart count-number-cart">
                            <span class="number"><?php echo is_object(WC()->cart) ? WC()->cart->get_cart_contents_count() : ''; ?></span>
                        </i>
                        <div class="cart-items">
                            <h3>سبد خرید</h3>
                            <div class="widget_shopping_cart_content"><?php woocommerce_mini_cart(); ?></div>
                        </div>
                    </div>
                    <?php
                } ?>
                <!-- End -->
                <?php
                if (isset($venus_options['show_search']) && $venus_options['show_search']) {
                    if ($venus_options['style_main_search_header'] == 'default') {
                        get_search_form();
                    } else {
                        get_template_part('template/search');
                    }
                } ?>
            </div>
            <!-- Start -->
            <?php if (isset($venus_options['show_cart']) && $venus_options['show_cart']) { ?>
                <div class="hwp-mini-cart-area ml-3 d-xl-none cart">
                    <i class="fal fa-shopping-cart count-number-cart">
                        <span class="number"><?php echo is_object(WC()->cart) ? WC()->cart->get_cart_contents_count() : ''; ?></span>
                    </i>
                    <div class="cart-items">
                        <h3>سبد خرید</h3>
                        <div class="widget_shopping_cart_content"><?php woocommerce_mini_cart(); ?></div>
                    </div>
                </div>
                <?php
            } ?>
            <!-- End -->
            <div class="ve-menu-icon">
                <a id="right-menu" href="#sidr-r" class="rwd-menu"></a>
            </div>
        </div>
        <div id="sidr-r" class="d-none menu-mob">
            <!-- Your content -->
            <a href="<?php echo home_url(); ?>" class="logo">
                <div class="logo-image"></div>
                <div class="logo-details">
                    <?php if (isset($venus_options['mobile_menu_title']) && $venus_options['mobile_menu_title']) { ?>
                        <img src="<?php echo $logo_phone_href; ?>" alt="<?php bloginfo('name'); ?>">
                    <?php } else { ?>
                        <div class="logo-title"><?php bloginfo('name'); ?></div>
                    <?php } ?>
                    <div class="logo-description"><?php bloginfo('description'); ?></div>
                </div>
            </a>
            <?php if (isset($venus_options['show_search']) && $venus_options['show_search']) { ?>
                <div class="search-mob">
                    <form role="search" method="get" class="search-form search-open d-flex text-left"
                          action="<?php echo home_url('/'); ?>">
                        <input type="search" class="search-field vstr_isHover"
                               placeholder="<?php echo $venus_options['text_box_search_main']; ?>"
                               value="<?php echo get_search_query() ?>" name="s">
                        <button class="btn">
                            <i class="fal fa-search" style="font-size: 24px"></i>
                        </button>
                    </form>
                </div>
            <?php } ?>
            <?php if ((isset($venus_options['menu_phone_color_bg_btn_main'])) && strlen($venus_options['menu_phone_color_bg_btn_main']) > 0) : ?>
                <style>
                    .sidr .mobile-menu-login a:hover, .sidr .mobile-menu-login a:focus, .sidr .mobile-menu-login a:active {
                        background-color: <?php echo $venus_options['menu_phone_color_bg_btn_main']; ?> !important;
                        box-shadow: 0 2px 12px <?php echo $venus_options['menu_phone_color_bg_btn_main']; ?>99;

                    }

                    .sidr .mobile-menu-login a {
                        box-shadow: 0 2px 12px <?php echo $venus_options['menu_phone_color_bg_btn_main']; ?>99;
                    }
                </style>
            <?php endif; ?>
            <?php if (isset($venus_options['mobile_menu']) && $venus_options['mobile_menu']) { ?>
                <?php if (has_nav_menu('mobile_menu')): ?>
                    <div class="nav-container-mob">
                        <?php wp_nav_menu(array('theme_location' => 'mobile_menu')) ?>
                    </div>
                <?php endif; ?>
            <?php } else { ?>
                <?php if (has_nav_menu('top_menu')): ?>
                    <div class="nav-container-mob">
                        <?php wp_nav_menu(array('theme_location' => 'top_menu')) ?>
                    </div>
                <?php endif; ?>
            <?php } ?>
            <div class="mobile-menu-login">
                <?php if (is_user_logged_in()):

                    global $current_user;
                    $display_user_name = $current_user->user_login;
                    $display_user_ID = $current_user->ID;

                    $current_page_URL = $_SERVER["REQUEST_URI"];
                    $logout_url = wp_logout_url($current_page_URL);

                    ?>
                    <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="btn-primary login">
                        پنل کاربری
                    </a>
                <?php else: ?>
                    <?php if (isset($venus_options['modal_login']) && $venus_options['modal_login']) {
                        if (is_plugin_active('digits/digit.php')) : echo do_shortcode('[dm-modal]');
                        else : ?>
                            <a href="#" onclick="jQuery.sidr('close', 'sidr-r');return false;" class="login"
                               data-toggle="modal"
                               data-target="#popup-login"><?php echo $venus_options['login_text_panel']; ?></a>
                        <?php
                        endif;
                    } else { ?>
                        <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                           class="login"><?php echo $venus_options['login_text_panel']; ?></a>
                    <?php } ?>
                <?php endif; ?>
            </div>
        </div>
    </header>
    <!-- End Header -->
</div>

