<?php
global $venus_options;
if (is_user_logged_in()):

    global $current_user;
    $display_user_name = $current_user->user_login;
    $display_user_ID = $current_user->ID;

    $downloads = 'downloads';
    $logout = 'customer-logout';
    $account_link = get_permalink(get_option('woocommerce_myaccount_page_id'));

    $current_page_URL = $_SERVER["REQUEST_URI"];
    $logout_url = wp_logout_url($current_page_URL);

    if ($venus_options['style_btn_profile_header'] == 'default') { ?>
        <a href="<?php echo site_url() . '/my-account' ?>" class="btn btn-primary user">
            <?php echo $venus_options['text_panel_header']; ?>
        </a>
    <?php } else { ?>
        <div class="dropdown">
            <button class="header-profile-btn border-0 shadow-none user" type="button" id="dropdownMenuBtnProfile"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php global $current_user;
                echo get_avatar($current_user->ID, 25); ?>
                <span class="my-auto"><?php echo $venus_options['text_panel_header']; ?></span>
                <i class="far fa-chevron-down"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-btn-profile" aria-labelledby="dropdownMenuBtnProfile">
                <?php if ($venus_options['modal_logged_menu_link']): ?>
                    <ul>
                        <li>
                            <a href="<?php echo esc_url($account_link); ?>" class="">پنل کاربری</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url(wc_get_account_endpoint_url($downloads)); ?>">دانلودها</a>
                        </li>
                        <li class="log-out">
                            <a href="<?php echo esc_url(wc_get_account_endpoint_url($logout)); ?>">
                                <i class="far fa-sign-out"></i> خروج از حساب
                            </a>
                        </li>

                    </ul>
                <?php else: ?>
                    <ul>
                        <?php
                        $args = array(
                            'menu_id' => 'z-header-menu-user',
                            'menu' => $venus_options['menu_panel_header'],
                        );
                        wp_nav_menu($args);
                        ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    <?php } ?>
<?php else: ?>
    <!-- Start Form Login & Register -->
    <?php if (isset($venus_options['modal_login']) && $venus_options['modal_login']) { ?>
        <?php if (is_plugin_active('digits/digit.php')) : echo do_shortcode('[dm-modal]'); else : ?>
            <button type="button" class="btn btn-primary user" data-toggle="modal" data-target="#popup-login">
                <?php echo $venus_options['login_text_panel']; ?>
            </button>
        <?php endif;
    } else { ?>
        <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
           class="btn btn-primary user"><?php echo $venus_options['login_text_panel']; ?></a>
    <?php } ?>
    <!-- End Form Login & Register -->
<?php endif; ?>
