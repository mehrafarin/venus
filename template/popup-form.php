<?php
/**
 * Login From Modal Template File
 */

$account_link = get_permalink(get_option('woocommerce_myaccount_page_id'));
$redirect = false;
?>
<!-- Modal -->
<div class="modal fade" id="popup-login" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered container" role="document">
            <div class="modal-content">
                <div class="modal-header mx-3 px-0 pb-2 border-bottom d-block">
                    <div class="nav nav-tabs border-0 tab-ajax-venus" id="nav-tab" role="tablist">
                        <a class="pop-head w-100 d-flex justify-content-between nav-item nav-link active"
                           id="nav-login-tab"
                           data-toggle="tab" href="#nav-login" role="tab" aria-controls="nav-login"
                           aria-selected="true"><span class="text-dark">قبلا عضو شده اید؟</span>ورود به سایت</a>
                        <a class="pop-head w-100 d-flex justify-content-between nav-item nav-link" id="nav-profile-tab"
                           data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"
                           aria-selected="false"><span class="text-dark">کاربر جدید هستید؟</span>ثبت نام در سایت</a>
                    </div>
                    <button type="button" class="close position-absolute" data-dismiss="modal" aria-label="Close" style="top: 5px; right: 10px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 pop-box-right">
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-login" role="tabpanel"
                                     aria-labelledby="nav-login-tab">
                                    <form method="post" class="login form-login"
                                          action="<?php echo esc_url($account_link); ?>">

                                        <?php do_action('woocommerce_login_form_start'); ?>
                                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            <label for="username"><?php esc_html_e('Username or email address', 'woocommerce'); ?>
                                                &nbsp;<span class="required">*</span></label>
                                            <input type="text"
                                                   class="woocommerce-Input woocommerce-Input--text input-text"
                                                   name="username"
                                                   id="username" autocomplete="username"
                                                   value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                                        </p>
                                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            <label for="password"><?php esc_html_e('Password', 'woocommerce'); ?>
                                                &nbsp;<span class="required">*</span></label>
                                            <input class="woocommerce-Input woocommerce-Input--text input-text"
                                                   type="password" name="password" id="password"
                                                   autocomplete="current-password"/>
                                        </p>

                                        <?php do_action('woocommerce_login_form'); ?>

                                        <p class="mt-2 mb-0">
                                            <?php wp_nonce_field('woocommerce-login'); ?>
                                            <?php if ($redirect): ?>
                                                <input type="hidden" name="redirect"
                                                       value="<?php echo esc_url($redirect) ?>"/>
                                            <?php endif ?>
                                            <input type="submit" class="submit_button btn w-100 text-center"
                                                   name="login" value="<?php esc_attr_e('ورود', 'venus'); ?>"/>
                                        </p>
                                        <div>
                                            <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"
                                               class="text-link d-inline-block pr-3 mt-3"><?php esc_html_e('رمز عبور را فراموش کرده اید؟', 'venus'); ?></a>
                                        </div>

                                    </form>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">
                                    <form method="post"
                                          class="woocommerce-form woocommerce-form-register register-wc-venus m-0 p-0 border-0"
                                          action="<?php echo esc_url($account_link); ?>">

                                        <?php do_action('woocommerce_register_form_start'); ?>

                                        <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="reg_username"><?php esc_html_e('Username', 'woocommerce'); ?>
                                                    &nbsp;<span class="required">*</span></label>
                                                <input type="text"
                                                       class="woocommerce-Input woocommerce-Input--text input-text"
                                                       name="username" id="reg_username" autocomplete="username"
                                                       value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                                            </p>

                                        <?php endif; ?>

                                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            <label for="reg_email"><?php esc_html_e('Email address', 'woocommerce'); ?>
                                                &nbsp;<span
                                                        class="required">*</span></label>
                                            <input type="email"
                                                   class="woocommerce-Input woocommerce-Input--text input-text"
                                                   name="email"
                                                   id="reg_email" autocomplete="email"
                                                   value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                                        </p>

                                        <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="reg_password"><?php esc_html_e('Password', 'woocommerce'); ?>
                                                    &nbsp;<span class="required">*</span></label>
                                                <input type="password"
                                                       class="woocommerce-Input woocommerce-Input--text input-text"
                                                       name="password" id="reg_password" autocomplete="new-password"/>
                                            </p>

                                        <?php else : ?>

                                            <p><?php esc_html_e('A password will be sent to your email address.', 'woocommerce'); ?></p>

                                        <?php endif; ?>

                                        <?php do_action('woocommerce_register_form'); ?>

                                        <p class="woocommerce-FormRow form-row">
                                            <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                                            <button type="submit" class="submit_button btn w-100 mt-2 text-center"
                                                    name="register"
                                                    value="<?php esc_attr_e('Register', 'woocommerce'); ?>"><?php esc_html_e('Register', 'woocommerce'); ?></button>
                                        </p>

                                        <?php do_action('woocommerce_register_form_end'); ?>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
