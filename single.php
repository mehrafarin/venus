<?php get_header();

global $venus_options;

$blog_sidebar = 'left';
$blog_title_style = 'styleone';

if (class_exists('Redux')) {
    $blog_sidebar = $venus_options['sidebar_position_single'];
    $blog_title_style = $venus_options['opt_select_style_single_post'];
}

$sidebar_position_single = isset($_GET['sidebar']) ? $_GET['sidebar'] : $blog_sidebar;

$blog_container_classes = array();
$blog_single_container_classes = array();

if ($sidebar_position_single == 'left' || $sidebar_position_single == 'right') {
    $blog_container_classes[] = 'has-sidebar';
    $blog_container_classes[] = 'd-lg-flex';
}

if ($sidebar_position_single == 'left') {
    $blog_container_classes[] = 'sidebar-left';
} elseif ($sidebar_position_single == 'right') {
    $blog_container_classes[] = 'sidebar-right';
}
if ($blog_title_style == 'styleone') {
    $blog_single_container_classes[] = 'blog-data-style-one';
};
//echo $blog_title_style;

?>

<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>
    <!-- Start Body -->
    <?php if ($blog_title_style == 'styleone'): ?>
    <div class="breadcrumbs style-one-breadcrumbs  text-center py-4 my-4 my-md-5 py-md-5">
        <?php the_breadcrumb(); ?>
    </div>
<?php endif; ?>

    <div class="container">
        <?php if ($blog_title_style == 'default'): ?>
            <div class="breadcrumbs text-center mt-5">
                <?php the_breadcrumb(); ?>
            </div>
            <div class="date-post text-center mt-4 text-muted"><?php the_time('j F Y'); ?></div>
            <h1 class="article-title mt-2"><?php the_title(); ?></h1>
        <?php endif; ?>
        <div class="blog-data-main <?php echo esc_attr(implode(' ', $blog_container_classes)); ?>">
            <div class="blog-data mt-2 mt-md-0 <?php echo esc_attr(implode(' ', $blog_single_container_classes)); ?>">
                <?php if ($blog_title_style == 'styleone'): ?>
                    <div class="title-blog-fit">
                        <div class="title-blog position-relative d-flex pt-3 justify-content-between">
                            <h1 class="article-title m-0 col pr-0 text-right"><?php the_title(); ?></h1>
                            <div class="date-post text-center text-muted">
                                <i class="fal fa-clock pl-2" style="font-size: 15px;"></i><?php the_time('j F Y'); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php get_template_part('/inc/blog/single', get_post_format()); ?>
                <div class="share-blog-link my-auto">
                    <ul class="social-box">
                        <li class="col"><a data-toggle="tooltip" data-placement="top" title="<?php _e( 'Facebook', 'venus-theme' ); ?>" href="http://www.facebook.com/sharer.php?u=<?php echo wp_get_shortlink(); ?>" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                        <li class="col"><a data-toggle="tooltip" data-placement="top" title="<?php _e( 'Twitter', 'venus-theme' ); ?>" href="http://www.twitter.com/share?url=<?php echo wp_get_shortlink(); ?>" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="col"><a data-toggle="tooltip" data-placement="top" title="<?php _e( 'Linkedin', 'venus-theme' ); ?>" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo wp_get_shortlink(); ?>" target="_blank"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                        <li class="col"><a data-toggle="tooltip" data-placement="top" title="<?php _e( 'Telegram', 'venus-theme' ); ?>" href="https://telegram.me/share/url?url=<?php echo wp_get_shortlink(); ?>" target="_blank"><i class="fab fa-telegram-plane" aria-hidden="true"></i></a></li>
                        <li class="col"><a data-toggle="tooltip" data-placement="top" title="<?php _e( 'Whatsapp', 'venus-theme' ); ?>" href="https://wa.me/?text=<?php echo wp_get_shortlink(); ?>" target="_blank"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
            <?php if ($sidebar_position_single !== 'none') : ?>
                <aside class="blog-sidebar d-none d-lg-block sticky-sidebar">
                    <div class="theiaStickySidebar">
                        <?php get_sidebar(); ?>
                    </div>
                </aside>
            <?php endif; ?>
        </div>
        <?php if (isset($venus_options['blog_single_show_related']) && $venus_options['blog_single_show_related']) : ?>
            <div class="related-posts">
                <h2><?php echo $venus_options['text_blog_single_related'] ?></h2>
                <?php
                // Default arguments
                $args = array(
                    'posts_per_page' => 3, // How many items to display
                    'post__not_in' => array(get_the_ID()), // Exclude current post
                    'no_found_rows' => true, // We don't ned pagination so this speeds up the query
                );
                // Check for current post category and add tax_query to the query arguments
                $cats = wp_get_post_terms(get_the_ID(), 'category');
                $cats_ids = array();
                foreach ($cats as $wpex_related_cat) {
                    $cats_ids[] = $wpex_related_cat->term_id;
                }
                if (!empty($cats_ids)) {
                    $args['category__in'] = $cats_ids;
                }
                // Query posts
                $wpex_query = new wp_query($args);
                // Loop through posts
                foreach ($wpex_query->posts as $post) : setup_postdata($post); ?>

                    <div class="rpost">
                        <div class="post-inner">
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <?php
                                    if (isset($venus_options['icon_blog_single_show_format_related']) && $venus_options['icon_blog_single_show_format_related']) :
                                        $format = get_post_format(get_the_ID());
                                        if ($format == 'audio') { ?>
                                            <div class="icon-format-blog d-flex align-middle">
                                                <i class="fal fa-music pr-3 m-auto" style="font-size: 75px;"></i>
                                            </div>
                                        <?php } elseif ($format == 'video') { ?>
                                            <div class="icon-format-blog d-flex align-middle">
                                                <i class="fal fa-play pl-4 m-auto" style="font-size: 75px;"></i>
                                            </div>
                                        <?php }; endif;
                                    ?>
                                    <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                         alt="<?php echo get_the_title($wpex_query->post->ID); ?>">
                                    <?php if (isset($venus_options['waves_blog_show_related']) && $venus_options['waves_blog_show_related']) : ?>
                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                             preserveAspectRatio="none" shape-rendering="auto">
                                            <defs>
                                                <path id="gentle-wave"
                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                            </defs>
                                            <g class="parallax">
                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                     fill="rgba(255,255,255,0.7"></use>
                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                     fill="rgba(255,255,255,0.5)"></use>
                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                     fill="rgba(255,255,255,0.3)"></use>
                                                <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                            </g>
                                        </svg>
                                    <?php endif; ?>
                                </figure>
                            </a>
                            <div class="post-detail">
                                <div class="category">
                                    <?php
                                    $category = get_the_category();

                                    if ($category[0]) {
                                        echo '<ul class="post-categories"><li><a href="' . get_category_link($category[0]->term_id) . '" rel="category tag">' . $category[0]->cat_name . '</a></li></ul>';
                                    }
                                    ?>
                                </div>
                                <a href="<?php the_permalink(); ?>"><span class="title m-0"><?php the_title(); ?></span></a>
                                <?php if ($venus_options['author_dis_show_relate'] == true): ?>
                                    <div class="author d-flex mt-2 mb-1">
                                        <div class="author-avatar author-avatar-blog">
                                            <?php echo get_avatar(get_the_author_meta('user_email'), '35', ''); ?>
                                        </div>
                                        <div class="author-info my-auto">
                                            <div class="author-name p-0"><?php the_author_link(); ?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php
// End loop
                endforeach;
                // Reset post data
                wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="mt-5 box-comment container" id="comments">
        <h2 class="section-title">نظرات</h2>
        <?php comments_template('', true); ?>
    </div>
    <!-- End Body -->
<?php endwhile; else : ?>
    <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
