<?php get_header();

global $venus_options;

$blog_sidebar = 'left';
$blog_syle_post = 'style_post_one';

if (class_exists('Redux')) {
    $blog_sidebar = $venus_options['sidebar_position'];
    $blog_syle_post = $venus_options['opt_select_style_category_show_post'];
}

$sidebar_position_single = isset($_GET['sidebar']) ? $_GET['sidebar'] : $blog_sidebar;

$blog_container_classes = array();

if ($sidebar_position_single == 'left' || $sidebar_position_single == 'right') {
    $blog_container_classes[] = 'has-sidebar';
    $blog_container_classes[] = 'd-flex';
}

if ($sidebar_position_single == 'left') {
    $blog_container_classes[] = 'sidebar-left';
} elseif ($sidebar_position_single == 'right') {
    $blog_container_classes[] = 'sidebar-right';
}

?>

<!-- Start Body -->
<div class="top-cat py-4 my-4 my-md-5 py-md-4 style-one-breadcrumbs-category">
    <h1 class="page-title text-center" style="font-size: 35px"><?php single_cat_title(); ?><span></span></h1>
    <div class="breadcrumbs text-center mt-4">
        <?php the_breadcrumb(); ?>
    </div>
</div>

<div class="container category">
    <div class="blog-data-main <?php echo esc_attr(implode(' ', $blog_container_classes)); ?>">
        <div class="row h-fit blog-data border-0 shadow-none post-categories-page">
            <?php if ($blog_syle_post == "default" || empty($blog_syle_post)) { ?>
                <?php if (have_posts()) : while (have_posts()) :
                    the_post(); ?>
                    <div class="col-md-4 category-post-style">
                        <a href="<?php the_permalink(); ?>">
                            <figure>
                                <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                     alt="<?php echo the_title(); ?>">
                            </figure>
                            <div class="blog-posts-inner">
                                <h2><?php the_title(); ?></h2>
                            </div>
                        </a>
                    </div>
                <?php endwhile; else : ?>
                    <p class="my-5"><?php esc_html_e('متاسفانه محتوایی یافت نشد'); ?></p>
                <?php endif;
                wp_reset_postdata(); ?>
            <?php } elseif ($blog_syle_post == "style_post_one") { ?>
                <?php if (have_posts()) : while (have_posts()) :
                    the_post(); ?>
                    <div class="col-md-4 px-1 category-post-style category-post-style-one">
                        <a href="<?php the_permalink(); ?>">
                            <figure class="position-relative shadow-none rounded-0">
                                <?php
                                if (isset($venus_options['icon_blog_all_show_format']) && $venus_options['icon_blog_all_show_format']) :
                                    $format = get_post_format(get_the_ID());
                                    if ($format == 'audio') { ?>
                                        <div class="icon-format-blog d-flex align-middle">
                                            <i class="fal fa-music pr-3 m-auto" style="font-size: 75px;"></i>
                                        </div>
                                    <?php } elseif ($format == 'video') { ?>
                                        <div class="icon-format-blog d-flex align-middle">
                                            <i class="fal fa-play pl-4 m-auto" style="font-size: 75px;"></i>
                                        </div>
                                    <?php }; endif;
                                ?>
                                <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                     alt="<?php echo the_title(); ?>">
                                <?php if (isset($venus_options['waves_blog_all_show']) && $venus_options['waves_blog_all_show']) : ?>
                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                         preserveAspectRatio="none" shape-rendering="auto">
                                        <defs>
                                            <path id="gentle-wave"
                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                        </defs>
                                        <g class="parallax">
                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                 fill="rgba(255,255,255,0.7"></use>
                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                 fill="rgba(255,255,255,0.5)"></use>
                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                 fill="rgba(255,255,255,0.3)"></use>
                                            <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                        </g>
                                    </svg>
                                <?php endif; ?>
                            </figure>
                            <div class="blog-posts-inner m-0">
                                <h2><?php the_title(); ?></h2>
                            </div>
                        </a>
                    </div>
                <?php endwhile; else : ?>
                    <p class="my-5"><?php esc_html_e('متاسفانه محتوایی یافت نشد'); ?></p>
                <?php endif;
                wp_reset_postdata(); ?>
            <?php } ?>
            <div class="page-numbers d-block w-100">
                <?php the_posts_pagination(array('mid_size' => 5, 'screen_reader_text' => __('&nbsp;'), 'prev_text' => __('→', 'textdomain'),
                    'next_text' => __('←', 'textdomain'),
                )); ?>
            </div>
        </div>
        <?php if ($sidebar_position_single !== 'none') : ?>
            <aside class="blog-sidebar d-none d-lg-block sticky-sidebar">
                <div class="theiaStickySidebar">
                    <?php get_sidebar(); ?>
                </div>
            </aside>
        <?php endif; ?>
    </div>

</div>
<!-- End Body -->

<?php get_footer(); ?>
