<?php global $venus_options; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="bigersoft">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
global $venus_options;
$header = $venus_options['select_header'];
switch ($header) {
    case 'venus_default_header':
        get_template_part('template/header/header');
        break;
    case 'no_header':
        break;
    default:
        $headerPost = $header;
        if ($headerPost != null) {
            echo \Elementor\Plugin::$instance->frontend->get_builder_content($headerPost);
        }
}
?>
