<?php get_header();
global $venus_options;
$result_search_style = 'style_result_search_one';

if (class_exists('Redux')) {
    $result_search_style = $venus_options['opt_select_style__show_result_search'];
}
?>

<!-- Start Body -->
<div class="container category mt-5 pt-3">
    <?php if(!empty(get_search_query())){ ?>
        <h1 class="title-page-shop pb-3 text-center"> جستجو برای : <?php the_search_query(); ?><span></span></h1>
    <div class="row post-categories-page">
        <?php
        if ($result_search_style == "default" || empty($result_search_style)) {
        if (have_posts()) : while (have_posts()) :the_post(); ?>
            <div class="col-md-4 category-post-style">
                <a href="<?php the_permalink(); ?>">
                    <figure>
                        <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                             alt="<?php echo the_title(); ?>">
                    </figure>
                    <div class="blog-posts-inner">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </a>
            </div>
        <?php endwhile; else : ?>
            <p class="d-block w-100 title-page-shop page-title text-center"><?php esc_html_e('متاسفانه ، محتوایی یافت نشد'); ?></p>
        <?php endif; ?>
        <?php } elseif ($result_search_style == 'style_result_search_one'){
            if (have_posts()) : while (have_posts()) :the_post(); ?>
                <div class="col-md-4 px-1 category-post-style category-post-style-one">
                    <a href="<?php the_permalink(); ?>">
                        <figure class="position-relative shadow-none rounded-0">
                            <?php
                            if (isset($venus_options['icon_search_all_show_format']) && $venus_options['icon_search_all_show_format']) :
                                $format = get_post_format(get_the_ID());
                                if ($format == 'audio') { ?>
                                    <div class="icon-format-blog d-flex align-middle">
                                        <i class="fal fa-music pr-3 m-auto" style="font-size: 75px;"></i>
                                    </div>
                                <?php } elseif ($format == 'video') { ?>
                                    <div class="icon-format-blog d-flex align-middle">
                                        <i class="fal fa-play pl-4 m-auto" style="font-size: 75px;"></i>
                                    </div>
                                <?php }; endif;
                            ?>
                            <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                 alt="<?php echo the_title(); ?>">
                            <?php if (isset($venus_options['waves_search_all_show']) && $venus_options['waves_search_all_show']) : ?>
                                <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                     preserveAspectRatio="none" shape-rendering="auto">
                                    <defs>
                                        <path id="gentle-wave"
                                              d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                    </defs>
                                    <g class="parallax">
                                        <use xlink:href="#gentle-wave" x="-12" y="11"
                                             fill="rgba(255,255,255,0.7"></use>
                                        <use xlink:href="#gentle-wave" x="25" y="13"
                                             fill="rgba(255,255,255,0.5)"></use>
                                        <use xlink:href="#gentle-wave" x="100" y="5"
                                             fill="rgba(255,255,255,0.3)"></use>
                                        <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                    </g>
                                </svg>
                            <?php endif; ?>
                        </figure>
                        <div class="blog-posts-inner m-0">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </a>
                </div>
            <?php endwhile; else : ?>
            <p class="d-block w-100 title-page-shop page-title text-center"><?php esc_html_e('متاسفانه ، محتوایی یافت نشد'); ?></p>
            <?php endif; ?>
        <?php } ?>
    </div>
    <div class="page-numbers">
        <?php the_posts_pagination(array('mid_size' => 5, 'screen_reader_text' => __('&nbsp;'),'prev_text' => __( '→', 'textdomain' ),
            'next_text' => __( '←', 'textdomain' ),
        )); ?>
    </div>
    <?php }else{ ?>
        <p class="d-block title-page-shop page-title text-center my-5 py-5">متاسفیم، موضوعی جهت جستجو یافت نشد.</p>
    <?php } ?>
</div>
<!-- End Body -->


<?php get_footer(); ?>
