<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Header_Eliges_Compare extends Widget_Base
{

    public function get_name()
    {
        return 'eliges-compare';
    }

    public function get_title()
    {
        return __('Eliges Compare', 'eliges-theme');
    }

    public function get_icon()
    {
        return 'eicon-cart';
    }

    public function get_categories()
    {
        return ['Eliges-Header'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Wishlist', 'eliges-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_responsive_control(
            'eliges_compare_alignment',
            [
                'label' => esc_html__('Alignment', 'eliges-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'eliges-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'eliges-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'eliges-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .el-compare' => 'text-align: {{VALUE}};'
                ],
                'default' => 'left',
            ]
        );
        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        ?>
        <?php
        if (defined('YITH_WOOCOMPARE')) {
            global $yith_woocompare;
            ?>
            <div class="el-compare">
                <div class="love-product yith-woocompare-counter" data-type="text" data-text_o="{{count}}">
                    <a class="yith-woocompare-open d-flex h-100"
                       href="<?php if ($yith_woocompare == ''): echo esc_url($yith_woocompare->obj->view_table_url()); endif;?>">
                        <i class="fal fa-random d-block m-auto"></i>
                        <span class="wishlist-num align-middle yith-woocompare-count"></span>
                    </a>
                </div>
            </div>
        <?php } else{
            echo esc_html__('Compare has disable', 'eliges-theme');
        }
    }

}
