<?php

namespace Elementor;

class Header_Venus_Menu_Phone extends Widget_Base
{

    public function get_name()
    {
        return 'venus-menu-phone';
    }

    public function get_title()
    {
        return __('منو موبایل ونوس', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-nav-menu';
    }

    public function get_categories()
    {
        return ['Venus-Header'];
    }

    public function get_menus()
    {
        $list = [];
        $menus = wp_get_nav_menus();
        foreach ($menus as $menu) {
            $list[$menu->slug] = $menu->name;
        }

        return $list;
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_main_menu',
            [
                'label' => __('منو موبایل ونوس', 'venus-theme'),
            ]
        );

        $this->add_control(
            'digikala_nav_menu',
            [
                'label' => esc_html__('انتخاب منو', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'options' => $this->get_menus(),
            ]
        );

        $this->add_control(
            'images_or_title',
            [
                'label' => __('استفاده از عکس به جای عنوان سایت', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus-theme'),
                'label_off' => __('غیرفعال', 'venus-theme'),
                'description' => __('در صورت فعال سازی این گزینه، به جای عنوان سایت ، عکس مورد نظر نمایش داده می شود.', 'venus-theme'),
            ]
        );

        $this->add_control(
            'search_menu_phone',
            [
                'label' => __('سرچ در منو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus-theme'),
                'label_off' => __('غیرفعال', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'img_site',
            [
                'label' => __('تصویر لوگو', 'venus-theme'),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'text_login',
            [
                'label' => __('متن دکمه ورود و ثبت نام', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'ورود و ثبت نام',
            ]
        );

        $this->add_control(
            'text_panel',
            [
                'label' => __('متن دکمه پنل کاربری', 'venus-theme'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'پنل کاربری',
            ]
        );

        $this->add_control(
            'text_search',
            [
                'label' => __('متن جستجو', 'venus-theme'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'جستجو...',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style',
            [
                'label' => __('منو موبایل', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => __('تایپوگرافی منو اصلی', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .venus-phone-menu i',
            ]
        );

        $this->add_control(
            'color_menu_item',
            [
                'label' => __('رنگ آیکون منو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .venus-phone-menu i ' => 'color: {{VALUE}}',
                ],
                'default' => '#000'
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_top_menu_phone',
                'label' => __('رنگ پس زمینه باکس عنوان سایت', 'venus-theme'),
                'types' => ['classic', 'gradient', 'video'],
                'selector' => '{{WRAPPER}} .sidr .logo',
            ]
        );

        $this->add_control(
            'color_title_site',
            [
                'label' => __('رنگ متن عنوان سایت', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .sidr .logo .logo-details .logo-title ' => 'color: {{VALUE}}',
                ],
                'default' => '#fff'
            ]
        );

        $this->add_control(
            'color_des_title_site',
            [
                'label' => __('رنگ معرفی کوتاه', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .sidr .logo .logo-details .logo-description' => 'color: {{VALUE}}',
                ],
                'default' => '#fff'
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        ?>
        <a id="right-menu" href="#sidr-r" class="venus-phone-menu d-flex">
            <i class="fal fa-bars"></i>
        </a>
        <div id="sidr-r" class="d-none menu-mob">
            <!-- Your content -->
            <a href="<?php echo home_url(); ?>" class="logo">
                <div class="logo-image"></div>
                <div class="logo-details">
                    <?php if ($settings['images_or_title']) { ?>
                        <img src="<?php echo $settings['img_site']['url']; ?>" alt="<?php bloginfo('name'); ?>">
                    <?php } else { ?>
                        <div class="logo-title"><?php bloginfo('name'); ?></div>
                    <?php } ?>
                    <div class="logo-description"><?php bloginfo('description'); ?></div>
                </div>
            </a>
            <?php if ($settings['search_menu_phone']) : ?>
                <div class="search-mob">
                    <form role="search" method="get" class="search-form search-open d-flex text-left"
                          action="<?php echo home_url('/'); ?>">
                        <input type="search" class="search-field vstr_isHover"
                               placeholder="<?php echo $settings['text_search']; ?>" value="<?php echo get_search_query() ?>" name="s">
                        <button class="btn">
                            <i class="fal fa-search" style="font-size: 24px"></i>
                        </button>
                    </form>
                </div>
            <?php endif; ?>
            <div class="nav-container-mob">
                <?php
                $args = array(
                    'menu' => $settings['digikala_nav_menu'],
                );
                wp_nav_menu($args);
                ?>
            </div>
            <div class="mobile-menu-login">
                <?php if (is_user_logged_in()):

                    global $current_user;
                    $display_user_name = $current_user->user_login;
                    $display_user_ID = $current_user->ID;

                    $current_page_URL = $_SERVER["REQUEST_URI"];
                    $logout_url = wp_logout_url($current_page_URL);

                    ?>
                    <a href="<?php echo site_url() . '/my-account' ?>" class="btn-primary login">
                        <?php echo $settings['text_panel']; ?>
                    </a>
                <?php else: ?>
                    <?php if (isset($venus_options['modal_login']) && $venus_options['modal_login']) { ?>
                        <a href="#" onclick="jQuery.sidr('close', 'sidr-r');return false;" class="login"
                           data-toggle="modal"
                           data-target="#popup-login"><?php echo $settings['text_login']; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                           class="login"><?php echo $settings['text_login']; ?></a>
                    <?php } ?>
                <?php endif; ?>
            </div>
        </div>
        <?php
    }
}
