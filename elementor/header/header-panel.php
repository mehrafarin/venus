<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Header_Venus_Panel extends Widget_Base
{

    public function get_name()
    {
        return 'venus-panel';
    }

    public function get_title()
    {
        return __('پنل کاربری ونوس', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-lock-user';
    }

    public function get_categories()
    {
        return ['Venus-Header'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('پنل کاربری', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'select_style_panel',
            [
                'label' => __('انتخاب استایل پنل کاربری', 'venus'),
                'type' => Controls_Manager::SELECT,
                'default' => 'default',
                'options' => [
                    'default' => __('پیش فرض', 'venus-theme'),
                    'version_one' => __('ورژن 1', 'venus-theme'),
                ],
            ]
        );

        $this->add_control(
            'popup_login',
            [
                'label' => __('ورود پاپ آپ', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus-theme'),
                'label_off' => __('غیرفعال', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'title_panel',
            [
                'label' => __('متن پنل کاربری', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('پنل کاربری', 'venus-theme'),
            ]
        );

        $this->add_control(
            'title_login',
            [
                'label' => __('متن ورود و ثبت نام', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('ورود و ثبت نام', 'venus-theme'),
            ]
        );

        $this->add_responsive_control(
            'btn_panel_alignment',
            [
                'label' => esc_html__('موقعیت', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .holder-panel-main' => 'text-align: {{VALUE}};'
                ],
                'default' => 'left',
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'style_section',
            [
                'label' => __('استایل پنل کاربری', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_btn_panel',
                'label' => __('بکگراند دکمه پنل کاربری', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .user',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'box_shadow_btn_panel',
                'label' => __('سایه دکمه پنل کاربری', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .user',
            ]
        );

        $this->add_control(
            'color_text_btn_panel',
            [
                'label' => __('رنگ متن پنل کاربری', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .user' => 'color: {{VALUE}}',
                ],
                'default' => '#000',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 't_btn_panel',
                'label' => __('تایپوگرافی متن پنل کاربری', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .user',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        global $venus_options;
        echo '<div class="holder-panel-main">';
        if (is_user_logged_in()):

            global $current_user;
            $display_user_name = $current_user->user_login;
            $display_user_ID = $current_user->ID;

            $downloads = 'downloads';
            $logout = 'customer-logout';
            $account_link = get_permalink(get_option('woocommerce_myaccount_page_id'));

            $current_page_URL = $_SERVER["REQUEST_URI"];
            $logout_url = wp_logout_url($current_page_URL);
            switch ($settings['select_style_panel']) {
                case "default": ?>
                    <a href="<?php echo site_url() . '/my-account' ?>" class="btn d-inline-block user float-none">
                        <?php echo $settings['title_panel']; ?>
                    </a>
                    <?php
                    break;
                case "version_one": ?>
                    <div class="dropdown">
                        <button class="header-profile-btn border-0 shadow-none user float-none" type="button"
                                id="dropdownMenuBtnProfile" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <?php global $current_user;
                            echo get_avatar($current_user->ID, 25); ?>
                            <span class="my-auto"><?php echo $settings['title_panel']; ?></span>
                            <i class="far fa-chevron-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-btn-profile" aria-labelledby="dropdownMenuBtnProfile">
                            <ul>
                                <li>
                                    <a href="<?php echo esc_url($account_link); ?>" class="">پنل کاربری</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(wc_get_account_endpoint_url($downloads)); ?>">دانلودها</a>
                                </li>
                                <li class="log-out">
                                    <a href="<?php echo esc_url(wc_get_account_endpoint_url($logout)); ?>"> <i
                                                class="far fa-sign-out"></i> خروج از حساب
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <?php
                    break;
                default:
                    echo "Your favorite color is neither red, blue, nor green!";
            }
        else: ?>
            <!-- Start Form Login & Register -->
            <?php if ($settings['popup_login']) { ?>
                <button type="button" class="btn user" data-toggle="modal" data-target="#popup-login">
                    <?php echo $settings['title_login']; ?>
                </button>
            <?php } else { ?>
                <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="btn user"><?php echo $settings['title_login']; ?></a>
            <?php } ?>
            <!-- End Form Login & Register -->
        <?php endif; ?>
        <?php
        echo '</div>';
    }

}
