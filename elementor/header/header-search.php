<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Header_Venus_Search extends Widget_Base
{

    public function get_name()
    {
        return 'venus-search';
    }

    public function get_title()
    {
        return __('سرچ ونوس', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-site-search';
    }

    public function get_categories()
    {
        return ['Venus-Header'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('سرچ هدر', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'select_style_search',
            [
                'label' => __('انتخاب استایل سرچ', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'default' => 'default',
                'options' => [
                    'default' => __('پیش فرض', 'venus-theme'),
                    'version_one' => __('ورژن 1', 'venus-theme'),
                ],
            ]
        );

        $this->add_control(
            'title_search',
            [
                'label' => __('متن جستجو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('جستجو', 'venus-theme'),
            ]
        );

        $this->add_responsive_control(
            'venus_search_alignment',
            [
                'label' => esc_html__('موقعیت', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .search-header-holder' => 'text-align: {{VALUE}};'
                ],
                'default' => 'left',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'search_style',
            [
                'label' => __('استایل', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'style_text_one',
                'label' => __('متن جستجو', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .text-color-search-header',
            ]
        );

        $this->add_control(
            'color_text_search',
            [
                'label' => __('رنگ متن جستجو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .text-color-search-header , .text-color-search-header::placeholder' => 'color: {{VALUE}}',
                ],
                'default' => '#000'
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_search',
                'label' => __('بکگراند باکس سرچ', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .venus-search-header-inner',
            ]
        );

        $this->add_control(
            've_color_icon_search',
            [
                'label' => __('رنگ آیکون جستجو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .color-btn-icon-search-header' => 'color: {{VALUE}}',
                ],
                'default' => '#000000'
            ]
        );

        $this->add_control(
            'search_icon_size',
            [
                'label' => __('سایز آیکون جستجو', 'venus-theme'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 30,
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .color-btn-icon-search-header' => 'font-size:{{SIZE}}px',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        switch ($settings['select_style_search']) {
            case "default": ?>
                <div class="search-header-holder">
                    <div class="search search-box float-none d-inline-block m-0">
                        <form method="get" class="text-left" action="<?php echo home_url('/'); ?>">
                            <input type="search" class="text-color-search-header"
                                   placeholder="<?php echo $settings['title_search']; ?>"
                                   value="<?php echo get_search_query() ?>" name="s">
                            <button class="btn pt-1 pl-0">
                                <i class="fal fa-search color-btn-icon-search-header"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <?php
                break;
            case "version_one": ?>
                <div class="search-header-holder">
                    <div class="dropdown ve--search float-none d-inline-block m-0">
                        <button class="venus-search-header" data-display="static" data-toggle="dropdown" id="search-header" role="button" aria-expanded="false">
                            <i class="color-btn-icon-search-header pt-1 fal fa-search"></i>
                        </button>
                        <div class="venus-search-header-inner dropdown-menu" aria-labelledby="search-header">
                            <form role="search" class="venus-search-form position-relative"
                                  action="<?php echo home_url('/'); ?>" method="get">
                                <input name="s" id="s" value="<?php echo get_search_query() ?>" type="text" placeholder="<?php echo $settings['title_search']; ?>" class="border-0 w-100 shadow-none text-color-search-header" autocomplete="off">
                                <button class="search-submit-header">
                                    <i class="fal fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
                break;
            default:
                echo "Your favorite color is neither red, blue, nor green!";
        }
    }
}
