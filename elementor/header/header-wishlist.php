<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Header_Eliges_Wishlist extends Widget_Base
{

    public function get_name()
    {
        return 'eliges-wishlist';
    }

    public function get_title()
    {
        return __('Eliges Wishlist', 'eliges-theme');
    }

    public function get_icon()
    {
        return 'eicon-favorite';
    }

    public function get_categories()
    {
        return ['Eliges-Header'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Wishlist', 'eliges-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

//        $this->add_control(
//            'eliges_woo_mini_cart_icon',
//            [
//                'label' => __('Icon', 'eliges-theme'),
//                'type' => \Elementor\Controls_Manager::ICONS,
//                'default' => [
//                    'value' => 'fas fa-shopping-bag',
//                    'library' => 'solid',
//                ],
//            ]
//        );
        $this->add_responsive_control(
            'eliges_wishlist_alignment',
            [
                'label' => esc_html__('Alignment', 'eliges-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'eliges-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'eliges-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'eliges-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .el-love-product' => 'text-align: {{VALUE}};'
                ],
                'default' => 'left',
            ]
        );
        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        ?>
        <?php if (defined('YITH_WCWL')) { ?>
        <div class="el-love-product">
            <div class="love-product">
                <a href="<?php echo site_url() . '/my-account/mywishlist' ?>" class="d-flex h-100">
                    <img src="<?php echo get_template_directory_uri() ?>/images/love-head.svg" class="m-auto">
                    <span class="wishlist-num align-middle">
                            <?php echo esc_html(rooshe_get_wishlist_count());; ?>
                        </span>
                </a>
            </div>
        </div>
    <?php }
    }

}
