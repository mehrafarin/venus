<?php

namespace Elementor;

class Header_Venus_Main_Menu extends Widget_Base
{

    public function get_name()
    {
        return 'main-menu';
    }

    public function get_title()
    {
        return __('منو اصلی', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-nav-menu';
    }

    public function get_categories()
    {
        return ['Venus-Header'];
    }

    public function get_menus()
    {
        $list = [];
        $menus = wp_get_nav_menus();
        foreach ($menus as $menu) {
            $list[$menu->slug] = $menu->name;
        }

        return $list;
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_main_menu',
            [
                'label' => __('منو اصلی', 'venus-theme'),
            ]
        );

        $this->add_control(
            'venus_nav_menu',
            [
                'label' => esc_html__('انتخاب منو', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'options' => $this->get_menus(),
            ]
        );

        $this->add_control(
            'pos_menu_alignment',
            [
                'label' => __('موقعیت', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'right',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}} .el-menu-venus-main ' => 'text-align: {{VALUE}};'
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style',
            [
                'label' => __('منو اصلی', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => __('تایپوگرافی منو اصلی', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .navigation ul li a',
            ]
        );

        $this->add_control(
            'color_menu_item',
            [
                'label' => __('رنگ منو اصلی', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .navigation ul li a ' => 'color: {{VALUE}}',
                ],
                'default' => '#222'
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_submenu',
            [
                'label' => __('ساب منو', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'submenu_type',
                'label' => __('تایپوگرافی ساب منو', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .navigation ul li ul li a',
            ]
        );

        $this->add_control(
            'submenu_color',
            [
                'label' => __('رنگ ساب منو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .navigation ul li ul li a' => 'color: {{VALUE}} !important',
                ],
                'default' => '#222'
            ]
        );

        $this->add_control(
            'submenu_color_hover',
            [
                'label' => __('رنگ هاور ساب منو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .navigation ul li ul li a:hover' => 'color: {{VALUE}} !important',
                ],
                'default' => '#fff'
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'item_submenu_background',
                'label' => __('پس زمینه ساب منو', 'venus-theme'),
                'types' => ['classic', 'gradient', 'video'],
                'selector' => '{{WRAPPER}} .navigation ul li ul',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        ?>
        <div class="venus-elementor-menus navigation">
            <?php
            $args = array(
                'container_class' => 'el-menu-venus-main',
                'fallback_cb' => '',
                'menu_id' => 'header-menu',
                'menu' => $settings['venus_nav_menu'],
            );
            wp_nav_menu($args);
            ?>
        </div>
        <a id="right-menu" href="#sidr-r" class="rwd-menu"></a>
        <div id="sidr-r" class="d-none menu-mob">
            <!-- Your content -->
            <a href="<?php echo home_url(); ?>" class="logo">
                <div class="logo-image"></div>
                <div class="logo-details">
                    <?php
                    if (isset($venus_options['logo_mobile_image']) && strlen($venus_options['logo_mobile_image']['url']) > 0) {
                        $logo_phone_href = $venus_options['logo_mobile_image']['url'];
                    }
                    if (isset($venus_options['mobile_menu_title']) && $venus_options['mobile_menu_title']) { ?>
                        <img src="<?php echo $logo_phone_href; ?>" alt="<?php bloginfo('name'); ?>">
                    <?php } else { ?>
                        <div class="logo-title"><?php bloginfo('name'); ?></div>
                    <?php } ?>
                    <div class="logo-description"><?php bloginfo('description'); ?></div>
                </div>
            </a>
            <?php if (isset($venus_options['show_search']) && $venus_options['show_search']) { ?>
                <div class="search-mob">
                    <form role="search" method="get" class="search-form search-open d-flex text-left"
                          action="<?php echo home_url('/'); ?>">
                        <input type="search" class="search-field vstr_isHover"
                               placeholder="<?php echo $venus_options['text_box_search_main']; ?>"
                               value="<?php echo get_search_query() ?>" name="s">
                        <button class="btn">
                            <i class="fal fa-search" style="font-size: 24px"></i>
                        </button>
                    </form>
                </div>
            <?php } ?>
            <?php if ((isset($venus_options['menu_phone_color_bg_btn_main'])) && strlen($venus_options['menu_phone_color_bg_btn_main']) > 0) : ?>
                <style>
                    .sidr .mobile-menu-login a:hover, .sidr .mobile-menu-login a:focus, .sidr .mobile-menu-login a:active {
                        background-color: <?php echo $venus_options['menu_phone_color_bg_btn_main']; ?> !important;
                        box-shadow: 0 2px 12px <?php echo $venus_options['menu_phone_color_bg_btn_main']; ?>99;

                    }

                    .sidr .mobile-menu-login a {
                        box-shadow: 0 2px 12px <?php echo $venus_options['menu_phone_color_bg_btn_main']; ?>99;
                    }
                </style>
            <?php endif; ?>
            <?php if (isset($venus_options['mobile_menu']) && $venus_options['mobile_menu']) { ?>
                <?php if (has_nav_menu('mobile_menu')): ?>
                    <div class="nav-container-mob">
                        <?php wp_nav_menu(array('theme_location' => 'mobile_menu')) ?>
                    </div>
                <?php endif; ?>
            <?php } else { ?>
                <?php if (has_nav_menu('top_menu')): ?>
                    <div class="nav-container-mob">
                        <?php wp_nav_menu(array('theme_location' => 'top_menu')) ?>
                    </div>
                <?php endif; ?>
            <?php } ?>
            <div class="mobile-menu-login">
                <?php if (is_user_logged_in()):

                    global $current_user;
                    $display_user_name = $current_user->user_login;
                    $display_user_ID = $current_user->ID;

                    $current_page_URL = $_SERVER["REQUEST_URI"];
                    $logout_url = wp_logout_url($current_page_URL);

                    ?>
                    <a href="<?php echo site_url() . '/my-account' ?>" class="btn-primary login">
                        پنل کاربری
                    </a>
                <?php else: ?>
                    <?php if (isset($venus_options['modal_login']) && $venus_options['modal_login']) { ?>
                        <a href="#" onclick="jQuery.sidr('close', 'sidr-r');return false;" class="login"
                           data-toggle="modal"
                           data-target="#popup-login"><?php echo $venus_options['login_text_panel']; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                           class="login"><?php echo $venus_options['login_text_panel']; ?></a>
                    <?php } ?>
                <?php endif; ?>
            </div>
        </div>

        <?php
    }
}
