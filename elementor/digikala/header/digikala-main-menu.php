<?php

namespace Elementor;

class Digikala_Header_Main_Menu extends Widget_Base
{

    public function get_name()
    {
        return 'digikala-main-menu';
    }

    public function get_title()
    {
        return __('منو اصلی دیجیکالا', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-nav-menu';
    }

    public function get_categories()
    {
        return ['Digikala-Header'];
    }

    public function get_menus()
    {
        $list = [];
        $menus = wp_get_nav_menus();
        foreach ($menus as $menu) {
            $list[$menu->slug] = $menu->name;
        }

        return $list;
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_main_menu',
            [
                'label' => __('منو اصلی', 'venus-theme'),
            ]
        );

        $this->add_control(
            'digikala_nav_menu',
            [
                'label' => esc_html__('انتخاب منو', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'options' => $this->get_menus(),
            ]
        );

        $this->add_control(
            'pos_menu_alignment',
            [
                'label' => __('موقعیت', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'right',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}} .el-menu-venus-main ' => 'text-align: {{VALUE}};'
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style',
            [
                'label' => __('منو اصلی', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => __('تایپوگرافی منو اصلی', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .navigation ul li a',
            ]
        );

        $this->add_control(
            'color_menu_item',
            [
                'label' => __('رنگ منو اصلی', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .navigation ul li a ' => 'color: {{VALUE}}',
                ],
                'default' => '#222'
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_submenu',
            [
                'label' => __('ساب منو', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'submenu_type',
                'label' => __('تایپوگرافی ساب منو', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .navigation ul li ul li a',
            ]
        );

        $this->add_control(
            'submenu_color',
            [
                'label' => __('رنگ ساب منو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .navigation ul li ul li a' => 'color: {{VALUE}} !important',
                ],
                'default' => '#222'
            ]
        );

        $this->add_control(
            'submenu_color_hover',
            [
                'label' => __('رنگ هاور ساب منو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .navigation ul li ul li a:hover' => 'color: {{VALUE}} !important',
                ],
                'default' => '#fff'
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'item_submenu_background',
                'label' => __('پس زمینه ساب منو', 'venus-theme'),
                'types' => ['classic', 'gradient', 'video'],
                'selector' => '{{WRAPPER}} .navigation ul li ul',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        ?>
        <div class="digikala-header-main-menu">
            <?php
            $args = array(
                'container_class' => 'digikala-holder-main-menu',
                'fallback_cb' => '',
                'menu_id' => 'header-menu',
                'menu' => $settings['digikala_nav_menu'],
            );
            wp_nav_menu($args);
            ?>
        </div>
        <?php
    }
}
