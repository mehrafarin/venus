<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Digiakal_Header_Search extends Widget_Base
{

    public function get_name()
    {
        return 'digikala-header-search';
    }

    public function get_title()
    {
        return __('سرچ دیجیکالا', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-site-search';
    }

    public function get_categories()
    {
        return ['Digikala-Header'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('سرچ هدر', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'title_search',
            [
                'label' => __('متن جستجو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('جستجو در دیجی‌کالا …', 'venus-theme'),
            ]
        );

        $this->add_responsive_control(
            'venus_search_alignment',
            [
                'label' => esc_html__('موقعیت باکس سرچ', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .search-header-holder' => 'text-align: {{VALUE}};'
                ],
                'default' => 'left',
            ]
        );

        $this->add_responsive_control(
            've_icon_search_alignment',
            [
                'label' => esc_html__('موقعیت آیکون سرچ', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .digikala-search-box form .btn' => '{{VALUE}}: 0 ;'
                ],
                'default' => 'right',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'search_style',
            [
                'label' => __('استایل', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'style_text_one',
                'label' => __('متن جستجو', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .digikala-search-box input',
            ]
        );

        $this->add_control(
            'color_text_search',
            [
                'label' => __('رنگ متن جستجو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .digikala-search-box input , .digikala-search-box input::placeholder' => 'color: {{VALUE}}',
                ],
                'default' => '#474747'
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_search',
                'label' => __('بکگراند باکس سرچ', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .digikala-search-box input',
            ]
        );

        $this->add_control(
            'color_icon_search',
            [
                'label' => __('رنگ آیکون جستجو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .digikala-search-box form .btn i' => 'color: {{VALUE}}',
                ],
                'default' => '#a1a3a8'
            ]
        );

        $this->add_control(
            'search_icon_size',
            [
                'label' => __('سایز آیکون جستجو', 'venus-theme'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 20,
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .digikala-search-box form .btn i' => 'font-size:{{SIZE}}px',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $pos_p = '';

        if ($settings['ve_icon_search_alignment'] == 'left'):
            $pos_p = 'pr-0';
        else:
            $pos_p = 'pl-0';
        endif;

        ?>
        <div class="digikala-search-header-holder">
            <div class="search digikala-search-box">
                <form method="get" class="text-left position-relative" action="<?php echo home_url('/'); ?>">
                    <input type="search" class="digikala-input-search-header"
                           placeholder="<?php echo $settings['title_search']; ?>"
                           value="<?php echo get_search_query() ?>" name="s">

                    <button class="btn pt-1 <?php echo $pos_p; ?>">
                        <i class="fal fa-search color-btn-icon-search-header"></i>
                    </button>
                </form>
            </div>
        </div>
        <?php
    }
}
