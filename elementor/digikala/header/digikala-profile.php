<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Digikala_Header_Profile extends Widget_Base
{

    public function get_name()
    {
        return 'digikala-profile';
    }

    public function get_title()
    {
        return __('پنل کاربری دیجیکالا', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-lock-user';
    }

    public function get_categories()
    {
        return ['Digikala-Header'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('پنل کاربری', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'popup_login',
            [
                'label' => __('ورود پاپ آپ', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus-theme'),
                'label_off' => __('غیرفعال', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'title_panel',
            [
                'label' => __('متن پنل کاربری', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('پنل کاربری', 'venus-theme'),
            ]
        );

        $this->add_control(
            'title_login',
            [
                'label' => __('متن ورود و ثبت نام', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('ورود و ثبت نام', 'venus-theme'),
            ]
        );

        $this->add_responsive_control(
            'btn_panel_alignment',
            [
                'label' => esc_html__('موقعیت', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .holder-panel-main' => 'text-align: {{VALUE}};'
                ],
                'default' => 'left',
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'style_section_log',
            [
                'label' => __('استایل پنل کاربری (حالت عادی)', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_btn_panel_log',
                'label' => __('بکگراند دکمه پنل کاربری', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .btn-login-digikala',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'box_shadow_btn_panel_log',
                'label' => __('سایه دکمه پنل کاربری', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .btn-login-digikala',
            ]
        );

        $this->add_control(
            'color_text_btn_panel_log',
            [
                'label' => __('رنگ متن پنل کاربری', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .btn-login-digikala' => 'color: {{VALUE}}',
                ],
                'default' => '#000',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 't_btn_panel_log',
                'label' => __('تایپوگرافی متن پنل کاربری', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .btn-login-digikala',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_section',
            [
                'label' => __('استایل پنل کاربری (حالت وارد شده)', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_btn_panel',
                'label' => __('بکگراند دکمه پنل کاربری', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .digikala-btn-profile',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'box_shadow_btn_panel',
                'label' => __('سایه دکمه پنل کاربری', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .digikala-btn-profile',
            ]
        );

        $this->add_control(
            'color_text_btn_panel',
            [
                'label' => __('رنگ متن پنل کاربری', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .digikala-btn-profile' => 'color: {{VALUE}}',
                ],
                'default' => '#000',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 't_btn_panel',
                'label' => __('تایپوگرافی متن پنل کاربری', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .digikala-btn-profile',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        global $venus_options;
        echo '<div class="holder-panel-main">';
        if (is_user_logged_in()):

            global $current_user;
            $display_user_name = $current_user->user_login;
            $display_user_ID = $current_user->ID;

            $downloads = 'downloads';
            $orders = 'orders';
            $logout = 'customer-logout';
            $account_link = get_permalink(get_option('woocommerce_myaccount_page_id'));

            $current_page_URL = $_SERVER["REQUEST_URI"];
            $logout_url = wp_logout_url($current_page_URL); ?>
            <div class="dropdown">
                <button class="header-profile-btn border-0 float-none digikala-btn-profile pb-0" type="button"
                        id="dropdownMenuBtnProfileDigikala" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    <i class="fal fa-user-alt my-auto ml-2" style="font-size: 22px"></i>
                    <i class="far fa-chevron-down mt-2"></i>
                </button>
                <div class="dropdown-menu digikala-dropdown-menu" aria-labelledby="dropdownMenuBtnProfileDigikala" style="width: 200px;">
                    <div class="digikala-info-user-profile">
                        <div class="profile-avatar">
                            <?php
                            global $current_user;
                            echo get_avatar($current_user->ID, 30);
                            ?>
                        </div>
                        <div class="digikala-user-name-profile text-right">
                            <span class="name-user-profile">
                            <?php
                            global $current_user;
                            echo $current_user->display_name
                            ?>
                            </span>
                            <span class="digikala-link-profile">
                                <a href="<?php echo esc_url($account_link); ?>">
                                    مشاهده حساب کاربری
                                </a>
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <a href="<?php echo esc_url(wc_get_account_endpoint_url($downloads)); ?>" class="digikala-download">دانلودها</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url(wc_get_account_endpoint_url($orders)); ?>" class="digikala-orders">سفارش‌های من</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url(wc_get_account_endpoint_url($logout)); ?>" class="log-out">
                                خروج از حساب کاربری
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        <?php
        else: ?>
            <!-- Start Form Login & Register -->
            <?php if ($settings['popup_login']) { ?>
                <button type="button" class="btn-login-digikala" data-toggle="modal" data-target="#popup-login">
                    <i class="fal fa-user-alt my-auto d-none" style="font-size: 22px"></i>
                    <span><?php echo $settings['title_login']; ?></span>
                </button>
            <?php } else { ?>
                <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="btn-login-digikala">
                    <i class="fal fa-user-alt my-auto d-none" style="font-size: 22px"></i>
                    <span><?php echo $settings['title_login']; ?></span>
                </a>
            <?php } ?>
            <!-- End Form Login & Register -->
        <?php endif; ?>
        <?php
        echo '</div>';
    }

}
