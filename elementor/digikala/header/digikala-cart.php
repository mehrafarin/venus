<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Digikala_Header_Cart extends Widget_Base
{

    public function get_name()
    {
        return 'digikala-header-cart';
    }

    public function get_title()
    {
        return __('سبد خرید دیجیکالا', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-cart';
    }

    public function get_categories()
    {
        return ['Digikala-Header'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('سبد خرید', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'title_cart',
            [
                'label' => __('متن سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('سبد خرید', 'venus-theme'),
            ]
        );
        $this->add_control(
            'venus_woo_mini_cart_icon',
            [
                'label' => __('آیکون سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::ICONS,
                'default' => [
                    'value' => 'fal fa-shopping-cart',
                    'library' => 'solid',
                ],
            ]
        );
        $this->add_responsive_control(
            'venus_woo_mini_cart_alignment',
            [
                'label' => esc_html__('موقعیت', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .hwp-mini-cart-area-holder' => 'text-align: {{VALUE}};'
                ],
                'default' => 'left',
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'style_section',
            [
                'label' => __('استایل سبد خرید', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_cart_main',
                'label' => __('بکگراند باکس سبد خرید', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .hwp-mini-cart-area .cart-items',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'box_shadow_cart_main',
                'label' => __('سایه باکس سبدخرید', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .hwp-mini-cart-area .cart-items',
            ]
        );

        $this->add_control(
            'color_cart_icon',
            [
                'label' => __('رنگ آیکون سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .hwp-mini-cart-area i' => 'color: {{VALUE}}',
                ],
                'default' => '#000',
            ]
        );

        $this->add_control(
            'cart_icon_size',
            [
                'label' => __('سایز آیکون سبد خرید', 'venus-theme'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 22,
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .hwp-mini-cart-area i' => 'font-size:{{SIZE}}px',
                ],
            ]
        );

        $this->add_control(
            'color_count_cart',
            [
                'label' => __('رنگ تعداد سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .hwp-mini-cart-area .count-number-cart .number' => 'color: {{VALUE}}',
                ],
                'default' => '#fff'
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_cart_count_main',
                'label' => __('بکگراند تعداد سبد خرید', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .hwp-mini-cart-area .count-number-cart .number',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'cart_count_type',
                'label' => __('تایپوگرافی عدد سبد خرید', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .hwp-mini-cart-area .count-number-cart .number',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'cart_content_style',
            [
                'label' => __('محتوای سبد خرید', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_text_cart_content_icon',
            [
                'label' => __('رنگ عنوان سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .hwp-mini-cart-area .cart-items h3' => 'color: {{VALUE}}',
                ],
                'default' => '#212529',
            ]
        );

        $this->add_control(
            'color_cart_content_sum_price',
            [
                'label' => __('رنگ متن ها', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .woocommerce-mini-cart__total.total' => 'color: {{VALUE}}',
                ],
                'default' => '#000',
            ]
        );

        $this->add_control(
            'color_cart_content_btn_carts',
            [
                'label' => __('رنگ متن مشاهده سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .woocommerce-mini-cart__buttons .button.wc-forward' => 'color: {{VALUE}}',
                ],
                'default' => '#fff',
            ]
        );

        $this->add_control(
            'bg_cart_content_btn_carts',
            [
                'label' => __('رنگ بکگراند دکمه مشاهده سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .woocommerce-mini-cart__buttons .button.wc-forward' => 'background-color: {{VALUE}}',
                ],
                'default' => '#78c577',
            ]
        );

        $this->add_control(
            'bg_hover_cart_content_btn_carts',
            [
                'label' => __('رنگ هاور دکمه مشاهده سبد خرید', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .woocommerce-mini-cart__buttons .button.wc-forward:hover' => 'background-color: {{VALUE}}',
                ],
                'default' => '#55b653',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'bg_shadow_cart_content_btn_carts',
                'label' => __('سایه دکمه مشاهده سبد خرید', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .woocommerce-mini-cart__buttons .button.wc-forward',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'cart_content_bg',
                'label' => __('بکگراند قسمت پایین', 'venus-theme'),
                'types' => ['classic', 'gradient', 'video'],
                'selector' => '{{WRAPPER}} .mini-cart-footer',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        ?>
        <?php
        $icon = $settings['venus_woo_mini_cart_icon']; ?>
        <div class="hwp-mini-cart-area-holder digikala-mini-cart">
            <div class="cart float-none d-inline-block text-right m-0">
                <i class="<?php echo $icon['value']; ?> count-number-cart">
                    <span class="number"><?php echo is_object(WC()->cart) ? WC()->cart->get_cart_contents_count() : ''; ?></span>
                </i>
                <div class="cart-items">
                    <div class="d-flex justify-content-between head-mini-cart">
                        <h3 class="p-0 my-auto"><span
                                    class="wm-cart"><?php echo is_object(WC()->cart) ? WC()->cart->get_cart_contents_count() : ''; ?></span>
                            کالا</h3>
                        <a href="<?php echo wc_get_cart_url(); ?>" class="my-auto d-flex">مشاهده سبد خرید <i
                                    class="fal fa-chevron-left arrow"></i></a>
                    </div>
                    <div class="digikala_cart_content"><?php ((WC()->cart != '') ? get_template_part('template/digikala-cart') : ''); ?></div>
                </div>
            </div>
        </div>
    <?php }

}
