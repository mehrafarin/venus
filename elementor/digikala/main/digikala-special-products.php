<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Digikala_Venus_Special_Products extends Widget_Base
{

    public function get_name()
    {
        return 'venus-instant-offer';
    }

    public function get_title()
    {
        return __('پیشنهادات لحظه ای', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-gallery-grid';
    }

    public function get_categories()
    {
        return ['digikala'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_content_instant_offer',
            [
                'label' => __('Instant product offer', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'total_number_products',
            [
                'label' => __('تعداد نمایش محصولات', 'venus-theme'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1,
                'max' => 20,
                'step' => 1,
                'default' => 5,
            ]
        );

        $this->add_control(
            'time',
            [
                'label' => __('سرعت نمایش اتوماتیک(ms)', 'venus-theme'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1000,
                'max' => 20000,
                'step' => 1000,
                'default' => 5000,
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display(); ?>

        <div class="product-amazing main-full-box-row-show-product w-auto">
            <p class="head-amazing-product">پیشنهادهای لحظه‌ای برای شما</p>
            <div class="slide-progress"></div>
            <?php
            $args = array(
                'posts_per_page' => $settings['total_number_products'],
                'post_type' => 'product',
                'post_status' => 'publish',
                'post__in' => array_merge(array(0), wc_get_product_ids_on_sale()),
                'meta_query' => array(
                    array(
                        'key' => '_stock_status',
                        'value' => 'instock',
                        'compare' => '=',
                    )
                ),
                'orderby' => 'rand');
            $offerquery = new \WP_Query($args);
            ?>
            <?php if ($offerquery->have_posts()) { ?>

                <div class="owl-amazing-carousel owl-carousel owl-theme owl-loaded" data-itemtime="<?php echo $settings['time']; ?>">
                    <?php while ($offerquery->have_posts()) : $offerquery->the_post();
                        global $product;
                        ?>
                        <div class="item">
                            <div class="digikala-product shadow-none">
                                <a href="<?php the_permalink(); ?>" class="digikala-img">
                                    <?php global $product;

                                    if ($product->is_on_sale()) {

                                        if (!$product->is_type('variable')) {

                                            $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                        } else {

                                            $max_percentage = 0;

                                            foreach ($product->get_children() as $child_id) {
                                                $variation = wc_get_product($child_id);
                                                $price = $variation->get_regular_price();
                                                $sale = $variation->get_sale_price();
                                                if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                if ($percentage > $max_percentage) {
                                                    $max_percentage = $percentage;
                                                }
                                            }

                                        }
                                        echo "<span class='badge-vip'>" . round($max_percentage) . "% </span>";
                                    }
                                    ?>
                                    <img src="<?php the_post_thumbnail_url(); ?>" class="digikala-inner-img-product" alt="">
                                </a>
                                <div class="digikala-title">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </div>
                                <div class="digikala-price">
                                    <span><?php echo $product->get_price_html(); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>

                </div>
                <?php
            } else {
                echo '<div class="col-12 not-msg">' . __('No product with the desired conditions was found.', 'venus-theme') . '</div>';
            }
            wp_reset_postdata();
            ?>

        </div>

        <?php
    }
}
