<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Digikala_Venus_Products extends Widget_Base
{

    public function get_name()
    {
        return 'digikala-products';
    }

    public function get_title()
    {
        return __('نمایش محصولات دیجیکالا', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-gallery-grid';
    }

    public function get_categories()
    {
        return ['digikala'];
    }

    protected function _register_controls()
    {

        $digikala_product_cate = array();
        $categories = get_terms("product_cat");
        if (!empty($categories) && !is_wp_error($categories)) {
            foreach ($categories as $category) {
                $digikala_product_cate[$category->slug] = $category->name;
            }
        }

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('نمایش محصولات', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'title_product_row',
            [
                'label' => __('عنوان ردیف', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('عنوان تست', 'venus-theme'),
            ]
        );

        $this->add_control(
            'courses_type',
            [
                'label' => __('نمایش :', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'default' => 'all',
                'options' => [
                    'all' => __('همه محصولات ها', 'venus-theme'),
                    'onsale' => __('محصولات تخفیف خورده', 'venus-theme'),
                ],
            ]
        );

        $this->add_control(
            'courses_cat_include',
            [
                'label' => __('دسته بندی', 'venus-theme'),
                'type' => Controls_Manager::SELECT2,
                'options' => $digikala_product_cate,
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('تعداد نمایش محصول', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => '6',
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('نمایش بر اساس', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'date' => __('جدیدترین', 'venus-theme'),
                    'modified' => __('آخرین بروزرسانی', 'venus-theme'),
                    'sales' => __('بیشترین فروش', 'venus-theme'),
                    'rand' => __('تصادفی', 'venus-theme'),
                    'price' => __('قیمت', 'venus-theme'),
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'product_style',
            [
                'label' => __('استایل', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title',
            [
                'label' => __('رنگ عنوان', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .main-full-box-row-show-product .head-sec a' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'color_title_text',
            [
                'label' => __('رنگ خط زیر عنوان', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .main-full-box-row-show-product .head-sec a:before' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_main',
                'label' => __('بکگراند', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .main-full-box-row-show-product',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'setting_owl',
            [
                'label' => __('تنظیمات اسلایدر', 'venus-theme'),
            ]
        );

        $this->add_control(
            'auto_play',
            [
                'label' => __('نمایش خودکار', 'venus-theme'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('روشن', 'venus-theme'),
                'label_off' => __('خاموش', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );

        $this->add_control(
            'time',
            [
                'label' => __('سرعت نمایش (ms)', 'venus-theme'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1000,
                'max' => 20000,
                'step' => 1000,
                'default' => 8000,
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();

        $args = array(
            'post_type' => 'product',
            'posts_per_page' => $settings['posts_per_page'],
            'orderby' => $settings['orderby'],
            'meta_query' => array(),
            'tax_query' => array(
                'relation' => 'AND',
            ),
        );

        $args['meta_query'][] = array(
            'key' => '_price',
            'value' => 0,
            'compare' => '>=',
            'type' => 'DECIMAL',
        );

        switch ($settings['courses_type']) {
            case 'onsale':
                $product_ids_on_sale = wc_get_product_ids_on_sale();
                $product_ids_on_sale[] = 0;
                $args['post__in'] = $product_ids_on_sale;
                break;
        }

        switch ($settings['orderby']) {
            case 'price':
                $args['meta_key'] = '_price';
                $args['orderby'] = 'meta_value_num';
                break;
            case 'sales':
                $args['meta_key'] = 'total_sales';
                $args['orderby'] = 'meta_value_num';
                break;
            default:
                $args['orderby'] = $settings['orderby'];
        }

        if (!empty($settings['courses_cat_include'])) {
            $cat_include = array();
            $settings['courses_cat_include'] = explode(',', $settings['courses_cat_include']);
            foreach ($settings['courses_cat_include'] as $category) {
                $term = term_exists($category, 'product_cat');
                if ($term !== 0 && $term !== null) {
                    $cat_include[] = $term['term_id'];
                }
            }
            if (!empty($cat_include)) {
                $args['tax_query'][] = array(
                    'taxonomy' => 'product_cat',
                    'terms' => $cat_include,
                    'operator' => 'IN',
                );
            }
        }

        $products_query = new \WP_Query($args);

        ?>
        <!--Row Show Product-->
        <div class="full-box-row-show-product">
            <div class="main-full-box-row-show-product py-2 px-4">
                <div class="head-sec">
                    <a href="#"><?php echo $settings['title_product_row']; ?></a>
                </div>
                <div class="index-full-box-row-show-product">
                    <div class="index-left-box-show-product">
                        <div class="owl-carousel owl-carousel-product owl-theme owl-loaded" <?php if ('yes' === $settings['auto_play']) {
                            echo 'data-itemplay="true"';
                        } else {
                            echo 'data-itemplay="false"';
                        } ?> data-itemtime="<?php echo $settings['time']; ?>">
                            <?php if ($products_query->have_posts()) : $i = 0; ?>
                                <?php while ($products_query->have_posts()) : $products_query->the_post();
                                    global $product;
                                    ?>
                                    <div class="item">
                                        <div class="digikala-product">
                                            <a href="<?php the_permalink(); ?>" class="digikala-img">
                                                <?php global $product;

                                                if ($product->is_on_sale()) {

                                                    if (!$product->is_type('variable')) {

                                                        $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                    } else {

                                                        $max_percentage = 0;

                                                        foreach ($product->get_children() as $child_id) {
                                                            $variation = wc_get_product($child_id);
                                                            $price = $variation->get_regular_price();
                                                            $sale = $variation->get_sale_price();
                                                            if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                            if ($percentage > $max_percentage) {
                                                                $max_percentage = $percentage;
                                                            }
                                                        }

                                                    }
                                                    echo "<span class='badge-vip'>" . round($max_percentage) . "% </span>";
                                                }
                                                ?>
                                                <img src="<?php the_post_thumbnail_url(); ?>" class="digikala-inner-img-product" alt="">
                                            </a>
                                            <div class="digikala-title">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </div>
                                            <div class="digikala-price">
                                                <span><?php echo $product->get_price_html(); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
