<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Digikala_Footer_Social extends Widget_Base
{

    public function get_name()
    {
        return 'digikala-footer-social';
    }

    public function get_title()
    {
        return __('شبکه اجتماعی دیجیکالا', 'venus-theme');
    }

    public function get_icon()
    {
        return 'eicon-social-icons';
    }

    public function get_categories()
    {
        return ['Digikala-Footer'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('شبکه های اجتماعی', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'text_right',
            [
                'label' => __('متن', 'venus-theme'),
                'type' => Controls_Manager::TEXT,
                'default' => __('ونوس‌کالا را در شبکه‌های اجتماعی دنبال کنید:', 'venus-theme'),
            ]
        );

        $this->add_control(
            'pos_menu_alignment',
            [
                'label' => __('موقعیت آیکون ها', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'right',
                'toggle' => true,
                'selectors' => [
                    '{{WRAPPER}} .social-icon-footer ul ' => 'text-align: {{VALUE}};'
                ],
            ]
        );

        $this_social = new Repeater();

        $this_social->add_control(
            'icon',
            [
                'label' => __('آیکون', 'venus-theme'),
                'type' => Controls_Manager::ICONS,

            ]
        );

        $this_social->add_control(
            'icon_link',
            [
                'label' => __('لینک', 'venus-theme'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'venus-theme'),
                'show_external' => true,
                'default' => [
                    'url' => get_site_url(),
                    'is_external' => true,
                    'nofollow' => true,
                ],
            ]
        );

        $this->add_control(
            'all_icon',
            [
                'label' => __('لیست شبکه های اجتماعی', 'venus-theme'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $this_social->get_controls(),
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        ?>
        <div class="footer-bottom-left d-block">
            <?php if (!empty($settings['text_right'])): ?>
                <p class="my-auto ml-auto">
                    <?php echo $settings['text_right']; ?>
                </p>
            <?php endif; ?>
            <!-- Social Footer -->
            <div class="social-icon-footer mt-3">
                <ul>
                    <?php foreach ($settings['all_icon'] as $item) {
                        $url_icon = $item['icon_link']['url'];
                        ?>
                        <li>
                            <a href="<?php echo $url_icon; ?>">
                                <?php \Elementor\Icons_Manager::render_icon($item['icon'], ['aria-hidden' => 'true']); ?>
                            </a>
                        </li>
                    <?php }; ?>
                </ul>
            </div>
        </div>
        <?php
    }

}
