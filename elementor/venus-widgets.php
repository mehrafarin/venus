<?php

class My_Elementor_Widgets
{

    protected static $instance = null;

    public static function get_instance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    protected function __construct()
    {

        //        Header Elements Venus Include

        require_once('header/header-menu.php');
        require_once('header/header-menu-phone.php');
        require_once('header/header-cart.php');
        require_once('header/header-panel.php');
        require_once('header/header-search.php');

        //        Body Elements Venus Include

        require_once('main/cdb_header.php');
        require_once('main/cdb_content_box.php');
        require_once('main/cdb_product.php');
        require_once('main/cdb_facts.php');
        require_once('main/cdb_blog.php');
        require_once('main/cdb_course_lessons.php');
        require_once('main/cdb_teachers.php');
        require_once('main/cdb_faqs.php');
        require_once('main/cdb_advice.php');
        require_once('main/cdb_student_contact.php');
        require_once('main/iron-willed.php');

        //        Footer Elements Venus Include

        global $venus_options;

        if ($venus_options['switch_digikala_venus']):

            //        Header Elements Digikala Include

            require_once('digikala/header/digikala-main-menu.php');
            require_once('digikala/header/digikala-header-search.php');
            require_once('digikala/header/digikala-profile.php');
            require_once('digikala/header/digikala-cart.php');

            //        Digikala Body Elements Include

            require_once('digikala/main/digikala-products.php');
            require_once('digikala/main/digikala-amazing-products.php');
            require_once('digikala/main/digikala-special-products.php');

            //        Digikala Footer Elements Include

            require_once('digikala/footer/digikala-menu-vertical.php');
            require_once('digikala/footer/digikala-social.php');

        endif;
        //        Landing Page Elements Include

        require_once('landing/landing-timer.php');
        require_once('landing/landing-product.php');

        add_action('elementor/widgets/widgets_registered', [$this, 'register_widgets']);
    }

    public function register_widgets()
    {
        // Start Venus Header Elements Venus
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Header_Venus_Main_Menu());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Header_Venus_Menu_Phone());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Header_Venus_Cart());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Header_Venus_Panel());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Header_Venus_Search());

        // Start Venus Body Elements
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_header());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_content_box());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_product());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_facts());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_blog());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_course_lessons());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_teachers());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_faqs());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_advice());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\venus_student_contacts());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Venus_Iron_Willed());

        // Start Venus Footer Elements
        global $venus_options;

        if ($venus_options['switch_digikala_venus']):

            // Start Header Digikala Elements
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Header_Main_Menu());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digiakal_Header_Search());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Header_Profile());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Header_Cart());

            // Start Body Digikala Elements
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digiakal_Header_Search());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Venus_Products());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Venus_Amazing_Products());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Venus_Special_Products());

            // Start Footer Digikala Elements
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Vertical_Menu());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Digikala_Footer_Social());

        endif;

        // Start Landing Page Elements
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Venus_Landing_Timer());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Venus_Landing_Product());

    }

}

// Widget Categories
function add_elementor_widget_categories($elements_manager)
{
    $elements_manager->add_category(
        'Venus-Header',
        [
            'title' => __('هدر ونوس', 'venus-theme'),
            'icon' => 'fa fa-plug',
        ]
    );
    $elements_manager->add_category(
        'venus',
        [
            'title' => __('ونوس', 'venus-theme'),
            'icon' => 'fa fa-plug',
        ]
    );
    $elements_manager->add_category(
        'Venus-Footer',
        [
            'title' => __('فوتر ونوس', 'venus-theme'),
            'icon' => 'fa fa-plug',
        ]
    );
    $elements_manager->add_category(
        'Digikala-Header',
        [
            'title' => __('هدر دیجیکالا', 'venus-theme'),
            'icon' => 'fa fa-plug',
        ]
    );
    $elements_manager->add_category(
        'digikala',
        [
            'title' => __('المان های دیجیکالا', 'venus-theme'),
            'icon' => 'fa fa-plug',
        ]
    );
    $elements_manager->add_category(
        'Digikala-Footer',
        [
            'title' => __('فوتر دیجیکالا', 'venus-theme'),
            'icon' => 'fa fa-plug',
        ]
    );
    $elements_manager->add_category(
        'Landing-Venus',
        [
            'title' => __('المان های لندیگ پیج', 'venus-theme'),
            'icon' => 'fa fa-plug',
        ]
    );
}

add_action('elementor/elements/categories_registered', 'add_elementor_widget_categories');

add_action('init', 'my_elementor_init');
function my_elementor_init()
{
    My_Elementor_Widgets::get_instance();
}

//Disable Google Fonts Elementor

add_filter('elementor/frontend/print_google_fonts', '__return_false');

//Register Panel styles Elementor
add_action('elementor/editor/before_enqueue_scripts', function () {
    wp_register_style('venus-el-style', Venus_URI . '/css/elementor.css', array(), '1.0');
    wp_enqueue_style('venus-el-style');
//    wp_register_style('venus-icon-style', Venus_URI . '/css/all.css', array(), '1.0');
//    wp_enqueue_style('venus-icon-style');
});
