<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Venus_Landing_Timer extends Widget_Base
{

    public function get_name()
    {
        return 'venus_landing_timer';
    }

    public function get_title()
    {
        return 'تایمر';
    }

    public function get_icon()
    {
        return 'eicon-clock-o';
    }

    public function get_categories()
    {
        return ['Landing-Venus'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'section_title',
            [
                'label' => __('تایمر لندینگ', 'venus'),
            ]
        );

        $this->add_control(
            'timer_icon',
            [
                'label' => __('آیکون', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::ICONS,
            ]
        );

        $this->add_control(
            'date_timer',
            [
                'label' => __('زمان تایمر', 'venus'),
                'label_block' => true,
                'type' => \Elementor\Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'text_end',
            [
                'label' => __('متن پایان', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('جشنواره به پایان رسید!', 'venus'),
            ]
        );


        $this->end_controls_section();

        $this->start_controls_section(
            'facts_color',
            [
                'label' => __('رنگبندی', 'venus'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'meteor_color_facts',
            [
                'label' => __('رنگ افکت خط ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            ]
        );


        $this->add_control(
            'txt_color_facts',
            [
                'label' => __('رنگ متن ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

//        $due_dates = $this->get_settings( 'date_timer' );
        ?>
        <div class="landing-inner-timer d-flex">
            <div class="landing-icon-timer">
                <?php \Elementor\Icons_Manager::render_icon($settings['timer_icon'], ['aria-hidden' => 'true']); ?>
            </div>
            <div class="countdown-item" data-date="<?php echo $settings['date_timer'] ?>"></div>
        </div>
        <?php
    }
}
