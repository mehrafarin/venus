<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Venus_Landing_Product extends Widget_Base
{

    public function get_name()
    {
        return 'widget-venus-landing-product';
    }

    public function get_title()
    {
        return 'نمایش محصولات';
    }

    public function get_icon()
    {
        return 'eicon-gallery-grid';
    }

    public function get_categories()
    {
        return ['Landing-Venus'];
    }

    function get_product_cat()
    {
        $terms = get_terms(array(
            'taxonomy' => 'product_cat',
            'hide_empty' => false,
        ));

        if (!empty($terms) && !is_wp_error($terms)) {
            foreach ($terms as $term) {
                $options[$term->term_id] = $term->name;
            }
            return $options;
        }
    }

    function get_product_tag()
    {
        $terms = get_terms(array(
            'taxonomy' => 'product_tag',
            'hide_empty' => false,
        ));

        if (!empty($terms) && !is_wp_error($terms)) {
            foreach ($terms as $term) {
                $options[$term->term_id] = $term->name;
            }
            return $options;
        }
    }

    protected function _register_controls()
    {

        $venus_product_cate = array();
        $categories = get_terms("product_cat");
        if (!empty($categories) && !is_wp_error($categories)) {
            foreach ($categories as $category) {
                $venus_product_cate[$category->slug] = $category->name;
            }
        }

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('نمایش محصول', 'venus'),
            ]
        );

        $this->add_control(
            'courses_type',
            [
                'label' => __('نوع نمایش دوره ها', 'venus'),
                'type' => Controls_Manager::SELECT,
                'default' => 'all',
                'label_block' => true,
                'options' => [
                    'all' => __('همه دوره ها', 'venus'),
                    'onsale' => __('دوره های تخفیف خورده', 'venus'),
                    'manual_selection' => __('انتخاب دستی', 'venus'),
                ],
            ]
        );

        $this->add_control(
            'courses_cat_include',
            [
                'label' => __('دسته بندی دوره ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::SELECT2,
                'options' => $venus_product_cate,
                'condition' => [
                    'courses_type' => ['all', 'onsale'],
                ],
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('تعداد نمایش محصولات', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => '6',
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('مرتب سازی بر اساس', 'venus'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'label_block' => true,
                'options' => [
                    'date' => __('جدیدترین', 'venus'),
                    'modified' => __('آخرین بروزرسانی', 'venus'),
                    'sales' => __('بیشترین فروش', 'venus'),
                    'rand' => __('تصادفی', 'venus'),
                    'price' => __('قیمت', 'venus'),
                ],
            ]
        );

        $this->add_control(
            'select_style_product',
            [
                'label' => __('انتخاب استایل نمایش محصولات', 'venus'),
                'type' => Controls_Manager::SELECT,
                'default' => 'default',
                'label_block' => true,
                'options' => [
                    'default' => __('پیش فرض', 'venus'),
                    'version_one' => __('استایل 1', 'venus'),
                    'version_two' => __('استایل 2', 'venus'),
                    'version_three' => __('استایل 3', 'venus'),
                ],
            ]
        );

        $this->add_control(
            've_text_btn_add_cart',
            [
                'label' => __('متن ثبت نام دوره', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('ثبت نام دوره', 'venus-theme'),
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'info_product',
            [
                'label' => __('اطلاعات دوره', 'venus'),
            ]
        );

        $this->add_control(
            'status_course_description',
            [
                'label' => __('توضیحات محصول', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus'),
                'label_off' => __('غیرفعال', 'venus'),
                'return_value' => 'Yes',
                'default' => 'Yes',
            ]
        );

        $this->add_control(
            'select_show_hide_waves_product',
            [
                'label' => __('موج ها', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('نمایش', 'venus'),
                'label_off' => __('پنهان', 'venus'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_section',
            [
                'label' => __('استایل ', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'text_color_reg',
            [
                'label' => __('رنگ متن ثبت نام دوره', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .ve-landing-add-cart , .ve-landing-add-cart-style-two .ve-landing-text-btn-reg' => 'color: {{VALUE}}',
                ],
                'default' => '#fff',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'type_text_reg',
                'label' => __('تایپوگرافی متن ثبت نام دوره', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .ve-landing-add-cart , .ve-landing-add-cart-style-two .ve-landing-text-btn-reg',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'shadow_reg',
                'label' => __('سایه دکمه ثبت نام دوره', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .ve-landing-add-cart:hover , .ve-landing-add-cart-style-two .ve-landing-text-btn-reg:hover',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_color_reg',
                'label' => __('بکگراند ثبت نام دوره', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .ve-landing-add-cart ,.ve-landing-add-cart-style-two .ve-landing-text-btn-reg',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();

        global $venus_options;

        global $product;

        $args = array(
            'post_type' => 'product',
            'posts_per_page' => $settings['posts_per_page'],
            'orderby' => $settings['orderby'],
            'meta_query' => array(),
            'tax_query' => array(
                'relation' => 'AND',
            ),
        );

        $args['meta_query'][] = array(
            'key' => '_price',
            'value' => 0,
            'compare' => '>=',
            'type' => 'DECIMAL',
        );

        switch ($settings['courses_type']) {
            case 'onsale':
                $product_ids_on_sale = wc_get_product_ids_on_sale();
                $product_ids_on_sale[] = 0;
                $args['post__in'] = $product_ids_on_sale;
                break;
        }

        switch ($settings['orderby']) {
            case 'price':
                $args['meta_key'] = '_price';
                $args['orderby'] = 'meta_value_num';
                break;
            case 'sales':
                $args['meta_key'] = 'total_sales';
                $args['orderby'] = 'meta_value_num';
                break;
            default:
                $args['orderby'] = $settings['orderby'];
        }

        if (!empty($settings['courses_cat_include'])) {
            $cat_include = array();
            $settings['courses_cat_include'] = explode(',', $settings['courses_cat_include']);
            foreach ($settings['courses_cat_include'] as $category) {
                $term = term_exists($category, 'product_cat');
                if ($term !== 0 && $term !== null) {
                    $cat_include[] = $term['term_id'];
                }
            }
            if (!empty($cat_include)) {
                $args['tax_query'][] = array(
                    'taxonomy' => 'product_cat',
                    'terms' => $cat_include,
                    'operator' => 'IN',
                );
            }
        }

        $products_query = new \WP_Query($args);

        ?>
        <!-- Start courses -->
        <?php


        switch ($settings['select_style_product']) {
            case 'default':
                ?>
                <div class="courses">
                    <div class="text-left">
                        <div class="row">
                            <?php if ($products_query->have_posts()) : $i = 0; ?>
                                <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                                    <div class="col-sm-12 col-md-6 col-xl-4 course">
                                        <div class="course-inner">
                                            <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                                <?php
                                                venus_course_status();
                                                $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                                if ($cu_status != 'soon') {
                                                    if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                        <div class="course-item-sale">
                                                            <?php
                                                            global $product;

                                                            if ($product->is_on_sale()) {

                                                                if (!$product->is_type('variable')) {

                                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                                } else {

                                                                    $max_percentage = 0;

                                                                    foreach ($product->get_children() as $child_id) {
                                                                        $variation = wc_get_product($child_id);
                                                                        $price = $variation->get_regular_price();
                                                                        $sale = $variation->get_sale_price();
                                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                        if ($percentage > $max_percentage) {
                                                                            $max_percentage = $percentage;
                                                                        }
                                                                    }

                                                                }
                                                                echo "<div class='sale-perc-badge'>";
                                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                                echo "</div>";
                                                            }
                                                            ?>
                                                        </div>
                                                    <?php }
                                                } ?>
                                                <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                                    <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             viewBox="0 24 150 28"
                                                             preserveAspectRatio="none" shape-rendering="auto">
                                                            <defs>
                                                                <path id="gentle-wave"
                                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                            </defs>
                                                            <g class="parallax">
                                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                     fill="rgba(255,255,255,0.7"></use>
                                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                                     fill="rgba(255,255,255,0.5)"></use>
                                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                                     fill="rgba(255,255,255,0.3)"></use>
                                                                <use xlink:href="#gentle-wave" x="90" y="7"
                                                                     fill="#fff"></use>
                                                            </g>
                                                        </svg>
                                                    <?php endif; ?>
                                                </a>
                                                <?php
                                                $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                                if ($link_show_course_intro_video) : ?>
                                                    <div class="video-button">
                                                        <a data-post-id="<?php echo get_the_ID(); ?>"
                                                           href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                           class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                    </div>
                                                <?php endif;
                                                if ($cu_status != 'soon') {
                                                    woocommerce_template_loop_add_to_cart();
                                                }
                                                ?>
                                            </header>
                                            <div class="course-detail">
                                                <?php
                                                $ve_h = '';
                                                if (empty($settings['status_course_description'])):
                                                    $ve_h = 'height: unset'; ?>
                                                    <style>
                                                        .ve-landing-product-bottom:before {
                                                            content: none;
                                                        }
                                                    </style>
                                                <?php endif; ?>
                                                <div class="course-top-content" style="<?php echo $ve_h; ?>">
                                                    <a href="<?php the_permalink(); ?>" class="title">
                                                        <h2><?php the_title(); ?></h2>
                                                    </a>
                                                    <?php if ($settings['status_course_description'] == 'Yes'): ?>
                                                        <div class="course-description">
                                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="row mx-0 ve-landing-product-bottom">
                                                    <div class="col-12 px-0 text-center ve-landing-price-course">
                                                        <?php
                                                        global $product;
                                                        echo $product->get_price_html();
                                                        ?>
                                                    </div>
                                                    <div class="col-12 p-0 text-right">
                                                        <a href="<?php echo ve_url_add_cart(); ?>"
                                                           class="ve-landing-add-cart"><?php echo $settings['ve_text_btn_add_cart']; ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
                break;
            case 'version_one':
                ?>
                <div class="courses">
                    <div class="text-left">
                        <div class="row">
                            <?php if ($products_query->have_posts()) : $i = 0; ?>
                                <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                                    <div class="col-sm-12 col-lg-6 course">
                                        <div class="course-inner row mx-0">
                                            <header class="col-12 col-sm-6 px-0"
                                                    style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;border-radius: 0px 15px 0 0;">
                                                <?php
                                                venus_course_status();
                                                $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                                if ($cu_status != 'soon') {
                                                    if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                        <div class="course-item-sale">
                                                            <?php
                                                            global $product;

                                                            if ($product->is_on_sale()) {

                                                                if (!$product->is_type('variable')) {

                                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                                } else {

                                                                    $max_percentage = 0;

                                                                    foreach ($product->get_children() as $child_id) {
                                                                        $variation = wc_get_product($child_id);
                                                                        $price = $variation->get_regular_price();
                                                                        $sale = $variation->get_sale_price();
                                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                        if ($percentage > $max_percentage) {
                                                                            $max_percentage = $percentage;
                                                                        }
                                                                    }

                                                                }
                                                                echo "<div class='sale-perc-badge'>";
                                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                                echo "</div>";
                                                            }
                                                            ?>
                                                        </div>
                                                    <?php }
                                                } ?>
                                                <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                                    <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             viewBox="0 24 150 28"
                                                             preserveAspectRatio="none" shape-rendering="auto">
                                                            <defs>
                                                                <path id="gentle-wave"
                                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                            </defs>
                                                            <g class="parallax">
                                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                     fill="rgba(255,255,255,0.7"></use>
                                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                                     fill="rgba(255,255,255,0.5)"></use>
                                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                                     fill="rgba(255,255,255,0.3)"></use>
                                                                <use xlink:href="#gentle-wave" x="90" y="7"
                                                                     fill="#fff"></use>
                                                            </g>
                                                        </svg>
                                                    <?php endif; ?>
                                                </a>
                                                <?php
                                                $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                                if ($link_show_course_intro_video) : ?>
                                                    <div class="video-button">
                                                        <a data-post-id="<?php echo get_the_ID(); ?>"
                                                           href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                           class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                    </div>
                                                <?php endif;
                                                if ($cu_status != 'soon') {
                                                    woocommerce_template_loop_add_to_cart();
                                                }
                                                ?>
                                            </header>
                                            <div class="course-detail col-12 col-sm-6">
                                                <?php
                                                $ve_h = '';
                                                if (empty($settings['status_course_description'])):
                                                    $ve_h = 'height: unset'; ?>
                                                    <style>
                                                        .ve-landing-product-bottom:before {
                                                            content: none;
                                                        }
                                                    </style>
                                                <?php endif; ?>
                                                <div class="course-top-content" style="<?php echo $ve_h; ?>">
                                                    <a href="<?php the_permalink(); ?>" class="title">
                                                        <h2><?php the_title(); ?></h2>
                                                    </a>
                                                    <?php if ($settings['status_course_description'] == 'Yes'): ?>
                                                        <div class="course-description">
                                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="row mx-0 ve-landing-product-bottom">
                                                    <div class="col-12 px-0 text-center ve-landing-price-course">
                                                        <?php
                                                        global $product;
                                                        echo $product->get_price_html();
                                                        ?>
                                                    </div>
                                                    <div class="col-12 p-0 text-right">
                                                        <a href="<?php echo ve_url_add_cart(); ?>"
                                                           class="ve-landing-add-cart"><?php echo $settings['ve_text_btn_add_cart']; ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
                break;
            case 'version_two':
                ?>
                <div class="courses">
                    <div class="text-left">
                        <div class="row mx-0">
                            <?php if ($products_query->have_posts()) : $i = 0; ?>
                                <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                                    <div class="col-12 course">
                                        <div class="course-inner ve-landing-prosuct-course-inner row mx-0">
                                            <header class="col-12 col-md-5 px-0 ve-thumb-landing-product"
                                                    style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;border-radius: 0px 15px 0 0;">
                                                <?php
                                                venus_course_status();
                                                $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                                if ($cu_status != 'soon') {
                                                    if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                        <div class="course-item-sale">
                                                            <?php
                                                            global $product;

                                                            if ($product->is_on_sale()) {

                                                                if (!$product->is_type('variable')) {

                                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                                } else {

                                                                    $max_percentage = 0;

                                                                    foreach ($product->get_children() as $child_id) {
                                                                        $variation = wc_get_product($child_id);
                                                                        $price = $variation->get_regular_price();
                                                                        $sale = $variation->get_sale_price();
                                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                        if ($percentage > $max_percentage) {
                                                                            $max_percentage = $percentage;
                                                                        }
                                                                    }

                                                                }
                                                                echo "<div class='sale-perc-badge'>";
                                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                                echo "</div>";
                                                            }
                                                            ?>
                                                        </div>
                                                    <?php }
                                                } ?>
                                                <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                                    <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             viewBox="0 24 150 28"
                                                             preserveAspectRatio="none" shape-rendering="auto">
                                                            <defs>
                                                                <path id="gentle-wave"
                                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                            </defs>
                                                            <g class="parallax">
                                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                     fill="rgba(255,255,255,0.7"></use>
                                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                                     fill="rgba(255,255,255,0.5)"></use>
                                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                                     fill="rgba(255,255,255,0.3)"></use>
                                                                <use xlink:href="#gentle-wave" x="90" y="7"
                                                                     fill="#fff"></use>
                                                            </g>
                                                        </svg>
                                                    <?php endif; ?>
                                                </a>
                                                <?php
                                                $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                                if ($link_show_course_intro_video) : ?>
                                                    <div class="video-button">
                                                        <a data-post-id="<?php echo get_the_ID(); ?>"
                                                           href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                           class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                    </div>
                                                <?php endif;
                                                if ($cu_status != 'soon') {
                                                    woocommerce_template_loop_add_to_cart();
                                                }
                                                ?>
                                            </header>
                                            <div class="course-detail ve-landing-product-course-detail col-12 col-md-7">
                                                <?php
                                                $ve_h = '';
                                                if (empty($settings['status_course_description'])):
                                                    $ve_h = 'height: unset'; ?>
                                                    <style>
                                                        .ve-landing-product-bottom:before {
                                                            content: none;
                                                        }
                                                    </style>
                                                <?php endif; ?>
                                                <div class="course-top-content"
                                                     style="<?php echo $ve_h; ?> height: unset;">
                                                    <a href="<?php the_permalink(); ?>"
                                                       class="ve-landing-product-title">
                                                        <h2><?php the_title(); ?></h2>
                                                    </a>
                                                    <?php if ($settings['status_course_description'] == 'Yes'): ?>
                                                        <div class="course-description">
                                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="row mx-0 ve-landing-product-bottom">
                                                    <div class="col-12 p-0 ve-inner-product-bottm">
                                                        <div class="ve-right-text-price float-right">
                                                            ارزش
                                                            <?php ve_price_product(); ?>
                                                        </div>
                                                        <a href="<?php echo ve_url_add_cart(); ?>"
                                                           class="ve-landing-add-cart-style-two float-left">
                                                            <?php if ($product->is_on_sale()) { ?>
                                                                <strong class="sales"><?php ve_price_product(); ?></strong>
                                                            <?php } ?>
                                                            <span class="ve-landing-text-btn-reg"><?php echo $settings['ve_text_btn_add_cart']; ?></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
                break;
            case 'version_three':
                ?>
                <div class="swiper-container swiper-product" data-loop="false">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php if ($products_query->have_posts()) : $i = 0; ?>
                            <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                                <div class="course pt-0 swiper-slide">
                                    <div class="course-inner">
                                        <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                            <?php
                                            venus_course_status();
                                            $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                            if ($cu_status != 'soon') {
                                                if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                    <div class="course-item-sale">
                                                        <?php
                                                        global $product;

                                                        if ($product->is_on_sale()) {

                                                            if (!$product->is_type('variable')) {

                                                                $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                            } else {

                                                                $max_percentage = 0;

                                                                foreach ($product->get_children() as $child_id) {
                                                                    $variation = wc_get_product($child_id);
                                                                    $price = $variation->get_regular_price();
                                                                    $sale = $variation->get_sale_price();
                                                                    if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                    if ($percentage > $max_percentage) {
                                                                        $max_percentage = $percentage;
                                                                    }
                                                                }

                                                            }
                                                            echo "<div class='sale-perc-badge'>";
                                                            echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                            echo "<div class='sale-badge-text'>تخفیف</div>";
                                                            echo "</div>";
                                                        }
                                                        ?>
                                                    </div>
                                                <?php }
                                            } ?>
                                            <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                                <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         viewBox="0 24 150 28"
                                                         preserveAspectRatio="none" shape-rendering="auto">
                                                        <defs>
                                                            <path id="gentle-wave"
                                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                        </defs>
                                                        <g class="parallax">
                                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                 fill="rgba(255,255,255,0.7"></use>
                                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                                 fill="rgba(255,255,255,0.5)"></use>
                                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                                 fill="rgba(255,255,255,0.3)"></use>
                                                            <use xlink:href="#gentle-wave" x="90" y="7"
                                                                 fill="#fff"></use>
                                                        </g>
                                                    </svg>
                                                <?php endif; ?>
                                            </a>
                                            <?php
                                            $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                            if ($link_show_course_intro_video) : ?>
                                                <div class="video-button">
                                                    <a data-post-id="<?php echo get_the_ID(); ?>"
                                                       href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                       class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                </div>
                                            <?php endif;
                                            if ($cu_status != 'soon') {
                                                woocommerce_template_loop_add_to_cart();
                                            }
                                            ?>
                                        </header>
                                        <div class="course-detail">
                                            <div class="course-top-content">
                                                <a href="<?php the_permalink(); ?>" class="title">
                                                    <h2><?php the_title(); ?></h2>
                                                </a>
                                                <div class="course-description">
                                                    <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                </div>
                                            </div>
                                            <div class="row mx-0 ve-landing-product-bottom">
                                                <div class="col-12 px-0 text-center ve-landing-price-course">
                                                    <?php
                                                    global $product;
                                                    echo $product->get_price_html();
                                                    ?>
                                                </div>
                                                <div class="col-12 p-0 text-right">
                                                    <a href="<?php echo ve_url_add_cart(); ?>"
                                                       class="ve-landing-add-cart"><?php echo $settings['ve_text_btn_add_cart']; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        <?php else : ?>
                            <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                        <?php endif; ?>
                    </div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev btn-prev-related op-blog-btn"></div>
                    <div class="swiper-button-next btn-next-related op-blog-btn"></div>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="courses">
                    <div class="text-left">
                        <div class="row">
                            <?php if ($products_query->have_posts()) : $i = 0; ?>
                                <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                                    <div class="col-sm-12 col-md-6 col-xl-4 course">
                                        <div class="course-inner">
                                            <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                                <?php
                                                venus_course_status();
                                                $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                                if ($cu_status != 'soon') {
                                                    if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                        <div class="course-item-sale">
                                                            <?php
                                                            global $product;

                                                            if ($product->is_on_sale()) {

                                                                if (!$product->is_type('variable')) {

                                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                                } else {

                                                                    $max_percentage = 0;

                                                                    foreach ($product->get_children() as $child_id) {
                                                                        $variation = wc_get_product($child_id);
                                                                        $price = $variation->get_regular_price();
                                                                        $sale = $variation->get_sale_price();
                                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                        if ($percentage > $max_percentage) {
                                                                            $max_percentage = $percentage;
                                                                        }
                                                                    }

                                                                }
                                                                echo "<div class='sale-perc-badge'>";
                                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                                echo "</div>";
                                                            }
                                                            ?>
                                                        </div>
                                                    <?php }
                                                } ?>
                                                <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                                    <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             viewBox="0 24 150 28"
                                                             preserveAspectRatio="none" shape-rendering="auto">
                                                            <defs>
                                                                <path id="gentle-wave"
                                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                            </defs>
                                                            <g class="parallax">
                                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                     fill="rgba(255,255,255,0.7"></use>
                                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                                     fill="rgba(255,255,255,0.5)"></use>
                                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                                     fill="rgba(255,255,255,0.3)"></use>
                                                                <use xlink:href="#gentle-wave" x="90" y="7"
                                                                     fill="#fff"></use>
                                                            </g>
                                                        </svg>
                                                    <?php endif; ?>
                                                </a>
                                                <?php
                                                $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                                if ($link_show_course_intro_video) : ?>
                                                    <div class="video-button">
                                                        <a data-post-id="<?php echo get_the_ID(); ?>"
                                                           href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                           class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                    </div>
                                                <?php endif;
                                                if ($cu_status != 'soon') {
                                                    woocommerce_template_loop_add_to_cart();
                                                }
                                                ?>
                                            </header>
                                            <div class="course-detail">
                                                <?php
                                                $ve_h = '';
                                                if (empty($settings['status_course_description'])):
                                                    $ve_h = 'height: unset'; ?>
                                                    <style>
                                                        .ve-landing-product-bottom:before {
                                                            content: none;
                                                        }
                                                    </style>
                                                <?php endif; ?>
                                                <div class="course-top-content" style="<?php echo $ve_h; ?>">
                                                    <a href="<?php the_permalink(); ?>" class="title">
                                                        <h2><?php the_title(); ?></h2>
                                                    </a>
                                                    <?php if ($settings['status_course_description'] == 'Yes'): ?>
                                                        <div class="course-description">
                                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="row mx-0 ve-landing-product-bottom">
                                                    <div class="col-12 px-0 text-center ve-landing-price-course">
                                                        <?php
                                                        global $product;
                                                        echo $product->get_price_html();
                                                        ?>
                                                    </div>
                                                    <div class="col-12 p-0 text-right">
                                                        <a href="<?php echo ve_url_add_cart(); ?>"
                                                           class="ve-landing-add-cart">ثبت نام دوره</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php
        }

        wp_reset_postdata(); ?>

        <?php

    }
}
