<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_facts extends Widget_Base
{

    public function get_name()
    {
        return 'venus_facts';
    }

    public function get_title()
    {
        return 'آمار و ارقام';
    }

    public function get_icon()
    {
        return 'eicon-counter';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'section_title',
            [
                'label' => __('نمایش محصول', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_one',
            [
                'label' => __('متن اول', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('آمار و ارقام نشانگر', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_two',
            [
                'label' => __('متن دوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('موفقیت دانشجویان', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_three',
            [
                'label' => __('متن سوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('ما می باشد', 'venus'),
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'facts_title',
            [
                'label' => __('اطلاعات آمار و ارقام', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_icon_one',
            [
                'label' => __('آیکون اول', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('fas fa-book', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_num_one',
            [
                'label' => __('عدد اول', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('19', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_title_one',
            [
                'label' => __('متن اول', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('دوره آموزشی', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_icon_two',
            [
                'label' => __('آیکون دوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('fa fa-photo-video', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_num_two',
            [
                'label' => __('عدد دوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('2560', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_title_two',
            [
                'label' => __('متن دوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('ساعت دوره آموزشی', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_icon_three',
            [
                'label' => __('آیکون سوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('fas fa-medal', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_num_three',
            [
                'label' => __('عدد سوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('12617', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_title_three',
            [
                'label' => __('متن سوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('فارغ التحصیل', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_icon_four',
            [
                'label' => __('آیکون چهارم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('fas fa-user-graduate', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_num_four',
            [
                'label' => __('عدد چهارم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('75288', 'venus'),
            ]
        );

        $this->add_control(
            'title_facts_title_four',
            [
                'label' => __('متن چهارم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('همیارجو', 'venus'),
            ]
        );


        $this->end_controls_section();

        $this->start_controls_section(
            'facts_color',
            [
                'label' => __('رنگبندی', 'venus'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'meteor_color_facts',
            [
                'label' => __('رنگ افکت خط ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            ]
        );


        $this->add_control(
            'txt_color_facts',
            [
                'label' => __('رنگ متن ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            ]
        );

        $this->add_control(
            'bg_color_facts',
            [
                'label' => __('رنگ پس زمینه آیتم ها', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers ul li' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'icon_color_facts',
            [
                'label' => __('رنگ آیکون آیتم ها', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#339AD9',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers ul li [class^="icon-"] i' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'number_color_facts',
            [
                'label' => __('رنگ عدد آیتم ها', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#339AD9',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers .state-numbers-inner .num' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'txt_color_facts_all',
            [
                'label' => __('رنگ متن آیتم ها', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#999',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers .state-numbers-inner .text' => 'color: {{VALUE}}',
                ],
            ]
        );



        $this->add_control(
            'bg_fe_color_facts',
            [
                'label' => __('پس زمینه آیتم ویژه', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#339AD9',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers ul li.students' => 'background-color: {{VALUE}}',
                ],
            ]
        );


        $this->add_control(
            'icon_color_facts_fe',
            [
                'label' => __('رنگ آیکون آیتم ویژه', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers ul li.students i' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'number_color_facts_fe',
            [
                'label' => __('رنگ عدد آیتم ویژه', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers ul li.students .num' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'txt_color_facts_fe',
            [
                'label' => __('رنگ متن آیتم ویژه', 'arsheh-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} .state .state-numbers ul li.students .text' => 'color: {{VALUE}}',
                ],
            ]
        );


        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
?>
        <?php if (!empty($settings['meteor_color_facts'])) { ?>
        <style>
            .state .state-text .meteor1, .state .state-text .meteor2, .state .state-text .meteor6 {
                background: -moz-linear-gradient(left, rgba(0, 0, 0, 0) 0%, <?php echo $settings['meteor_color_facts'] ?> 100%);
                background: -webkit-linear-gradient(left, rgba(0, 0, 0, 0) 0%, <?php echo $settings['meteor_color_facts'] ?> 100%);
                background: linear-gradient(to right, rgba(0, 0, 0, 0) 0%, <?php echo $settings['meteor_color_facts'] ?> 100%);
            }

            .state .state-text .meteor3, .state .state-text .meteor4, .state .state-text .meteor5, .state .state-text .meteor7 {
                background: -moz-linear-gradient(left, <?php echo $settings['meteor_color_facts'] ?> 0%, rgba(0, 0, 0, 0) 100%);
                background: -webkit-linear-gradient(left, <?php echo $settings['meteor_color_facts'] ?> 0%, rgba(0, 0, 0, 0) 100%);
                background: linear-gradient(to right, <?php echo $settings['meteor_color_facts'] ?> 0%, rgba(0, 0, 0, 0) 100%);
            }
        </style>
    <?php }; ?>
        <?php if (!empty($settings['txt_color_facts'])) { ?>
        <style>
            .state .state-text p {
                color: <?php echo $settings['txt_color_facts']; ?>;
            }
        </style>
    <?php }; ?>
        <?php
        echo "
<!-- Start state -->
<div>
    <div class='state p-0'>
        <div class='row'>
            <div class='col-md-12 col-lg-6 state-text'>
                <p class='text-right'>
                    $settings[title_facts_one]
                    <br>
                    <strong>$settings[title_facts_two]</strong>
                    <br>
                    <span>$settings[title_facts_three]</span>‍‍‍
                </p>
                <div class='meteor1'></div>
                <div class='meteor2'></div>
                <div class='meteor3'></div>
                <div class='meteor4'></div>
                <div class='meteor5'></div>
                <div class='meteor6'></div>
                <div class='meteor7'></div>
            </div>
            <div class='col-md-12 col-lg-6 state-numbers'>
                <div class='state-numbers-inner'>
                    <ul class=''>
                        <li class='students'>
                            <span class='icon-student px-3'>
                                <i class='$settings[title_facts_icon_four]'></i>
                            </span>
                            <div class='num'>$settings[title_facts_num_four]</div>
                            <div class='text'>$settings[title_facts_title_four]</div>
                        </li>
                        <li class='courses'>
                            <span class='icon-books'>
                                <i class='$settings[title_facts_icon_one]'></i>
                            </span>
                            <div class='num'>$settings[title_facts_num_one]</div>
                            <div class='text'>$settings[title_facts_title_one]</div>
                        </li>
                        <li class='certificate'>
                            <span class='icon-diploma'>
                                <i class='$settings[title_facts_icon_three]'></i>
                            </span>
                            <div class='num'>
                                $settings[title_facts_num_three]
                            </div>
                            <div class='text'>$settings[title_facts_title_three]</div>
                        </li>
                        <li class='hours'>
                            <span class='icon-camera'>
                                <i class='$settings[title_facts_icon_two]'></i>
                            </span>
                            <div class='num'>$settings[title_facts_num_two]</div>
                            <div class='text'>$settings[title_facts_title_two]</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End state -->";
    }
}
