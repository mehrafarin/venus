<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_teachers extends Widget_Base
{

    public function get_name()
    {
        return 'venus-teacher';
    }

    public function get_title()
    {
        return 'مدرس دوره';
    }

    public function get_icon()
    {
        return 'eicon-person';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('مدرس دوره', 'venus'),
            ]
        );

        $this->add_control(
            'title_teacher',
            [
                'label' => __('نام مدرس', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'subtitle_teacher',
            [
                'label' => __('سمت یا دستاورد مدرس', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'dec_teacher',
            [
                'label' => __('درباره مدرس', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'description' => __('در این قسمت در حد یک جمله کوتاه در رابطه با مدرس مورد نظر بنویسید.', 'venus'),
            ]
        );

        $this->add_control(
            'image_teacher',
            [
                'label' => __('تصویر شاخص', 'venus'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
                'description' => __('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'teacher_style',
            [
                'label' => __('استایل', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'teacher_name',
                'label' => __('نام مدرس', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .ve-teacher .teacher-name .ve-teach-name',
            ]
        );

        $this->add_control(
            'color_teacher_name',
            [
                'label' => __('رنگ نام مدرس', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .ve-teacher .teacher-name .ve-teach-name' => 'color: {{VALUE}}',
                ],
                'default' => '#333333'
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'side_teacher',
                'label' => __('سمت مدرس', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .ve-teacher .teacher-name .teacher-position',
            ]
        );

        $this->add_control(
            'color_side_teacher',
            [
                'label' => __('رنگ سمت مدرس', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .ve-teacher .teacher-name .teacher-position' => 'color: {{VALUE}}',
                ],
                'default' => '#333333'
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'des_teacher',
                'label' => __('درباره مدرس', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .ve-teacher .teacher-info p',
            ]
        );

        $this->add_control(
            'color_des_teacher',
            [
                'label' => __('رنگ درباره مدرس', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .ve-teacher .teacher-info p' => 'color: {{VALUE}}',
                ],
                'default' => '#666'
            ]
        );

        $this->add_control(
            'rad_pic_teacher',
            [
                'label' => __('گردی تصویر', 'venus-theme'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 8,
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ve-teacher .avatar img' => 'border-radius:{{SIZE}}px',
                ],
            ]
        );

        $this->end_controls_section();


    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();

        ?>
        <?php if (!empty($settings['title_teacher'])): ?>
        <div class="col-12">
            <div class="teacher ve-teacher row">
                <div class="col-xs-12 col-sm-4 avatar">
                    <img src="<?php echo $settings['image_teacher']['url']; ?>">
                </div>
                <div class="col-sm-8 col-xs-12">
                    <div class="teacher-name">
                        <div class="ve-teach-name"><?php echo $settings['title_teacher']; ?></div>
                        <div class="teacher-position"><?php echo $settings['subtitle_teacher']; ?></div>
                    </div>
                    <div class="teacher-info">
                        <p><?php echo $settings['dec_teacher']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
        <?php

    }
}
