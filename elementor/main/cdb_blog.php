<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_blog extends Widget_Base
{

    public function get_name()
    {
        return 'venus_blog';
    }

    public function get_title()
    {
        return 'بلاگ';
    }

    public function get_icon()
    {
        return 'eicon-post-excerpt';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {
        $categories_array = array();
        $categories = get_categories(array('taxonomy' => 'category',));
        foreach ($categories as $category) {
            $categories_array[$category->term_id] = $category->name;
        }

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('بلاگ', 'venus'),
            ]
        );

        $this->add_control(
            'select_style_blog',
            [
                'label' => __( 'انتخاب استایل بلاگ', 'venus' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'default',
                'options' => [
                    'default'  => __( 'پیش فرض', 'venus' ),
                    'version_one' => __( 'ورژن 1', 'venus' ),
                    'version_two' => __( 'ورژن 2', 'venus' ),
                ],
                'label_block' => true,
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __( 'دسته بندی پست ها', 'venus' ),
                'type' => Controls_Manager::SELECT,
                'options' => $categories_array,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'num_blog_post',
            [
                'label' => __('تعداد نمایش پست ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('3', 'venus'),
            ]
        );

        $this->add_responsive_control(
            've_title_blog_alignment',
            [
                'label' => esc_html__('موقعیت عنوان پست', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'right' => [
                        'title' => esc_html__('راست', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                    'center' => [
                        'title' => esc_html__('وسط', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'left' => [
                        'title' => esc_html__('چپ', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .related-posts .rpost .post-inner .post-detail .title' => 'text-align: {{VALUE}};'
                ],
                'default' => 'right',
                'condition' => [
                    'select_style_blog' => ['version_one', 'version_two'],
                ],
            ]
        );

        $this->add_responsive_control(
            've_title_blog_alignment_one',
            [
                'label' => esc_html__('موقعیت عنوان پست', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'right' => [
                        'title' => esc_html__('راست', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                    'center' => [
                        'title' => esc_html__('وسط', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'left' => [
                        'title' => esc_html__('چپ', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .blog-posts .row > div.first-post a .blog-posts-inner h2 , .blog-posts .row > div.another-posts a .blog-posts-inner h2' => 'text-align: {{VALUE}};'
                ],
                'default' => 'right',
                'condition' => [
                    'select_style_blog' => 'default',
                ],
            ]
        );


        $this->add_control(
            'btn_cat_text',
            [
                'label' => __('عنوان دکمه', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('مقالات دیگر', 'venus'),
                'description' => __('در صورت خالی گذاشتن غیرفعال می شود.', 'venus'),
            ]
        );

        $this->add_control(
            'btn_cat_link',
            [
                'label' => __('لینک دکمه (Url)', 'venus'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'venus'),
                'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'blog_format',
            [
                'label' => __('استایل بلاگ', 'venus'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_bg_btn_blog',
            [
                'label' => __('رنگ دکمه', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'description' => __('رنگ دکمه مشاهده همه', 'venus'),
            ]
        );

        $this->add_control(
            'style_format_post_blog',
            [
                'label' => __( 'نوع نوشته', 'venus' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __( 'نمایش', 'venus' ),
                'label_off' => __( 'پنهان', 'venus' ),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'select_show_hide_waves',
            [
                'label' => __( 'موج ها', 'venus' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __( 'نمایش', 'venus' ),
                'label_off' => __( 'پنهان', 'venus' ),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();


        $all_posts_args = array(

            'post_type' => array('post'),
            'posts_per_page' => 1,
            'cat' => $settings['category']
        );

        $all_posts = new \WP_Query($all_posts_args);

        $two_args = array(

            'post_type' => array('post'),
            'posts_per_page' => 2,
            'offset' => 1,
            'cat' => $settings['category']
        );

        $two_posts = new \WP_Query($two_args);

        $style_posts_blog_args = array(
            'post_type' => array('post'),
            'posts_per_page' => $settings['num_blog_post'],
            'cat' => $settings['category']
        );

        $style_posts = new \WP_Query($style_posts_blog_args);

        $url_btn_one = $settings['btn_cat_link']['url'];
        $url_btn_one_target = $settings['btn_cat_link']['is_external'] ? ' target="_blank"' : '';
        $url_btn_one_nofollow = $settings['btn_cat_link']['nofollow'] ? ' rel="nofollow"' : '';

        ?>
        <?php if ($settings['color_bg_btn_blog'] != '') { ?>
        <style>
            .blog-posts a.more-articles {
                background-color: <?php echo $settings['color_bg_btn_blog']; ?>;
                box-shadow: 0 2px 12px <?php echo $settings['color_bg_btn_blog']; ?>99;
            }

            .blog-posts a.more-articles:hover {
                background-color: <?php echo $settings['color_bg_btn_blog']; ?>;
                box-shadow: 0 2px 12px<?php echo $settings['color_bg_btn_blog']; ?>;
            }
        </style>
    <?php }; ?>
        <!-- Start Blog -->
        <?php if ($settings['select_style_blog'] == "default") { ?>
        <div>
            <div class="blog-posts">
                <div class="row position-relative">
                    <div class="blog-posts-bg"></div>

                    <div class="col-lg-8 col-md-12 first-post">
                        <?php if ($all_posts->have_posts()) : ?>

                            <?php while ($all_posts->have_posts()):$all_posts->the_post(); ?>
                                <a href="<?php the_permalink(); ?>">
                                    <figure>
                                        <img src="<?php the_post_thumbnail_url('post-size'); ?>"
                                             alt="<?php echo get_the_title($all_posts->post->ID); ?>">
                                    </figure>
                                    <div class="blog-posts-inner">
                                        <div class="category">
                                            <ul>
                                                <li> <?php print get_the_category(get_the_ID())[0]->name; ?></li>
                                            </ul>
                                        </div>
                                        <h2><?php echo get_the_title($all_posts->post->ID); ?></h2>
                                    </div>
                                    <?php if ($settings['select_show_hide_waves'] == 'yes'): ?>
                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                             preserveAspectRatio="none" shape-rendering="auto">
                                            <defs>
                                                <path id="gentle-wave"
                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                            </defs>
                                            <g class="parallax">
                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                     fill="rgba(255,255,255,0.7"></use>
                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                     fill="rgba(255,255,255,0.5)"></use>
                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                     fill="rgba(255,255,255,0.3)"></use>
                                                <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                            </g>
                                        </svg>
                                    <?php endif; ?>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <?php wp_reset_query(); ?>

                    <div class="col-lg-4 col-md-12 another-posts">
                        <?php if ($two_posts->have_posts()) : ?>
                            <?php while ($two_posts->have_posts()):$two_posts->the_post(); ?>
                                <a href="<?php the_permalink(); ?>">
                                    <figure>
                                        <img src="<?php the_post_thumbnail_url('post-size-two'); ?>"
                                             alt="<?php echo get_the_title($two_posts->post->ID); ?>">
                                    </figure>
                                    <div class="blog-posts-inner">
                                        <div class="category">
                                            <ul>
                                                <li> <?php print get_the_category(get_the_ID())[0]->name; ?></li>
                                            </ul>
                                        </div>
                                        <h2><?php echo get_the_title($two_posts->post->ID); ?></h2>
                                    </div>
                                    <?php if ($settings['select_show_hide_waves'] == 'yes'): ?>
                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                             preserveAspectRatio="none" shape-rendering="auto">
                                            <defs>
                                                <path id="gentle-wave"
                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                            </defs>
                                            <g class="parallax">
                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                     fill="rgba(255,255,255,0.7"></use>
                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                     fill="rgba(255,255,255,0.5)"></use>
                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                     fill="rgba(255,255,255,0.3)"></use>
                                                <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                            </g>
                                        </svg>
                                    <?php endif; ?>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if (!empty($settings['btn_cat_text'])) { ?>
                    <a href="<?php echo $url_btn_one ?>" <?php echo $url_btn_one_target; echo $url_btn_one_nofollow ?>
                       class="more-articles"><?php echo $settings['btn_cat_text'] ?></a>
                <?php }; ?>
            </div>
        </div>
    <?php } elseif ($settings['select_style_blog'] == "version_one") { ?>
        <?php if ($style_posts->have_posts()) : ?>
            <div class="related-posts pt-0">
                <?php while ($style_posts->have_posts()):$style_posts->the_post(); ?>
                    <div class="rpost">
                        <div class="post-inner">
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <?php
                                    if ($settings['style_format_post_blog'] == 'yes') {
                                        $format = get_post_format(get_the_ID());
                                        if ($format == 'audio') { ?>
                                            <div class="icon-format-blog d-flex align-middle">
                                                <i class="fal fa-music pr-3 m-auto"></i>
                                            </div>
                                        <?php } elseif ($format == 'video') { ?>
                                            <div class="icon-format-blog d-flex align-middle">
                                                <i class="fal fa-play pl-4 m-auto"></i>
                                            </div>
                                        <?php }
                                    }
                                    ?>
                                    <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                         alt="<?php echo get_the_title($style_posts->post->ID); ?>">
                                    <?php if ($settings['select_show_hide_waves'] == 'yes'): ?>
                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                             preserveAspectRatio="none" shape-rendering="auto">
                                            <defs>
                                                <path id="gentle-wave"
                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                            </defs>
                                            <g class="parallax">
                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                     fill="rgba(255,255,255,0.7"></use>
                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                     fill="rgba(255,255,255,0.5)"></use>
                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                     fill="rgba(255,255,255,0.3)"></use>
                                                <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                            </g>
                                        </svg>
                                    <?php endif; ?>
                                </figure>
                            </a>
                            <div class="post-detail py-2">
                                <div class="category m-0" style="color: #23527c !important;">
                                    <?php the_time('j F Y'); ?>
                                </div>
                                <a href="<?php the_permalink(); ?>">
                                <span class="title my-2"
                                      style="min-height: auto;font-size: 15px;line-height: 22px;height: 43px;"><?php the_title(); ?></span></a>
                                <div class="author d-flex mt-2 mb-1 align-middle">
                                    <div class="author-avatar author-avatar-blog">
                                        <?php echo get_avatar(get_the_author_meta('user_email'), '35', ''); ?>
                                    </div>
                                    <div class="author-info my-auto">
                                        <div class="author-name p-0"><?php the_author_link(); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php if (!empty($settings['btn_cat_text'])) { ?>
                    <div class="clearfix"></div>
                    <div class="blog-posts text-left">
                        <a href="<?php echo $url_btn_one ?>" <?php echo $url_btn_one_target; echo $url_btn_one_nofollow ?>
                           class="more-articles ml-4"><?php echo $settings['btn_cat_text'] ?></a>
                    </div>
                <?php }; ?>
            </div>
        <?php endif; ?>
    <?php } elseif ($settings['select_style_blog'] == "version_two") { ?>
        <div class="swiper-container swiper-blog">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php if ($style_posts->have_posts()) : ?>
                    <?php while ($style_posts->have_posts()):$style_posts->the_post(); ?>
                        <div class="related-posts swiper-slide pt-0">
                            <div class="rpost float-none pt-0 px-0 pb-2 w-auto">
                                <div class="post-inner">
                                    <a href="<?php the_permalink(); ?>">
                                        <figure>
                                            <?php
                                            if ($settings['style_format_post_blog'] == 'yes') {
                                                $format = get_post_format(get_the_ID());
                                                if ($format == 'audio') { ?>
                                                    <div class="icon-format-blog d-flex align-middle">
                                                        <i class="fal fa-music pr-3 m-auto"></i>
                                                    </div>
                                                <?php } elseif ($format == 'video') { ?>
                                                    <div class="icon-format-blog d-flex align-middle">
                                                        <i class="fal fa-play pl-4 m-auto"></i>
                                                    </div>
                                                <?php }
                                            } ?>
                                            <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                                 alt="<?php echo get_the_title($style_posts->post->ID); ?>">
                                            <?php if ($settings['select_show_hide_waves'] == 'yes'): ?>
                                                <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                     preserveAspectRatio="none" shape-rendering="auto">
                                                    <defs>
                                                        <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                    </defs>
                                                    <g class="parallax">
                                                        <use xlink:href="#gentle-wave" x="-12" y="11"
                                                             fill="rgba(255,255,255,0.7"></use>
                                                        <use xlink:href="#gentle-wave" x="25" y="13"
                                                             fill="rgba(255,255,255,0.5)"></use>
                                                        <use xlink:href="#gentle-wave" x="100" y="5"
                                                             fill="rgba(255,255,255,0.3)"></use>
                                                        <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                    </g>
                                                </svg>
                                            <?php endif; ?>
                                        </figure>
                                    </a>
                                    <div class="post-detail py-2">
                                        <div class="category m-0" style="color: #23527c !important;">
                                            <?php the_time('j F Y'); ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">
                                        <span class="title my-2"
                                              style="min-height: auto;font-size: 15px;line-height: 22px;height: 43px;"><?php the_title(); ?></span></a>
                                        <div class="author d-flex mt-2 mb-1">
                                            <div class="author-avatar author-avatar-blog">
                                                <?php echo get_avatar(get_the_author_meta('user_email'), '35', ''); ?>
                                            </div>
                                            <div class="author-info my-auto">
                                                <div class="author-name p-0"><?php the_author_link(); ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>
            </div>
            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev btn-prev-related op-blog-btn"></div>
            <div class="swiper-button-next btn-next-related op-blog-btn"></div>
        </div>
    <?php }; ?>

        <!-- End Blog -->

<?php
    }

//    protected function _content_template()
//    {
//        <h2 class="section-title m-0">{{{ settings.title_contents }}}</h2>
//    }
}
