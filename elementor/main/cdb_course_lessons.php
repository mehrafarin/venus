<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_course_lessons extends Widget_Base
{

    public function get_name()
    {
        return 'venus_course_lessons';
    }

    public function get_title()
    {
        return 'سرفصل دوره';
    }

    public function get_icon()
    {
        return 'eicon-bullet-list';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'section_title',
            [
                'label' => __('نمایش محصول', 'venus'),
            ]
        );

        $this->add_control(
            'id_tab',
            [
                'label' => __('آیدی سرفصل', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('به هیچ وجه آیدی انتخاب شده نباید با آیدی سر فصل های دیگر مشابه باشد(ترجیها عدد باشد)', 'venus'),
            ]
        );

        $this->add_control(
            'course_lesson_title', [
                'label' => __('عنوان سر فصل', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'course_lesson_subtitle', [
                'label' => __('توضیح کوتاه سرفصل', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('در این قسمت در حد یک جمله کوتاه در رابطه با سرفصل مورد نظر بنویسید.', 'venus'),

            ]
        );

        $this->add_control(
            'course_lesson_img',
            [
                'label' => __('تصویر شاخص', 'venus'),
                'type' => Controls_Manager::MEDIA,
//                'default' => [
//                    'url' => Utils::get_placeholder_image_src(),
//                ],
            ]
        );

        $course_lesson = new Repeater();

        $course_lesson->add_control(
            'id_meeting',
            [
                'label' => __('آیدی جلسه', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('به هیچ وجه آیدی انتخاب شده نباید با آیدی جلسات دیگر مشابه باشد حتی در سرفصل های دیگر همین محصول(ترجیها عدد باشد)', 'venus'),
            ]
        );

        $course_lesson->add_control(
            'title_meeting',
            [
                'label' => __('عنوان جلسه', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $course_lesson->add_control(
            'time_meeting',
            [
                'label' => __('زمان جلسه', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $course_lesson->add_control(
            'content_meeting',
            [
                'label' => __('محتوای جلسه', 'venus'),
                'type' => Controls_Manager::WYSIWYG,
                'label_block' => true,
            ]
        );

        $course_lesson->add_control(
            'private_lesson',
            [
                'label' => __('آیکون خصوصی', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('نمایش', 'venus'),
                'label_off' => __('پنهان', 'venus'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $course_lesson->add_control(
            'download_lesson',
            [
                'label' => __('لینک فایل دانلودی', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('لینک دانلود فایل این جلسه فقط بعد از خرید به کاربر نمایش داده می شود. لطفا لینک فایل های اصلی دوره رو توسط ووکامرس قراردهید تا کاربران نتوانند لینک دانلود را با بقیه به اشتراک بگذارند.(از اینجا برای مثال لینک های تمرینات و فایل های مورد نیاز رو بزارید که زیاد مهم نیستند )', 'venus'),
            ]
        );

        $course_lesson->add_control(
            'txt_btn_preview',
            [
                'label' => __('حالت نمایش دکمه پیش نمایش', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus'),
                'label_off' => __('غیرفعال', 'venus'),
                'return_value' => 'yes',
                'default' => 'yes',
                'description' => __('با فعال کردن این بخش فقط ایکون پیش نمایش ، نمایش داده خواهد شد.', 'venus'),
            ]
        );

        $course_lesson->add_control(
            'text_btn_preview_video',
            [
                'label' => __('متن دکمه پیش نمایش ویدئویی', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => __('پیش نمایش', 'venus'),
            ]
        );

        $course_lesson->add_control(
            'preview_video',
            [
                'label' => __('لینک پیش نمایش ویدئویی', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('این ویدئو به صورت پاپ آپ هنگام کلیک روی دکمه پیشنمایش باز خواهد شد. (فقط لینک مستقیم ویدئو را وارد کنید).', 'venus'),
            ]
        );

        $this->add_control(
            'cdb_course_lesson_meeting',
            [
                'label' => __('جلسات', 'venus'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $course_lesson->get_controls(),
                'title_field' => '{{{ title_meeting }}}',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_section',
            [
                'label' => __( 'استایل', 'venus' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'btn_color_preview',
            [
                'label' => __('رنگ دکمه پیش نمایش', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
                'selectors' => [
                    '{{WRAPPER}} .btn-preview-lesson' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_section();


    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        ?>
        <div>
            <div>
                <?php if (!empty($settings['course_lesson_title'])): ?>
                    <div class="card ve-course-lessons" id="accordion_tab">
                        <div class="card-header" id="heading<?php echo $settings['id_tab']; ?>">
                            <button class="btn btn-link d-flex collapsed" data-toggle="collapse"
                                    data-target="#jal<?php echo $settings['id_tab']; ?>" aria-expanded="true"
                                    aria-controls="jal<?php echo $settings['id_tab']; ?>">
                                <i>
                                    <img src="<?php echo $settings['course_lesson_img']['url']; ?>" alt="">
                                </i>
                                <div class="lesson-inner text-right my-auto mr-3">
                                    <div class="lesson-title mb-2"><?php echo $settings['course_lesson_title']; ?></div>
                                    <?php if (!empty($settings['course_lesson_subtitle'])) : ?><p
                                            class="m-0"><?php echo $settings['course_lesson_subtitle']; ?></p><?php endif; ?>
                                </div>
                                <i class="fas fa-chevron-down my-auto ml-2 mr-auto"></i>
                            </button>
                        </div>
                        <div id="jal<?php echo $settings['id_tab']; ?>" class="collapse in" aria-labelledby="heading<?php echo $settings['id_tab']; ?>" data-parent="#accordion_tab" aria-expanded="true">
                            <div class="card-body">
                                <ul class="lessons-list">
                                    <?php foreach ($settings['cdb_course_lesson_meeting'] as $item) { ?>

                                        <!-- Start -->
                                        <?php
                                        $bought_course = false;
                                        $current_user = wp_get_current_user();
//                                        if (!empty($current_user->user_email) and !empty($current_user->ID)) {
//
//                                        }
                                        if (wc_customer_bought_product($current_user->user_email, $current_user->ID, get_the_id())) {
                                            $bought_course = true;
                                        }
                                        global $product;
                                        $current_user = wp_get_current_user();
                                        ?>
                                        <?php if (!empty($item['title_meeting'])): ?>
                                            <li>
                                                <div class="download-detail" id="accordion_meeting">
                                                    <div class="detail-header d-flex" id="detail-heading0">
                                                        <button class="sub-btn collapsed" data-toggle="collapse" data-target="#dcollapse<?php echo $item['id_meeting']; ?>" aria-expanded="false" aria-controls="collapse<?php echo $item['id_meeting']; ?>"></button>
                                                        <a class="session collapsed" data-toggle="collapse" data-target="#dcollapse<?php echo $item['id_meeting']; ?>" aria-expanded="false" aria-controls="collapse<?php echo $item['id_meeting']; ?>">
                                                            <?php echo $item['title_meeting']; ?>
                                                        </a>
                                                        <?php if ((!empty($item['preview_video']))) : ?>
                                                            <div class="info-lesson-venus h-fit mr-auto">
                                                                <?php if (!empty($item['preview_video'])): ?>
                                                                    <a class="btn-preview-lesson preview-button" href="<?php echo $item['preview_video'] ?>">
                                                                        <i class="fal fa-play ml-1 <?php if (empty($item['txt_btn_preview'])) : ?>ml-xl-2<?php endif; ?>"></i>
                                                                        <?php if (empty($item['txt_btn_preview'])) : ?>
                                                                            <span>
                                                                            <?php
                                                                            //    esc_html_e('پیش نمایش', 'venus');
                                                                            echo $item['text_btn_preview_video']; ?>
                                                                            </span>
                                                                        <?php endif; ?>
                                                                    </a>
                                                                <?php endif; ?>
                                                            </div>
                                                        <?php endif; ?>

                                                        <?php if (!empty($item['download_lesson'])): ?>
                                                            <?php if ($bought_course): ?>
                                                                <a class="btn-down-lesson tag-a-lesson my-lg-auto <?php if (empty($item['preview_video'])) : ?>mr-auto<?php endif; ?>"
                                                                   href="<?php echo $item['download_lesson']; ?>"><i
                                                                            class="fal fa-download d-block"></i></a>
                                                            <?php else: ?>
                                                                <div class="btn-down-lesson my-lg-auto <?php if (empty($item['preview_video'])) : ?>mr-auto<?php endif; ?>">
                                                                    <i class="fal fa-download d-block"></i></div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>

                                                        <?php if (!empty($item['private_lesson'])): ?>
                                                            <div class="btn-private-lesson text-center <?php if (empty($item['preview_video']) & empty($item['download_lesson'])) : ?>mr-auto<?php endif; ?>">
                                                                <?php

                                                                if (wc_customer_bought_product($current_user->user_ID, $current_user->ID, $product->get_id())) {
                                                                    echo '<i class="fal fa-lock-open-alt"></i>';
                                                                } else {
                                                                    echo '<i class="fal fa-lock-alt"></i>';
                                                                }
                                                                ?>
                                                                <span>
                                                                    <?php
                                                                    if (wc_customer_bought_product($current_user->user_ID, $current_user->ID, $product->get_id())) {
                                                                        esc_html_e('در دسترس', 'venus');
                                                                    } else {
                                                                        esc_html_e('خصوصی', 'venus');
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="time"><?php echo $item['time_meeting']; ?></div>
                                                    <div id="dcollapse<?php echo $item['id_meeting']; ?>" class="collapse" aria-labelledby="detail-heading1" data-parent="#accordion_meeting" aria-expanded="false" style="height: 0px;">
                                                        <div class="detail-list">
                                                            <?php
                                                            if (!empty($item['private_lesson']) and $item['private_lesson']) {
                                                                if ($bought_course) {
                                                                    echo $item['content_meeting'];
                                                                } else {
                                                                        esc_html_e('برای مشاهده این جلسه باید دوره را خریداری کنید.', 'venus');
                                                                }
                                                            } else {
                                                                echo $item['content_meeting'];
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif; ?>


                                        <!-- End -->

                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php

    }


}