<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_faqs extends Widget_Base
{

    public function get_name()
    {
        return 'venus-faqs';
    }

    public function get_title()
    {
        return 'سوال';
    }

    public function get_icon()
    {
        return 'eicon-blockquote';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('سوالات متداول', 'venus'),
            ]
        );

        $this->add_control(
            'id_faq',
            [
                'label' => __('آیدی سوال', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('به هیچ وجه آیدی انتخاب شده نباید با آیدی سر سوال های دیگر مشابه باشد(ترجیها عدد باشد)', 'venus'),
            ]
        );

        $this->add_control(
            'title_faq',
            [
                'label' => __('سوال', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('سوال مورد نظر خود را بنویسید', 'venus'),
            ]
        );

        $this->add_control(
            'answer_faq',
            [
                'label' => __('جواب سوال', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('جواب سوال را بنویسید', 'venus'),
            ]
        );

        $this->end_controls_section();


    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        ?>

        <div class="row">
            <div class="col-12">
                <div class="panel-group ve-faq-panel-group" id="accordion">
                    <?php if (!empty($settings['title_faq'])): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <button class="accordion-toggle collapsed" data-toggle="collapse"
                                            data-target="#faq_<?php echo $settings['id_faq']; ?>" aria-expanded="true"
                                            aria-controls="faq_<?php echo $settings['id_faq']; ?>">
                                        <?php echo $settings['title_faq']; ?>
                                    </button>
                                </h4>
                            </div>
                            <div id="faq_<?php echo $settings['id_faq']; ?>" class="collapse in"
                                 aria-labelledby="heading0" data-parent="#accordion"
                                 aria-expanded="true" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?php echo $settings['answer_faq']; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php

    }
}
