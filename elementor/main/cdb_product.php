<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_product extends Widget_Base
{

    public function get_name()
    {
        return 'widget-product';
    }

    public function get_title()
    {
        return 'نمایش محصول';
    }

    public function get_icon()
    {
        return 'eicon-gallery-grid';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {

        $venus_product_cate = array();
        $categories = get_terms("product_cat");
        if (!empty($categories) && !is_wp_error($categories)) {
            foreach ($categories as $category) {
                $venus_product_cate[$category->slug] = $category->name;
            }
        }

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('نمایش محصول', 'venus-theme'),
            ]
        );

        $this->add_control(
            'courses_type',
            [
                'label' => __('نوع نمایش دوره ها', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'default' => 'all',
                'options' => [
                    'all' => __('همه دوره ها', 'venus-theme'),
                    'onsale' => __('دوره های تخفیف خورده', 'venus-theme'),
                ],
                'label_block' => true,
            ]
        );

        $this->add_control(
            'courses_cat_include',
            [
                'label' => __('دسته بندی دوره ها', 'venus-theme'),
                'type' => Controls_Manager::SELECT2,
                'options' => $venus_product_cate,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('تعداد نمایش محصولات', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => '6',
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('مرتب سازی بر اساس', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'date' => __('جدیدترین', 'venus-theme'),
                    'modified' => __('آخرین بروزرسانی', 'venus-theme'),
                    'sales' => __('بیشترین فروش', 'venus-theme'),
                    'rand' => __('تصادفی', 'venus-theme'),
                    'price' => __('قیمت', 'venus-theme'),
                ],
                'label_block' => true,
            ]
        );

        $this->add_control(
            'select_style_product',
            [
                'label' => __('انتخاب استایل نمایش محصولات', 'venus-theme'),
                'type' => Controls_Manager::SELECT,
                'default' => 'default',
                'options' => [
                    'default' => __('پیش فرض', 'venus-theme'),
                    'version_one' => __('ورژن 1', 'venus-theme'),
                ],
                'label_block' => true,
            ]
        );

        $this->add_control(
            'select_style_sidebar_home',
            [
                'label' => __('حالت نمایش', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus-theme'),
                'label_off' => __('غیرفعال', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'No',
                'description' => __('برای استفاده در صفحه سایدبار دار فعال کنید.(کاربرد برای استایل پیش فرض)', 'venus-theme'),
            ]
        );

        $this->add_responsive_control(
            'venus_title_alignment',
            [
                'label' => esc_html__('موقعیت عنوان محصول', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'right' => [
                        'title' => esc_html__('راست', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                    'center' => [
                        'title' => esc_html__('وسط', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'left' => [
                        'title' => esc_html__('چپ', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .course .course-detail .course-top-content h2' => 'text-align: {{VALUE}};'
                ],
                'default' => 'right',
            ]
        );

        $this->add_responsive_control(
            've_des_product_alignment',
            [
                'label' => esc_html__('موقعیت توضیحات محصول', 'venus-theme'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'right' => [
                        'title' => esc_html__('راست', 'venus-theme'),
                        'icon' => 'fa fa-align-right',
                    ],
                    'center' => [
                        'title' => esc_html__('وسط', 'venus-theme'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'left' => [
                        'title' => esc_html__('چپ', 'venus-theme'),
                        'icon' => 'fa fa-align-left',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .course .course-detail .course-top-content .course-description' => 'text-align: {{VALUE}};'
                ],
                'default' => 'right',
            ]
        );

        $this->add_control(
            'btn_title_link_product',
            [
                'label' => __('متن دکمه', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('تمامی دوره ها', 'venus-theme'),
                'description' => __('در صورت خالی گذاشتن غیرفعال می شود.', 'venus-theme'),
            ]
        );


        $this->add_control(
            'link_btn_product',
            [
                'label' => __('لینک دکمه (Url)', 'venus-theme'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'venus-theme'),
                'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'product_style',
            [
                'label' => __('استایل', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title_product',
            [
                'label' => __('رنگ عنوان محصول', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#007bff',
                'selectors' => [
                    '{{WRAPPER}} .course .course-detail .course-top-content h2' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'des_product',
            [
                'label' => __('رنگ توضیحات محصول', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'default' => '#212529',
                'selectors' => [
                    '{{WRAPPER}} .course .course-detail .course-top-content .course-description' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'bg_color_btn_product',
            [
                'label' => __('رنگ دکمه', 'venus-theme'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'description' => __('رنگ دکمه مشاهده همه محصولات', 'venus-theme'),
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'info_product',
            [
                'label' => __('اطلاعات دوره', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'style_info_bottom_product',
            [
                'label' => __('اطلاعات دوره', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus-theme'),
                'label_off' => __('غیرفعال', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'yes',
                'description' => __('حالت نمایشی(سطح دوره ، نوع دوره ، هزینه دوره)', 'venus-theme'),
            ]
        );

        $this->add_control(
            'money_only_show_product',
            [
                'label' => __('نمایش هزینه دوره', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus-theme'),
                'label_off' => __('غیرفعال', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'no',
                'description' => esc_html__('در صورت غیرفعال بودن فقط هزینه دوره نمایش داده خواهد شد.', 'venus-theme'),
            ]
        );

        $this->add_control(
            'select_show_hide_waves_product',
            [
                'label' => __('موج ها', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('نمایش', 'venus-theme'),
                'label_off' => __('پنهان', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        ?>
        <?php
        global $venus_options;

        $args = array(
            'post_type' => 'product',
            'posts_per_page' => $settings['posts_per_page'],
            'orderby' => $settings['orderby'],
            'meta_query' => array(),
            'tax_query' => array(
                'relation' => 'AND',
            ),
        );

        $args['meta_query'][] = array(
            'key' => '_price',
            'value' => 0,
            'compare' => '>=',
            'type' => 'DECIMAL',
        );

        switch ($settings['courses_type']) {
            case 'onsale':
                $product_ids_on_sale = wc_get_product_ids_on_sale();
                $product_ids_on_sale[] = 0;
                $args['post__in'] = $product_ids_on_sale;
                break;
        }

        switch ($settings['orderby']) {
            case 'price':
                $args['meta_key'] = '_price';
                $args['orderby'] = 'meta_value_num';
                break;
            case 'sales':
                $args['meta_key'] = 'total_sales';
                $args['orderby'] = 'meta_value_num';
                break;
            default:
                $args['orderby'] = $settings['orderby'];
        }

        if (!empty($settings['courses_cat_include'])) {
            $cat_include = array();
            $settings['courses_cat_include'] = explode(',', $settings['courses_cat_include']);
            foreach ($settings['courses_cat_include'] as $category) {
                $term = term_exists($category, 'product_cat');
                if ($term !== 0 && $term !== null) {
                    $cat_include[] = $term['term_id'];
                }
            }
            if (!empty($cat_include)) {
                $args['tax_query'][] = array(
                    'taxonomy' => 'product_cat',
                    'terms' => $cat_include,
                    'operator' => 'IN',
                );
            }
        }

        $products_query = new \WP_Query($args);

        $url_btn_pro = $settings['link_btn_product']['url'];
        $url_btn_pro_target = $settings['link_btn_product']['is_external'] ? ' target="_blank"' : '';
        $url_btn_pro_nofollow = $settings['link_btn_product']['nofollow'] ? ' rel="nofollow"' : '';
        ?>
        <?php if ($settings['bg_color_btn_product'] != '') { ?>
        <style>
            .courses .course-more {
                background-color: <?php echo esc_attr($settings['bg_color_btn_product']); ?>;
                box-shadow: 0 2px 12px <?php echo esc_attr($settings['bg_color_btn_product']); ?>99;
            }

            .courses .course-more:hover {
                background-color: <?php echo esc_attr($settings['bg_color_btn_product']); ?>;
                box-shadow: 0 2px 12px<?php echo esc_attr($settings['bg_color_btn_product']); ?>;
            }
        </style>
    <?php }; ?>
        <!-- Start courses -->
        <?php if ($settings['select_style_product'] == "default") { ?>
        <div class="courses">
            <div class="text-left">
                <div class="row">
                    <?php if ($products_query->have_posts()) : $i = 0; ?>
                        <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                            <?php if ($settings['select_style_sidebar_home'] == 'yes') { ?>
                                <div class="col-sm-12 col-md-6 course">
                                    <div class="course-inner">
                                        <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                            <?php
                                            venus_course_status();
                                            $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                            if ($cu_status != 'soon') {
                                                if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                    <div class="course-item-sale">
                                                        <?php
                                                        global $product;

                                                        if ($product->is_on_sale()) {

                                                            if (!$product->is_type('variable')) {

                                                                $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                            } else {

                                                                $max_percentage = 0;

                                                                foreach ($product->get_children() as $child_id) {
                                                                    $variation = wc_get_product($child_id);
                                                                    $price = $variation->get_regular_price();
                                                                    $sale = $variation->get_sale_price();
                                                                    if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                    if ($percentage > $max_percentage) {
                                                                        $max_percentage = $percentage;
                                                                    }
                                                                }

                                                            }
                                                            echo "<div class='sale-perc-badge'>";
                                                            echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                            echo "<div class='sale-badge-text'>تخفیف</div>";
                                                            echo "</div>";
                                                        }
                                                        ?>
                                                    </div>
                                                <?php }
                                            }
                                            ?>
                                            <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                                <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         viewBox="0 24 150 28"
                                                         preserveAspectRatio="none" shape-rendering="auto">
                                                        <defs>
                                                            <path id="gentle-wave"
                                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                        </defs>
                                                        <g class="parallax">
                                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                 fill="rgba(255,255,255,0.7"></use>
                                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                                 fill="rgba(255,255,255,0.5)"></use>
                                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                                 fill="rgba(255,255,255,0.3)"></use>
                                                            <use xlink:href="#gentle-wave" x="90" y="7"
                                                                 fill="#fff"></use>
                                                        </g>
                                                    </svg>
                                                <?php endif; ?>
                                            </a>
                                            <?php
                                            $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                            if ($link_show_course_intro_video) : ?>
                                                <div class="video-button">
                                                    <a data-post-id="<?php echo get_the_ID(); ?>"
                                                       href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                       class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                </div>
                                            <?php endif;
                                            if ($cu_status != 'soon') {
                                                woocommerce_template_loop_add_to_cart();
                                            }
                                            ?>
                                        </header>
                                        <div class="course-detail">
                                            <div class="course-top-content">
                                                <a href="<?php the_permalink(); ?>" class="title">
                                                    <h2><?php the_title(); ?></h2>
                                                </a>
                                                <div class="course-description">
                                                    <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                </div>
                                            </div>
                                            <?php if ($settings['style_info_bottom_product'] == true) { ?>
                                                <div class="course-information position-relative d-flex">
                                                    <?php if ($settings['money_only_show_product'] != 'yes') { ?>
                                                        <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                            <div class="col p-0 text-right">
                                                                <span class="level-course">سطح</span>
                                                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                            </div>
                                                        <?php } else {
                                                        } ?>
                                                        <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                            <div class="col p-0 text-right">
                                                                <span class="level-course">دوره</span>
                                                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                            </div>
                                                        <?php } else {
                                                        } ?>
                                                    <?php } ?>
                                                    <div class="col p-0 text-center price-course m-auto">
                                                        <?php
                                                        global $product;
                                                        echo $product->get_price_html(); ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="col-sm-12 col-md-6 col-xl-4 course">
                                    <div class="course-inner">
                                        <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                            <?php
                                            venus_course_status();
                                            $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                            if ($cu_status != 'soon') {
                                                if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                    <div class="course-item-sale">
                                                        <?php
                                                        global $product;

                                                        if ($product->is_on_sale()) {

                                                            if (!$product->is_type('variable')) {

                                                                $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                            } else {

                                                                $max_percentage = 0;

                                                                foreach ($product->get_children() as $child_id) {
                                                                    $variation = wc_get_product($child_id);
                                                                    $price = $variation->get_regular_price();
                                                                    $sale = $variation->get_sale_price();
                                                                    if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                    if ($percentage > $max_percentage) {
                                                                        $max_percentage = $percentage;
                                                                    }
                                                                }

                                                            }
                                                            echo "<div class='sale-perc-badge'>";
                                                            echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                            echo "<div class='sale-badge-text'>تخفیف</div>";
                                                            echo "</div>";
                                                        }
                                                        ?>
                                                    </div>
                                                <?php }
                                            } ?>
                                            <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                                <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         viewBox="0 24 150 28"
                                                         preserveAspectRatio="none" shape-rendering="auto">
                                                        <defs>
                                                            <path id="gentle-wave"
                                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                        </defs>
                                                        <g class="parallax">
                                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                                 fill="rgba(255,255,255,0.7"></use>
                                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                                 fill="rgba(255,255,255,0.5)"></use>
                                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                                 fill="rgba(255,255,255,0.3)"></use>
                                                            <use xlink:href="#gentle-wave" x="90" y="7"
                                                                 fill="#fff"></use>
                                                        </g>
                                                    </svg>
                                                <?php endif; ?>
                                            </a>
                                            <?php
                                            $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                            if ($link_show_course_intro_video) : ?>
                                                <div class="video-button">
                                                    <a data-post-id="<?php echo get_the_ID(); ?>"
                                                       href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                       class="intro-play-icon"><i class="fal fa-play"></i></a>
                                                </div>
                                            <?php endif;
                                            if ($cu_status != 'soon') {
                                                woocommerce_template_loop_add_to_cart();
                                            }
                                            ?>
                                        </header>
                                        <div class="course-detail">
                                            <div class="course-top-content">
                                                <a href="<?php the_permalink(); ?>" class="title">
                                                    <h2><?php the_title(); ?></h2>
                                                </a>
                                                <div class="course-description">
                                                    <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                                </div>
                                            </div>
                                            <?php if ($settings['style_info_bottom_product'] == true) { ?>
                                                <div class="course-information position-relative d-flex">
                                                    <?php if ($settings['money_only_show_product'] != 'yes') { ?>
                                                        <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                            <div class="col p-0 text-right">
                                                                <span class="level-course">سطح</span>
                                                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                            </div>
                                                        <?php } else {
                                                        } ?>
                                                        <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                            <div class="col p-0 text-right">
                                                                <span class="level-course">دوره</span>
                                                                <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                            </div>
                                                        <?php } else {
                                                        } ?>
                                                    <?php } ?>
                                                    <div class="col p-0 text-center price-course m-auto">
                                                        <?php
                                                        global $product;
                                                        echo $product->get_price_html();
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $i++; endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php else : ?>
                        <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                    <?php endif; ?>
                </div>
                <?php if (!empty($settings['btn_title_link_product'])) { ?>
                    <a href="<?php echo $url_btn_pro; ?>" <?php echo $url_btn_pro_target . $url_btn_pro_nofollow ?>
                       class="course-more"><?php echo $settings['btn_title_link_product'] ?></a>
                <?php } ?>
            </div>
        </div>
        <!-- End courses -->
    <?php } elseif ($settings['select_style_product'] == "version_one") { ?>

        <div class="swiper-container swiper-product" data-loop="false">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <?php if ($products_query->have_posts()) : $i = 0; ?>
                    <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                        <div class="course pt-0 swiper-slide">
                            <div class="course-inner">
                                <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                    <?php
                                    venus_course_status();
                                    $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                    if ($cu_status != 'soon') {
                                        if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                            <div class="course-item-sale">
                                                <?php
                                                global $product;

                                                if ($product->is_on_sale()) {

                                                    if (!$product->is_type('variable')) {

                                                        $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                    } else {

                                                        $max_percentage = 0;

                                                        foreach ($product->get_children() as $child_id) {
                                                            $variation = wc_get_product($child_id);
                                                            $price = $variation->get_regular_price();
                                                            $sale = $variation->get_sale_price();
                                                            if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                            if ($percentage > $max_percentage) {
                                                                $max_percentage = $percentage;
                                                            }
                                                        }

                                                    }
                                                    echo "<div class='sale-perc-badge'>";
                                                    echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                    echo "<div class='sale-badge-text'>تخفیف</div>";
                                                    echo "</div>";
                                                }
                                                ?>
                                            </div>
                                        <?php }
                                    } ?>
                                    <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                        <?php if ($settings['select_show_hide_waves_product'] == true): ?>
                                            <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                 preserveAspectRatio="none" shape-rendering="auto">
                                                <defs>
                                                    <path id="gentle-wave"
                                                          d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                </defs>
                                                <g class="parallax">
                                                    <use xlink:href="#gentle-wave" x="-12" y="11"
                                                         fill="rgba(255,255,255,0.7"></use>
                                                    <use xlink:href="#gentle-wave" x="25" y="13"
                                                         fill="rgba(255,255,255,0.5)"></use>
                                                    <use xlink:href="#gentle-wave" x="100" y="5"
                                                         fill="rgba(255,255,255,0.3)"></use>
                                                    <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                </g>
                                            </svg>
                                        <?php endif; ?>
                                    </a>
                                    <?php
                                    $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                    if ($link_show_course_intro_video) : ?>
                                        <div class="video-button">
                                            <a data-post-id="<?php echo get_the_ID(); ?>"
                                               href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                               class="intro-play-icon"><i class="fal fa-play"></i></a>
                                        </div>
                                    <?php endif;
                                    if ($cu_status != 'soon') {
                                        woocommerce_template_loop_add_to_cart();
                                    }
                                    ?>
                                </header>
                                <div class="course-detail">
                                    <div class="course-top-content">
                                        <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2>
                                        </a>
                                        <div class="course-description">
                                            <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                        </div>
                                    </div>
                                    <?php if ($settings['style_info_bottom_product'] == true) { ?>
                                        <div class="course-information position-relative d-flex">
                                            <?php if ($settings['money_only_show_product'] != 'yes') { ?>
                                                <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                    <div class="col p-0 text-right">
                                                        <span class="level-course">سطح</span>
                                                        <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                    </div>
                                                <?php } else {
                                                } ?>
                                                <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                    <div class="col p-0 text-right">
                                                        <span class="level-course">دوره</span>
                                                        <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                    </div>
                                                <?php } else {
                                                } ?>
                                            <?php } ?>
                                            <div class="col p-0 text-center price-course m-auto">
                                                <?php
                                                global $product;
                                                echo $product->get_price_html();
                                                ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php $i++; endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php else : ?>
                    <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                <?php endif; ?>
            </div>
            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev btn-prev-related op-blog-btn"></div>
            <div class="swiper-button-next btn-next-related op-blog-btn"></div>
        </div>
        <!--    <div class="swiper-pagination w-100"></div>-->
    <?php } ?>

        <?php wp_reset_postdata(); ?>

        <?php

    }
}
