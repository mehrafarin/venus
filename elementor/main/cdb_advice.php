<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_advice extends Widget_Base
{

    public function get_name()
    {
        return 'venus-advice';
    }

    public function get_title()
    {
        return 'درخواست مشاوره';
    }

    public function get_icon()
    {
        return 'eicon-form-horizontal';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('درخواست مشاوره', 'venus'),
            ]
        );

        $this->add_control(
            'text_one_advice',
            [
                'label' => __('متن', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('میتوانید متن دلخواه خود را وارد کنید', 'venus'),
            ]
        );

        $this->add_control(
            'shortcut_advice',
            [
                'label' => __('شورت کد فرم مشاوره', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('شورت کات فرم ساز خود را وارد نمایید', 'venus'),
            ]
        );

        $this->add_control(
            'image_advice',
            [
                'label' => __('تصویر پس زمینه', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::MEDIA,
                'placeholder' => __('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
                'default' => [
                    'url' => '',
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'info_call',
            [
                'label' => __('اطلاعات تماس', 'venus'),
            ]
        );

        $this->add_control(
            'text_call_advice',
            [
                'label' => __('متن شماره تماس', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => __('شماره تماس :', 'venus'),
                'description' => __('میتوانید متن دلخواه خود را وارد کنید', 'venus'),
            ]
        );

        $this->add_control(
            'number_call_advice',
            [
                'label' => __('شماره تماس', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'description' => __('میتوانید متن دلخواه خود را وارد کنید', 'venus'),
            ]
        );

        $this->add_control(
            'link_call_advice',
            [
                'label' => __('لینک (Url)', 'venus'),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'placeholder' => __('tel:09300512632', 'venus'),
                'description' => __('برای مثال (tel:02100000)', 'venus'),
                'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                ],
            ]
        );

        $this->add_control(
            'text_so_advice',
            [
                'label' => __('متن دلخواه', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => __('اینستاگرام : ', 'venus'),
                'description' => __('میتوانید متن دلخواه خود را وارد کنید', 'venus'),
            ]
        );

        $this->add_control(
            'number_so_advice',
            [
                'label' => __('متن دلخواه', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => __('bigersoft@', 'venus'),
                'description' => __('میتوانید متن دلخواه خود را وارد کنید', 'venus'),
            ]
        );

        $this->add_control(
            'link_so_advice',
            [
                'label' => __('لینک (Url)', 'venus'),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'placeholder' => __('https://instagram.com/bigersoft', 'venus'),
                'description' => __('لینک برای متن دلخواه', 'venus'),
                'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        $target_call = $settings['link_call_advice']['is_external'] ? ' target="_blank"' : '';
        $nofollow_cal = $settings['link_call_advice']['nofollow'] ? ' rel="nofollow"' : '';
        $target_so = $settings['link_call_advice']['is_external'] ? ' target="_blank"' : '';
        $nofollow_so = $settings['link_call_advice']['nofollow'] ? ' rel="nofollow"' : '';
        ?>
        <!-- START Advice -->
<!--        <div id="course-advice">-->
            <div class="row style-form">
                <div class="advice col-md-12">
                    <div class="row advice-content">
                        <div class="col-12 col-lg-7 py-3 form-box">
                            <div class="text mb-3">
                                <?php echo $settings['text_one_advice']; ?>
                            </div>
                            <div class="form-contact my-4">
                                <?php echo do_shortcode($settings['shortcut_advice']); ?>
                            </div>
                        </div>
                        <div class="d-none col-lg-5 pic-box d-lg-flex" style="background: url(<?php echo $settings['image_advice']['url']; ?>);">
                            <div class="contact-us w-100 mt-auto">
                                <a class="item d-flex justify-content-between" href="<?php echo $settings['link_call_advice']['url']; ?>"<?php echo $target_call . $nofollow_cal ?>>
                                    <span class="name"><?php echo $settings['text_call_advice']; ?></span>
                                    <span class="content"><?php echo $settings['number_call_advice']; ?></span>
                                </a>
                                <hr style="margin-top: 1rem !important;margin-bottom: 1rem !important;">
                                <p>
                                    <a class="item d-flex justify-content-between" href="<?php if(!empty($settings['link_so_advice']['url'])): echo $settings['link_so_advice']['url']; endif; ?>"<?php echo $target_so . $nofollow_so ?> >
                                        <span class="name"><?php echo $settings['text_so_advice']; ?></span>
                                        <span class="content"><?php echo $settings['number_so_advice']; ?></span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--        </div>-->
        <!-- END Advice -->
        <?php

    }
}
