<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class venus_student_contacts extends Widget_Base
{

    public function get_name()
    {
        return 'venus-student-contacts';
    }

    public function get_title()
    {
        return 'مخاطبین دوره';
    }

    public function get_icon()
    {
        return 'eicon-person';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('مخاطبین دوره', 'venus'),
            ]
        );
        $this->add_control(
            'top_title_course_contacts',
            [
                'label' => __('محتوای باکس', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $need = new Repeater();
        $need->add_control(
            'switch_unreq',
            [
                'label' => __('توصیه', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('می شود', 'venus'),
                'label_off' => __('نمی شود', 'venus'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );
        $need->add_control(
            'title_unreq',
            [
                'label' => __('عنوان توصیه', 'venus'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );
        $need->add_control(
            'content',
            [
                'label' => __('محتوای باکس', 'venus'),
                'type' => Controls_Manager::WYSIWYG,
                'label_block' => true,
            ]
        );
        $this->add_control(
            'unreq',
            [
                'label' => __('توصیه', 'venus'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $need->get_controls(),
                'title_field' => '{{{ title_unreq }}}',
            ]
        );
        $this->end_controls_section();
        $this->start_controls_section(
            'img_reg',
            [
                'label' => __('تصاویر', 'venus'),
            ]
        );
        $this->add_control(
            'pic_left',
            [
                'label' => __('تصاویر', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus'),
                'label_off' => __('غیرفعال', 'venus'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );
        $this->add_control(
            'image_req_one',
            [
                'label' => __('تصویر اول', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::MEDIA,
                'placeholder' => __('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
                'default' => [
                    'url' => '',
                ]
            ]
        );
        $this->add_control(
            'image_req_two',
            [
                'label' => __('تصویر دوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::MEDIA,
                'placeholder' => __('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
                'default' => [
                    'url' => '',
                ]
            ]
        );

        $this->add_control(
            'image_req_three',
            [
                'label' => __('تصویر سوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::MEDIA,
                'placeholder' => __('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
                'default' => [
                    'url' => '',
                ]
            ]
        );

        $this->add_control(
            'image_req_four',
            [
                'label' => __('تصویر چهارم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::MEDIA,
                'placeholder' => __('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
                'default' => [
                    'url' => '',
                ]
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'style_section',
            [
                'label' => __( 'استایل', 'venus' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_green_text',
            [
                'label' => __('رنگ متن توصیه می شود', 'venus'),
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
                'selectors' => [
                    '{{WRAPPER}} .course-requirement .requirement-right .unreq .unreq-title ' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'color_green',
            [
                'label' => __('رنگ دایره توصیه می شود', 'venus'),
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
                'selectors' => [
                    '{{WRAPPER}} .course-requirement .requirement-right .unreq ul li:before , .course-requirement .requirement-right .unreq .unreq-title:before' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'color_show',
                'label' => __('رنگ سایه دایره توصیه می شود', 'venus'),
                'selector' => '{{WRAPPER}} .course-requirement .requirement-right .unreq ul li:before , .course-requirement .requirement-right .unreq .unreq-title:before',
            ]
        );

        $this->add_control(
            'color_red_text',
            [
                'label' => __('رنگ متن توصیه نمی شود', 'venus'),
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
                'selectors' => [
                    '{{WRAPPER}} .course-requirement .requirement-right .req .req-title ' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'color_red',
            [
                'label' => __('رنگ دایره توصیه نمی شود', 'venus'),
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
                'selectors' => [
                    '{{WRAPPER}} .course-requirement .requirement-right .req .req-title:before , .course-requirement .requirement-right .req ul li:before' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'color_show_red',
                'label' => __('رنگ سایه دایره توصیه می شود', 'venus'),
                'selector' => '{{WRAPPER}} .course-requirement .requirement-right .req .req-title:before , .course-requirement .requirement-right .req ul li:before',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        ?>
        <div class="course-requirement mt-0">
            <div class="row">
                <div class="col-12 col-xl-7 requirement-right">
                    <p class="text-justify"><?php echo $settings['top_title_course_contacts']; ?></p>
                    <?php foreach ($settings['unreq'] as $item) {
                        if ($item['switch_unreq'] == true) {
                            ?>
                            <div class="unreq">
                                <div class="unreq-title"><?php echo $item['title_unreq']; ?></div>
                                <?php echo do_shortcode($item['content']); ?>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="req">
                                <div class="req-title"><?php echo $item['title_unreq']; ?></div>
                                <?php echo do_shortcode($item['content']); ?>
                            </div>
                            <?php
                        }
                        ?>
                    <?php } ?>
                </div>
                <?php if ($settings['pic_left']): ?>
                <div class="d-none d-xl-block col-md-5 requirement-left px-1">
                    <div class="requirement-left-inner">
                        <div class="req-photo1"
                             style="background: url(<?php echo $settings['image_req_one']['url']; ?>) no-repeat;"></div>
                        <div class="req-photo2"
                             style="background: url(<?php echo $settings['image_req_two']['url']; ?>) no-repeat;"></div>
                        <div class="req-photo3"
                             style="background: url(<?php echo $settings['image_req_three']['url']; ?>) no-repeat;"></div>
                        <div class="req-photo4"
                             style="background: url(<?php echo $settings['image_req_four']['url']; ?>) no-repeat;"></div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php

    }
}
