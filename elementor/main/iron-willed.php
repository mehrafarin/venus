<?php

namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Venus_Iron_Willed extends Widget_Base
{

    public function get_name()
    {
        return 'widget-iron-willed';
    }

    public function get_title()
    {
        return 'نظرات دانشجویان';
    }

    public function get_icon()
    {
        return 'eicon-gallery-grid';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {

        $venus_product_cate = array();
        $categories = get_terms("product_cat");
        if (!empty($categories) && !is_wp_error($categories)) {
            foreach ($categories as $category) {
                $venus_product_cate[$category->slug] = $category->name;
            }
        }

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('نظرات دانشجویان', 'venus'),
            ]
        );

        $this->add_control(
            'courses_cat_include',
            [
                'label' => __('دسته بندی', 'venus'),
                'type' => Controls_Manager::SELECT2,
                'options' => $venus_product_cate,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'comments_per_page',
            [
                'label' => __('تعداد نمایش نظرات', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => '6',
            ]
        );

        $this->add_control(
            'movie_text',
            [
                'label' => __('متن مشاهده ویدیو', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => 'مشاهده ویدئو',
            ]
        );

        $this->add_control(
            'show_movie_text',
            [
                'label' => __('دکمه مشاهده ویدیو', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('نمایش', 'venus'),
                'label_off' => __('پنهان', 'venus'),
                'return_value' => 'Yes',
                'default' => 'Yes',
            ]
        );

        $this->add_control(
            'status_link_comment',
            [
                'label' => __('لینک نظرات', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus'),
                'label_off' => __('غیرفعال', 'venus'),
                'return_value' => 'Yes',
                'default' => 'Yes',
            ]
        );

        $this->add_control(
            'status_excerpt_comment',
            [
                'label' => __('خلاصه نظرات', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus'),
                'label_off' => __('غیرفعال', 'venus'),
                'return_value' => 'Yes',
                'default' => 'Yes',
            ]
        );

        $this->add_control(
            'status_pic_comment',
            [
                'label' => __('تصویر نظرات', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus'),
                'label_off' => __('غیرفعال', 'venus'),
                'return_value' => 'Yes',
                'default' => 'Yes',
            ]
        );

        $this->add_control(
            'status_course_comment',
            [
                'label' => __('نام دوره', 'venus'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('فعال', 'venus'),
                'label_off' => __('غیرفعال', 'venus'),
                'return_value' => 'Yes',
                'default' => 'Yes',
            ]
        );

        $this->add_control(
            've_img_link_comments',
            [
                'label' => __('تصویر شاخص', 'venus'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Venus_URI . '/images/play_2.png',
                ],
            ]
        );

//        $this->add_control(
//            'orderby',
//            [
//                'label' => __('مرتب سازی بر اساس', 'venus'),
//                'type' => Controls_Manager::SELECT,
//                'default' => 'date',
//                'options' => [
//                    'date' => __('جدیدترین', 'venus'),
//                    'modified' => __('آخرین بروزرسانی', 'venus'),
//                    'sales' => __('بیشترین فروش', 'venus'),
//                    'rand' => __('تصادفی', 'venus'),
//                    'price' => __('قیمت', 'venus'),
//                ],
//            ]
//        );

        $this->end_controls_section();


        $this->start_controls_section(
            'setting_owl',
            [
                'label' => __('تنظیمات اسلایدر', 'venus-theme'),
            ]
        );

        $this->add_control(
            'logo_loop',
            [
                'label' => __('تکرار نظرات', 'venus-theme'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('روشن', 'venus-theme'),
                'label_off' => __('خاموش', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'auto_play',
            [
                'label' => __('نمایش خودکار', 'venus-theme'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('روشن', 'venus-theme'),
                'label_off' => __('خاموش', 'venus-theme'),
                'return_value' => 'yes',
                'default' => 'no',
            ]
        );

        $this->add_control(
            'time',
            [
                'label' => __('سرعت نمایش (ms)', 'venus-theme'),
                'type' => Controls_Manager::NUMBER,
                'min' => 1000,
                'max' => 20000,
                'step' => 1000,
                'default' => 8000,
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'comments_style',
            [
                'label' => __('استایل', 'venus-theme'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'des_comments',
                'label' => __('متن خلاصه نظر', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .iron-willed-shortcode-container .item .header-content',
            ]
        );

        $this->add_control(
            'color_des_comments',
            [
                'label' => __('رنگ متن خلاصه نظر', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .iron-willed-shortcode-container .item .header-content' => 'color: {{VALUE}}',
                ],
                'default' => '#212529'
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'author_name',
                'label' => __('نام دانشجو', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .iron-willed-shortcode-container .item .footer-content .iron-willed-name p',
            ]
        );

        $this->add_control(
            'color_author_name',
            [
                'label' => __('رنگ نام دانشجو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .iron-willed-shortcode-container .item .footer-content .iron-willed-name p' => 'color: {{VALUE}}',
                ],
                'default' => '#212529'
            ]
        );


        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'course_comments',
                'label' => __('نام دوره', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .iron-willed-shortcode-container .item .footer-content .iron-willed-name span',
            ]
        );

        $this->add_control(
            'color_course',
            [
                'label' => __('رنگ نام دوره', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .iron-willed-shortcode-container .item .footer-content .iron-willed-name span' => 'color: {{VALUE}}',
                ],
                'default' => '#212529'
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'link_comments',
                'label' => __('متن مشاهده ویدیو', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .iron-willed-shortcode-container .item .footer-content .iron-willed-inner-link .iron-willed-btn',
            ]
        );

        $this->add_control(
            'color_link_comments',
            [
                'label' => __('رنگ مشاهده ویدیو', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .iron-willed-shortcode-container .item .footer-content .iron-willed-inner-link .iron-willed-btn' => 'color: {{VALUE}}',
                ],
                'default' => '#212529'
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bg_box_comments',
                'label' => __('بکگراند باکس نظر', 'venus-theme'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .iron-willed-shortcode-container .item',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        ?>
        <?php
        global $venus_options;

        $args = array(
            'post_type' => 'student_comments',
            'posts_per_page' => $settings['comments_per_page'],
//            'orderby' => $settings['orderby'],
//            'meta_query' => array(),
//            'tax_query' => array(
//                'relation' => 'AND',
//            ),
        );


//        if (!empty($settings['courses_cat_include'])) {
//            $cat_include = array();
//            $settings['courses_cat_include'] = explode(',', $settings['courses_cat_include']);
//            foreach ($settings['courses_cat_include'] as $category) {
//                $term = term_exists($category, 'product_cat');
//                if ($term !== 0 && $term !== null) {
//                    $cat_include[] = $term['term_id'];
//                }
//            }
//            if (!empty($cat_include)) {
//                $args['tax_query'][] = array(
//                    'taxonomy' => 'product_cat',
//                    'terms' => $cat_include,
//                    'operator' => 'IN',
//                );
//            }
//        }

        $comments_query = new \WP_Query($args);

        ?>
        <section id="ironwilled-home-slider">
            <span class="slider-fade-out right" style="display: inline;"></span>
            <span class="slider-fade-out left" style="display: inline;"></span>
            <div class="main-item-iron-willed">
                <div class="iron-willed-shortcode-container owl-iron-willed-el owl-carousel owl-theme owl-loaded owl-drag"
                    <?php
                    if ('yes' === $settings['logo_loop']) {
                        echo 'data-itemloop="true"';
                    } else {
                        echo 'data-itemloop="false"';
                    }
                    if ('yes' === $settings['auto_play']) {
                        echo 'data-itemplay="true"';
                    } else {
                        echo 'data-itemplay="false"';
                    } ?> data-itemtime="<?php echo $settings['time']; ?>"
                >
                    <div class="owl-stage-outer">
                        <div class="owl-stage">
                            <?php if ($comments_query->have_posts()) : ?>
                                <?php while ($comments_query->have_posts()) : $comments_query->the_post();

                                    $thumb_comments = get_the_post_thumbnail_url(get_the_ID());

                                    if (empty($thumb_comments)) {
                                        $thumb_comments = Venus_URI . '/images/person.png';
                                    }

                                    $prefix = '_venus_';

                                    $course_student = get_post_meta(get_the_ID(), $prefix . 'title_course', true);

                                    $excerpt_comment_student = get_post_meta(get_the_ID(), $prefix . 'excerpt_comment', true);

                                    $item_h = '';
                                    $footer_h = '';

                                    if (empty($settings['status_excerpt_comment'])): $item_h = 'min-height: unset;';
                                        $footer_h = 'min-height: 124px;'; endif;

                                    ?>
                                    <div class="owl-item">
                                        <article class="item" style="<?php echo $item_h; ?>">
                                            <?php if ($settings['status_excerpt_comment'] == 'Yes'): ?>
                                                <div class="header-content">
                                                    <p><?php echo $excerpt_comment_student; ?></p>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($settings['status_link_comment'] == 'Yes'): ?>
                                            <a href="<?php the_permalink() ?>" class="inner-link">
                                                <?php endif; ?>
                                                <div class="footer-content d-flex" style="<?php echo $footer_h; ?>">
                                                    <?php if ($settings['status_pic_comment'] == 'Yes'): ?>
                                                        <div class="img iron-willed-user-img"
                                                             style="background-image: url(<?php echo $thumb_comments; ?>)"></div>
                                                    <?php endif; ?>
                                                    <div class="iron-willed-name">
                                                        <p><?php the_title(); ?></p>
                                                        <?php if ($settings['status_course_comment'] == 'Yes'): ?>
                                                            <span><?php echo $course_student; ?></span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <?php
                                                    if ($settings['show_movie_text'] == 'Yes'): ?>
                                                        <div class="iron-willed-inner-link mr-auto">
                                                            <img src="<?php echo $settings['ve_img_link_comments']['url']; ?>"
                                                                 class="mx-auto mb-2">
                                                            <span class="iron-willed-btn"><?php echo $settings['movie_text']; ?></span>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <?php if ($settings['status_link_comment'] == 'Yes'): ?>
                                            </a>
                                        <?php endif; ?>
                                        </article>
                                    </div>
                                <?php
                                endwhile;
                            else :
                                echo '<p>' . esc_html_e('متاسفانه نظری پیدا نشد') . '</p>';
                            endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php wp_reset_postdata();
    }
}
