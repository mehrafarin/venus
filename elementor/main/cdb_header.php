<?php

namespace Elementor;

class venus_header extends Widget_Base
{

    public function get_name()
    {
        return 'home-header';
    }

    public function get_title()
    {
        return 'اسلاید هدر';
    }

    public function get_icon()
    {
        return 'eicon-archive-title';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('اسلاید هدر', 'venus'),
            ]
        );

        $this->add_control(
            'title_row_one',
            [
                'label' => __('متن اول', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('اینجا ...', 'venus'),
//                'placeholder' => __('Enter your title', 'elementor'),
            ]
        );

        $this->add_control(
            'title_row_two',
            [
                'label' => __('متن دوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('شروع آینده موفق شغلی شماست', 'venus'),
//                'placeholder' => __('Enter your title', 'elementor'),
            ]
        );

        $this->add_control(
            'title_row_three',
            [
                'label' => __('متن سوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('افزایش مهارت های تخصصی', 'venus'),
//                'placeholder' => __('Enter your title', 'elementor'),
            ]
        );

        $this->add_control(
            'color_txt_right_header',
            [
                'label' => __('رنگ متن ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .lms-top-header .header-detail p' => 'color: {{VALUE}}',
                ],
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            ]
        );

        $this->add_control(
            'btn_title_link',
            [
                'label' => __('متن دکمه', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('دوره های آموزشی', 'venus'),
//                'placeholder' => __('Enter your title', 'elementor'),
            ]
        );

        $this->add_control(
            'color_bg_btn_header',
            [
                'label' => __('رنگ دکمه', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
                'default' => '#f22d33',
            ]
        );

        $this->add_control(
            'link',
            [
                'label' => __('لینک دکمه (Url)', 'venus'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'venus'),
                'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'left-side',
            [
                'label' => __('باکس سمت چپ', 'venus'),
            ]
        );

        $this->add_control(
            'image_header',
            [
                'label' => __('تصویر پس زمینه', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::MEDIA,
                'placeholder' => __('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
                'default' => [
                    'url' => '',
                ]
            ]
        );

        $this->add_control(
            'color_bg_header',
            [
                'label' => __('رنگ پس زمینه', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            ]
        );

        $this->add_control(
            'title_row_left_one',
            [
                'label' => __('متن اول', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('۳۵۰۰ ساعت آموزش', 'venus'),
//                'placeholder' => __('Enter your title', 'elementor'),
            ]
        );

        $this->add_control(
            'title_row_left_two',
            [
                'label' => __('متن دوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('در حوزه های طراحی وب و', 'venus'),
//                'placeholder' => __('Enter your title', 'elementor'),
            ]
        );

        $this->add_control(
            'title_row_left_three',
            [
                'label' => __('متن سوم', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('کسب و کار', 'venus'),
//                'placeholder' => __('Enter your title', 'elementor'),
            ]
        );

        $this->add_control(
            'color_txt_header',
            [
                'label' => __('رنگ متن ها', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .lms-top-header .header-bg p' => 'color: {{VALUE}}',
                ],
                'placeholder' => __('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            ]
        );


        $this->end_controls_section();

    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        $url_btn_one = $settings['link']['url'];
        $url_btn_one_target = $settings['link']['is_external'] ? ' target="_blank"' : '';
        $url_btn_one_nofollow = $settings['link']['nofollow'] ? ' rel="nofollow"' : '';
        $url_img_header = $settings['image_header']['url'];
        echo "
        
    <div class='row'>
        <div class='lms-top-header'>
            <div class='header-detail'>
                <p class='px-3'>
                    <span class='text1 mb-2'>$settings[title_row_one]</span>
                    <span class='text2 mb-2'>$settings[title_row_two]</span>
                    <span class='text3'>$settings[title_row_three]</span>
                </p>
                <style>
                    .lms-top-header .header-detail a.header-register {
                    background-color: $settings[color_bg_btn_header];
                    box-shadow: 0 2px 12px $settings[color_bg_btn_header];
                    }
                    .lms-top-header .header-detail a.header-register:hover {
                    background-color: $settings[color_bg_btn_header]99;
                    }
                </style>
                
                <a href='$url_btn_one'  $url_btn_one_target  $url_btn_one_nofollow class='header-register mx-3'>$settings[btn_title_link]</a>
            </div>
            <div class='header-bg col-md-5 col-xs-5'>
                <style>
                    .lms-top-header .header-bg .header-bg-image:after {
                    background: $settings[color_bg_header];
                    background: linear-gradient(324deg, $settings[color_bg_header] 0%, $settings[color_bg_header] 100%);
                    }
                </style>
                <div class='header-bg-image' style='background: url($url_img_header) right no-repeat;'></div>
                <p><strong>$settings[title_row_left_one]</strong>
                    <br>
                    $settings[title_row_left_two]
                    <br>
                    $settings[title_row_left_three]
                </p>
                <div class='meteor1'></div>
                <div class='meteor2'></div>
                <div class='meteor3'></div>
                <div class='meteor4'></div>
                <div class='meteor5'></div>
                <div class='meteor6'></div>
            </div>
        </div>
    </div>

        
        ";

    }

    protected function _content_template()
    {
        ?>
        <div class="row">
            <div class="lms-top-header">
                <div class="header-detail">
                    <p class="px-3">
                        <span class="text1 mb-2">{{{ settings.title_row_one }}}</span>
                        <span class="text2 mb-2">{{{ settings.title_row_two }}}</span>
                        <span class="text3">{{{ settings.title_row_three }}}</span>
                    </p>
                    <style>
                        .lms-top-header .header-detail a.header-register {
                        background-color: {{{ settings.color_bg_btn_header }}};
                        box-shadow: 0 2px 12px {{{ settings.color_bg_btn_header }}};
                        }
                        .lms-top-header .header-detail a.header-register:hover {
                        background-color: {{{ settings.color_bg_btn_header }}}99;
                        }
                    </style>
                    <#
                    var target = settings.link.is_external ? ' target="_blank"' : '';
                    var nofollow = settings.link.nofollow ? ' rel="nofollow"' : '';
                    #>
                    <a href="{{{ settings.link.url }}}" {{ target }}{{ nofollow }}
                       class="header-register mx-3">{{{ settings.btn_title_link }}}</a>
                </div>
                <div class="header-bg col-md-5 col-xs-5">
                    <style>
                        .lms-top-header .header-bg .header-bg-image:after {
                        background: {{{ settings.color_bg_header }}};
                        background: linear-gradient(324deg, {{{ settings.color_bg_header }}} 0%, {{{ settings.color_bg_header }}} 100%);
                        }
                    </style>
                    <div class="header-bg-image" style="background: url({{{ settings.image_header.url }}}) right no-repeat;"></div>
                    <p><strong>{{{ settings.title_row_left_one }}}</strong>
                        <br>
                        {{{ settings.title_row_left_two }}}
                        <br>
                        {{{ settings.title_row_left_three }}}
                    </p>
                    <div class="meteor1"></div>
                    <div class="meteor2"></div>
                    <div class="meteor3"></div>
                    <div class="meteor4"></div>
                    <div class="meteor5"></div>
                    <div class="meteor6"></div>
                </div>
            </div>
        </div>
        <?php
    }



}
