<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class venus_content_box extends Widget_Base
{

    public function get_name()
    {
        return 'cdb-content-box';
    }

    public function get_title()
    {
        return 'عنوان محتوا';
    }

    public function get_icon()
    {
        return 'eicon-animated-headline';
    }

    public function get_categories()
    {
        return ['venus'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'section_title',
            [
                'label' => __('نمایش محصول', 'venus'),
            ]
        );
        $this->add_control(
            'title_contents',
            [
                'label' => __('عنوان بخش', 'venus'),
                'label_block' => true,
                'type' => Controls_Manager::TEXT,
                'default' => __('عنوان تست', 'venus'),
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'venus_title_box',
            [
                'label' => __('بلاگ', 'venus'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'style_color_text',
                'label' => __('تایپوگرافی عنوان', 'venus-theme'),
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .section-title',
            ]
        );

        $this->add_control(
            'color_text',
            [
                'label' => __('رنگ عنوان', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .section-title' => 'color: {{VALUE}}',
                ],
                'default' => '#333'
            ]
        );

        $this->add_control(
            'under_color_text',
            [
                'label' => __('رنگ خط کناری باکس', 'venus-theme'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'scheme' => [
                    'type' => \Elementor\Core\Schemes\Color::get_type(),
                    'value' => \Elementor\Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .section-title:before' => 'background-color: {{VALUE}}',
                ],
                'default' => '#F8C44F'
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        echo "<h2 class='section-title m-0'>$settings[title_contents]</h2>";
    }

    protected function _content_template()
    {
        ?>
        <h2 class="section-title m-0">{{{ settings.title_contents }}}</h2>
        <?php
    }
}
