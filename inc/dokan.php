<?php
if (!function_exists('physical_display_seller_name')) {

    function physical_display_seller_name()
    {
        if (is_plugin_active('dokan-lite/dokan.php')) :

            // Get the author ID (the vendor ID)
            $vendor_id = get_post_field('post_author', get_the_id());
            // Get the WP_User object (the vendor) from author ID
            $vendor = new WP_User($vendor_id);

            $store_info = dokan_get_store_info($vendor_id); // Get the store data
            $store_name = $store_info['store_name'];          // Get the store name
            $store_url = dokan_get_store_url($vendor_id);  // Get the store URL

            $vendor_name = $vendor->display_name;              // Get the vendor name
            $address = $vendor->billing_address_1;           // Get the vendor address
            $postcode = $vendor->billing_postcode;          // Get the vendor postcode
            $city = $vendor->billing_city;              // Get the vendor city
            $state = $vendor->billing_state;             // Get the vendor state
            $country = $vendor->billing_country;           // Get the vendor country

            // Display the seller name linked to the store
            printf('<div class="v-name-seller"><b>فروشنده:</b> <a href="%s">%s</a></div>', $store_url, $store_name);
        else:
            global $current_user;
            printf('<div class="v-name-seller"><b>فروشنده:</b> <p>%s</p></div>', $current_user->display_name);
        endif;
    }
}
if (!function_exists('physical_stock_product')) {

    function physical_stock_product()
    {
        global $product, $current_user;

        $in_stock = $product->is_in_stock();

        if (is_plugin_active('dokan-lite/dokan.php')) :

            // Get the author ID (the vendor ID)
            $vendor_id = get_post_field('post_author', get_the_id());
            // Get the WP_User object (the vendor) from author ID
            $vendor = new WP_User($vendor_id);
            $store_info = dokan_get_store_info($vendor_id); // Get the store data
            $store_name = $store_info['store_name'];          // Get the store name

            if ($in_stock) : echo '<div class="physical-stock-product">موجود در انبار ' . $store_name . '</div>'; endif;
        else:
            if ($in_stock) : echo '<div class="physical-stock-product">موجود در انبار ' . $current_user->display_name . '</div>'; endif;
        endif;

    }
}
if (!function_exists('physical_guarantee_product')) {

    function physical_guarantee_product()
    {
        $prefix = '_venus_';

        $physical_guarantee_product = get_post_meta(get_the_ID(), $prefix . 'physical_guarantee_product', true);

        if (!empty($physical_guarantee_product)) :
            echo '<div class="v-guarantee-product">' . $physical_guarantee_product . '</div>';
        endif;
    }
}

/**
 * Start New Filed
 */

add_action('dokan_new_product_after_product_tags', 've_new_product_field_dokan', 10);

function ve_new_product_field_dokan()
{ ?>

    <div class="dokan-form-group">
        <input type="text" class="dokan-form-control" name="_venus_physical_name_en_product"
               placeholder="<?php esc_attr_e('عنوان انگلیسی محصول', 'venus-theme'); ?>">
    </div>

    <div class="dokan-form-group">
        <input type="text" class="dokan-form-control" name="_venus_physical_guarantee_product"
               placeholder="<?php esc_attr_e('گارانتی محصول', 'venus-theme'); ?>">
    </div>

    <?php
}

add_action('dokan_new_product_added', 've_save_add_product_meta_dokan', 10, 2);
add_action('dokan_product_updated', 've_save_add_product_meta_dokan', 10, 2);

function ve_save_add_product_meta_dokan($product_id, $postdata)
{

    if (!dokan_is_user_seller(get_current_user_id())) {
        return;
    }

    if (!empty($postdata['_venus_physical_name_en_product'])) {
        update_post_meta($product_id, '_venus_physical_name_en_product', $postdata['_venus_physical_name_en_product']);
    }

    if (!empty($postdata['_venus_physical_guarantee_product'])) {
        update_post_meta($product_id, '_venus_physical_guarantee_product', $postdata['_venus_physical_guarantee_product']);
    }
}

/*
* Showing field data on product edit page
*/

add_action('dokan_product_edit_after_product_tags', 'show_on_edit_page', 99, 2);

function show_on_edit_page($post, $post_id)
{
    $ve_physical_name_en_product = get_post_meta($post_id, '_venus_physical_name_en_product', true);
    $ve_physical_guarantee_product = get_post_meta($post_id, '_venus_physical_guarantee_product', true);
    ?>
    <div class="dokan-form-group">
        <input type="hidden" name="new_field" id="dokan-edit-product-id" value="<?php echo esc_attr($post_id); ?>"/>
        <label for="_venus_physical_name_en_product">عنوان انگلیسی محصول:</label>
        <?php dokan_post_input_box($post_id, '_venus_physical_name_en_product', array('value' => $ve_physical_name_en_product)); ?>
        <div class="dokan-product-title-alert dokan-hide">
            <?php esc_html_e('Please enter product code!', 'dokan-lite'); ?>
        </div>
    </div>
    <div class="dokan-form-group">
        <input type="hidden" name="new_field" id="dokan-edit-product-id" value="<?php echo esc_attr($post_id); ?>"/>
        <label for="_venus_physical_name_en_product">گارانتی محصول:</label>
        <?php dokan_post_input_box($post_id, '_venus_physical_guarantee_product', array('value' => $ve_physical_guarantee_product)); ?>
        <div class="dokan-product-title-alert dokan-hide">
            <?php esc_html_e('Please enter product code!', 'dokan-lite'); ?>
        </div>
    </div>
    <?php

}