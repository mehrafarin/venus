<?php

// Select Style Product
function venus_style_page_products()
{
    $screens = array('product');
    foreach ($screens as $screen) {
        add_meta_box('select_style_product', 'استایل صفحه محصول', 'style_product', $screen, 'side', 'high');
    }
}

function style_product($post)
{
    $style_products = get_post_meta($post->ID, 'select_style_page_products', true);
    ?>
    <div class="meta-box-row">
        <label for="select_style_page_products" class="ctr_course">استایل صفحه محصول :</label>
        <select name="select_style_page_products" id="select_style_page_products" class="postbox">
            <option value="default" <?php selected($style_products, 'default'); ?>>استایل پیش فرض تنظیمات</option>
            <option value="hamyarco" <?php selected($style_products, 'hamyarco'); ?>>استایل اول (همیارآکادمی)</option>
            <?php
            global $venus_options;
            if ($venus_options['switch_digikala_venus']): ?>
                <option value="digikala" <?php selected($style_products, 'digikala'); ?>>استایل دوم (دیجیکالا)</option>
            <?php endif; ?>

        </select>
    </div>
    <?php
}

function product_style_save($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (array_key_exists('select_style_page_products', $_POST)) {
        update_post_meta(
            $post_id,
            'select_style_page_products',
            $_POST['select_style_page_products']
        );
    }

}

add_action('add_meta_boxes', 'venus_style_page_products');
add_action('save_post', 'product_style_save');

function product_venus_meta_box()
{
    $screens = array('product');
    foreach ($screens as $screen) {
        add_meta_box('product_dic_box', 'تنظیمات دوره', 'dic_product_content', $screen, 'advanced', 'high');
    }
}

function dic_product_content($post)
{
    $product_dic = get_post_meta($post->ID, 'product_dic', true);
    $cor_intro_video = get_post_meta($post->ID, 'cor_intro_video', true);
    ?>
    <div class="meta-box-row">
        <label class="ctr_course" for="short_text_box">توضیح کوتاه (جهت نمایش در باکس محصول)</label>
        <input type="text" name="type_dic_product" id="short_text_box" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $product_dic; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="intro_video">لینک ویدئو معرفی دوره</label>
        <input type="text" name="link_cor_intro_video" id="intro_video" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $cor_intro_video; ?>">
    </div>
    <?php
}

function product_dic_save($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!isset($_POST['type_dic_product'])) {
        return;
    }
    $end_date = sanitize_text_field($_POST['type_dic_product']);

    update_post_meta($post_id, 'product_dic', $end_date);

    if (!isset($_POST['link_cor_intro_video'])) {
        return;
    }
    $link_core_intro_date = sanitize_text_field($_POST['link_cor_intro_video']);

    update_post_meta($post_id, 'cor_intro_video', $link_core_intro_date);
}

add_action('add_meta_boxes', 'product_venus_meta_box');
add_action('save_post', 'product_dic_save');

/* Start BackGround Product */
function bg_product_venus()
{
    $screens = array('product');
    foreach ($screens as $screen) {
        add_meta_box('bg_box', 'بکگراند هدر محصول', 'bg_content', $screen);
    }
}

function bg_content($post)
{
    $bg_date = get_post_meta($post->ID, 'bg_date', true);

    ?>
    <div class="meta-box-row">
        <p>تصویر پس زمینه محصول : </p>
        <input id="ve-link-bg-product" type="text" size="36" name="upload_image" value="<?php echo $bg_date; ?>"/>
        <button class="ve-btn-upload" id="ve_up_bg_single_product">آپلود عکس</button>
        <img src="" alt="" class="ve-preview-img-uploaded" id="ve_show_bg_product">
    </div>
    <?php
}

function bg_save($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!isset($_POST['upload_image'])) {
        return;
    }
    $bg_date = sanitize_text_field($_POST['upload_image']);


    update_post_meta($post_id, 'bg_date', $bg_date);
}

add_action('add_meta_boxes', 'bg_product_venus');
add_action('save_post', 'bg_save');

/* Start Modal Course */
function product_venus_type_course()
{
    $screens = array('product');
    foreach ($screens as $screen) {
        add_meta_box('pro_type_course', 'اطلاعات دوره', 'type_course_pro', $screen, 'side', 'high');
    }
}

function type_course_pro($post)
{
    $pro_type = get_post_meta($post->ID, 'pro_type', true);
    $level_course = get_post_meta($post->ID, 'level_course', true);
    $course_status = get_post_meta($post->ID, 'course_status_course', true);
    $timer_course_status = get_post_meta($post->ID, 'timer_course_status', true);

    ?>
    <div class="meta-box-row">
        <label class="ctr_course">نوع دوره (حضوری ، غیرحضوری)</label>
        <input type="text" name="type_product" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $pro_type; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="level_course">سطح دوره (پیشرفته)</label>
        <input type="text" name="level_course_control" id="level_course" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $level_course; ?>">
    </div>

    <div class="meta-box-row">
        <label for="course_status" class="ctr_course">وضعیت دوره :</label>
        <select name="course_status" id="course_status" class="postbox">
            <option value="">انتخاب کنید...</option>
            <option value="soon" <?php selected($course_status, 'soon'); ?>>به زودی...</option>
            <option value="loading" <?php selected($course_status, 'loading'); ?>>درحال برگزاری</option>
            <option value="registration" <?php selected($course_status, 'registration'); ?>>پیش ثبت نام</option>
            <option value="complete" <?php selected($course_status, 'complete'); ?>>تکمیل شده</option>
        </select>
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="timer_course_status">وضعیت دوره باید در حالت به زودی باشد تا نمایش داده
            شود.(تاریخ مورد نظر را وارد نمایید)</label>
        <input type="text" name="timer_course_status" id="timer_course_status" style="width: 100%;height: 30px;"
               class="input" value="<?php echo $timer_course_status; ?>">
    </div>
    <?php
}

function product_type_save($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!isset($_POST['type_product'])) {
        return;
    }
    $type_date = sanitize_text_field($_POST['type_product']);
    update_post_meta($post_id, 'pro_type', $type_date);

    if (!isset($_POST['level_course_control'])) {
        return;
    }
    $level_date = sanitize_text_field($_POST['level_course_control']);
    update_post_meta($post_id, 'level_course', $level_date);

    if (array_key_exists('course_status', $_POST)) {
        update_post_meta(
            $post_id,
            'course_status_course',
            $_POST['course_status']
        );
    }

    if (!isset($_POST['timer_course_status'])) {
        return;
    }
    $timer_course_status_date = sanitize_text_field($_POST['timer_course_status']);
    update_post_meta($post_id, 'timer_course_status', $timer_course_status_date);
}

add_action('add_meta_boxes', 'product_venus_type_course');
add_action('save_post', 'product_type_save');

/* Start Control Course */
function product_venus_control_course()
{
    $screens = array('product');
    foreach ($screens as $screen) {
        add_meta_box('pro_control_course', 'باکس کنترل', 'control_course_pro', $screen, 'side', 'high');
    }
}

function control_course_pro($post)
{
    $pro_control = get_post_meta($post->ID, 'pro_control', true);
    $course_introduction = get_post_meta($post->ID, 'course_introduction', true);
    $session_sessions = get_post_meta($post->ID, 'session_sessions', true);
    $course_requirement = get_post_meta($post->ID, 'course_requirement', true);
    $course_teacher = get_post_meta($post->ID, 'course_teacher', true);
    $questions = get_post_meta($post->ID, 'questions', true);
    $opinions = get_post_meta($post->ID, 'opinions', true);
    $consultation_request = get_post_meta($post->ID, 'consultation_request', true);
    $register_btn = get_post_meta($post->ID, 'register_btn', true);
    $register_btn_center = get_post_meta($post->ID, 'register_btn_center', true);
    $price_text_sidebar = get_post_meta($post->ID, 'price_text_sidebar', true);
    ?>
    <div class="meta-box-row">
        <label class="ctr_course" for="sales_number">متن تعداد فروش</label>
        <input type="text" name="type_control" id="sales_number" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $pro_control; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="course_introduction">متن(معرفی دوره)</label>
        <input type="text" name="course_introduction_control" id="course_introduction" style="width: 100%;height: 30px;"
               class="input" value="<?php echo $course_introduction; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="session_sessions">متن(جلسات دوره)</label>
        <input type="text" name="session_sessions_control" id="session_sessions" style="width: 100%;height: 30px;"
               class="input" value="<?php echo $session_sessions; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="course_requirement">متن(مخاطبین دوره)</label>
        <input type="text" name="course_requirement_control" id="course_requirement" style="width: 100%;height: 30px;"
               class="input" value="<?php echo $course_requirement; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="course_teacher">متن(مدرس دوره)</label>
        <input type="text" name="course_teacher_control" id="course_teacher" style="width: 100%;height: 30px;"
               class="input" value="<?php echo $course_teacher; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="questions">متن(سوالات متداول)</label>
        <input type="text" name="questions_control" id="questions" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $questions; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="opinions">متن(دیدگاه ها)</label>
        <input type="text" name="opinions_control" id="opinions" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $opinions; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="consultation_request">دکمه(درخواست مشاوره)</label>
        <input type="text" name="consultation_request_control" id="consultation_request"
               style="width: 100%;height: 30px;" class="input" value="<?php echo $consultation_request; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="register_btn">دکمه(اطلاعات ثبت نام)</label>
        <input type="text" name="register_btn_control" id="register_btn" style="width: 100%;height: 30px;" class="input"
               value="<?php echo $register_btn; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="register_btn_center">دکمه(ثبت نام دوره)</label>
        <input type="text" name="register_btn_control_center" id="register_btn_center" style="width: 100%;height: 30px;"
               class="input"
               value="<?php echo $register_btn_center; ?>">
    </div>
    <div class="meta-box-row">
        <label class="ctr_course" for="price_text_sidebar">متن قیمت دوره (سایدبار)</label>
        <input type="text" name="price_text_sidebar" id="price_text_sidebar" style="width: 100%;height: 30px;"
               class="input"
               value="<?php echo $price_text_sidebar; ?>">
    </div>
    <?php
}

function product_control_save($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!isset($_POST['type_control'])) {
        return;
    }
    $control_date = sanitize_text_field($_POST['type_control']);
    update_post_meta($post_id, 'pro_control', $control_date);

    if (!isset($_POST['course_introduction_control'])) {
        return;
    }
    $course_introduction_date = sanitize_text_field($_POST['course_introduction_control']);
    update_post_meta($post_id, 'course_introduction', $course_introduction_date);

    if (!isset($_POST['session_sessions_control'])) {
        return;
    }
    $session_sessions_date = sanitize_text_field($_POST['session_sessions_control']);
    update_post_meta($post_id, 'session_sessions', $session_sessions_date);

    if (!isset($_POST['course_requirement_control'])) {
        return;
    }
    $course_requirement_date = sanitize_text_field($_POST['course_requirement_control']);
    update_post_meta($post_id, 'course_requirement', $course_requirement_date);

    if (!isset($_POST['course_teacher_control'])) {
        return;
    }
    $course_teacher_date = sanitize_text_field($_POST['course_teacher_control']);
    update_post_meta($post_id, 'course_teacher', $course_teacher_date);

    if (!isset($_POST['questions_control'])) {
        return;
    }
    $questions_date = sanitize_text_field($_POST['questions_control']);
    update_post_meta($post_id, 'questions', $questions_date);

    if (!isset($_POST['opinions_control'])) {
        return;
    }
    $opinions_date = sanitize_text_field($_POST['opinions_control']);
    update_post_meta($post_id, 'opinions', $opinions_date);

    if (!isset($_POST['consultation_request_control'])) {
        return;
    }
    $consultation_request_date = sanitize_text_field($_POST['consultation_request_control']);
    update_post_meta($post_id, 'consultation_request', $consultation_request_date);

    if (!isset($_POST['register_btn_control'])) {
        return;
    }
    $register_btn_date = sanitize_text_field($_POST['register_btn_control']);
    update_post_meta($post_id, 'register_btn', $register_btn_date);

    if (!isset($_POST['register_btn_control_center'])) {
        return;
    }
    $register_btn_center_date = sanitize_text_field($_POST['register_btn_control_center']);
    update_post_meta($post_id, 'register_btn_center', $register_btn_center_date);

    if (!isset($_POST['price_text_sidebar'])) {
        return;
    }
    $price_text_sidebar_date = sanitize_text_field($_POST['price_text_sidebar']);
    update_post_meta($post_id, 'price_text_sidebar', $price_text_sidebar_date);

}

add_action('add_meta_boxes', 'product_venus_control_course');
add_action('save_post', 'product_control_save');

/* Start Blog Post */
function blog_format_venus_meta_box()
{
    $screens = array('post');
    foreach ($screens as $screen) {
        add_meta_box('blog_format_all', 'تنظیمات نوشته', 'blog_format_select', $screen, 'advanced', 'high');
    }
}

function blog_format_select($post)
{
    $url_video_blog = get_post_meta($post->ID, 'url_video_blog', true);
    $url_cover_video_blog = get_post_meta($post->ID, 'url_cover_video_blog', true);
    $url_audio_blog = get_post_meta($post->ID, 'url_audio_blog', true);
    $blog_single_picture = get_post_meta($post->ID, 'blog_single_picture', true);
    ?>
    <div class="meta-box-row">
        <p style="font-size: 15px">تصویر شاخص</p>
        <p>اگر میخواهید تصویر شاخص در صفحه نوشته نمایش داده نشود انتخاب کنید</p>
        <input type="radio" name="blog_single_picture_show" class="input"
               value="di_single_blog_pic" <?php if ($blog_single_picture == "di_single_blog_pic") {
            echo "checked";
        } ?>>نمایش نده
        <input type="radio" name="blog_single_picture_show" class="input"
               value="en_single_blog_pic" <?php if ($blog_single_picture == "en_single_blog_pic") {
            echo "checked";
        } ?>>نمایش بده
        <hr>

        <p style="font-size: 15px">تنظیمات ویدئو</p>
        <p>اگر میخواهید از ویدئو استفاده کنید حتما فرمت نوشته را اول روی ویدئو انتخاب کنید</p>
        <hr>
        <p style="font-size: 15px;width: 20%;display: inline-block;">لینک ویدئوی شما</p>
        <input type="text" name="blog_video_link" style="width: 50%;height: 30px;" class="input"
               value="<?php echo $url_video_blog; ?>">
        <hr>
        <p style="font-size: 15px;width: 20%;display: inline-block;">کاور ویدئو شما</p>
        <input type="text" name="blog_cover_video_link" style="width: 50%;height: 30px;" class="input"
               value="<?php echo $url_cover_video_blog; ?>">
        <hr>
        <p style="font-size: 15px">تنظیمات فرمت صوتی</p>
        <p>اگر میخواهید از ویدئو استفاده کنید حتما فرمت نوشته را اول روی ویدئو انتخاب کنید</p>
        <hr>
        <p style="font-size: 15px;width: 20%;display: inline-block;">لینک فایل صوتی شما</p>
        <input type="text" name="blog_audio_link" style="width: 50%;height: 30px;" class="input"
               value="<?php echo $url_audio_blog; ?>">
        <hr>
    </div>

    <?php
}

function blog_format_save($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!isset($_POST['blog_video_link'])) {
        return;
    }
    $url_video_blog_date = sanitize_text_field($_POST['blog_video_link']);

    update_post_meta($post_id, 'url_video_blog', $url_video_blog_date);

    if (!isset($_POST['blog_cover_video_link'])) {
        return;
    }
    $url_cover_video_blog_date = sanitize_text_field($_POST['blog_cover_video_link']);

    update_post_meta($post_id, 'url_cover_video_blog', $url_cover_video_blog_date);

    if (!isset($_POST['blog_audio_link'])) {
        return;
    }
    $url_audio_blog_date = sanitize_text_field($_POST['blog_audio_link']);

    update_post_meta($post_id, 'url_audio_blog', $url_audio_blog_date);

    if (!isset($_POST['blog_single_picture_show'])) {
        return;
    }
    $blog_single_picture_date = sanitize_text_field($_POST['blog_single_picture_show']);

    update_post_meta($post_id, 'blog_single_picture', $blog_single_picture_date);

}

add_action('add_meta_boxes', 'blog_format_venus_meta_box');
add_action('save_post', 'blog_format_save');
