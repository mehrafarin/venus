<?php

// If its not active disable these functions
$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));

if (is_array($active_plugins) && !in_array('elementor/elementor.php', $active_plugins)) {
    return;
};





// Support for Shortcodes
$venus_el_templates_path = get_parent_theme_file_path('inc/lib/el/');

$venus_el_shortcodes = array(

    // Other
    'all_widget'

);

foreach ($venus_el_shortcodes as $shortcode_template) {
    require_once $venus_el_templates_path . $shortcode_template . '/widget.php';
}
