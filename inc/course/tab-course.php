<?php

/**
 * Venus Metaboxes
 */

add_action('cmb2_admin_init', 'venus_metaboxes');

function venus_metaboxes()
{
    $prefix = '_venus_';

    $courses_metaboxes = new_cmb2_box(array(
        'id' => 'product_metabox',
        'title' => esc_html__('تنظیمات تب های دوره', 'venus'),
        'object_types' => array('product'),
        'context' => 'normal',
        'priority' => 'core',
        'tab_group' => 'venus_main_options',
        'show_names' => true,
    ));

    $courses_group_feture = $courses_metaboxes->add_field(array(
        'id' => 'feture_group',
        'type' => 'group',
        'description' => __('تب های دوره', 'venus'),
        'options' => array(
            'group_title' => __('تب دوره', 'venus'),
            'add_button' => __('افزودن تب دوره جدید', 'venus'),
            'remove_button' => __('حذف تب دوره', 'venus'),
            'sortable' => true,
        ),
    ));

    $courses_metaboxes->add_group_field($courses_group_feture, array(
        'name' => esc_html__('عنوان تب', 'venus'),
        'id' => $prefix . 'feture_title',
        'type' => 'text',
    ));

    $courses_metaboxes->add_group_field($courses_group_feture, array(
        'name' => esc_html__('محتوای تب', 'venus'),
        'id' => $prefix . 'feture_input',
        'type' => 'wysiwyg',
    ));
}
