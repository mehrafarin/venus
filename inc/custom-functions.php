<?php

if (!function_exists('ve_url_add_cart')) {
    function ve_url_add_cart()
    {
        $ve_url = site_url() . '/checkout/?add-to-cart=' . get_the_ID();

        return $ve_url;
    }
}

if (!function_exists('ve_price_product')) {
    function ve_price_product()
    {
        global $product;

        echo $product->get_price_html();

    }
}

add_action('admin_enqueue_scripts', 've_up_bg_img_scripts');

function ve_up_bg_img_scripts()
{
    wp_enqueue_script('ve-tools-up-admin', Venus_URI . "/js/tools-admin.js");
}