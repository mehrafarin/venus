<?php
// Venus Metaboxes
add_action('cmb2_admin_init', 'venus_metaboxes_single_product');

function venus_metaboxes_single_product()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_venus_';

    $physical_metabox_product = new_cmb2_box(array(
        'id' => 'v_metabox_single_product',
        'title' => esc_html__('تنظیمات محصول فیزیکی', 'venus'),
        'object_types' => array('product'),
        'context' => 'normal',
        'priority' => 'core',
        'tab_group' => 'v_main_options_single_product',
        'show_names' => true,
    ));

    $physical_metabox_product->add_field(array(
        'name' => esc_html__('عنوان انگلیسی محصول:', 'venus'),
        'id' => $prefix . 'physical_name_en_product',
        'type' => 'text',
    ));

    $physical_metabox_product->add_field(array(
        'name' => esc_html__('گارانتی محصول:', 'venus'),
        'id' => $prefix . 'physical_guarantee_product',
        'type' => 'text',
    ));

}
