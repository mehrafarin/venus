<?php


vc_map(array(
    'base' => 'cdb_advice',
    'name' => esc_html__('درخواست مشاوره', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('باکس درخواست مشاوره', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __('عنوان باکس', 'text-domain'),
            'param_name' => 'title_advice',
            'value' => __('درخواست مشاوره', 'text-domain'),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن اول', 'text-domain'),
            'param_name' => 'text_one_advice',
            'value' => __('شما این فرصت را دارید، با تکمیل فرم زیر، قبل از انتخاب دوره آموزشی مناسب خود، از مشاوره رایگان کارشناسان آموزشی مجموعه نت گراف استفاده نمائید.', 'text-domain'),
            'description'    => esc_html__( 'میتوانید متن دلخواه خود را وارد کنید', 'venus' ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('شورت کات فرم مشاوره', 'venus'),
            'param_name' => 'content',
            'description' => esc_html__('شورت کات فرم ساز خود را وارد نمایید', 'venus'),
        ),
        array(
            'type'           => 'attach_image',
            'heading'        => esc_html__( 'تصویر پس زمینه', 'venus' ),
            'param_name'     => 'image_advice',
            'value'          => '',
            'description'    => esc_html__( 'انتخاب تصویر از کتابخانه رسانه ها', 'venus' )
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن اول', 'venus'),
            'param_name' => 'text_two_advice',
            'value' => __('شماره تماس: ', 'venus'),
            'description'    => esc_html__( 'میتوانید متن دلخواه خود را وارد کنید', 'venus' ),
            'group' => 'باکس شماره تماس',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('شماره تماس', 'venus'),
            'param_name' => 'text_three_advice',
            'description'    => esc_html__( 'میتوانید متن دلخواه خود را وارد کنید', 'venus' ),
            'group' => 'باکس شماره تماس',
        ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__('لینک (Url)', 'venus'),
            'param_name' => 'link_text_one',
            'description'    => esc_html__( 'برای مثال (tel:02100000)', 'venus' ),
            'group' => 'باکس شماره تماس',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن اول', 'venus'),
            'param_name' => 'text_four_advice',
            'value' => __('اینستاگرام: ', 'venus'),
            'description'    => esc_html__( 'میتوانید متن دلخواه خود را وارد کنید', 'venus' ),
            'group' => 'باکس شبکه اجتماعی',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن دوم', 'venus'),
            'param_name' => 'text_five_advice',
            'value' => __('bigersoft@', 'venus'),
            'description'    => esc_html__( 'میتوانید متن دلخواه خود را وارد کنید', 'venus' ),
            'group' => 'باکس شبکه اجتماعی',
        ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__('لینک (Url)', 'venus'),
            'param_name' => 'link_text_two',
            'group' => 'باکس شبکه اجتماعی',
        ),
    )
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Advice extends WPBakeryShortCode
    {
    }
}
