<?php

vc_map( array(
	'base'             => 'cdb_animated_counter',
	'name'             => esc_html__( 'شمارنده انیمیشنی', 'venus' ),
	'description'      => esc_html__( 'شمارنده اعداد', 'venus' ),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'category'         => esc_html__( 'ونوس', 'venus' ),
	'params'           => array(
		array(
			'type'          => 'textfield',
			'heading'       => esc_html__('عدد(مقدار)','venus' ),
			'param_name'    => 'value',
			'description'   => esc_html__('عدد مورد نظر خود را وارد نمایید','venus' ),
			'save_always'   => true,
			'admin_label'   => true,
		),
		array(
			'type'          => 'textarea',
			'heading'       => esc_html__('عنوان','venus' ),
			'param_name'    => 'label',
			'save_always'   => true,
			'admin_label'   => true,
		),
		array(
			'type'          => 'dropdown',
			'heading'       => esc_html__( 'رنگ بندی', 'venus' ),
			'param_name'    => 'color_scheme',
			'value'         => array(
				esc_html__( 'مشکی', 'venus' ) => 'dark',
				esc_html__( 'روشن', 'venus' ) => 'light',
				esc_html__( 'سفارشی', 'venus' ) => 'custom'
			),
			'save_always'   => true,
			'admin_label'   => true,
		),
		array(
			'type'          => 'colorpicker',
			'heading'       => esc_html__( 'رنگ متن دلخواه', 'venus' ),
			'param_name'    => 'text_color',
			'dependency'    => array(
				'element' => 'color_scheme',
				'value' => array( 'custom' )
			)
		),
		array(
			'type'           => 'textfield',
			'heading'        => esc_html__( 'نام کلاس اضافی', 'venus' ),
			'param_name'     => 'el_class',
			'description'    => esc_html__( 'اگر میخواهید به المان خاصی از محتوا استایل متفاوتی دهید، از این فیلد برای افزودن کلاس استفاده کنید و سپس آنرا به فایل css ارجاع دهید.', 'venus' ),
		),
		array(
			'type'       => 'css_editor',
			'param_name' => 'css',
			'group'      => esc_html__( 'تنظیمات طراحی', 'venus' ),
		)
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {
	class WPBakeryShortCode_Cdb_Animated_Counter extends WPBakeryShortCode {}
}