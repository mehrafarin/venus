<?php

vc_map(array(
    'base' => 'cdb_contents',
    'name' => esc_html__('باکس محتوا', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'as_parent' => array('all'),
    'description' => esc_html__('در باکس محتوا المان ها را اضافه کنید', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            "type" => "dropdown",
            "heading" => __("استایل", "venus"),
            "param_name" => "select_style_title_box",
            "value" => array(
                esc_html__('پیش فرض', 'venus') => 'default',
                esc_html__('ورژن 1', 'venus') => 'version_one',
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان بخش', 'venus'),
            'param_name' => 'title_contents',
            'value' => 'معرفی دوره',
            'holder' => 'div',
        ),
        array(
            'type' 			=> 'textfield',
            'heading' 		=> esc_html__( 'عنوان', 'venus' ),
            'param_name' 	=> 'title',
            'description'	=> esc_html__( 'عنوان دکمه را وارد کنید.', 'venus' ),
            'value' 		=> esc_html__( 'متن دکمه', 'venus' ),
            'admin_label'   => true
        ),
        array(
            'type'			=> 'vc_link',
            'heading'		=> esc_html__( 'URL (لینک)', 'venus' ),
            'param_name'	=> 'link',
            'description'	=> esc_html__( 'لینک دکمه را تنظیم کنید.', 'venus' )
        ),
        array(
            'type' 			=> 'dropdown',
            'heading' 		=> esc_html__( 'استایل', 'venus' ),
            'param_name'	=> 'style',
            'description'	=> esc_html__( 'استایل دکمه را انتخاب کنید', 'venus' ),
            'value' 		=> array(
                esc_html__( 'تو پر', 'venus' ) => 'filled',
                esc_html__( 'حاشیه', 'venus' ) => 'border',
            ),
            'std'			=> 'filled',
            'admin_label'   => true
        ),
        array(
            'type' 			=> 'colorpicker',
            'heading' 		=> esc_html__( 'رنگ', 'venus' ),
            'param_name' 	=> 'color',
            'description'	=> esc_html__( 'رنگ دکمه', 'venus' )
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__('Css', 'venus'),
            'param_name' => 'css',
            'group' => esc_html__('تنظیمات طراحی', 'venus')
        )
    ),
    'js_view' => 'VcColumnView'
));

vc_map(array(
    'base' => 'cdb_content',
    'name' => esc_html__('ویدیو', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('افزودن ویدیو', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('لینک ویدیو', 'venus'),
            'param_name' => 'video_link',
            'description' => esc_html__('در این قسمت لینک مستقیم ویدیو را قرار دهید', 'venus'),
            'holder' => 'div'
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('کاور ویدیو', 'venus'),
            'param_name' => 'image_cover',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus')
        ),
    )
));

if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Cdb_Contents extends WPBakeryShortCodesContainer
    {
    }
}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Content extends WPBakeryShortCode
    {
    }
}
