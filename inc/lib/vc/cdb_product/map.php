<?php

//function gaga_lite_category_lists( ) {
//
//    $categories = get_categories(
//
//        array(
//            'hide_empty' =>  0,
//            //'exclude'  =>  1,
//            'taxonomy'   =>  'product_cat' // mention taxonomy here.
//        )
//    );
//
//
//    $category_lists = array();
//
//    $category_lists[0] = __( 'Select Category' , 'gaga-lite' );
//
//    foreach( $categories as $category ){
//
//        $category_lists[$category->name] = $category->term_id;
//    }
//
//    return $category_lists;
//
//}
vc_map(array(
    'base' => 'cdb_product',
    'name' => esc_html__('نمایش محصول', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('نمایش محصولات', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('نوع نمایش دوره ها', 'venus'),
            'param_name' => 'courses_type',
            'value' => array(
                esc_html__('همه دوره ها', 'venus') => 'all',
                esc_html__('دوره های تخفیف خورده', 'venus') => 'onsale',
            ),
            'admin_label' => true
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('تعداد نمایش محصولات', 'venus'),
            'param_name' => 'posts_per_page',
            'std' => 6,
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('مرتب سازی بر اساس', 'venus'),
            'param_name' => 'orderby',
            'value' => array(
                esc_html__('جدیدترین', 'venus') => 'date',
                esc_html__('آخرین بروزرسانی', 'venus') => 'modified',
                esc_html__('بیشترین فروش', 'venus') => 'sales',
                esc_html__('تصادفی', 'venus') => 'rand',
                esc_html__('قیمت', 'venus') => 'price',
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('دسته بندی دوره ها', 'venus'),
            'param_name' => 'courses_cat_include',
            'description' => __('نامک دسته بندی های مورد نظر خود را وارد کرده و با کاما ', ' از هم جدایشان کنید.', 'venus'),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('انتخاب استایل نمایش محصولات', 'venus'),
            'param_name' => 'select_style_product',
            'value' => array(
                esc_html__('پیش فرض', 'venus') => 'default',
                esc_html__('ورژن 1', 'venus') => 'version_one',
            ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('حالت نمایش', 'venus'),
            'description' => esc_html__('برای استفاده در صفحه سایدبار دار فعال کنید.(کاربرد برای استایل پیش فرض)', 'venus'),
            'param_name' => 'select_style_sidebar_home',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('عنوان دکمه', 'venus'),
            'param_name' => 'btn_title_link_product',
            'value' => __('تمامی دوره ها', 'venus'),
            'description' => __('در صورت خالی گذاشتن غیرفعال می شود.', 'venus'),
            'admin_label' => false,
        ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__('لینک دکمه (Url)', 'venus'),
            'param_name' => 'link_btn_product',
        ),
        array(
            'type' => 'colorpicker',
            'heading' => esc_html__('رنگ دکمه', 'venus'),
            'param_name' => 'bgcolor_btn_product',
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('اطلاعات دوره', 'venus'),
            'description' => __('حالت نمایشی(سطح دوره ، نوع دوره ، هزینه دوره)', 'venus'),
            'param_name' => 'style_info_bottom_product',
            'value' => array(
                esc_html__('فعال', 'venus') => 'enable',
                esc_html__('غیرفعال', 'venus') => 'disable',
            ),
            'group' => esc_html__('اطلاعات دوره', 'venus'),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('نمایش هزینه دوره', 'venus'),
            'description' => esc_html__('در صورت فعال بودن فقط هزینه دوره نمایش داده خواهد شد.', 'venus'),
            'param_name' => 'money_only_show_product',
            'value' => array('فعال / غیرفعال' => 'value'),
            'group' => esc_html__('اطلاعات دوره', 'venus'),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('موج ها', 'venus'),
            'description' => esc_html__('با فعال کردن این گزینه امواج دیگر نمایش داده نمی شود. ', 'venus'),
            'param_name' => 'select_show_hide_waves_product',
            'value' => array('غیرفعال' => 'value'),
            'group' => esc_html__('اطلاعات دوره', 'venus'),
        ),
    )
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Product extends WPBakeryShortCode
    {
    }
}
