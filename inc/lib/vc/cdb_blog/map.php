<?php

$categories_array = array();
$categories = get_categories(array('taxonomy' => 'category',));
foreach ($categories as $category) {
    $categories_array[$category->name] = $category->term_id;
}
vc_map(array(
    'base' => 'cdb_blog',
    'name' => esc_html__('بلاگ', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('نمایش مقالات یا مطالب بلاگ', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            "type" => "dropdown",
            "heading" => __("انتخاب استایل بلاگ", "venus"),
            "param_name" => "select_style_blog",
            "value" => array(
                esc_html__( 'پیش فرض', 'venus' ) => 'default',
                esc_html__( 'ورژن 1', 'venus' ) => 'version_one',
                esc_html__( 'ورژن 2', 'venus' ) => 'version_two',
            ),
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("دسته بندی پست ها", "venus"),
            "param_name" => "category",
            "value" => $categories_array,
            "description" => __("دسته بندی مورد نظر را انتخاب کنید", "venus"),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('تعداد نمایش پست ها', 'venus'),
            'param_name' => 'num_blog_post',
            'value' => '3',
            'admin_label' => false,
        ),
        array(
            'type' => 'textfield',
            'heading' => __('عنوان دکمه', 'venus'),
            'param_name' => 'btn_cat_text',
            'value' => __('مقالات دیگر', 'venus'),
            "description" => __("در صورت خالی گذاشتن غیرفعال می شود.", "venus"),
            'admin_label' => false,
        ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__('لینک دکمه (Url)', 'venus'),
            'param_name' => 'btn_cat_link',
        ),
        array(
            'type'           => 'colorpicker',
            'heading'        => esc_html__( 'رنگ دکمه', 'venus' ),
            'param_name'     => 'colorbgbtn_blog',
            'description'    => esc_html__( 'رنگ مورد نظر خود را انتخاب کنید', 'venus' ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('نام نویسنده', 'venus'),
            'description' => esc_html__('با فعال کردن این گزینه نام نویسند دیگر نمایش داده نمی شود. ', 'venus'),
            'param_name' => 'select_show_hide_author',
            'value' => array('غیرفعال' => 'value'),
        ),
        array(
            "type" => "dropdown",
            "heading" => __("نوع نوشته", "venus"),
            "description" => __("حالت نمایشی آیکون ویدیو و پادکست", "venus"),
            "param_name" => "style_format_post_blogs",
            "value" => array(
                esc_html__('فعال', 'venus') => 'enable',
                esc_html__('غیرفعال', 'venus') => 'disable',
            ),
            'group'	=> esc_html__('نوع نوشته', 'venus' ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __('موج ها', 'venus'),
            'description' => esc_html__('با فعال کردن این گزینه امواج دیگر نمایش داده نمی شود. ', 'venus'),
            'param_name' => 'select_show_hide_waves',
            'value' => array('غیرفعال' => 'value'),
            'group'	=> esc_html__('نوع نوشته', 'venus' ),
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'Css', 'venus' ),
            'param_name' => 'css',
            'group' => esc_html__( 'تنظیمات طراحی', 'venus' )
        )
    )
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Blog extends WPBakeryShortCode
    {
    }
}
