<?php


vc_map(array(
    'base' => 'cdb_header',
    'name' => esc_html__('اسلاید هدر', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('نمایش متن توضیحات', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __('متن اول', 'text-domain'),
            'param_name' => 'title_row_one',
            'value' => __('اینجا ...', 'text-domain'),
            'admin_label' => false,
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن دوم', 'venus'),
            'param_name' => 'title_row_two',
            'value' => __('شروع آینده موفق شغلی شماست', 'venus'),
            'admin_label' => false,
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن سوم', 'venus'),
            'param_name' => 'title_row_three',
            'value' => __('افزایش مهارت های تخصصی', 'venus'),
            'admin_label' => false,
        ),
        array(
            'type' => 'colorpicker',
            'heading' => esc_html__('رنگ متن ها', 'venus'),
            'param_name' => 'color_txt_right_header',
            'description' => esc_html__('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن دکمه', 'venus'),
            'param_name' => 'btn_title_link',
            'value' => __('دوره های آموزشی', 'venus'),
            'admin_label' => false,
            "description" => __("در صورت خالی گذاشتن غیرفعال می شود.", "venus"),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => esc_html__('رنگ دکمه', 'venus'),
            'param_name' => 'colorbgbtn_header',
            'description' => esc_html__('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
        ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__('لینک دکمه (Url)', 'venus'),
            'param_name' => 'link',
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('تصویر پس زمینه', 'venus'),
            'param_name' => 'image_header',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
            'group' => 'باکس سمت چپ'
        ),
        array(
            'type' => 'colorpicker',
            'heading' => esc_html__('رنگ پس زمینه', 'venus'),
            'param_name' => 'colorbg_header',
            'description' => esc_html__('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            'group' => 'باکس سمت چپ'
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن اول', 'venus'),
            'param_name' => 'title_row_left_one',
            'value' => __('۳۵۰۰ ساعت آموزش', 'venus'),
            'admin_label' => false,
            'group' => 'باکس سمت چپ',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن دوم', 'venus'),
            'param_name' => 'title_row_left_two',
            'value' => __('در حوزه های طراحی وب و', 'venus'),
            'admin_label' => false,
            'group' => 'باکس سمت چپ',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('متن سوم', 'venus'),
            'param_name' => 'title_row_left_three',
            'value' => __('کسب و کار', 'venus'),
            'admin_label' => false,
            'group' => 'باکس سمت چپ',
        ),
        array(
            'type' => 'colorpicker',
            'heading' => esc_html__('رنگ متن ها', 'venus'),
            'param_name' => 'color_txt_header',
            'description' => esc_html__('رنگ مورد نظر خود را انتخاب کنید', 'venus'),
            'group' => 'باکس سمت چپ'
        ),
    )
));

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Header extends WPBakeryShortCode
    {
    }
}
