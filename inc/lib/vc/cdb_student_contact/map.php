<?php

vc_map(array(
    'base' => 'cdb_student_contact',
    'name' => esc_html__('مخاطبین دوره', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('در باکس مخاطبین دوره', 'venus'),
    'as_parent' => array('only' => 'cdb_unreq_course_contacts'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان باکس مخاطبین دوره', 'venus'),
            'param_name' => 'title_course_contacts',
            'value' => 'مخاطبین دوره',
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('محتوای باکس', 'venus'),
            'param_name' => 'top_title_course_contacts',
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('تصویر اول', 'venus'),
            'param_name' => 'image_req_one',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
            'group' => 'تصاویر'
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('تصویر دوم', 'venus'),
            'param_name' => 'image_req_two',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
            'group' => 'تصاویر'
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('تصویر سوم', 'venus'),
            'param_name' => 'image_req_three',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
            'group' => 'تصاویر'
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('تصویر چهارم', 'venus'),
            'param_name' => 'image_req_four',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus'),
            'group' => 'تصاویر'
        ),
    ),
    'js_view' => 'VcColumnView'
));

vc_map(array(
    'base' => 'cdb_unreq_course_contacts',
    'name' => esc_html__('توصیه', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'as_child' => array('only' => 'cdb_student_contact'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('انتخاب توصیه', 'venus'),
            'param_name' => 'switch_unreq',
            'description' => __( 'در صورت غیرفعال بودن حالت توصیه نمی شود فعال می شود.', 'venus' ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان توصیه', 'venus'),
            'param_name' => 'title_unreq',
        ),
        array(
            'type' => 'textarea_html',
            'heading' => esc_html__('محتوای باکس', 'venus'),
            'param_name' => 'content',
            'holder' => 'div',
        ),
    ),
));

if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_cdb_student_contact extends WPBakeryShortCodesContainer
    {
    }
}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Unreq_Course_Contacts extends WPBakeryShortCode
    {
    }
}
