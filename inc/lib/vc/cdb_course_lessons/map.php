<?php

vc_map(array(
    'base' => 'cdb_course_lessons',
    'name' => esc_html__('سرفصل های دوره', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'as_parent' => array('only' => 'cdb_course_lesson'),
    'description' => esc_html__('افزودن سرفصل و دروس دوره', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان بخش', 'venus'),
            'param_name' => 'title',
            'value' => 'جلسات دوره',
            'holder' => 'div'
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__('Css', 'venus'),
            'param_name' => 'css',
            'group' => esc_html__('تنظیمات طراحی', 'venus')
        )
    ),
    'js_view' => 'VcColumnView'
));

vc_map(array(
    'base' => 'cdb_course_lesson',
    'name' => esc_html__('سرفصل', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('سرفصل های دوره', 'venus'),
    'as_parent' => array('only' => 'cdb_course_lesson_meeting'),
    'as_child' => array('only' => 'cdb_course_lessons'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('آیدی سرفصل', 'venus'),
            'param_name' => 'id_tab',
            'description' => esc_html__('به هیچ وجه آیدی انتخاب شده نباید با آیدی سر فصل های دیگر مشابه باشد(ترجیها عدد باشد)', 'venus')
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان سر فصل', 'venus'),
            'param_name' => 'title',
            'holder' => 'div'
        ),
        array(
            'description' => esc_html__('در این قسمت در حد یک جمله کوتاه در رابطه با سرفصل مورد نظر بنویسید.', 'venus'),
            'type' => 'textfield',
            'heading' => esc_html__('توضیح کوتاه سرفصل', 'venus'),
            'param_name' => 'subtitle',
            'holder' => 'div',
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('تصویر شاخص', 'venus'),
            'param_name' => 'image',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus')
        ),
    ),
    'js_view' => 'VcColumnView'
));

vc_map(array(
    'base' => 'cdb_course_lesson_meeting',
    'name' => esc_html__('جلسات', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('جلسات سرفصل ها', 'venus'),
    'as_child' => array('only' => 'cdb_course_lesson'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('آیدی جلسه', 'venus'),
            'param_name' => 'id_meeting',
            'description' => esc_html__('به هیچ وجه آیدی انتخاب شده نباید با آیدی جلسات دیگر مشابه باشد حتی در سرفصل های دیگر همین محصول(ترجیها عدد باشد)', 'venus')
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان جلسه', 'venus'),
            'param_name' => 'title_meeting',
            'holder' => 'div'
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('زمان جلسه', 'venus'),
            'param_name' => 'time_meeting',
            'holder' => 'div',
        ),
        array(
            'type' => 'textarea_html',
            'heading' => esc_html__('محتوای جلسه', 'venus'),
            'param_name' => 'content',
            'holder' => 'div',
        ),
        array(
            'type'	 => 'checkbox',
            'heading' => esc_html__('آیکون خصوصی', 'venus'),
            'param_name' => 'private_lesson',
            'value' => array('نمایش' => 'value'),
            'description' => esc_html__('با تیک زدن این بخش توضیحات جلسه نمایش داده نمی شود.', 'venus'),
            'group'	=> esc_html__('اطلاعات جلسه', 'venus' ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('لینک فایل دانلودی', 'venus'),
            'description' => esc_html__('لینک دانلود فایل این جلسه فقط بعد از خرید به کاربر نمایش داده می شود. لطفا لینک فایل های اصلی دوره رو توسط ووکامرس قراردهید تا کاربران نتوانند لینک دانلود را با بقیه به اشتراک بگذارند.(از اینجا برای مثال لینک های تمرینات و فایل های مورد نیاز رو بزارید که زیاد مهم نیستند )', 'venus'),
            'param_name' => 'download_lesson',
            'group' => esc_html__('اطلاعات جلسه', 'venus'),
        ),
        array(
            'type'	 => 'checkbox',
            'heading' => esc_html__('حالت نمایش دکمه', 'venus'),
            'param_name' => 'txt_btn_preview',
            'description' => esc_html__('با تیک زدن این بخش فقط ایکون پیش نمایش ، نمایش داده خواهد شد.', 'venus'),
            'group'	=> esc_html__('پیش نمایش ویدئویی', 'venus' ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__( 'متن دکمه پیش نمایش ویدئویی', 'venus' ),
            'value' => __('پیش نمایش', 'venus'),
            'param_name' => 'text_btn_preview_video',
            'group'	=> esc_html__('پیش نمایش ویدئویی', 'venus' ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__( 'لینک پیش نمایش ویدئویی', 'venus' ),
            'description' => esc_html__('این ویدئو به صورت پاپ آپ هنگام کلیک روی دکمه پیشنمایش باز خواهد شد. (فقط لینک مستقیم ویدئو را وارد کنید).', 'venus'),
            'param_name' => 'preview_video',
            'group'	=> esc_html__('پیش نمایش ویدئویی', 'venus' ),
        ),
        array(
            'type'           => 'colorpicker',
            'heading'        => esc_html__( 'رنگ دکمه', 'venus' ),
            'param_name'     => 'btn_color_preview',
            'description'    => esc_html__( 'رنگ مورد نظر خود را انتخاب کنید', 'venus' ),
            'group'	=> esc_html__('پیش نمایش ویدئویی', 'venus' ),
        ),
    ),
    'js_view' => 'VcColumnView'
));

if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Cdb_Course_Lessons extends WPBakeryShortCodesContainer
    {
    }
}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Course_Lesson extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_lesson_Meeting extends WPBakeryShortCode
    {
    }
}
