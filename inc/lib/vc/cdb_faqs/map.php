<?php

vc_map(array(
    'base' => 'cdb_faqs',
    'name' => esc_html__('سوالات متداول', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'as_parent' => array('only' => 'cdb_faq'),
    'description' => esc_html__('افزودن سوالات متداول', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان بخش', 'venus'),
            'param_name' => 'title_faqs',
            'value' => 'سوالات متداول',
            'holder' => 'div'
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__('Css', 'venus'),
            'param_name' => 'css',
            'group' => esc_html__('تنظیمات طراحی', 'venus')
        )
    ),
    'js_view' => 'VcColumnView'
));

vc_map(array(
    'base' => 'cdb_faq',
    'name' => esc_html__('سوال', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('افزودن سوال', 'venus'),
    'as_child' => array('only' => 'cdb_faqs'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__( 'آیدی سوال', 'venus' ),
            'param_name' => 'id_faq',
            'description'    => esc_html__( 'به هیچ وجه آیدی انتخاب شده نباید با آیدی سر سوال های دیگر مشابه باشد(ترجیها عدد باشد)', 'venus' )
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('سوال', 'venus'),
            'description' => esc_html__('سوال مورد نظر خود را بنویسید', 'venus'),
            'param_name' => 'title_faq',
            'holder' => 'div'
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('جواب سوال', 'venus'),
            'param_name' => 'answer_faq',
            'description' => esc_html__('جواب سوال را بنویسید', 'venus'),
            'holder' => 'div',
        ),
    )
));

if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Cdb_Faqs extends WPBakeryShortCodesContainer
    {
    }
}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Faq extends WPBakeryShortCode
    {
    }
}
