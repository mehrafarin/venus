<?php

vc_map(array(
    'base' => 'cdb_teachers',
    'name' => esc_html__('مدرسین دوره', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'as_parent' => array('only' => 'cdb_teacher'),
    'description' => esc_html__('افزودن مدرس', 'venus'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('عنوان بخش', 'venus'),
            'param_name' => 'title_teachers',
            'value' => 'مدرسین دوره',
            'holder' => 'div'
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_html__('Css', 'venus'),
            'param_name' => 'css',
            'group' => esc_html__('تنظیمات طراحی', 'venus')
        )
    ),
    'js_view' => 'VcColumnView'
));

vc_map(array(
    'base' => 'cdb_teacher',
    'name' => esc_html__('مدرس', 'venus'),
    'icon' => get_template_directory_uri() . '/images/favicon.png',
    'description' => esc_html__('افزودن مدرس', 'venus'),
    'as_child' => array('only' => 'cdb_teachers'),
    'category' => esc_html__('ونوس', 'venus'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('نام مدرس', 'venus'),
            'param_name' => 'title_teacher',
            'holder' => 'div'
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('سمت یا دستاورد مدرس', 'venus'),
            'param_name' => 'subtitle_teacher',
            'holder' => 'div',
        ),
        array(
            'description' => esc_html__('در این قسمت در حد یک جمله کوتاه در رابطه با مدرس مورد نظر بنویسید.', 'venus'),
            'type' => 'textfield',
            'heading' => esc_html__('درباره مدرس', 'venus'),
            'param_name' => 'dec_teacher',
            'holder' => 'div',
        ),
        array(
            'type' => 'attach_image',
            'heading' => esc_html__('تصویر مدرس', 'venus'),
            'param_name' => 'image_teacher',
            'value' => '',
            'description' => esc_html__('انتخاب تصویر از کتابخانه رسانه ها', 'venus')
        ),
    )
));

if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Cdb_Teachers extends WPBakeryShortCodesContainer
    {
    }
}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_Cdb_Teacher extends WPBakeryShortCode
    {
    }
}
