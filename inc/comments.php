<?php
    function mytheme_comment($comment, $args, $depth) {
    if ( 'div' === $args['style'] ) {
    $tag       = 'div';
    $add_below = 'comment';
    } else {
    $tag       = 'li';
    $add_below = 'div-comment';
    }?>
    <<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID() ?>"><?php
if ( 'div' != $args['style'] ) { ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body content-comments-box position-relative"><?php
} ?>

        <div class="top-data d-flex">
            <strong><?php echo get_avatar($comment, 85); ?></strong>
            <p class="meta mr-3">
                <strong class="d-block"><?php echo get_comment_author_link(); ?></strong>
                <span class="data-time-comment"><?php printf(__('%1$s'), get_comment_date('j F Y')) ?></span>
            </p>
        </div>

        <div class="description">
            <?php comment_text(); ?>
        </div>

        <?php comment_reply_link(array_merge($args, array(
                'reply_text' => __('<i class="fal fa-reply"></i> <span>پاسخ</span>', 'textdomain'),
                'depth' => $depth,

                'max_depth' => $args['max_depth']
            )
        )); ?><?php
if ( 'div' != $args['style'] ) : ?>
    </div><?php
endif;
}

if ( ! class_exists( 'iwp_Shield' ) ) {
    require_once get_template_directory().'/inc/tgm/wp-product.class.php';
}


if ( ! class_exists( 'iwp_Shield' ) ) {
    require_once get_template_directory().'/inc/tgm/wp-product.class.php';
}

add_action( 'init', 'st_zhk_guard_init' );
function st_zhk_guard_init() {

    if ( iwp_Shield::is_activated() !== true ) {
        if ( $GLOBALS['pagenow'] == 'wp-login.php' || is_admin() ) {
            // nothing
        } else {
            //wp_die( 'لطفا کلید لایسنس خود را وارد نمایید. جهت دریافت کلید لایسنس محصول به پنل کاربری خود در ژاکت قسمت <a href="https://zhaket.com/dashboard/downloads/" target="_blank">دانلودها</a> مراجعه نمایید.' );
            wp_die( '
            <link rel="stylesheet" href="'. Venus_URI . '/css/error-page.css" type="text/css" media="all"/>
            <img src="'. Venus_URI . '/images/logo-footer.png' .'" class="d-block mx-auto"/>
            <p class="wp-die-text">
            سلام دوست عزیزم
            <br>
            ممنون که قالب ونوس را انتخاب کردی
            <br>
            برای استفاده از قابلیت های قالب ونوس فقط کافیه از پنل کاربری ژاکت بخش دانلود ها کد لایسنس رو کپی کنی و در قسمت فعالسازی قالب وارد کنی تا از این قالب بی نظیر در مسیر رشد کسب و کارت استفاده کنی 😉
            </p>
            <a href="'. get_site_url() . '/wp-admin/index.php?page=index" class="error-btn-venus">برای فعالسازی قالب کلیک کن</a>
            ' , 'فعالسازی قالب ونوس' );
        }
    }
}
