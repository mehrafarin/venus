<?php

if (!class_exists('Redux')) {
    return;
}

$opt_name = "venus_options";

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version' => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type' => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => __('تنظیمات قالب', 'venusshop'),
    'page_title' => __('تنظیمات قالب', 'venusshop'),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => false,
    // Use a asynchronous font on the front end or font string
    'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
//    'admin_bar_icon' => 'dashicons dashicons-admin-tools',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => 'venusshop',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    // Show the time the page took to load, etc
    'update_notice' => false,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => false,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => '',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => 'venus_options',
    // Page slug used to denote the panel
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    'footer_credit' => false,                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

    'use_cdn' => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    //'compiler'             => true,

    // HINTS
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'light',
            'shadow' => true,
            'rounded' => false,
            'style' => '',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'click mouseleave',
            ),
        ),
    )
);

Redux::setArgs($opt_name, $args);

// -> START Fields
Redux::setSection($opt_name, array(
    'title' => __('Styling', 'venusshop'),
    'id' => 'styling_section',
    'icon' => 'fal fa-paint-brush',
    'fields' => array(
        array(
            'id'       => 've_main_font',
            'type'     => 'select',
            'title' => __('فونت سایت', 'venus-theme'),
            'subtitle' => __('فونت سایت را انتخاب کنید.', 'venus-theme'),
            'options'  => array(
                'yekan' => __('ایران یکان', 'venus-theme'),
                'sans' =>  __('ایران سنس', 'venus-theme'),
                'dnsans' =>  __('ایران سنس دست نویس', 'venus-theme'),
                'farhang' =>  __('فرهنگ', 'venus-theme'),
            ),
            'default'  => 'yekan',
        ),
        array(
            'id' => 'body_background',
            'type' => 'background',
            'title' => __('Body Background', 'venusshop'),
            'desc' => __('Design site body background.', 'venusshop'),
            'default' => array(
                'background-color' => '#ffffff',
            ),
            'output' => array('background' => 'body')
        ),
        array(
            'id' => 'body_color',
            'type' => 'color',
            'title' => __('Body Color', 'venusshop'),
            'desc' => __('Choose site body text color.', 'venusshop'),
            'default' => '#212529',
            'output' => array('color' => 'body')
        ),
        array(
            'id' => 'body_link_color',
            'type' => 'color',
            'title' => __('Links Color', 'venusshop'),
            'desc' => __('Choose site links color.', 'venusshop'),
            'default' => '#007bff',
            'output' => array('color' => 'a')
        ),
        array(
            'id' => 'body_link_hover_color',
            'type' => 'color_rgba',
            'title' => __('Links Hover Color', 'venusshop'),
            'desc' => __('Choose site links hover color.', 'venusshop'),
            'default' => array(
                'color' => '#0056b3',
                'alpha' => 1
            ),
            'output' => array('color' => 'a:hover')
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Top Bar', 'venusshop'),
    'id' => 'top_bar_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'top_bar_background',
            'type' => 'background',
            'title' => __('Background', 'venusshop'),
            'desc' => __('Design your background to show on top bar.', 'venusshop'),
            'preview_height' => '60px',
            'default' => array(
                'background-repeat' => 'repeat-x',
                'background-position' => 'center top',
                'background-image' => get_template_directory_uri() . '/images/Topbar.jpg',
            ),
            'output' => array('background' => '.top-header-banner')
        ),
        array(
            'id' => 'top_bar_text_color',
            'type' => 'color',
            'title' => __('Text Color', 'venusshop'),
            'subtitle' => __('Pick a color for the top bar text.', 'venusshop'),
            'default' => '#ffffff',
            'output' => array('color' => '.top-header-banner, .top-header-banner a')
        ),
        array(
            'id' => 'top_bar_text_hover_color',
            'type' => 'color_rgba',
            'title' => __('Text Hover Color', 'venusshop'),
            'subtitle' => __('Pick a color for the top bar text hover.', 'venusshop'),
            'default' => array(
                'color' => '#ffffff',
                'alpha' => 1
            ),
            'output' => array('color' => '.top-header-banner:hover, .top-header-banner a:hover')
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Header', 'venusshop'),
    'id' => 'header_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'bg_header_color_top',
            'type' => 'color',
            'title' => __('رنگ پس زمینه هدر', 'venusshop'),
            'subtitle' => __('Pick a color for the header text.', 'venusshop'),
            'default' => '#fff',
            'output' => array('background-color' => '.main-header , .venus-header-fixed.fixed-active')
        ),
        array(
            'id' => 'header_color',
            'type' => 'color',
            'title' => __('Text Color', 'venusshop'),
            'subtitle' => __('Pick a color for the header text.', 'venusshop'),
            'default' => '#212529',
            'output' => array('color' => 'header')
        ),
        array(
            'id' => 'search_btn_icon',
            'type' => 'color',
            'title' => __('Search Button Icon Color', 'venusshop'),
            'subtitle' => __('Pick a color for the search button icon.', 'venusshop'),
            'default' => '#000000',
            'output' => array('color' => '.color-btn-icon-search-header')
        ),
        array(
            'id' => 'search_text_input',
            'type' => 'color',
            'title' => __('رنگ متن باکس جستجو', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن باکس جستجو.', 'venusshop'),
            'default' => '#000000',
            'output' => array('color' => '.text-color-search-header')
        ),
        array(
            'id' => 'cart_main_color',
            'type' => 'color',
            'title' => __('Cart Main Color', 'venusshop'),
            'subtitle' => __('Pick a color for the cart box main color.', 'venusshop'),
            'default' => '#000000',
            'output' => array('color' => '.hwp-mini-cart-area i')
        ),
        array(
            'id' => 'btn_color_text_userpanel',
            'type' => 'color',
            'title' => __('رنگ متن ورود و ثبت نام', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن ورود و ثبت نام.', 'venusshop'),
            'default' => '#000000',
            'output' => array('color' => '.register .user')
        ),
        array(
            'id' => 'btn_userpanel_bg',
            'type' => 'color',
            'title' => __('رنگ پس زمینه ورود و ثبت نام', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه ورود و ثبت نام.', 'venusshop'),
            'default' => '#FFD000',
            'output' => array('background-color' => '.register .user')
        ),

    )
));

Redux::setSection($opt_name, array(
    'title' => __('Main Menu', 'venusshop'),
    'id' => 'menu_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'menu_color_text_main_top',
            'type' => 'color',
            'title' => __('رنگ متن منو اصلی', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن منو.', 'venusshop'),
            'output' => array('color' => '.navigation ul li a')
        ),
        array(
            'id' => 'menu_color_hover_text_main_top',
            'type' => 'color_rgba',
            'title' => __('رنگ هاور منو اصلی', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای هاور منو.', 'venusshop'),
            'output' => array('background-color' => '.navigation ul li a:hover')
        ),
        array(
            'id' => 'menu_color_hover_active_text_main_top',
            'type' => 'color_rgba',
            'title' => __('رنگ هاور منو اصلی در حالت فعال', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای هاور منو درحالت فعال.', 'venusshop'),
            'output' => array('background-color' => '.navigation ul li.current-menu-item a')
        ),
        array(
            'id' => 'menu_color_under_bg_text_main_top',
            'type' => 'color',
            'title' => __('رنگ پس زمینه زیر منو اصلی', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه زیر منو.', 'venusshop'),
            'output' => array('background-color' => '.navigation ul li ul')
        ),
        array(
            'id' => 'menu_color_under_text_main_top',
            'type' => 'color',
            'title' => __('رنگ متن زیر منو اصلی', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن زیر منو.', 'venusshop'),
            'output' => array('color' => '.navigation ul li ul li a')
        ),
        array(
            'id' => 'menu_color_under_hover_text_main_top',
            'type' => 'color',
            'title' => __('رنگ هاور متن زیر منو اصلی', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای هاور متن زیر منو.', 'venusshop'),
            'output' => array('color' => '.navigation ul li ul li a:hover')
        ),
        array(
            'id' => 'menu_color_under_hover_bg_text_main_top',
            'type' => 'color',
            'title' => __('رنگ پس زمینه هاور زیر منو اصلی', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه هاور منو.', 'venusshop'),
            'output' => array('background' => '.navigation ul li ul li a:hover')
        ),
        array(
            'id' => 'menu_color_under_hover_active_bg_text_main_top',
            'type' => 'color_rgba',
            'title' => __('رنگ پس زمینه هاور زیر منو اصلی در حالت فعال', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه هاور منو در حالت فعال.', 'venusshop'),
            'output' => array('background' => '.navigation ul li ul li.current-menu-item a')
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('منو موبایل', 'venusshop'),
    'id' => 'menu_phone_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'menu_phone_color_bg_main_top',
            'type' => 'color',
            'title' => __('رنگ پس زمینه باکس عنوان سایت', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه باکس عنوان سایت', 'venusshop'),
            'output' => array('background' => '.sidr .logo')
        ),
        array(
            'id' => 'menu_phone_color_text_main_top',
            'type' => 'color',
            'title' => __('رنگ متن عنوان سایت', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن عنوان سایت', 'venusshop'),
            'output' => array('color' => '.sidr .logo .logo-details .logo-title')
        ),
        array(
            'id' => 'menu_phone_color_text_under_main_top',
            'type' => 'color',
            'title' => __('رنگ معرفی کوتاه', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای معرفی کوتاه', 'venusshop'),
            'output' => array('color' => '.sidr .logo .logo-details .logo-description')
        ),
        array(
            'id' => 'menu_phone_color_bg_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه اصلی', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه اصلی', 'venusshop'),
            'output' => array('background-color' => '.sidr ,.search-mob , .nav-container-mob , .sidr .mobile-menu-login , .sidr ul li ul li')
        ),
        array(
            'id' => 'menu_phone_ul_color_text_main',
            'type' => 'color',
            'title' => __('رنگ متن منو', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن منو', 'venusshop'),
            'output' => array('color' => '.sidr ul li a , .sub-menu-arrow , .sidr ul li ul li a')
        ),
        array(
            'id' => 'menu_phone_color_bg_btn_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه دکمه ورود و پنل کاربری', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه دکمه ورود و پنل کاربری', 'venusshop'),
            'output' => array('background-color' => '.sidr .mobile-menu-login a')
        ),
        array(
            'id' => 'menu_phone_color_bg_btn_text_main',
            'type' => 'color',
            'title' => __('رنگ متن دکمه ورود و پنل کاربری', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن دکمه ورود و پنل کاربری', 'venusshop'),
            'output' => array('color' => '.sidr .mobile-menu-login a')
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Footer', 'venusshop'),
    'id' => 'footer_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'footer_background',
            'type' => 'color_gradient',
            'title' => __('Background', 'venusshop'),
            'desc' => __('پس زمینه موردنظر خود را برای فوتر مشخص کنید.', 'venusshop'),
            'default' => array(
                'from' => '#d70d59',
                'to' => '#fc3924'
            ),
        ),
        array(
            'id' => 'footer_background_two',
            'type' => 'color',
            'title' => __('دومین پس زمینه فوتر', 'venusshop'),
            'desc' => __('پس زمینه موردنظر خود را برای فوتر مشخص کنید.', 'venusshop'),
            'output' => array('background' => '.footer-back:before')
        ),
        array(
            'id' => 'go_top_bg_color',
            'type' => 'color',
            'title' => __('Go To Top Icon Color', 'venusshop'),
            'subtitle' => __('Pick a color for the go to top icon.', 'venusshop'),
            'output' => array('background-color' => '#scrollTop:after, #scrollTop:before , #scrollTop')
        ),
//        array(
//            'id' => 'social_menu_color_text',
//            'type' => 'color',
//            'title' => __('رنگ آیکون شبکه های اجتماعی', 'venusshop'),
//            'subtitle' => __('انتخاب رنگ برای آیکون شبکه های اجتماعی', 'venusshop'),
//            'output' => array('color' => '.social-down li a')
//        ),
//        array(
//            'id' => 'social_menu_hover_color_text',
//            'type' => 'color',
//            'title' => __('رنگ هاور آیکون شبکه های اجتماعی', 'venusshop'),
//            'subtitle' => __('انتخاب رنگ برای هاور آیکون شبکه های اجتماعی', 'venusshop'),
//            'output' => array('color' => '.social-down li:hover a')
//        ),
        array(
            'id' => 'footer_menu_color_text',
            'type' => 'color',
            'title' => __('رنگ متن منو فوتر', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن منو فوتر', 'venusshop'),
            'output' => array('color' => '.menu-footer li a')
        ),
        array(
            'id' => 'footer_menu_hover_color_text',
            'type' => 'color',
            'title' => __('رنگ هاور متن منو فوتر', 'venusshop'),
            'subtitle' => __('انتخاب رنگ هاور برای متن منو فوتر', 'venusshop'),
            'output' => array('color' => '.menu-footer ul li:hover a')
        ),
        array(
            'id' => 'footer_copyright_text',
            'type' => 'color',
            'title' => __('Footer CopyRight Text Color', 'venusshop'),
            'subtitle' => __('Pick a color for the copyright text.', 'venusshop'),
            'output' => array('color' => '.prower')
        ),
        array(
            'id' => 'padding_top_footer_num',
            'type' => 'slider',
            'title' => __('فاصله فوتر از المان ها', 'venusshop'),
//            'desc'		=> __('', 'venusshop'),
            "default" => 310,
            "min" => 0,
            "step" => 1,
            "max" => 1000,
            'display_value' => 'text',
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('شبکه های اجتماعی', 'venusshop'),
    'id' => 'footer_social_style',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'color_icon_social',
            'type' => 'color',
            'title' => __('رنگ آیکون ها', 'venusshop'),
            'subtitle' => __('رنگ آیکون شبکه های اجتماعی', 'venusshop'),
            'output' => array('background-color' => '.footer-social .footer-social-icon span')
        ),
        array(
            'id' => 'color_icon_social_hover',
            'type' => 'color',
            'title' => __('رنگ هاور آیکون ها', 'venusshop'),
            'subtitle' => __('رنگ هاور آیکون شبکه های اجتماعی', 'venusshop'),
            'output' => array('background-color' => '.footer-social .footer-social-icon span:hover')
        ),

    )
));

Redux::setSection($opt_name, array(
    'title' => __('Shop', 'venusshop'),
    'id' => 'shop_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'padding_top_product_page',
            'type' => 'slider',
            'title' => __('فاصله محتوا صفحه دوره از هدر', 'venusshop'),
//            'desc'		=> __('', 'venusshop'),
            "default" => 120,
            "min" => 0,
            "step" => 1,
            "max" => 1000,
            'display_value' => 'text',
        ),
        array(
            'id' => 'color_text_product_main',
            'type' => 'color',
            'title' => __('رنگ عنوان محصول', 'venusshop'),
            'subtitle' => __('انتخاب رنگ عنوان محصول در صفحه محصول', 'venusshop'),
            'output' => array('color' => '.product-title')
        ),
        array(
            'id' => 'color_text_box_product_main',
            'type' => 'color',
            'title' => __('رنگ عنوان باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ عنوان باکس در صفحه محصول(مثال: معرفی دوره)', 'venusshop'),
            'output' => array('color' => '.single-product .section-title')
        ),
        array(
            'id' => 'color_text_box_border_product_main',
            'type' => 'color',
            'title' => __('رنگ خط عنوان باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ عنوان خط باکس در صفحه محصول(مثال: معرفی دوره)', 'venusshop'),
            'output' => array('background-color' => '.single-product .section-title:before')
        ),
        array(
            'id' => 'color_bg_course_lessons_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه جلسات دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه جلسات دوره', 'venusshop'),
            'output' => array('background-color' => '.course-content #course-lessons , .course-content #course-lessons:after')
        ),
        array(
            'id' => 'color_bg_course_lessons_card_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه سرفصل دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه سرفصل دوره', 'venusshop'),
            'output' => array('background' => '.course-content #course-lessons .card')
        ),
        array(
            'id' => 'color_bg_course_lessons_card_text_product_main',
            'type' => 'color',
            'title' => __('رنگ عنوان سرفصل دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ عنوان سرفصل دوره', 'venusshop'),
            'output' => array('color' => '.course-content #course-lessons .card .lesson-inner .lesson-title , .course-content #course-lessons .card button i.fa-chevron-down , .course-content #course-lessons .card .card-body > ul > li .session , .course-content #course-lessons .card .detail-list ul li')
        ),
        array(
            'id' => 'color_bg_course_lessons_card_text_under_product_main',
            'type' => 'color',
            'title' => __('رنگ  توضیح عنوان سرفصل دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ توضیح عنوان سرفصل دوره', 'venusshop'),
            'output' => array('color' => '.course-content #course-lessons .card .lesson-inner p , .course-content #course-lessons .card .card-body > ul > li .time')
        ),
        array(
            'id' => 'color_bg_course_lessons_card_text_under_detail_list_product_main',
            'type' => 'color',
            'title' => __('رنگ  پس زمینه توضیحات جلسات دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه توضیحات جلسه دوره دوره', 'venusshop'),
            'output' => array('background-color' => '.course-content #course-lessons .card .detail-list')
        ),
        array(
            'id' => 'color_teacher_name_product_main',
            'type' => 'color',
            'title' => __('رنگ متن نام مدرس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن نام مدرس', 'venusshop'),
            'output' => array('color' => '.course-content #course-teachers .teacher-name')
        ),
        array(
            'id' => 'color_teacher_name_detail_product_main',
            'type' => 'color',
            'title' => __('رنگ متن توضیحات مدرس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن توضیحات مدرس', 'venusshop'),
            'output' => array('color' => '.course-content #course-teachers .teacher-info p')
        ),
        array(
            'id' => 'color_text_faq_product_main',
            'type' => 'color',
            'title' => __('رنگ عنوان سوالات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ عنوان سوالات', 'venusshop'),
            'output' => array('color' => '.course-content #course-faq .panel .panel-heading .panel-title button.accordion-toggle')
        ),
        array(
            'id' => 'color_text_faq_icon_product_main',
            'type' => 'color',
            'title' => __('رنگ آیکون عنوان سوالات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ آیکون عنوان سوالات', 'venusshop'),
            'output' => array('color' => '.course-content #course-faq .panel .panel-heading .panel-title button.accordion-toggle:before')
        ),
        array(
            'id' => 'color_text_faq_under_product_main',
            'type' => 'color',
            'title' => __('رنگ خط جداکننده سوالات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ خط جداکننده سوالات', 'venusshop'),
            'output' => array('border-color' => '.course-content #course-faq .panel')
        ),
        array(
            'id' => 'color_text_faq_answer_product_main',
            'type' => 'color',
            'title' => __('رنگ متن جواب سوالات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن جواب سوالات', 'venusshop'),
            'output' => array('color' => '.course-content #course-faq .panel .panel-body')
        ),
        array(
            'id' => 'color_bg_advice_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه درخواست مشاوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه درخواست مشاوره', 'venusshop'),
            'output' => array('background-color' => '.style-form .advice .advice-content .form-box')
        ),
        array(
            'id' => 'color_text_advice_product_main',
            'type' => 'color',
            'title' => __('رنگ متن های داخل درخواست مشاوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن های داخل درخواست مشاوره', 'venusshop'),
            'output' => array('color' => '.style-form .advice .advice-content .form-box')
        ),
        array(
            'id' => 'color_bg_title_register_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه نوع دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه نوع دوره', 'venusshop'),
            'output' => array('background-color' => '.course-register .title-register')
        ),
        array(
            'id' => 'color_text_title_register_product_main',
            'type' => 'color',
            'title' => __('رنگ متن نوع دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن نوع دوره', 'venusshop'),
            'output' => array('color' => '.course-register .title-register')
        ),
        array(
            'id' => 'color_bg_box_register_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه باکس ثبت نام دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه باکس ثبت نام دوره', 'venusshop'),
            'output' => array('background' => '.course-register .content-register')
        ),
        array(
            'id' => 'color_bg_btn_register_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه دکمه ثبت نام دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه دکمه ثبت نام دوره', 'venusshop'),
        ),
        array(
            'id' => 'color_bg_btn_text_register_product_main',
            'type' => 'color',
            'title' => __('رنگ متن دکمه ثبت نام دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن دکمه ثبت نام دوره', 'venusshop'),
        ),
        array(
            'id' => 'color_text_price_register_product_main',
            'type' => 'color',
            'title' => __('رنگ قیمت ثبت نام دوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ قیمت ثبت نام دوره', 'venusshop'),
            'output' => array('color' => '.price-box .woocommerce-Price-amount')
        ),
        array(
            'id' => 'color_btn_info_register_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه اطلاعات ثبت نام', 'venusshop'),
            'subtitle' => __('انتخاب پس زمینه اطلاعات ثبت نام(سایدبار)', 'venusshop'),
            'output' => array('background-color' => '.sidebar .sidebar-inner .sidebar-buttons a')
        ),
        array(
            'id' => 'color_btn_info_text_register_product_main',
            'type' => 'color',
            'title' => __('رنگ متن اطلاعات ثبت نام', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن اطلاعات ثبت نام(سایدبار)', 'venusshop'),
            'output' => array('color' => '.sidebar .sidebar-inner .sidebar-buttons a')
        ),
        array(
            'id' => 'color_btn_advice_product_main',
            'type' => 'color',
            'title' => __('رنگ متن درخواست مشاوره', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن درخواست مشاوره(سایدبار)', 'venusshop'),
            'output' => array('color' => '.sidebar .sidebar-inner .sidebar-buttons a.requet-course-advice')
        ),
        array(
            'id' => 'color_btn_related_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه دکمه اسلایدر', 'venusshop'),
            'subtitle' => __('انتخاب رنگ پس زمینه دکمه سلایدر دوره های مرتبط', 'venusshop'),
            'output' => array('background' => '.swiper-button-next, .swiper-button-prev')
        ),
        array(
            'id' => 'color_btn_icon_related_product_main',
            'type' => 'color',
            'title' => __('رنگ آیکون دکمه اسلایدر', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای آیکون دکمه سلایدر دوره های مرتبط', 'venusshop'),
            'default' => '#000000',
            'output' => array('color' => '.swiper-button-next:after, .swiper-button-prev:after')
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('پنل کاربری', 'venusshop'),
    'id' => 'dashboard_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'user_name_color_text',
            'type' => 'color',
            'title' => __('رنگ نام نمایشی کاربر', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای نام نمایشی کاربر', 'venusshop'),
            'output' => array('color' => '.woocommerce-MyAccount-navigation .upanel-sidebar-inner .user-name')
        ),
        array(
            'id' => 'dashboard_menu_right_color_text',
            'type' => 'color',
            'title' => __('رنگ متن منو پنل کاربری', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن منو پنل کاربری', 'venusshop'),
            'output' => array('color' => '.woocommerce-MyAccount-navigation .upanel-sidebar-inner .panel-navigation ul li a')
        ),
        array(
            'id' => 'dashboard_menu_right_hover_color_text',
            'type' => 'color_gradient',
            'title' => __('رنگ هاور منو پنل کاربری', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای هاور منو پنل کاربری', 'venusshop'),
            'default' => array(
                'from' => '#d70d59',
                'to' => '#fc3924'
            ),
        ),
        array(
            'id' => 'user_panel_left_bg',
            'type' => 'color',
            'title' => __('رنگ پس زمینه', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه  باکس سمت چپ', 'venusshop'),
            'output' => array('background-color' => '.upanel-content .upanel-content-inner , .upanel-content .upanel-content-inner:before , .upanel-content .upanel-content-inner:after')
        ),
        array(
            'id' => 'user_panel_icon_bg_top_color',
            'type' => 'color_gradient',
            'title' => __('رنگ پس زمینه آیکون باکس اطلاعات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه آیکون  باکس اطلاعات', 'venusshop'),
            'default' => array(
                'from' => '#d70d59',
                'to' => '#fc3924'
            ),
        ),
        array(
            'id' => 'user_panel_icon_bg_color',
            'type' => 'color',
            'title' => __('رنگ پس زمینه باکس اطلاعات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه  باکس اطلاعات', 'venusshop'),
            'output' => array(
                'background' => '.status-user-widget ul li , .notifications-box',
                'background-color' => '.p-con-notif',
            )
        ),
        array(
            'id' => 'user_panel_icon_text_color',
            'type' => 'color',
            'title' => __('رنگ متن داخل باکس اطلاعات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن داخل  باکس اطلاعات', 'venusshop'),
            'default' => '#212529',
            'output' => array('color' => '.status-user-widget ul li , .status-user-widget ul li .key_wrapper .wc-amount a , .notifications-box')
        ),
        array(
            'id' => 'user_panel_icon_border_color',
            'type' => 'color',
            'title' => __('رنگ خط دور باکس اطلاعات', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای خط دور  باکس اطلاعات', 'venusshop'),
            'output' => array('border-color' => '.status-user-widget ul li , .notifications-box')
        ),
        array(
            'id' => 'user_login_icon_img',
            'type' => 'media',
            'title' => __('عکس لوگو در صفحه ورود به حساب کاربری', 'venusshop'),
            'default' => array('url' => get_template_directory_uri() . '/images/user-login.png'),
            'desc' => __('عکس مورد نظر خود را برای نمایش انتخاب کنید.(سایز عکس  132*132)', 'venusshop'),
        ),
        array(
            'id' => 'user_login_icon_bg_color',
            'type' => 'color',
            'title' => __('رنگ پس زمینه لوگو ورود به حساب کاربری', 'venusshop'),
            'subtitle' => __('رنگ مورد نظر خود را انتخاب کنید.', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('سایدبار', 'venusshop'),
    'id' => 'sidebar_style_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'side_bg_widget',
            'type' => 'color',
            'title' => __('پس زمینه باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه باکس ابزارک', 'venusshop'),
            'default' => '#fff',
            'output' => array('background-color' => '.blog-sidebar .widget')
        ),
        array(
            'id' => 'side_color_title_widget',
            'type' => 'color',
            'title' => __('عنوان باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای عنوان باکس ابزارک', 'venusshop'),
            'default' => '#212529',
            'output' => array('color' => '.blog-sidebar .widget .widget-title')
        ),
        array(
            'id' => 'side_line_color_title_widget',
            'type' => 'color',
            'title' => __('خط کناری عنوان', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای خط کناری عنوان باکس ابزارک', 'venusshop'),
            'default' => '#f7b625',
            'output' => array('background-color' => '.blog-sidebar .widget .widget-title:before')
        ),
        array(
            'id' => 'side_under_line_color_title_widget',
            'type' => 'color',
            'title' => __('خط زیر عنوان', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای خط زیر عنوان باکس ابزارک', 'venusshop'),
            'default' => '#f4f4f4',
            'output' => array('background-color' => '.blog-sidebar .widget .widget-title:after')
        ),
        array(
            'id' => 'side_color_in_text_widget',
            'type' => 'color',
            'title' => __('متن داخلی باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن های داخل باکس ابزارک', 'venusshop'),
            'default' => '#464749',
            'output' => array('color' => '.blog-sidebar .widget ul li a')
        ),
        array(
            'id' => 'side_hover_color_in_text_widget',
            'type' => 'color',
            'title' => __('هاور متن داخلی باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ هاور برای متن های داخل باکس ابزارک', 'venusshop'),
            'default' => '#f7b625',
            'output' => array(
                'color' => '.blog-sidebar .widget ul li a:hover , .blog-sidebar .woocommerce .product_list_widget li a:hover .product-title'
            )
        ),
        array(
            'id' => 'side_color_in_text_widget',
            'type' => 'color',
            'title' => __('حاشیه کناری متن داخلی باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای حاشیه کناری متن های داخل باکس ابزارک', 'venusshop'),
            'default' => '#d6e0e2',
            'output' => array(
                'border-color' => '.blog-sidebar .widget ul li , .blog-sidebar .widget ul li:before'
            )
        ),
        array(
            'id' => 'side_hover_in_text_widget',
            'type' => 'color',
            'title' => __('هاور حاشیه کناری متن داخلی باکس', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای هاور حاشیه کناری متن های داخل باکس ابزارک', 'venusshop'),
            'default' => '#f7b625',
            'important' => true,
            'output' => array(
                'border-color' => '.blog-sidebar .widget ul li:hover',
                'background' => '.blog-sidebar .widget ul li:hover:before'
            )
        ),
//        array(
//            'id' => 'top_bar_text_hover_color',
//            'type' => 'color_rgba',
//            'title' => __('Text Hover Color', 'venusshop'),
//            'subtitle' => __('Pick a color for the top bar text hover.', 'venusshop'),
//            'default' => array(
//                'color' => '#ffffff',
//                'alpha' => 1
//            ),
//            'output' => array('color' => '.top-header-banner:hover, .top-header-banner a:hover')
//        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('General', 'venusshop'),
    'id' => 'general_section',
    'icon' => 'fal fa-home',
    'fields' => array(
        array(
            'id' => 'favicon',
            'type' => 'media',
            'title' => __('Your Favicon', 'venusshop'),
            'default' => array('url' => get_template_directory_uri() . '/images/favicon.png'),
            'desc' => __('Choose your favicon to show on browser.', 'venusshop'),
        ),
        array(
            'id' => 'main_back_to_top',
            'type' => 'switch',
            'title' => __('بازگشت به بالا', 'venusshop'),
            'subtitle' => __('نمایش بازگشت به بالا', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('حالت تعمیر', 'venusshop'),
    'id' => 'venus_maintenance',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'switch_maintenance',
            'type' => 'switch',
            'title' => __('فعال کردن حالت در دست تعمیر', 'venusshop'),
            'subtitle' => __('در این حالت صفحه در دست تعمیر به کاربران نمایش داده می شود.', 'venusshop'),
            'default' => false,
        ),
        array(
            'id' => 'venus_maintenance_img',
            'type' => 'media',
            'title' => __('تصویر', 'venusshop'),
            'default' => array('url' => get_template_directory_uri() . '/images/maintenance.svg'),
            'desc' => __('تصویر مورد نظر خود را انتخاب نمایید', 'venusshop'),
            'required' => array('switch_maintenance', '=', true),
        ),
        array(
            'id' => 'venus_maintenance_title',
            'type' => 'text',
            'title' => __('عنوان', 'venusshop'),
            'subtitle' => __('عنوان در دست تعمیر', 'venusshop'),
            'default' => __('حالت بروزرسانی', 'venusshop'),
            'required' => array('switch_maintenance', '=', true),
        ),
        array(
            'id' => 'venus_maintenance_editor',
            'type' => 'editor',
            'title' => __('متن توضیح', 'venusshop'),
            'default' => __('مهندسین ما در حال کار بر روی سایت هستند و به زودی خواهیم آمد :)', 'venusshop'),
            'args' => array(
                'wpautop' => false,
                'media_buttons' => false,
                'textarea_rows' => 5,
                'teeny' => false,
                'quicktags' => true,
            ),
            'required' => array('switch_maintenance', '=', true),
        ),
        array(
            'id' => 'venus_maintenance_background',
            'type' => 'background',
            'title' => __('Body Background', 'venusshop'),
            'desc' => __('Design site body background.', 'venusshop'),
            'default' => array(
                'background-color' => '#ffffff',
            ),
            'output' => array('background' => '.venus-maintenance'),
            'required' => array('switch_maintenance', '=', true),

        ),
    ),
));

Redux::setSection($opt_name, array(
    'title' => __('Header', 'venusshop'),
    'id' => 'header_section',
    'icon' => 'fal fa-bookmark',
    'fields' => array(
        array(
            'id' => 'select_header',
            'type' => 'select',
            'title' => __('استایل هدر', 'venus-theme'),
            'options' => venus_config_get_header_array(),
            'default' => 'venus_default_header',
        ),
        array(
            'id' => 'header_logo_mood',
            'type' => 'switch',
            'title' => __('لوگو هدر', 'venusshop'),
            'subtitle' => __('فعال و غیر فعال کردن لوگو هدر', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('select_header', '=', 'venus_default_header'),
        ),
        array(
            'id' => 'logo',
            'type' => 'media',
            'title' => __('Your Logo', 'venusshop'),
            'default' => array(
                'url' => get_template_directory_uri() . '/images/logo_header.png',
            ),
            'desc' => __('Choose your logo to show on header', 'venusshop'),
            'required' => array('header_logo_mood', '=', true),
        ),
        array(
            'id' => 'header_sticky_menu',
            'type' => 'switch',
            'title' => __('منوی چسبان', 'venusshop'),
            'subtitle' => __('فعال و غیر فعال کردن منوی سربرگ چسبان', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('select_header', '=', 'venus_default_header'),
        ),
        array(
            'id' => 'show_cart',
            'type' => 'switch',
            'title' => __('Dispaly cart box', 'venusshop'),
            'subtitle' => __('Dispaly cart box in header or not', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('select_header', '=', 'venus_default_header'),
        ),
    )
));
//Redux::setSection($opt_name, array(
//    'title' => __('سبد خرید', 'venusshop'),
//    'id' => 'cart_header_section',
//    'subsection' => true,
//    'fields' => array(
//        array(
//            'id' => 'style_btn_cart_header',
//            'type' => 'select',
//            'title' => __('استایل سبد خرید', 'venusshop'),
//            'subtitle' => __('حالت نمایش سبد خرید را انتخاب کنید', 'venusshop'),
//            'options' => array(
//                'default' => 'پیش فرض',
//                'style_one' => 'استایل اول',
//            ),
//            'default' => 'default',
//        ),
//    )
//));
Redux::setSection($opt_name, array(
    'title' => __('Top Bar', 'venusshop'),
    'id' => 'top_bar_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'top_bar',
            'type' => 'switch',
            'title' => __('Enable top bar', 'venusshop'),
            'subtitle' => __('Enable top bar for display custom offers', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'top_bar_height',
            'type' => 'dimensions',
            'units' => array('px'),
            'title' => __('Top Bar Height', 'venusshop'),
            'subtitle' => __('Insert your desired top bar height in pixel.', 'venusshop'),
            'default' => array(
                'height' => '60',
                'units' => 'px'
            ),
            'width' => false,
            'required' => array('top_bar', '=', true),
            'output' => array('height' => '.top-header-banner')
        ),
        array(
            'id' => 'top_bar_text',
            'type' => 'text',
            'title' => __('Your Text', 'venusshop'),
            'required' => array('top_bar', '=', true)
        ),
        array(
            'id' => 'top_bar_link',
            'type' => 'text',
            'title' => __('Top Bar Link', 'venusshop'),
            'subtitle' => __('Insert your link for top bar', 'venusshop'),
            'required' => array('top_bar', '=', true)
        ),
    )
));

//Redux::setSection($opt_name, array(
//    'title' => __('Main Menu', 'venusshop'),
//    'id' => 'main_menu_section',
//    'icon' => 'el el-list',
//    'subsection' => true,
//
//));

Redux::setSection($opt_name, array(
    'title' => __('منو موبایل', 'venusshop'),
    'id' => 'main_menu_mobile_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'mobile_menu',
            'type' => 'switch',
            'title' => __('Using Different Menu On Mobile Theme', 'venusshop'),
            'subtitle' => __('If enable this option, menu mobile location add to nav and you can choose different menu.', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'mobile_menu_title',
            'type' => 'switch',
            'title' => __('استفاده از عکس به جای عنوان سایت', 'venusshop'),
            'subtitle' => __('در صورت فعال سازی این گزینه، به جای عنوان سایت ، عکس مورد نظر نمایش داده می شود.', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'logo_mobile_image',
            'type' => 'media',
            'title' => __('Your Logo', 'venusshop'),
            'required' => array('mobile_menu_title', '=', true),
            'desc' => __('Choose your logo to show on header', 'venusshop'),
        ),

    )
));

Redux::setSection($opt_name, array(
    'title' => __('ورود و عضویت', 'venusshop'),
    'id' => 'user_account_section',
    'icon' => 'fal fa-user-alt',
    'fields' => array(
        array(
            'id' => 'show_account_user',
            'type' => 'switch',
            'title' => __('نمایش ورود و عضویت', 'venusshop'),
            'subtitle' => __('نمایش یا عدم نمایش دکمه ورود و عضویت در هدر', 'venusshop'),
            'default' => 1,
            'on' => __('فعال', 'venusshop'),
            'off' => __('غیرفعال', 'venusshop'),
        ),
        array(
            'id' => 'modal_login',
            'type' => 'switch',
            'title' => __('Use Popup Login', 'venusshop'),
            'subtitle' => __('Use popup login instead of login page.', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('show_account_user', '=', true)
        ),
        array(
            'id' => 'style_btn_profile_header',
            'type' => 'select',
            'title' => __('استایل دکمه پنل کاربری', 'venusshop'),
            'subtitle' => __('حالت نمایش دکمه پنل کاربری را انتخاب کنید', 'venusshop'),
            'options' => array(
                'default' => 'پیش فرض',
                'style_one' => 'استایل اول',
            ),
            'default' => 'style_one',
            'required' => array('show_account_user', '=', true)
        ),
        array(
            'id' => 'login_text_panel',
            'type' => 'text',
            'title' => __('متن دکمه ورود و ثبت نام', 'venusshop'),
            'subtitle' => __('شما می توانید متن ورود و ثبت نام را تغییر دهید.', 'venusshop'),
            'default' => __('ورود و ثبت نام', 'venusshop'),
            'required' => array('show_account_user', '=', true)
        ),
        array(
            'id' => 'text_panel_header',
            'type' => 'text',
            'title' => __('متن پنل کاربری', 'venusshop'),
            'default' => __('پنل کاربری', 'venusshop'),
            'required' => array('show_account_user', '=', true)
        ),
    )
));
Redux::setSection($opt_name, array(
    'title' => __('حالت وارد شده', 'venus-theme'),
    'id' => 'user_logged_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'modal_logged_menu_link',
            'type' => 'switch',
            'title' => __('منو پنل کاربری', 'venus-theme'),
            'subtitle' => __('منو دکمه پنل کاربری که در هدر می باشد.', 'venus-theme'),
            'default' => 1,
            'on' => __('پیش فرض', 'venus-theme'),
            'off' => __('انتخاب منو', 'venus-theme'),
        ),
        array(
            'id' => 'menu_panel_header',
            'type' => 'select',
            'title' => __('انتخاب منو', 'venus-theme'),
            'subtitle' => __('حالت نمایش دکمه پنل کاربری را انتخاب کنید', 'venus-theme'),
            'options' => venus_menu_panel(),
            'required' => array('modal_logged_menu_link', '=', false)
        ),

    )
));
Redux::setSection($opt_name, array(
    'title' => __('جستجو', 'venusshop'),
    'id' => 'main_search_section',
    'icon' => 'fal fa-search',
    'fields' => array(
        array(
            'id' => 'style_main_search_header',
            'type' => 'select',
            'title' => __('استایل جستجو', 'venusshop'),
            'subtitle' => __('حالت نمایش جستجو را انتخاب کنید', 'venusshop'),
            'options' => array(
                'default' => 'پیش فرض',
                'style_one' => 'استایل اول',
            ),
            'default' => 'default',
        ),
        array(
            'id' => 'show_search',
            'type' => 'switch',
            'title' => __('Dispaly search box', 'venusshop'),
            'subtitle' => __('نمایش یا عدم نمایش جعبه جستجو', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'text_box_search_main',
            'type' => 'text',
            'title' => __('متن باکس سرچ', 'venusshop'),
            'subtitle' => __('متن باکس سرچ خود را وارد نمایید', 'venusshop'),
            'default' => __('جستجو …', 'venusshop'),
        ),
        array(
            'id' => 'opt_select_style__show_result_search',
            'type' => 'select',
            'title' => __('استایل نمایش جستجو', 'venusshop'),
            'subtitle' => __('حالت نمایش نتیجه جستجو ها را انتخاب کنید', 'venusshop'),
            'options' => array(
                'default' => 'پیش فرض',
                'style_result_search_one' => 'استایل اول',
            ),
            'default' => 'style_result_search_one',
        ),
        array(
            'id' => 'icon_search_all_show_format',
            'type' => 'switch',
            'title' => __('نوع نوشته', 'venusshop'),
            'subtitle' => __('حالت نمایشی آیکون ویدیو و پادکست', 'venusshop'),
            'default' => 1,
            'on' => __('فعال', 'venusshop'),
            'off' => __('غیرفعال', 'venusshop'),
            'required' => array('opt_select_style__show_result_search', '=', 'style_result_search_one'),
        ),
        array(
            'id' => 'waves_search_all_show',
            'type' => 'switch',
            'title' => __('شکل امواج', 'venusshop'),
            'subtitle' => __('حالت نمایشی موج ها در مقالات مرتبط', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('opt_select_style__show_result_search', '=', 'style_result_search_one'),
        ),
    )
));
Redux::setSection($opt_name, array(
    'title' => __('Shop', 'venusshop'),
    'id' => 'shop_section',
    'icon' => 'fal fa-shopping-cart',
    'fields' => array(
        array(
            'id' => 'add_cart_course_screen',
            'type' => 'switch',
            'title' => __('دکمه خرید دوره ها', 'venusshop'),
            'subtitle' => __('بر روی کاور دوره نمایش داده می شود.', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'discount_percent',
            'type' => 'switch',
            'title' => __('Display Discount Percent', 'venusshop'),
            'subtitle' => __('Display discount percent on shop pages and product sliders.', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'info_product_bottom',
            'type' => 'switch',
            'title' => __('اطلاعات دوره', 'venusshop'),
            'subtitle' => __('حالت نمایشی(سطح دوره ، نوع دوره ، هزینه دوره)', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'waves_product_show',
            'type' => 'switch',
            'title' => __('شکل امواج', 'venusshop'),
            'subtitle' => __('حالت نمایشی موج ها در صفحه ارشیو محصولات', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Product Page', 'venusshop'),
    'id' => 'product_page_section',
    'icon' => 'fal fa-desktop',
    'fields' => array(
        array(
            'id' => 'style_page_products',
            'type' => 'select',
            'title' => 'استایل صفحه محصول',
            'subtitle' => 'استایل کلی صفحات محصولات سایت خود را انتخاب کنید.',
            'options' => array(
                'product-style-one' => 'استایل اول (همیارآکادمی)',
                'product-style-two' => 'استایل دوم (دیجیکالا)',
            ),
            'default' => 'product-style-one'
        ),
        array(
            'id' => 'status_cart_quantity',
            'type' => 'switch',
            'title' => __('تعداد خرید', 'venusshop'),
            'subtitle' => __('حالت نمایش انتخاب تعداد خرید در صفحه محصول', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'status_add_cart_popup',
            'type' => 'switch',
            'title' => __('پاپ آپ افزودن به سبد خرید', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'buy_less',
            'title' => esc_html__('جلوگیری از خرید مجدد دوره', 'venusshop'),
            'subtitle' => esc_html__('فعال و غیر فعال کردن جلوگیری از خرید مجدد دوره', 'venusshop'),
            'type' => 'switch',
            'default' => true,
            'on' => esc_html__('فعال', 'venusshop'),
            'off' => esc_html__('غیرفعال', 'venusshop')
        ),
        array(
            'id' => 'buy_less_text',
            'type' => 'text',
            'default' => 'دانشجوی دوره هستید',
            'title' => esc_html__('متن بعد از خرید دوره', 'venusshop'),
            'required' => array('buy_less', '=', true),
        ),
        array(
            'id' => 'show_related',
            'type' => 'switch',
            'title' => __('Dispaly Related Products', 'venusshop'),
            'subtitle' => __('Dispaly related products on product page', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'info_product_bottom_show_related',
            'type' => 'switch',
            'title' => __('اطلاعات دوره', 'venusshop'),
            'subtitle' => __('حالت نمایشی(سطح دوره ، نوع دوره ، هزینه دوره)دوره های مرتبط', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('show_related', '=', true),
        ),
        array(
            'id' => 'waves_product_show_related',
            'type' => 'switch',
            'title' => __('شکل امواج', 'venusshop'),
            'subtitle' => __('حالت نمایشی موج ها در دوره های مرتبط', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('show_related', '=', true),
        ),
        array(
            'id' => 'show_related_text',
            'type' => 'text',
            'title' => __('عنوان تب دوره های مرتبط', 'venusshop'),
            'subtitle' => __('عنوان موردنظر خود برای تب دوره های مرتبط را وارد کنید.', 'venusshop'),
            'default' => __('دوره های مرتبط', 'venusshop'),
            'required' => array('show_related', '=', true),

        ),
        array(
            'id' => 'text_amazing_product',
            'type' => 'text',
            'title' => __('متن پیشنهاد شگفت انگیز', 'venusshop'),
            'subtitle' => __('شما می توانید متن پیشنهاد شگفت انگیز دوره را تغییر دهید.', 'venusshop'),
            'default' => __('پیشنهاد شگفت انگیز', 'venusshop'),
        ),
        array(
            'id' => 'text_register_btn_product_page',
            'type' => 'text',
            'title' => __('متن ثبت نام دوره', 'venusshop'),
            'subtitle' => __('شما می توانید متن ثبت نام دوره را تغییر دهید.', 'venusshop'),
            'default' => __('ثبت نام دوره', 'venusshop'),
        ),
        array(
            'id' => 'text_register_btn_fixed_product_page',
            'type' => 'text',
            'title' => __('متن ثابت ثبت نام دوره', 'venusshop'),
            'subtitle' => __('این متن در حالت موبایل برای دسترسی سریع به دکمه ثبت نام می باشد.', 'venusshop'),
            'default' => __('ثبت نام دوره', 'venusshop'),
        ),
        array(
            'id' => 'reviews_tab_heading',
            'type' => 'text',
            'title' => __('Reviews Tab Title', 'venusshop'),
            'subtitle' => __('Insert your title for reviews tab.', 'venusshop'),
            'default' => __('دیدگاهها', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('آرشیو محصولات', 'venusshop'),
    'id' => 'shop_section_archive',
    'subsection' => true,
//    'icon' => 'el el-shopping-cart',
    'fields' => array(
        array(
            'id' => 'style_archive_products',
            'type' => 'select',
            'title' => 'استایل صفحه آرشیو محصولات',
            'subtitle' => 'استایل نمایش محصولات در صفحه دسته بندی خود را انتخاب کنید.',
            'options' => array(
                'archive-product-style-one' => 'استایل اول (همیارآکادمی)',
                'archive-product-style-two' => 'استایل دوم (دیجیکالا)',
            ),
            'default' => 'archive-product-style-one'
        ),
        array(
            'id' => 'sidebar_position_shop',
            'type' => 'image_select',
            'title' => esc_html__('موقعیت سایدبار آرشیو محصولات', 'venusshop'),
            'subtitle' => esc_html__('موقعیت سایدبار آرشیو محصولات را تنظیم یا مخفی کنید', 'venusshop'),
            'default' => 'none',
            'options' => array(
                'none' => array(
                    'alt' => esc_html__('No Sidebar', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                ),
                'left' => array(
                    'alt' => esc_html__('Sidebar Left', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                ),
                'right' => array(
                    'alt' => esc_html__('Sidebar Right', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                ),
            )
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('حالت موبایل', 'venusshop'),
    'id' => 'shop_phone_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'add_cart_btn_phone_bottom',
            'type' => 'switch',
            'title' => __('دکمه ثبت نام دوره', 'venusshop'),
            'subtitle' => __('می توانید دکمه را در حالت موبایل فعال یا غیرفعال کنید.', 'venusshop'),
            'default' => 1,
            'on' => __('فعال', 'venusshop'),
            'off' => __('غیرفعال', 'venusshop'),
        ),
        array(
            'id' => 'style_add_cart_fix_products',
            'type' => 'select',
            'title' => 'استایل دکمه ثبت نام ثابت',
            'subtitle' => 'استایل نمایش دکمه ثابت ثبت نام دوره را انتخاب کنید.',
            'options' => array(
                'btn-fix-add-cart-one' => 'استایل اول (ساده)',
                'btn-fix-add-cart-two' => 'استایل دوم (دارای عکس و قیمت)',
            ),
            'default' => 'btn-fix-add-cart-one'
        ),
    )
));


Redux::setSection($opt_name, array(
    'title' => __('Comment', 'venusshop'),
    'id' => 'comment_section',
    'icon' => 'fal fa-comments-alt',
    'fields' => array(
        array(
            'id' => 'color_text_comment_product_main',
            'type' => 'color',
            'title' => __('رنگ متن کامنت', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای متن کامنت', 'venusshop'),
            'output' => array(
                'color' => '.box-comment .comment-list li .content-comments-box .description',
                'border-color' => '.box-comment .comment-list li , .box-comment .comment-list li ul li',
            )
        ),
        array(
            'id' => 'color_bg_comment_product_main',
            'type' => 'color',
            'title' => __('رنگ پس زمینه باکس کامنت', 'venusshop'),
            'subtitle' => __('انتخاب رنگ برای پس زمینه  باکس کامنت', 'venusshop'),
            'output' => array(
                'background' => '.comment-respond',
            )
        ),
        array(
            'id' => 'color_bg_text_all_comment_product_main',
            'type' => 'color',
            'title' => __('رنگ متن های باکس کامنت', 'venusshop'),
            'subtitle' => __('انتخاب رنگ متن های باکس کامنت', 'venusshop'),
            'output' => array(
                'color' => '.comment-respond',
            )
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Account', 'venusshop'),
    'id' => 'account_section',
    'icon' => 'fal fa-user-edit',
    'fields' => array(
        array(
            'id' => 'select_dashboard',
            'type' => 'select',
            'title' => __('استایل پنل کاربری', 'venus-theme'),
            'options' => array(
                'default' => 'پیش فرض',
                'style_one_dashboard' => 'استایل دیجیکالا',
            ),
            'default' => 'default',
        ),
        array(
            'id' => 'text_login_form_user_woo',
            'type' => 'editor',
            'title' => __('متن کنار فرم عضویت و ورود', 'venusshop'),
            'args' => array(
                'wpautop' => false,
                'teeny' => true
            ),
        ),
        array(
            'id' => 'account_notify',
            'type' => 'switch',
            'title' => __('Display Notifications', 'venusshop'),
            'subtitle' => __('Display notifications tab on account page.', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
        array(
            'id' => 'notify_title_text_new',
            'type' => 'text',
            'title' => __('متن جدیدترین اطلاعیه ها', 'venusshop'),
            'subtitle' => __('شما می توانید متن جدیدترین اطلاعیه ها را تغییر دهید.', 'venusshop'),
            'required' => array('account_notify', '=', true),
            'default' => __('جدیدترین اطلاعیه ها', 'venusshop'),
        ),
        array(
            'id' => 'repetitive_notify_count',
            'type' => 'slider',
            'title' => __('تعداد نمایش اطلاعیه', 'venusshop'),
            'desc' => __('تعداد نمایش اطلاعیه را مشخص کنید،عدد صفر همه موارد رو نمایش می دهد.', 'venusshop'),
            "default" => 3,
            "min" => 0,
            "step" => 1,
            "max" => 10,
            'required' => array('account_notify', '=', true),
            'display_value' => 'text',
        ),
        array(
            'id' => 'account_profile_pic',
            'type' => 'switch',
            'title' => __('تغییر پروفایل کاربری', 'venusshop'),
            'subtitle' => __('با فعال کردن این امکان کاربر میتواند در اطلاعات حساب کاربری خود عکس پروفایل خود را آپلود کند و تغییر دهد.', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('باکس اطلاعات', 'venusshop'),
    'id' => 'info_box_dashboard',
    'subsection' => true,
    'desc' => __('از این بخش شما باکس های اطلاعاتی در پنل کاربری را کنترل می کنید.', 'venusshop'),
    'fields' => array(
        array(
            'id' => 've_info_dashboard_title_box_one',
            'type' => 'text',
            'title' => __('عنوان باکس اول', 'venusshop'),
            'subtitle' => __('', 'venusshop'),
            'default' => __('دوره', 'venusshop'),
        ),
        array(
            'id' => 've_info_dashboard_sub_text_box_one',
            'type' => 'text',
            'title' => __('زیر عنوان باکس اول', 'venusshop'),
            'subtitle' => __('', 'venusshop'),
            'default' => __('در سایت وجود دارد', 'venusshop'),
        ),
        array(
            'id' => 've_info_dashboard_title_box_two',
            'type' => 'text',
            'title' => __('عنوان باکس دوم', 'venusshop'),
            'subtitle' => __('', 'venusshop'),
            'default' => __('دوره', 'venusshop'),
        ),
        array(
            'id' => 've_info_dashboard_sub_text_box_two',
            'type' => 'text',
            'title' => __('زیر عنوان باکس دوم', 'venusshop'),
            'subtitle' => __('', 'venusshop'),
            'default' => __('ثبت نام کرده اید', 'venusshop'),
        ),
        array(
            'id' => 've_info_dashboard_title_box_three',
            'type' => 'text',
            'title' => __('عنوان باکس سوم', 'venusshop'),
            'subtitle' => __('', 'venusshop'),
            'default' => __('دوره', 'venusshop'),
        ),
        array(
            'id' => 've_info_dashboard_sub_text_box_three',
            'type' => 'text',
            'title' => __('زیر عنوان باکس سوم', 'venusshop'),
            'subtitle' => __('', 'venusshop'),
            'default' => __('در انتظار پرداخت', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Blog', 'venusshop'),
    'id' => 'blog_manage_section',
    'icon' => 'fal fa-pen',
    'fields' => array(
        array(
            'id' => 'opt_select_style_single_post',
            'type' => 'select',
            'title' => __('استایل نوشته تکی', 'venusshop'),
            'subtitle' => __('حالت نمایش نوشته تکی را انتخاب کنید', 'venusshop'),
            'options' => array(
                'default' => 'پیش فرض',
                'styleone' => 'استایل اول',
            ),
            'default' => 'styleone',
        ),
        array(
            'id' => 'breadcrumbs_bg_single_post',
            'type' => 'color_rgba',
            'title' => __(' رنگ پس زمینه بردکرامب', 'venusshop'),
            'desc' => __('نوشته تکی رنگ نوار بالا (دسته بندی و موقعیت) را انتخاب نمایید', 'venusshop'),
            'default' => array(
                'color' => '#eee',
                'alpha' => 1
            ),
            'output' => array('background' => '.style-one-breadcrumbs'),
            'required' => array('opt_select_style_single_post', '=', 'styleone')
        ),
        array(
            'id' => 'breadcrumbs_bg_category',
            'type' => 'color_rgba',
            'title' => __(' رنگ پس زمینه بردکرامب', 'venusshop'),
            'desc' => __('ارشیو بلاگ رنگ نوار بالا (دسته بندی و موقعیت) را انتخاب نمایید', 'venusshop'),
            'default' => array(
                'color' => '#eee',
                'alpha' => 1
            ),
            'output' => array('background' => '.style-one-breadcrumbs-category'),
        ),
        array(
            'id' => 'sidebar_position',
            'type' => 'image_select',
            'title' => esc_html__('موقعیت سایدبار آرشیو نوشته ها', 'venusshop'),
            'subtitle' => esc_html__('موقعیت سایدبار بلاگ را تنظیم یا مخفی کنید', 'venusshop'),
            'default' => 'left',
            'options' => array(
                'none' => array(
                    'alt' => esc_html__('No Sidebar', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                ),
                'left' => array(
                    'alt' => esc_html__('Sidebar Left', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                ),
                'right' => array(
                    'alt' => esc_html__('Sidebar Right', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                ),
            )
        ),
        array(
            'id' => 'sidebar_position_single',
            'type' => 'image_select',
            'title' => esc_html__('موقعیت سایدبار نوشته های تکی', 'venusshop'),
            'subtitle' => esc_html__('موقعیت سایدبار بلاگ را تنظیم یا مخفی کنید', 'venusshop'),
            'default' => 'left',
            'options' => array(
                'none' => array(
                    'alt' => esc_html__('No Sidebar', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                ),
                'left' => array(
                    'alt' => esc_html__('Sidebar Left', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                ),
                'right' => array(
                    'alt' => esc_html__('Sidebar Right', 'venusshop'),
                    'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                ),
            )
        ),
        array(
            'id' => 'author_dis_show_single_blog',
            'type' => 'switch',
            'title' => __('نام نویسنده', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('ارشیو مقالات', 'venusshop'),
    'id' => 'all_blog_category_show_page',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'opt_select_style_category_show_post',
            'type' => 'select',
            'title' => __('استایل نمایش نوشته ها', 'venusshop'),
            'subtitle' => __('حالت نمایش نوشته ها را انتخاب کنید', 'venusshop'),
            'options' => array(
                'default' => 'پیش فرض',
                'style_post_one' => 'استایل اول',
            ),
            'default' => 'style_post_one',
        ),
        array(
            'id' => 'icon_blog_all_show_format',
            'type' => 'switch',
            'title' => __('نوع نوشته', 'venusshop'),
            'subtitle' => __('حالت نمایشی آیکون ویدیو و پادکست', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('opt_select_style_category_show_post', '=', 'style_post_one'),
        ),
        array(
            'id' => 'waves_blog_all_show',
            'type' => 'switch',
            'title' => __('شکل امواج', 'venusshop'),
            'subtitle' => __('حالت نمایشی موج ها در مقالات مرتبط', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('opt_select_style_category_show_post', '=', 'style_post_one'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('مقالات مرتبط', 'venusshop'),
    'id' => 'all_blog_single_show_related',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'blog_single_show_related',
            'type' => 'switch',
            'title' => __('نمایش مقالات مرتبط', 'venusshop'),
            'subtitle' => __('نمایش مقالات مرتبط در صفحه نوشته', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop')
        ),
        array(
            'id' => 'text_blog_single_related',
            'type' => 'text',
            'title' => __('عنوان تب مقالات مرتبط', 'venusshop'),
            'subtitle' => __('عنوان موردنظر خود برای تب مقالات مرتبط را وارد کنید.', 'venusshop'),
            'default' => __('مقالات مرتبط', 'venusshop'),
            'required' => array('blog_single_show_related', '=', true),
        ),
        array(
            'id' => 'icon_blog_single_show_format_related',
            'type' => 'switch',
            'title' => __('نوع نوشته', 'venusshop'),
            'subtitle' => __('حالت نمایشی آیکون ویدیو و پادکست', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('blog_single_show_related', '=', true),
        ),
        array(
            'id' => 'waves_blog_show_related',
            'type' => 'switch',
            'title' => __('شکل امواج', 'venusshop'),
            'subtitle' => __('حالت نمایشی موج ها در مقالات مرتبط', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('blog_single_show_related', '=', true),
        ),
        array(
            'id' => 'author_dis_show_relate',
            'type' => 'switch',
            'title' => __('نام نویسنده', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('محصول فیزیکی', 'venusshop'),
    'id' => 'physical_section',
    'icon' => 'fal fa-cog',
    'fields' => array()
));

Redux::setSection($opt_name, array(
    'title' => __('صفحه محصول', 'venusshop'),
    'id' => 'physical_single_product_section',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'switch_digikala_venus',
            'type' => 'switch',
            'title' => __('دمو ونوس کالا', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop')
        ),
        array(
            'id' => 'switch_status_physical_name_seller',
            'type' => 'switch',
            'title' => __('نام فروشنده', 'venusshop'),
            'subtitle' => __('حالت نمایش نام فروشنده در صفحه محصول', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('switch_digikala_venus', '=', true),
        ),
        array(
            'id' => 'switch_status_physical_guarantee',
            'type' => 'switch',
            'title' => __('گارانتی محصول', 'venusshop'),
            'subtitle' => __('حالت نمایش گارانتی محصول', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('switch_digikala_venus', '=', true),
        ),
        array(
            'id' => 'switch_status_physical_in_stock',
            'type' => 'switch',
            'title' => __('موجودی انبار', 'venusshop'),
            'subtitle' => __('حالت نمایش موجودی انبار', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('switch_digikala_venus', '=', true),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('افزونه ها', 'venusshop'),
    'desc' => __('از این بخش شما میتوانید بخش استایل افزونه های هماهنگ شده با قالب را کنترل می کنید.', 'venusshop'),
    'id' => 've_plugin_section',
    'icon' => 'fal fa-cog',
    'fields' => array(
        array(
            'id' => 'style_plugin_ticket',
            'type' => 'switch',
            'title' => __('افزونه تیکت وردپرس', 'venusshop'),
            'subtitle' => __('استفاده از استایل ونوس برای افزونه تیکت پشتیبانی پیشرفته وردپرس', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop')
        ),
        array(
            'id' => 'style_plugin_yith_woocommerce_deposits',
            'type' => 'switch',
            'title' => __('افزونه فروش اقساطی ووکامرس', 'venusshop'),
            'subtitle' => __('استفاده از استایل ونوس برای افزونه فروش اقساطی ووکامرس(Yith)', 'venusshop'),
            'default' => 0,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop')
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Socials', 'venusshop'),
    'id' => 'social_section',
    'icon' => 'fal fa-share-alt',
    'desc' => __('If you want to hide any of this social, just empty link field.', 'venusshop'),
    'fields' => array(
        array(
            'id' => 'social-facebook',
            'type' => 'text',
            'title' => __('Facebook', 'venusshop'),
            'subtitle' => __('Insert your facebook link', 'venusshop'),
        ),
        array(
            'id' => 'social-twitter',
            'type' => 'text',
            'title' => __('Twitter', 'venusshop'),
            'subtitle' => __('Insert your twitter link', 'venusshop'),
        ),
        array(
            'id' => 'social-googleplus',
            'type' => 'text',
            'title' => __('Google Plus', 'venusshop'),
            'subtitle' => __('Insert your googleplus link', 'venusshop'),
        ),
        array(
            'id' => 'social-instagram',
            'type' => 'text',
            'title' => __('Instagram', 'venusshop'),
            'subtitle' => __('Insert your instagram link', 'venusshop'),
        ),
        array(
            'id' => 'social-aparat',
            'type' => 'text',
            'title' => __('Aparat', 'venusshop'),
            'subtitle' => __('Insert your aparat link', 'venusshop'),
        ),
        array(
            'id' => 'social-telegram',
            'type' => 'text',
            'title' => __('Telegram', 'venusshop'),
            'subtitle' => __('Insert your telegram link', 'venusshop'),
        ),
        array(
            'id' => 'social-youtube',
            'type' => 'text',
            'title' => __('Youtube', 'venusshop'),
            'subtitle' => __('Insert your youtube link', 'venusshop'),
        ),
        array(
            'id' => 'social-pinterest',
            'type' => 'text',
            'title' => __('Pinterest', 'venusshop'),
            'subtitle' => __('Insert your pinterest link', 'venusshop'),
        ),
    )
));

//Redux::setSection($opt_name, array(
//    'title' => __('Google reCaptcha', 'venusshop'),
//    'id' => 'captcha_section',
//    'icon' => 'el el-lock',
//    'fields' => array(
//        array(
//            'id' => 'enable_reCaptcha',
//            'type' => 'switch',
//            'title' => __('Enable Google Recaptcha', 'venusshop'),
//            'subtitle' => __('Enable to show google reCaptcha on popup login form.', 'venusshop'),
//            'default' => 0,
//            'on' => __('Enable', 'venusshop'),
//            'off' => __('Disable', 'venusshop'),
//        ),
//        array(
//            'id' => 'recaptcha_info',
//            'type' => 'info',
//            'title' => __('Google reCaptcha', 'venusshop'),
//            'style' => 'info',
//            'desc' => __('<p>If you do not have keys already then visit <kbd><a href = "https://www.google.com/recaptcha/admin">https://www.google.com/recaptcha/admin</a></kbd></p>', 'nikanshop')
//        ),
//        array(
//            'id' => 'recaptha_site_key',
//            'type' => 'text',
//            'title' => __('Site Key', 'venusshop'),
//            'subtitle' => __('Insert your reCaptcha site key', 'venusshop'),
//            'required' => array('enable_reCaptcha', '=', true)
//        ),
//        array(
//            'id' => 'recaptha_secret_key',
//            'type' => 'text',
//            'title' => __('Secret Key', 'venusshop'),
//            'subtitle' => __('Insert your reCaptcha secret key', 'venusshop'),
//            'required' => array('enable_reCaptcha', '=', true)
//        ),
//    )
//));
Redux::setSection($opt_name, array(
    'title' => __('Footer', 'venusshop'),
    'id' => 'footer_section',
    'icon' => 'fas fa-horizontal-rule',
    'fields' => array(
        array(
            'id' => 'select_footer',
            'type' => 'select',
            'title' => __('استایل فوتر', 'venus-theme'),
            'options' => venus_config_get_footer_array(),
            'default' => 'venus_default_footer',
        ),
        array(
            'id' => 'logo_footer_switch',
            'type' => 'switch',
            'title' => __('لوگو فوتر', 'venusshop'),
            'subtitle' => __('فعال / غیرفعال کردن لوگو فوتر', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('select_footer', '=', 'venus_default_footer'),
        ),
        array(
            'id' => 'logo_footer',
            'type' => 'media',
            'title' => __('Your Logo', 'venusshop'),
            'default' => array(
                'url' => get_template_directory_uri() . '/images/logo-footer.png',
            ),
            'desc' => __('Choose your logo to show on header', 'venusshop'),
            'required' => array('select_footer', '=', 'venus_default_footer'),
        ),
        array(
            'id' => 'copyright_footer',
            'type' => 'text',
            'title' => __('CopyRight Text', 'venusshop'),
            'default' => '© تمام حقوق این سایت متعلق به ونوس می باشد.',
            'subtitle' => __('Insert your copyright text', 'venusshop'),
            'required' => array('select_footer', '=', 'venus_default_footer'),
        ),
        array(
            'id' => 'nemad_dis_enb_btn',
            'type' => 'switch',
            'title' => __('لوگو نماد در موبایل', 'venusshop'),
            'subtitle' => __('فعال / غیرفعال کردن نماد در حالت موبایل', 'venusshop'),
            'default' => 1,
            'on' => __('Enable', 'venusshop'),
            'off' => __('Disable', 'venusshop'),
            'required' => array('select_footer', '=', 'venus_default_footer'),
        ),
        array(
            'id' => 'logo_enamad',
            'type' => 'editor',
            'title' => __('تصویر لوگو نماد', 'venusshop'),
            'args' => array(
                'wpautop' => false,
                'teeny' => true
            ),
            'required' => array('select_footer', '=', 'venus_default_footer'),
        ),
    )
));
Redux::setSection($opt_name, array(
    'title' => __('Others', 'venusshop'),
    'id' => 'others_section',
    'icon' => 'fal fa-cog',
    'fields' => array(
        array(
            'id' => 'img_404',
            'type' => 'media',
            'title' => __('Your 404 Page Image', 'venusshop'),
            'default' => array('url' => get_template_directory_uri() . '/images/source.gif'),
            'desc' => __('Choose your image to show on 404 page.', 'venusshop'),
        ),
        array(
            'id' => 'img_lost_password',
            'type' => 'media',
            'title' => __('تصویر صفحه بازیابی رمزعبور', 'venusshop'),
            'default' => array('url' => get_template_directory_uri() . '/images/password.jpg'),
            'desc' => __('انتخاب تصویر موردنظر برای نمایش در صفحه بازیابی رمزعبور.', 'venusshop'),
        ),
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Custom Code', 'venusshop'),
    'icon' => 'el el-css',
    'fields' => array(
        array(
            'title' => __('Custom CSS', 'venusshop'),
            'subtitle' => __('Paste your custom CSS code here. The code will be added to the header of your site.', 'venusshop'),
            'id' => 'venus_custom_css',
            'type' => 'ace_editor',
            'mode' => 'css',
        ),
        array(
            'title' => __('Header JavaScript Code', 'venusshop'),
            'subtitle' => __('Here is the place to paste any JS code you might want to add to be loaded in the header of your website.', 'venusshop'),
            'id' => 'venus_custom_js_header',
            'type' => 'ace_editor',
            'mode' => 'javascript',
        ),
        array(
            'title' => __('Footer JavaScript Code', 'venusshop'),
            'subtitle' => __('Here is the place to paste any JS code you might want to add to be loaded in the footer of your website.', 'venusshop'),
            'id' => 'venus_custom_js_footer',
            'type' => 'ace_editor',
            'mode' => 'javascript',
        ),

    )
));

/*
     * <--- END SECTIONS
     */
function venus_config_get_header_array()
{
    $args = array(
        'post_type' => 'venus-header',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => -1
    );
    $headers = new WP_Query($args);
    $headers = $headers->get_posts();
    $header_array = array('venus_default_header' => __('هدر پیش فرض ونوس', 'venus-theme'), 'no_header' => __('بدون هدر', 'venus-theme'));
    if (count($headers) > 0) {
        foreach ($headers as $header) {
            $header_array[$header->ID] = $header->post_title;
        }
    }
    return $header_array;
}

function venus_config_get_footer_array()
{
    $args = array(
        'post_type' => 'venus-footer',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => -1
    );
    $footers = new WP_Query($args);
    $footers = $footers->get_posts();
    $footer_array = array('venus_default_footer' => __('فوتر پیش فرض ونوس', 'venus-theme'), 'no_footer' => __('بدون فوتر', 'venus-theme'));
    if (count($footers) > 0) {
        foreach ($footers as $footer) {
            $footer_array[$footer->ID] = $footer->post_title;
        }
    }
    return $footer_array;
}

function venus_menu_panel()
{
    $list = [];
    $menus = wp_get_nav_menus();
    foreach ($menus as $menu) {
        $list[$menu->slug] = $menu->name;
    }

    return $list;
}
