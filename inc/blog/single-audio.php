<?php
$url_audio_format_blog = get_post_meta(get_the_ID(), 'url_audio_blog', true);
?>
<?php
if (has_excerpt()) : ?>
    <div class="article-excerpt mt-4">
        <?php
        if (has_excerpt()) {
            the_excerpt();
        }
        ?>
    </div>
<?php endif; ?>
<?php
$dic_pic_single_blog = get_post_meta(get_the_ID(), 'blog_single_picture', true);
//echo $dic_pic_single_blog;
if (has_post_thumbnail() & empty($dic_pic_single_blog) || $dic_pic_single_blog == 'en_single_blog_pic') : ?>
    <div class="article-image mb-3">
        <?php the_post_thumbnail('post-thumbnails');
        $attr =  array(
            'mp3'      => $url_audio_format_blog,
        );
        echo wp_audio_shortcode(  $attr );
        ?>
    </div>
<?php endif; ?>
<div class="article-content">
    <?php the_content(); ?>
</div>
<?php if (has_tag()) : ?>
    <div class="tags">
        <?php the_tags('', '', '') ?>
    </div>
<?php endif; ?>
<?php
global $venus_options;
if ($venus_options['author_dis_show_single_blog'] == true): ?>
    <div class="author d-flex mb-3">
        <div class="author-avatar float-none">
            <?php echo get_avatar(get_the_author_meta('user_email'), '80', ''); ?>
        </div>
        <div class="author-info">
            <div class="author-name"><?php the_author_link(); ?></div>
            <p class="author-about">
                <?php the_author_meta('description'); ?>
            </p>
        </div>
    </div>
<?php endif; ?>
