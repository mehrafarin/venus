<?php

global $venus_options;

/**
 * Redux Related Functions
 */
function codebean_remove_demo_mode_link()
{
    if (class_exists('ReduxFrameworkPlugin')) {
        remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
    }
    if (class_exists('ReduxFrameworkPlugin')) {
        remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));
    }
}

add_action('init', 'codebean_remove_demo_mode_link');

function codebean_remove_redux_menu()
{
    remove_submenu_page('tools.php', 'redux-about');
}

add_action('admin_menu', 'codebean_remove_redux_menu', 12);

// Function that echoes generated style attributes
function venus_inline_style($value)
{
    echo venus_get_inline_style($value);
}

// Function that generates style attribute and returns generated string
function venus_get_inline_style($value)
{
    return venus_get_inline_attr($value, 'style', ';');
}

// Generate multiple inline attributes
function venus_get_inline_attrs($attrs)
{
    $output = '';

    if (is_array($attrs) && count($attrs)) {
        foreach ($attrs as $attr => $value) {
            $output .= ' ' . venus_get_inline_attr($value, $attr);
        }
    }

    $output = ltrim($output);

    return $output;
}

// Function that echoes class attribute
function venus_class_attribute($value)
{
    echo venus_get_class_attribute($value);
}

// Function that returns generated class attribute
function venus_get_class_attribute($value)
{
    return venus_get_inline_attr($value, 'class', ' ');
}

// Function that generates html attribute
function venus_get_inline_attr($value, $attr, $glue = '')
{
    if (!empty($value)) {

        if (is_array($value) && count($value)) {
            $properties = implode($glue, $value);
        } elseif ($value !== '') {
            $properties = $value;
        }

        return $attr . '="' . esc_attr($properties) . '"';
    }

    return '';
}

/*  Sell Product Countdown
/* --------------------------------------------------------------------- */
if (!function_exists('venus_pro_sell_countdown')) {
    function venus_pro_sell_countdown()
    {
        global $product;
        global $venus_options;

        if ($product->is_on_sale()) :
            $time_sale_end = get_post_meta($product->get_id(), '_sale_price_dates_to', true);
            $time_sale_start = get_post_meta($product->get_id(), '_sale_price_dates_from', true);
        endif;

        $timezone = wc_timezone_string();

        ?>
        <?php if ($product->is_on_sale() && $time_sale_end) : ?>
        <div class="countvenus-title-timer">
            <div class="deal-text text-center"><i
                        class="fal fa-alarm-clock pl-1"></i><span><?php echo __($venus_options['text_amazing_product'], 'venus-theme'); ?></span>
            </div>
            <div class="countvenus-timer-product-sell">
                <div class="countdown-item justify-content-center"
                     data-date="<?php echo date('Y-m-d 24:00:00', $time_sale_end); ?>"></div>
            </div>
        </div>
    <?php endif; ?>

    <?php }
}

add_action('woocommerce_single_product_countdown', 'venus_pro_sell_countdown', 14);
function widgetregister()
{
    register_sidebar(array(
        'name' => 'نوار کناری وبلاگ',
        'id' => 'sidebar_blog',
        'description' => 'ابزارک ها را اینجا اضافه کنید برای نمایش در سایدبار نوشته های بلاگ.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="widget-title">',
        'after_title' => '</h5>',
    ));

    register_sidebar(array(
        'name' => 'سایدبار صفحات',
        'id' => 'sidebar_page',
        'description' => 'ابزارک ها را اینجا اضافه کنید برای نمایش در سایدبار نوشته های بلاگ.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="widget-title">',
        'after_title' => '</h5>',
    ));

    register_sidebar(array(
        'name' => 'آرشیو فروشگاه',
        'id' => 'sidebar_product',
        'description' => 'ابزارک ها را اینجا اضافه کنید برای نمایش در سایدبار آرشیو محصولات.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="widget-title">',
        'after_title' => '</h5>',
    ));
}

add_action('widgets_init', 'widgetregister');

//remove_action woocommerce_single_product_summary
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
// Remove related_products product page
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

if ($venus_options['account_profile_pic'] == true) {
    require_once get_template_directory() . '/inc/tgm/pic-prof.php';
}

//Start Sweet alert 2
if ($venus_options['status_add_cart_popup']):
    add_filter('wc_add_to_cart_message_html', 'wc_add_to_cart_message_filter', 10, 2);
    function wc_add_to_cart_message_filter()
    {
        add_action('wp_footer', 'item_count_check');
        function item_count_check()
        {
            ?>
            <script>
                function addcartsweet() {
                    Swal.fire({
                        icon: 'success',
                        title: '<?php the_title(); ?>',
                        text: 'به سبد شما افزوده شد',
                        type: 'success',
                        showCancelButton: true,
                        cancelButtonText: 'بستن',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'مشاهده سبد خرید'
                    }).then(function (result) {
                        if (result.value) {
                            location.assign("<?php echo esc_url(wc_get_page_permalink('cart')) ?>")
                        }
                    });
                }

                addcartsweet();
            </script>
            <?php
        }
    }
endif;
add_filter('woocommerce_get_price_html', 'price_free_zero_empty', 100, 2);
function price_free_zero_empty($price, $product)
{

    if ('' === $product->get_price() || 0 == $product->get_price() & $product->is_on_sale() ) {

        $price = '<del>' . wc_price($product->get_regular_price()) . '</del><span class="woocommerce-Price-amount amount d-block">'. __('رایگان', 'venus') . '</span>';

    } elseif ('' === $product->get_price() || 0 == $product->get_price()) {

        $price = '<span class="woocommerce-Price-amount amount">' . __('رایگان', 'venus') . '</span>';

    }

    $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);

    if ($cu_status == 'soon') {
        $price = '<span class="woocommerce-Price-amount amount">' . __('به زودی...', 'venus') . '</span>';
    }

    return $price;
}


//Start Course Status
if (!function_exists('venus_course_status')) {
    function venus_course_status()
    {
        $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
        switch ($cu_status) {
            case 'soon' :
                echo '<div class="bag-course-status badge-info">';
                echo 'به زودی...';
                echo '</div>';
                break;
            case 'loading' :
                echo '<div class="bag-course-status badge-success">';
                echo 'درحال برگزاری';
                echo '</div>';
                break;
            case 'registration' :
                echo '<div class="bag-course-status badge-danger">';
                echo 'پیش ثبت نام';
                echo '</div>';
                break;
            case 'complete' :
                echo '<div class="bag-course-status badge-secondary">';
                echo 'تکمیل شده';
                echo '</div>';
                break;
        }
    }
}

//Add Favicon in Header
function add_favicon_head()
{
    global $venus_options;

    if (isset($venus_options['favicon']) && strlen($venus_options['favicon']['url']) > 0) {
        $favicon_href = $venus_options['favicon']['url'];
    } else {
        $favicon_href = get_template_directory_uri() . '/images/favicon.png';
    }
    ?>
    <link rel="shortcut icon" href="<?php echo $favicon_href; ?>"/>
    <?php if ((isset($venus_options['venus_custom_css'])) && strlen($venus_options['venus_custom_css']) > 0) : ?>
    <style>
        <?php echo $venus_options['venus_custom_css']; ?>
    </style>
<?php endif; ?>

    <?php if ((isset($venus_options['venus_custom_js_header'])) && strlen($venus_options['venus_custom_js_header']) > 0) : ?>
    <script type="text/javascript">
        <?php echo $venus_options['venus_custom_js_header']; ?>
    </script>
<?php endif; ?>
    <?php if (isset($venus_options['dashboard_menu_right_color_text']) && $venus_options['dashboard_menu_right_color_text']) { ?>
    <style>
        .woocommerce-MyAccount-navigation .upanel-sidebar-inner .panel-navigation ul li:before {
            color: <?php echo $venus_options['dashboard_menu_right_color_text']; ?> !important;
        }

        .my-account-section-title {
            color: <?php echo $venus_options['dashboard_menu_right_color_text']; ?> !important;
        }

        .my-account-section-title:before {
            color: <?php echo $venus_options['dashboard_menu_right_color_text']; ?> !important;
        }
    </style>
<?php } ?>

    <?php if (isset($venus_options['dashboard_menu_right_hover_color_text']) && $venus_options['dashboard_menu_right_hover_color_text']) { ?>
    <style>
        .woocommerce-MyAccount-navigation .upanel-sidebar-inner .panel-navigation ul li a:before {
            background-image: linear-gradient(270deg, <?php echo $venus_options['dashboard_menu_right_hover_color_text']['from']; ?> 0%, <?php echo $venus_options['dashboard_menu_right_hover_color_text']['to']; ?> 100%) !important;
        }

        .my-account-section-title {
            background-image: linear-gradient(270deg, <?php echo $venus_options['dashboard_menu_right_hover_color_text']['from']; ?> 0%, <?php echo $venus_options['dashboard_menu_right_hover_color_text']['to']; ?> 100%) !important;
        }
    </style>
<?php } ?>
    <?php
    if (isset($venus_options['top_bar']) && $venus_options['top_bar']) { ?>
        <?php if ($venus_options['top_bar_link']) {
            echo '<a href="' . $venus_options['top_bar_link'] . '" target="_blank">';
        } ?>
        <div class="top-header-banner">
            <?php echo $venus_options['top_bar_text']; ?>
        </div>
        <?php if ($venus_options['top_bar_link']) {
            echo '</a>';
        } ?>
    <?php } ?>

    <?php

}

add_action('wp_head', 'add_favicon_head');

function add_footer()
{
    global $venus_options;

    ?>
    <!-- Popper js -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/swiper.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/sweetalert2@9.js"></script>

    <script>
        var mySwiper = new Swiper('.swiper-related', {
            slidesPerView: 2,
            spaceBetween: 0,
            slidesPerGroup: 2,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
        var digi_related = new Swiper('.swiper-related-digikala', {
            slidesPerView: 1,
            spaceBetween: 0,
            // slidesPerGroup: 4,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2
                },
                768: {
                    slidesPerView: 3
                },
                992: {
                    slidesPerView: 3
                },
                1200: {
                    slidesPerView: 4
                }
            }
        })
    </script>
    <script>
        var mySwiper = new Swiper('.swiper-product', {
            slidesPerView: 1,
            spaceBetween: 20,
            slidesPerGroup: 1,
            loop: true,
            autoplay: {
                delay: 9000,
                disableOnInteraction: false,
            },
            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
            }
        })
    </script>
    <script>
        var mySwiper = new Swiper('.swiper-blog', {
            slidesPerView: 1,
            spaceBetween: 20,
            slidesPerGroup: 1,
            loop: true,
            autoplay: {
                delay: 9000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
            }
        });
    </script>
    <?php
    // Scroll top
    if (isset($venus_options['main_back_to_top']) && $venus_options['main_back_to_top']) { ?>
        <a id="scrollTop"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></a>
    <?php } ?>

    <?php if ((isset($venus_options['venus_custom_js_footer'])) && strlen($venus_options['venus_custom_js_footer']) > 0) : ?>
    <script type="text/javascript">
        <?php echo $venus_options['venus_custom_js_footer']; ?>
    </script>
<?php endif; ?>
    <?php
}

add_action('wp_footer', 'add_footer');

function add_header()
{
    ?>
    <div class="video_popup_wrrapper">
        <div class="video_popup_overlay"></div>
        <div class="video_popup_inner"></div>
    </div>
    <?php
}

add_action('wp_head', 'add_header');

/**
 * Show Price Physical Product
 */
if (!function_exists('physical_product_price')) {

    function physical_product_price()
    {
        global $product;
        echo '<p class="c-price-product text-left mt-2 mb-0">' . $product->get_price_html() . '</p>';
    }
}
/**
 * Show Price Physical Product
 */
if (!function_exists('physical_is_in_stock')) {

    function physical_is_in_stock()
    {
        echo '<div class="c-product-stock-title">ناموجود</div>';
        echo '<div class="c-product-stock-body">
                                        متاسفانه این کالا در حال حاضر موجود نیست. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید.
                                        </div>';
    }
}
