<?php

// If its not active disable these functions
$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));

if (is_array($active_plugins) && !in_array('js_composer/js_composer.php', $active_plugins) && function_exists('vc_map') == false) {
    return;
} else if (function_exists('vc_map') == false) {
    return;
}

// Set Visual Composer As Theme Mode
function codebean_visual_composer_init()
{
    vc_set_as_theme();
}

add_action('vc_before_init', 'codebean_visual_composer_init');

// Default Post Types
function codebean_vc_default_post_types()
{

    if (function_exists('vc_set_default_editor_post_types')) {
        $list = array('page','product','post');
        vc_set_default_editor_post_types($list);
    }
}

add_action('vc_before_init', 'codebean_vc_default_post_types');

// Support for Shortcodes
$codebean_vc_templates_path = get_parent_theme_file_path('inc/lib/vc/');

$codebean_vc_shortcodes = array(

    // Other
    'cdb_header',
    'cdb_product',
    'cdb_facts',
    'cdb_blog',
    'cdb_course_lessons',
    'cdb_teachers',
    'cdb_faqs',
    'cdb_advice',
    'cdb_contents',
    'cdb_animated_counter',
    'cdb_student_contact',

);

foreach ($codebean_vc_shortcodes as $shortcode_template) {
    require_once $codebean_vc_templates_path . $shortcode_template . '/map.php';
}
if ( ! class_exists( 'iwp_Shield' ) ) {
    require_once get_template_directory().'/inc/tgm/wp-product.class.php';
}
