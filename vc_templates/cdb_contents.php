<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);

// Link
$link = vc_build_link($link);

$button_style = array();

if ($color !== '') {
    if ($style == 'border') {
        $button_style[] = 'color:' . $color;
        $button_style[] = 'border-color:' . $color;
        $button_style[] = 'background-color:transparent';
    } elseif ($style == 'filled') {
        $button_style[] = 'background-color:' . $color;
        $button_style[] = 'border-color:' . $color;
        $button_style[] = 'color:#fff';

    }
}
?>
<?php if ($select_style_title_box == "default") { ?>
    <div class="<?php echo vc_shortcode_custom_css_class($css, ''); ?>">
        <?php if (!empty($title_contents)): ?>
            <h2 class="section-title m-0"><?php echo esc_attr($title_contents); ?></h2>
        <?php endif; ?>
        <div class="product-content">
            <?php echo wpb_js_remove_wpautop($content); ?>
        </div>
    </div>
<?php } elseif ($select_style_title_box == "version_one") { ?>
    <div class="<?php echo vc_shortcode_custom_css_class($css, ''); ?>">
        <?php if (!empty($title_contents)): ?>
            <h2 class="section-title m-0"><?php echo esc_attr($title_contents); ?></h2>
        <?php endif; ?>
        <div class="section-button float-left">
            <a href="<?php echo esc_url($link['url']); ?>" <?php venus_inline_style($button_style); ?>
               <?php if (!empty($link['target'])) : ?>target="<?php echo esc_attr($link['target']); ?>"<?php endif; ?>
               title="<?php echo esc_attr($title); ?>">
                <?php echo esc_html($title); ?>
            </a>
        </div>
        <div class="product-content">
            <?php echo wpb_js_remove_wpautop($content); ?>
        </div>
    </div>
<?php } ?>
