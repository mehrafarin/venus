<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);
$link_text_one = ('||' === $link_text_one) ? '' : $link_text_one;
$link_text_one = vc_build_link($link_text_one);
$a_href = $link_text_one['url'];
$a_target = $link_text_one['target'];

$link_text_two = ('||' === $link_text_two) ? '' : $link_text_two;
$link_text_two = vc_build_link($link_text_two);
$a_one_href = $link_text_two['url'];
$a_one_target = $link_text_two['target'];

$gallery = shortcode_atts(
    array(
        'image_advice'  =>  'image_advice',
    ), $atts );

$image_ids=explode(',',$gallery['image_advice']);
$image_no = 1;
foreach( $image_ids as $image_id ) {
    $images = wp_get_attachment_image_src($image_id, 'full');
    $image_no++;
}
?>

<!-- START Advice -->
<div id="course-advice">
    <h2 class="section-title"><?php echo esc_attr($title_advice); ?></h2>
    <div class="row style-form">
        <div class="advice col-md-12">
            <div class="row advice-content">
                <div class="col-12 col-lg-7 py-3 form-box">
                    <div class="text mb-3">
                        <?php echo esc_attr($text_one_advice); ?>
                    </div>
                    <div class="form-contact my-4">
                        <?php echo do_shortcode( $content ); ?>
                    </div>
                </div>
                <div class="d-none col-lg-5 pic-box d-lg-flex" style="background: url(<?php echo $images[0]; ?>);">
                    <div class="contact-us w-100 mt-auto">
                        <a class="item d-flex justify-content-between" href="<?php echo esc_url($a_href); ?>" target="<?php echo esc_url($a_target); ?>">
                            <span class="name"><?php echo esc_attr($text_two_advice); ?></span>
                            <span class="content"><?php echo esc_attr($text_three_advice); ?></span>
                        </a>
                        <hr>
                        <p>
                            <a class="item d-flex justify-content-between" href="<?php echo esc_url($a_one_href); ?>" target="<?php echo esc_url($a_one_target); ?>" rel="noopener noreferrer">
                                <span class="name"><?php echo esc_attr($text_four_advice); ?></span>
                                <span class="content"><?php echo esc_attr($text_five_advice); ?></span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Advice -->
<?php wp_reset_postdata(); ?>
