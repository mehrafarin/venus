<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}


extract($atts);
$gallery = shortcode_atts(
    array(
        'image'  =>  'image',
    ), $atts );

$image_ids=explode(',',$gallery['image']);
$image_no = 1;
foreach( $image_ids as $image_id ) {
    $images = wp_get_attachment_image_src($image_id, 'full');
    $image_no++;
}
//print_r($images[0]);
?>
<?php if (!empty($title)): ?>
    <div class="card" id="accordion">
        <div class="card-header" id="heading<?php echo esc_attr($id_tab); ?>">
            <button class="btn btn-link d-flex collapsed" data-toggle="collapse" data-target="#jal<?php echo esc_attr($id_tab); ?>"
                    aria-expanded="true" aria-controls="jal<?php echo esc_attr($id_tab); ?>">
                <i>
                    <img src="<?php echo $images[0]; ?>" alt="">
                </i>
                <div class="lesson-inner text-right my-auto mr-3">
                    <div class="lesson-title mb-2"><?php echo esc_attr($title); ?></div>
                    <?php if (!empty($subtitle)) : ?><p class="m-0"><?php echo esc_attr($subtitle); ?></p><?php endif; ?>
                </div>
                <i class="fas fa-chevron-down my-auto ml-2 mr-auto"></i>
            </button>
        </div>
        <div id="jal<?php echo esc_attr($id_tab); ?>" class="collapse in" aria-labelledby="heading<?php echo esc_attr($id_tab); ?>" data-parent="#accordion"
             aria-expanded="true">
            <div class="card-body">
                <ul class="lessons-list">
                    <?php echo wpb_js_remove_wpautop($content); ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>


