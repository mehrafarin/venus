<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}
extract($atts);

//print_r($images[0]);
?>
<?php if (!empty($title_faq)): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <button class="accordion-toggle collapsed" data-toggle="collapse" data-target="#faq_<?php echo esc_attr($id_faq); ?>" aria-expanded="true" aria-controls="faq_<?php echo esc_attr($id_faq); ?>">
                    <?php echo esc_attr($title_faq); ?>
                </button>
            </h4>
        </div>
        <div id="faq_<?php echo esc_attr($id_faq); ?>" class="collapse in" aria-labelledby="heading0" data-parent="#accordion"
             aria-expanded="true" class="panel-collapse collapse">
            <div class="panel-body">
                <?php echo esc_attr($answer_faq); ?>
            </div>
        </div>
    </div>
<?php endif; ?>


