<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);

$btn_cat_link = ('||' === $btn_cat_link) ? '' : $btn_cat_link;
$btn_cat_link = vc_build_link($btn_cat_link);
$btn_cat_href = $btn_cat_link['url'];
$btn_cat_target = $btn_cat_link['target'];

$all_posts_args = array(

    'post_type' => array('post'),
    'posts_per_page' => 1,
    'cat' => $category
);

$all_posts = new WP_Query($all_posts_args);

$two_args = array(

    'post_type' => array('post'),
    'posts_per_page' => 2,
    'offset' => 1,
    'cat' => $category
);

$two_posts = new WP_Query($two_args);

$style_posts_blog_args = array(

    'post_type' => array('post'),
    'posts_per_page' => $num_blog_post,
    'cat' => $category
);

$style_posts = new WP_Query($style_posts_blog_args);

wp_reset_postdata();

?>
<?php if ($colorbgbtn_blog != '') { ?>
    <style>
        .blog-posts a.more-articles {
            background-color: <?php echo esc_attr($colorbgbtn_blog); ?>;
            box-shadow: 0 2px 12px <?php echo esc_attr($colorbgbtn_blog); ?>99;
        }

        .blog-posts a.more-articles:hover {
            background-color: <?php echo esc_attr($colorbgbtn_blog); ?>;
            box-shadow: 0 2px 12px<?php echo esc_attr($colorbgbtn_blog); ?>;
        }
    </style>
<?php }; ?>
<!-- Start Blog -->
<?php if ($select_style_blog == "default") { ?>
    <div>
        <div class="blog-posts">
            <div class="row position-relative">
                <div class="blog-posts-bg"></div>

                <div class="col-lg-8 col-md-12 first-post">
                    <?php if ($all_posts->have_posts()) : ?>

                        <?php while ($all_posts->have_posts()):$all_posts->the_post(); ?>
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <img src="<?php the_post_thumbnail_url('post-size'); ?>"
                                         alt="<?php echo get_the_title($all_posts->post->ID); ?>">
                                </figure>
                                <div class="blog-posts-inner">
                                    <div class="category">
                                        <ul>
                                            <li> <?php print get_the_category(get_the_ID())[0]->name; ?></li>
                                        </ul>
                                    </div>
                                    <h2><?php echo get_the_title($all_posts->post->ID); ?></h2>
                                </div>
                                <?php if ($select_show_hide_waves == false): ?>
                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                         preserveAspectRatio="none" shape-rendering="auto">
                                        <defs>
                                            <path id="gentle-wave"
                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                        </defs>
                                        <g class="parallax">
                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                 fill="rgba(255,255,255,0.7"></use>
                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                 fill="rgba(255,255,255,0.5)"></use>
                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                 fill="rgba(255,255,255,0.3)"></use>
                                            <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                        </g>
                                    </svg>
                                <?php endif; ?>
                            </a>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <?php wp_reset_query(); ?>

                <div class="col-lg-4 col-md-12 another-posts">
                    <?php if ($two_posts->have_posts()) : ?>
                        <?php while ($two_posts->have_posts()):$two_posts->the_post(); ?>
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <img src="<?php the_post_thumbnail_url('post-size-two'); ?>"
                                         alt="<?php echo get_the_title($two_posts->post->ID); ?>">
                                </figure>
                                <div class="blog-posts-inner">
                                    <div class="category">
                                        <ul>
                                            <li> <?php print get_the_category(get_the_ID())[0]->name; ?></li>
                                        </ul>
                                    </div>
                                    <h2><?php echo get_the_title($two_posts->post->ID); ?></h2>
                                </div>
                                <?php if ($select_show_hide_waves == false): ?>
                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                         preserveAspectRatio="none" shape-rendering="auto">
                                        <defs>
                                            <path id="gentle-wave"
                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                        </defs>
                                        <g class="parallax">
                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                 fill="rgba(255,255,255,0.7"></use>
                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                 fill="rgba(255,255,255,0.5)"></use>
                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                 fill="rgba(255,255,255,0.3)"></use>
                                            <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                        </g>
                                    </svg>
                                <?php endif; ?>
                            </a>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (!empty($btn_cat_text)) { ?>
                <a href="<?php echo esc_url($btn_cat_href); ?>" target="<?php echo esc_url($btn_cat_target); ?>"
                   class="more-articles"><?php echo $btn_cat_text ?></a>
            <?php }; ?>
        </div>
    </div>
<?php } elseif ($select_style_blog == "version_one") { ?>
    <?php if ($style_posts->have_posts()) : ?>
        <div class="related-posts pt-0">
            <?php while ($style_posts->have_posts()):$style_posts->the_post(); ?>
                <div class="rpost">
                    <div class="post-inner">
                        <a href="<?php the_permalink(); ?>">
                            <figure>
                                <?php
                                if ($style_format_post_blogs == 'enable') {
                                    $format = get_post_format(get_the_ID());
                                    if ($format == 'audio') { ?>
                                        <div class="icon-format-blog d-flex align-middle">
                                            <i class="fal fa-music pr-3 m-auto"></i>
                                        </div>
                                    <?php } elseif ($format == 'video') { ?>
                                        <div class="icon-format-blog d-flex align-middle">
                                            <i class="fal fa-play pl-4 m-auto"></i>
                                        </div>
                                    <?php }
                                }
                                ?>
                                <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                     alt="<?php echo get_the_title($style_posts->post->ID); ?>">
                                <?php if ($select_show_hide_waves == false): ?>
                                    <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                         preserveAspectRatio="none" shape-rendering="auto">
                                        <defs>
                                            <path id="gentle-wave"
                                                  d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                        </defs>
                                        <g class="parallax">
                                            <use xlink:href="#gentle-wave" x="-12" y="11"
                                                 fill="rgba(255,255,255,0.7"></use>
                                            <use xlink:href="#gentle-wave" x="25" y="13"
                                                 fill="rgba(255,255,255,0.5)"></use>
                                            <use xlink:href="#gentle-wave" x="100" y="5"
                                                 fill="rgba(255,255,255,0.3)"></use>
                                            <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                        </g>
                                    </svg>
                                <?php endif; ?>
                            </figure>
                        </a>
                        <div class="post-detail py-2">
                            <div class="category m-0" style="color: #23527c !important;">
                                <?php the_time('j F Y'); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>">
                                <span class="title my-2"
                                      style="min-height: auto;font-size: 15px;line-height: 22px;height: 43px;"><?php the_title(); ?></span></a>
                            <?php if ($select_show_hide_author == false): ?>
                                <div class="author d-flex mt-2 mb-1 align-middle">
                                    <div class="author-avatar author-avatar-blog">
                                        <?php echo get_avatar(get_the_author_meta('user_email'), '35', ''); ?>
                                    </div>
                                    <div class="author-info my-auto">
                                        <div class="author-name p-0"><?php the_author_link(); ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php if (!empty($btn_cat_text)) { ?>
                <div class="clearfix"></div>
                <div class="blog-posts text-left">
                    <a href="<?php echo esc_url($btn_cat_href); ?>" target="<?php echo esc_url($btn_cat_target); ?>"
                       class="more-articles ml-4"><?php echo $btn_cat_text ?></a>
                </div>
            <?php }; ?>
        </div>
    <?php endif; ?>
<?php } elseif ($select_style_blog == "version_two") { ?>
    <div class="swiper-container swiper-blog">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php if ($style_posts->have_posts()) : ?>
                <?php while ($style_posts->have_posts()):$style_posts->the_post(); ?>
                    <div class="related-posts swiper-slide pt-0">
                        <div class="rpost float-none pt-0 px-0 pb-2 w-auto">
                            <div class="post-inner">
                                <a href="<?php the_permalink(); ?>">
                                    <figure>
                                        <?php
                                        if ($style_format_post_blogs == 'enable') {
                                            $format = get_post_format(get_the_ID());
                                            if ($format == 'audio') { ?>
                                                <div class="icon-format-blog d-flex align-middle">
                                                    <i class="fal fa-music pr-3 m-auto"></i>
                                                </div>
                                            <?php } elseif ($format == 'video') { ?>
                                                <div class="icon-format-blog d-flex align-middle">
                                                    <i class="fal fa-play pl-4 m-auto"></i>
                                                </div>
                                            <?php }
                                        } ?>
                                        <img src="<?php the_post_thumbnail_url('post-size-related'); ?>"
                                             alt="<?php echo get_the_title($style_posts->post->ID); ?>">
                                        <?php if ($select_show_hide_waves == false): ?>
                                            <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                 preserveAspectRatio="none" shape-rendering="auto">
                                                <defs>
                                                    <path id="gentle-wave"
                                                          d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                </defs>
                                                <g class="parallax">
                                                    <use xlink:href="#gentle-wave" x="-12" y="11"
                                                         fill="rgba(255,255,255,0.7"></use>
                                                    <use xlink:href="#gentle-wave" x="25" y="13"
                                                         fill="rgba(255,255,255,0.5)"></use>
                                                    <use xlink:href="#gentle-wave" x="100" y="5"
                                                         fill="rgba(255,255,255,0.3)"></use>
                                                    <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                </g>
                                            </svg>
                                        <?php endif; ?>
                                    </figure>
                                </a>
                                <div class="post-detail py-2">
                                    <div class="category m-0" style="color: #23527c !important;">
                                        <?php the_time('j F Y'); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>">
                                        <span class="title my-2"
                                              style="min-height: auto;font-size: 15px;line-height: 22px;height: 43px;"><?php the_title(); ?></span></a>
                                    <?php if ($select_show_hide_author == false): ?>
                                    <div class="author d-flex mt-2 mb-1">
                                        <div class="author-avatar author-avatar-blog">
                                            <?php echo get_avatar(get_the_author_meta('user_email'), '35', ''); ?>
                                        </div>
                                            <div class="author-info my-auto">
                                                <div class="author-name p-0"><?php the_author_link(); ?></div>
                                            </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev btn-prev-related op-blog-btn"></div>
        <div class="swiper-button-next btn-next-related op-blog-btn"></div>
    </div>
<?php }; ?>

<!-- End Blog -->

<?php wp_reset_postdata(); ?>
