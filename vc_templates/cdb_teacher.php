<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}


extract($atts);
$gallery = shortcode_atts(
    array(
        'image_teacher'  =>  'image_teacher',
    ), $atts );

$image_ids=explode(',',$gallery['image_teacher']);
$image_no = 1;
foreach( $image_ids as $image_id ) {
    $images = wp_get_attachment_image_src($image_id, 'full');
    $image_no++;
}
//print_r($images[0]);
?>
<?php if (!empty($title_teacher)): ?>
    <div class="col-12">
        <div class="teacher row">
            <div class="col-xs-12 col-sm-4 avatar">
                <img src="<?php echo $images[0]; ?>">
            </div>
            <div class="col-sm-8 col-xs-12">
                <div class="teacher-name"><?php echo esc_attr($title_teacher); ?>
                    <div class="teacher-position"><?php echo esc_attr($subtitle_teacher); ?></div>
                </div>
                <div class="teacher-info">
                    <p><?php echo esc_attr($dec_teacher); ?></p>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>


