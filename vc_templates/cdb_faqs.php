<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);


?>
<div id="course-faq" class="<?php echo vc_shortcode_custom_css_class($css, ''); ?> mt-5">
    <?php if (!empty($title_faqs)): ?>
        <h2 class="section-title"><?php echo esc_attr($title_faqs); ?></h2>
    <?php endif; ?>
    <div class="row">
        <div class="col-12">
            <div class="panel-group" id="accordion">
                <?php echo wpb_js_remove_wpautop($content); ?>
            </div>
        </div>
    </div>
</div>
