<?php
// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}
extract($atts);

$gallery = shortcode_atts(
    array(
        'image_req_one' => 'image_req_one',
    ), $atts);

$image_ids = explode(',', $gallery['image_req_one']);
$image_no = 1;
foreach ($image_ids as $image_id) {
    $images = wp_get_attachment_image_src($image_id, 'full');
    $image_no++;
}
//img 1
$gallery_one = shortcode_atts(
    array(
        'image_req_two' => 'image_req_two',
    ), $atts);

$image_ids_one = explode(',', $gallery_one['image_req_two']);
$image_no_one = 1;
foreach ($image_ids_one as $image_id_one) {
    $images_one = wp_get_attachment_image_src($image_id_one, 'full');
    $image_no_one++;
}
//img 2
$gallery_two = shortcode_atts(
    array(
        'image_req_three' => 'image_req_three',
    ), $atts);

$image_ids_two = explode(',', $gallery_two['image_req_three']);
$image_no_two = 1;
foreach ($image_ids_two as $image_id_two) {
    $images_two = wp_get_attachment_image_src($image_id_two, 'full');
    $image_no_two++;
}
//img 3
$gallery_three = shortcode_atts(
    array(
        'image_req_four' => 'image_req_four',
    ), $atts);

$image_ids_three = explode(',', $gallery_three['image_req_four']);
$image_no_three = 1;
foreach ($image_ids_three as $image_id_three) {
    $images_three = wp_get_attachment_image_src($image_id_three, 'full');
    $image_no_three++;
}
?>
<div id="course-requirement">
    <div class="course-requirement">
        <h2 class="section-title"><?php echo $title_course_contacts ?></h2>
        <div class="row">
            <div class="col-12 col-xl-7 requirement-right">
                <p class="text-justify"><?php echo $top_title_course_contacts; ?></p>
                <?php echo wpb_js_remove_wpautop($content); ?>
            </div>
            <div class="d-none d-xl-block col-md-5 requirement-left px-1">
                <div class="requirement-left-inner">
                    <div class="req-photo1" <?php if (!empty($images[0])): ?> style="background: url(<?php echo $images[0]; ?>) no-repeat;" <?php endif; ?>></div>
                    <div class="req-photo2" <?php if (!empty($images_one[0])): ?> style="background: url(<?php echo $images_one[0]; ?>) no-repeat;" <?php endif; ?>></div>
                    <div class="req-photo3" <?php if (!empty($images_two[0])): ?> style="background: url(<?php echo $images_two[0]; ?>) no-repeat;" <?php endif; ?>></div>
                    <div class="req-photo4" <?php if (!empty($images_three[0])): ?> style="background: url(<?php echo $images_three[0]; ?>) no-repeat;" <?php endif; ?>></div>
                </div>
            </div>
        </div>
    </div>
</div>
