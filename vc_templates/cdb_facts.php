<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);

?>
<?php if (!empty($meteor_color_facts)) { ?>
    <style>
        .state .state-text .meteor1, .state .state-text .meteor2, .state .state-text .meteor6 {
            background: -moz-linear-gradient(left, rgba(0, 0, 0, 0) 0%, <?php echo esc_attr($meteor_color_facts); ?> 100%);
            background: -webkit-linear-gradient(left, rgba(0, 0, 0, 0) 0%, <?php echo esc_attr($meteor_color_facts); ?> 100%);
            background: linear-gradient(to right, rgba(0, 0, 0, 0) 0%, <?php echo esc_attr($meteor_color_facts); ?> 100%);
        }

        .state .state-text .meteor3, .state .state-text .meteor4, .state .state-text .meteor5, .state .state-text .meteor7 {
            background: -moz-linear-gradient(left, <?php echo esc_attr($meteor_color_facts); ?> 0%, rgba(0, 0, 0, 0) 100%);
            background: -webkit-linear-gradient(left, <?php echo esc_attr($meteor_color_facts); ?> 0%, rgba(0, 0, 0, 0) 100%);
            background: linear-gradient(to right, <?php echo esc_attr($meteor_color_facts); ?> 0%, rgba(0, 0, 0, 0) 100%);
        }
    </style>
<?php }; ?>
<?php if (!empty($txt_color_facts)) { ?>
    <style>
        .state .state-text p {
            color: <?php echo esc_attr($txt_color_facts); ?>;
        }
    </style>
<?php }; ?>
<!-- Start state -->
<div>
    <div class="state">
        <div class="row">
            <div class="col-md-12 col-lg-6 state-text">
                <p class="text-right">
                    <?php echo $title_facts_one ?>
                    <br>
                    <strong><?php echo $title_facts_two ?></strong>
                    <br>
                    <span><?php echo $title_facts_three ?></span>‍‍‍
                </p>
                <div class="meteor1"></div>
                <div class="meteor2"></div>
                <div class="meteor3"></div>
                <div class="meteor4"></div>
                <div class="meteor5"></div>
                <div class="meteor6"></div>
                <div class="meteor7"></div>
            </div>
            <div class="col-md-12 col-lg-6 state-numbers">
                <div class="state-numbers-inner">
                    <ul class="">
                        <li class="students">
                            <span class="icon-student px-3">
                                <i class="<?php echo $title_facts_icon_four ?>"></i>
                            </span>
                            <div class="num"><?php echo $title_facts_num_four ?></div>
                            <div class="text"><?php echo $title_facts_title_four ?></div>
                        </li>
                        <li class="courses">
                            <span class="icon-books">
                                <i class="<?php echo $title_facts_icon_one ?>"></i>
                            </span>
                            <div class="num"><?php echo $title_facts_num_one ?></div>
                            <div class="text"><?php echo $title_facts_title_one ?></div>
                        </li>
                        <li class="certificate">
                            <span class="icon-diploma">
                                <i class="<?php echo $title_facts_icon_three ?>"></i>
                            </span>
                            <div class="num">
                                <?php echo $title_facts_num_three ?>
                            </div>
                            <div class="text"><?php echo $title_facts_title_three ?></div>
                        </li>
                        <li class="hours">
                            <span class="icon-camera">
                                <i class="<?php echo $title_facts_icon_two ?>"></i>
                            </span>
                            <div class="num"><?php echo $title_facts_num_two ?></div>
                            <div class="text"><?php echo $title_facts_title_two ?></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End state -->

<?php wp_reset_postdata(); ?>
