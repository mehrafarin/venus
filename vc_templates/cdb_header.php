<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);
$link = ('||' === $link) ? '' : $link;
$link = vc_build_link($link);
$a_href = $link['url'];
$a_target = $link['target'];
$gallery = shortcode_atts(
    array(
        'image_header' => 'image_header',
    ), $atts);

$image_ids = explode(',', $gallery['image_header']);
$image_no = 1;
foreach ($image_ids as $image_id) {
    $images = wp_get_attachment_image_src($image_id, 'full');
    $image_no++;
}
?>

<div class="container">
    <div class="row">
        <div class="lms-top-header">
            <div class="header-detail">
                <?php if ($color_txt_right_header != '') { ?>
                    <style>
                        .lms-top-header .header-detail p {
                            color: <?php echo $color_txt_right_header; ?>;
                        }
                    </style>
                <?php } ?>
                <p class="px-3">
                    <span class="text1 mb-2"><?php echo $title_row_one ?></span>
                    <span class="text2 mb-2"><?php echo $title_row_two ?></span>
                    <span class="text3"><?php echo $title_row_three ?></span>
                </p>
                <?php if ($colorbgbtn_header != '') { ?>
                    <style>
                        .lms-top-header .header-detail a.header-register {
                            background-color: <?php echo esc_attr($colorbgbtn_header); ?>;
                            box-shadow: 0 2px 12px <?php echo esc_attr($colorbgbtn_header); ?>;
                        }

                        .lms-top-header .header-detail a.header-register:hover {
                            background-color: <?php echo esc_attr($colorbgbtn_header); ?>99;
                        }
                    </style>
                <?php }; ?>
                <?php if (!empty($btn_title_link)) : ?>
                    <a href="<?php echo esc_url($a_href); ?>" target="<?php echo esc_url($a_target); ?>"
                       class="header-register mx-3"><?php echo $btn_title_link ?></a>
                <?php endif; ?>
            </div>
            <div class="header-bg col-md-5 col-xs-5">
                <style>
                    .lms-top-header .header-bg .header-bg-image:after {
                        background: <?php echo esc_attr($colorbg_header); ?>;
                        background: linear-gradient(324deg, <?php echo esc_attr($colorbg_header); ?> 0%, <?php echo esc_attr($colorbg_header); ?> 100%);
                    }
                </style>
                <div class="header-bg-image" style="background: url(<?php echo $images[0]; ?>) right no-repeat;"></div>
                <?php if ($color_txt_header != '') { ?>
                    <style>
                        .lms-top-header .header-bg p {
                            color: <?php echo $color_txt_header; ?>;
                        }
                    </style>
                <?php } ?>
                <p><strong><?php echo $title_row_left_one ?></strong>
                    <br>
                    <?php echo $title_row_left_two ?>
                    <br>
                    <?php echo $title_row_left_three ?>
                </p>
                <div class="meteor1"></div>
                <div class="meteor2"></div>
                <div class="meteor3"></div>
                <div class="meteor4"></div>
                <div class="meteor5"></div>
                <div class="meteor6"></div>
            </div>
        </div>
    </div>
</div>

<?php wp_reset_postdata(); ?>
