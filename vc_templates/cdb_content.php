    <?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}


extract($atts);
$gallery = shortcode_atts(
    array(
        'image_cover'  =>  'image_cover',
    ), $atts );

$image_ids=explode(',',$gallery['image_cover']);
$image_no = 1;
foreach( $image_ids as $image_id ) {
    $images = wp_get_attachment_image_src($image_id, 'full');
    $image_no++;
}
//print_r($images[0]);
?>
<?php if (!empty($video_link)): ?>
    <video class="rounded mb-4 outline" width="100%" controls poster="<?php echo $images[0]; ?>">
        <source src="<?php echo esc_attr($video_link); ?>" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
<?php endif; ?>


