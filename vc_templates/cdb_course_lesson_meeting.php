<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}
extract($atts);
// Get current user and check if he bought current course
$bought_course = false;
$current_user = wp_get_current_user();
if (!empty($current_user->user_email) and !empty($current_user->ID)) {
    if (wc_customer_bought_product($current_user->user_email, $current_user->ID, get_the_id())) {
        $bought_course = true;
    }
}
global $product;
$current_user = wp_get_current_user();
?>
<?php if (!empty($title_meeting)): ?>
    <li>
        <div class="download-detail" id="accordion_meeting">
            <div class="detail-header d-flex" id="detail-heading0">
                <button class="sub-btn collapsed" data-toggle="collapse"
                        data-target="#dcollapse<?php echo esc_attr($id_meeting); ?>" aria-expanded="false"
                        aria-controls="collapse<?php echo esc_attr($id_meeting); ?>"></button>
                <a class="session collapsed" data-toggle="collapse"
                   data-target="#dcollapse<?php echo esc_attr($id_meeting); ?>" aria-expanded="false"
                   aria-controls="collapse<?php echo esc_attr($id_meeting); ?>">
                    <?php echo esc_attr($title_meeting); ?>
                </a>
                <?php if ((!empty($preview_video))) : ?>
                    <div class="info-lesson-venus h-fit mr-auto">
                        <?php if (!empty($preview_video)): ?>
                            <a class="btn-preview-lesson preview-button"
                               <?php if (!empty($btn_color_preview)): ?>style="background-color: <?php echo esc_attr($btn_color_preview); ?>;"<?php endif; ?>
                               href="<?php echo esc_url($preview_video); ?>">
                                <i class="fal fa-play ml-1 <?php if (empty($txt_btn_preview)) : ?>ml-xl-2<?php endif; ?>"></i>
                                <?php if (empty($txt_btn_preview)) : ?>
                                    <span>
                                    <?php
                                    //    esc_html_e('پیش نمایش', 'venus');
                                    echo $text_btn_preview_video; ?>
                                </span>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($download_lesson)): ?>
                    <?php if ($bought_course): ?>
                        <a class="btn-down-lesson tag-a-lesson my-lg-auto <?php if (empty($preview_video)) : ?>mr-auto<?php endif; ?>"
                           href="<?php echo esc_url($download_lesson); ?>"><i class="fal fa-download d-block"></i></a>
                    <?php else: ?>
                        <div class="btn-down-lesson my-lg-auto <?php if (empty($preview_video)) : ?>mr-auto<?php endif; ?>">
                            <i class="fal fa-download d-block"></i></div>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (!empty($private_lesson)): ?>
                    <div class="btn-private-lesson text-center <?php if (empty($preview_video) & empty($download_lesson)) : ?>mr-auto<?php endif; ?>">
                        <?php
                        if (wc_customer_bought_product($current_user->user_ID, $current_user->ID, $product->get_id())) {
                            echo '<i class="fal fa-lock-open-alt"></i>';
                        } else {
                            echo '<i class="fal fa-lock-alt"></i>';
                        }
                        ?>
                        <span>
						<?php
                        if (wc_customer_bought_product($current_user->user_ID, $current_user->ID, $product->get_id())) {
                            esc_html_e('در دسترس', 'venus');
                        } else {
                            esc_html_e('خصوصی', 'venus');
                        }
                        ?>
					</span>
                    </div>
                <?php endif; ?>
            </div>

            <div class="time"><?php echo esc_attr($time_meeting); ?></div>
            <div id="dcollapse<?php echo esc_attr($id_meeting); ?>" class="collapse" aria-labelledby="detail-heading1"
                 data-parent="#accordion_meeting" aria-expanded="false" style="height: 0px;">
                <div class="detail-list">
                    <?php
                    if (!empty($private_lesson) and $private_lesson) {
                        if ($bought_course) {
                            echo do_shortcode($content);
                        } else {
                            esc_html_e('برای مشاهده این جلسه باید دوره را خریداری کنید.', 'venus');
                        }
                    } else {
                        echo do_shortcode($content);
                    }
                    ?>
                </div>
            </div>
        </div>
    </li>
<?php endif; ?>


