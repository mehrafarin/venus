<?php
global $venus_options;
// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);
$link_btn_product = ('||' === $link_btn_product) ? '' : $link_btn_product;
$link_btn_product = vc_build_link($link_btn_product);
$a_product_href = $link_btn_product['url'];
$a_product_target = $link_btn_product['target'];

$args = array(
    'post_type' => 'product',
    'posts_per_page' => $posts_per_page,
    'orderby' => $orderby,
    'meta_query' => array(),
    'tax_query' => array(
        'relation' => 'AND',
    ),
);

$args['meta_query'][] = array(
    'key' => '_price',
    'value' => 0,
    'compare' => '>=',
    'type' => 'DECIMAL',
);

switch ($courses_type) {
    case 'onsale':
        $product_ids_on_sale = wc_get_product_ids_on_sale();
        $product_ids_on_sale[] = 0;
        $args['post__in'] = $product_ids_on_sale;
        break;
}

switch ($orderby) {
    case 'price':
        $args['meta_key'] = '_price';
        $args['orderby'] = 'meta_value_num';
        break;
    case 'sales':
        $args['meta_key'] = 'total_sales';
        $args['orderby'] = 'meta_value_num';
        break;
    default:
        $args['orderby'] = $orderby;
}

if (!empty($courses_cat_include)) {
    $cat_include = array();
    $courses_cat_include = explode(',', $courses_cat_include);
    foreach ($courses_cat_include as $category) {
        $term = term_exists($category, 'product_cat');
        if ($term !== 0 && $term !== null) {
            $cat_include[] = $term['term_id'];
        }
    }
    if (!empty($cat_include)) {
        $args['tax_query'][] = array(
            'taxonomy' => 'product_cat',
            'terms' => $cat_include,
            'operator' => 'IN',
        );
    }
}

$products_query = new WP_Query($args);

?>
<!--Start Style-->
<?php if ($bgcolor_btn_product != '') { ?>
    <style>
        .courses .course-more {
            background-color: <?php echo esc_attr($bgcolor_btn_product); ?>;
            box-shadow: 0 2px 12px <?php echo esc_attr($bgcolor_btn_product); ?>99;
        }

        .courses .course-more:hover {
            background-color: <?php echo esc_attr($bgcolor_btn_product); ?>;
            box-shadow: 0 2px 12px<?php echo esc_attr($bgcolor_btn_product); ?>;
        }
    </style>
<?php }; ?>
<!-- Start courses -->
<?php if ($select_style_product == "default") { ?>
    <div class="courses">
        <div class="text-left">
            <div class="row">
                <?php if ($products_query->have_posts()) : $i = 0; ?>
                    <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                        <?php if ($select_style_sidebar_home == false) { ?>
                            <div class="col-sm-12 col-md-6 col-xl-4 course">
                                <div class="course-inner">
                                    <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                        <?php
                                        venus_course_status();

                                        $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);

                                        if ($cu_status != 'soon') {
                                            if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                <div class="course-item-sale">
                                                    <?php
                                                    global $product;
                                                    if ($product->is_on_sale()) {

                                                        if (!$product->is_type('variable')) {

                                                            $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                        } else {

                                                            $max_percentage = 0;

                                                            foreach ($product->get_children() as $child_id) {
                                                                $variation = wc_get_product($child_id);
                                                                $price = $variation->get_regular_price();
                                                                $sale = $variation->get_sale_price();
                                                                if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                if ($percentage > $max_percentage) {
                                                                    $max_percentage = $percentage;
                                                                }
                                                            }

                                                        }
                                                        echo "<div class='sale-perc-badge'>";
                                                        echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                        echo "<div class='sale-badge-text'>تخفیف</div>";
                                                        echo "</div>";
                                                    }
                                                    ?>
                                                </div>
                                            <?php }
                                        }
                                        ?>
                                        <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                            <?php if ($select_show_hide_waves_product == false): ?>
                                                <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                     preserveAspectRatio="none" shape-rendering="auto">
                                                    <defs>
                                                        <path id="gentle-wave"
                                                              d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                    </defs>
                                                    <g class="parallax">
                                                        <use xlink:href="#gentle-wave" x="-12" y="11"
                                                             fill="rgba(255,255,255,0.7"></use>
                                                        <use xlink:href="#gentle-wave" x="25" y="13"
                                                             fill="rgba(255,255,255,0.5)"></use>
                                                        <use xlink:href="#gentle-wave" x="100" y="5"
                                                             fill="rgba(255,255,255,0.3)"></use>
                                                        <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                    </g>
                                                </svg>
                                            <?php endif; ?>
                                        </a>
                                        <?php $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                        if ($link_show_course_intro_video) : ?>
                                            <div class="video-button">
                                                <a data-post-id="<?php echo get_the_ID(); ?>"
                                                   href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                   class="intro-play-icon"><i class="fal fa-play"></i></a>
                                            </div>
                                        <?php
                                        endif;
                                        if ($cu_status != 'soon') {
                                            woocommerce_template_loop_add_to_cart();
                                        }
                                        ?>
                                    </header>
                                    <div class="course-detail">
                                        <div class="course-top-content">
                                            <a href="<?php the_permalink(); ?>" class="title">
                                                <h2><?php the_title(); ?></h2>
                                            </a>
                                            <div class="course-description">
                                                <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                            </div>
                                        </div>
                                        <?php if ($style_info_bottom_product == "enable") { ?>
                                            <div class="course-information position-relative d-flex">
                                                <?php if ($money_only_show_product == false) { ?>
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">سطح</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">دوره</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                <?php } ?>
                                                <div class="col p-0 text-center price-course m-auto">
                                                    <?php
                                                    global $product;
                                                    echo $product->get_price_html(); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-sm-12 col-md-6 course">
                                <div class="course-inner">
                                    <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                        <?php
                                        venus_course_status();

                                        $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);

                                        if ($cu_status != 'soon') {
                                            if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                                <div class="course-item-sale">
                                                    <?php
                                                    global $product;

                                                    if ($product->is_on_sale()) {

                                                        if (!$product->is_type('variable')) {

                                                            $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                        } else {

                                                            $max_percentage = 0;

                                                            foreach ($product->get_children() as $child_id) {
                                                                $variation = wc_get_product($child_id);
                                                                $price = $variation->get_regular_price();
                                                                $sale = $variation->get_sale_price();
                                                                if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                                if ($percentage > $max_percentage) {
                                                                    $max_percentage = $percentage;
                                                                }
                                                            }

                                                        }
                                                        echo "<div class='sale-perc-badge'>";
                                                        echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                        echo "<div class='sale-badge-text'>تخفیف</div>";
                                                        echo "</div>";
                                                    }
                                                    ?>
                                                </div>
                                            <?php }
                                        }
                                        ?>
                                        <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                            <?php if ($select_show_hide_waves_product == false): ?>
                                                <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                                     preserveAspectRatio="none" shape-rendering="auto">
                                                    <defs>
                                                        <path id="gentle-wave"
                                                              d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                                    </defs>
                                                    <g class="parallax">
                                                        <use xlink:href="#gentle-wave" x="-12" y="11"
                                                             fill="rgba(255,255,255,0.7"></use>
                                                        <use xlink:href="#gentle-wave" x="25" y="13"
                                                             fill="rgba(255,255,255,0.5)"></use>
                                                        <use xlink:href="#gentle-wave" x="100" y="5"
                                                             fill="rgba(255,255,255,0.3)"></use>
                                                        <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                                    </g>
                                                </svg>
                                            <?php endif; ?>
                                        </a>
                                        <?php
                                        $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                        if ($link_show_course_intro_video) : ?>
                                            <div class="video-button">
                                                <a data-post-id="<?php echo get_the_ID(); ?>"
                                                   href="<?php echo esc_url($link_show_course_intro_video); ?>"
                                                   class="intro-play-icon"><i class="fal fa-play"></i></a>
                                            </div>
                                        <?php endif;
                                        if ($cu_status != 'soon') {
                                            woocommerce_template_loop_add_to_cart();
                                        }
                                        ?>
                                    </header>
                                    <div class="course-detail">
                                        <div class="course-top-content">
                                            <a href="<?php the_permalink(); ?>" class="title">
                                                <h2><?php the_title(); ?></h2>
                                            </a>
                                            <div class="course-description">
                                                <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                            </div>
                                        </div>
                                        <?php if ($style_info_bottom_product == "enable") { ?>
                                            <div class="course-information position-relative d-flex">
                                                <?php if ($money_only_show_product == false) { ?>
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">سطح</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                    <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                        <div class="col p-0 text-right">
                                                            <span class="level-course">دوره</span>
                                                            <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                        </div>
                                                    <?php } else {
                                                    } ?>
                                                <?php } ?>
                                                <div class="col p-0 text-center price-course m-auto">
                                                    <?php
                                                    global $product;
                                                    echo $product->get_price_html(); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $i++; endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php else : ?>
                    <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
                <?php endif; ?>
            </div>
            <?php if (!empty($btn_title_link_product)) { ?>
                <a href="<?php echo esc_url($a_product_href); ?>" target="<?php echo esc_url($a_product_target); ?>"
                   class="course-more"><?php echo $btn_title_link_product ?></a>
            <?php } ?>
        </div>
    </div>
    <!-- End courses -->
<?php } elseif ($select_style_product == "version_one") { ?>

    <div class="swiper-container swiper-product" data-loop="false">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php if ($products_query->have_posts()) : $i = 0; ?>
                <?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
                    <div class="course pt-0 swiper-slide">
                        <div class="course-inner">
                            <header style="background: url(<?php the_post_thumbnail_url(); ?>) center no-repeat;">
                                <?php
                                venus_course_status();

                                $cu_status = get_post_meta(get_the_ID(), 'course_status_course', true);
                                if ($cu_status != 'soon') {
                                    if (isset($venus_options['discount_percent']) && $venus_options['discount_percent']) { ?>
                                        <div class="course-item-sale">
                                            <?php
                                            global $product;

                                            if ($product->is_on_sale()) {

                                                if (!$product->is_type('variable')) {

                                                    $max_percentage = (($product->get_regular_price() - $product->get_sale_price()) / $product->get_regular_price()) * 100;

                                                } else {

                                                    $max_percentage = 0;

                                                    foreach ($product->get_children() as $child_id) {
                                                        $variation = wc_get_product($child_id);
                                                        $price = $variation->get_regular_price();
                                                        $sale = $variation->get_sale_price();
                                                        if ($price != 0 && !empty($sale)) $percentage = ($price - $sale) / $price * 100;
                                                        if ($percentage > $max_percentage) {
                                                            $max_percentage = $percentage;
                                                        }
                                                    }

                                                }
                                                echo "<div class='sale-perc-badge'>";
                                                echo "<div class='sale-perc'>" . round($max_percentage) . "% </div>";
                                                echo "<div class='sale-badge-text'>تخفیف</div>";
                                                echo "</div>";
                                            }
                                            ?>
                                        </div>
                                    <?php }
                                }
                                ?>
                                <a href="<?php the_permalink(); ?>" class="course-inner-head-a">
                                    <?php if ($select_show_hide_waves_product == false): ?>
                                        <svg class="venus-waves" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28"
                                             preserveAspectRatio="none" shape-rendering="auto">
                                            <defs>
                                                <path id="gentle-wave"
                                                      d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
                                            </defs>
                                            <g class="parallax">
                                                <use xlink:href="#gentle-wave" x="-12" y="11"
                                                     fill="rgba(255,255,255,0.7"></use>
                                                <use xlink:href="#gentle-wave" x="25" y="13"
                                                     fill="rgba(255,255,255,0.5)"></use>
                                                <use xlink:href="#gentle-wave" x="100" y="5"
                                                     fill="rgba(255,255,255,0.3)"></use>
                                                <use xlink:href="#gentle-wave" x="90" y="7" fill="#fff"></use>
                                            </g>
                                        </svg>
                                    <?php endif; ?>
                                </a>
                                <?php
                                $link_show_course_intro_video = get_post_meta(get_the_ID(), 'cor_intro_video', true);
                                if ($link_show_course_intro_video) : ?>
                                    <div class="video-button">
                                        <a data-post-id="<?php echo get_the_ID(); ?>" href="<?php echo esc_url($link_show_course_intro_video); ?>" class="intro-play-icon">
                                            <i class="fal fa-play"></i>
                                        </a>
                                    </div>
                                <?php endif;
                                if ($cu_status != 'soon') {
                                    woocommerce_template_loop_add_to_cart();
                                }
                                ?>
                            </header>
                            <div class="course-detail">
                                <div class="course-top-content">
                                    <a href="<?php the_permalink(); ?>" class="title"><h2><?php the_title(); ?></h2></a>
                                    <div class="course-description">
                                        <?php echo get_post_meta(get_the_ID(), 'product_dic', true) ?>
                                    </div>
                                </div>
                                <?php if ($style_info_bottom_product == "enable") { ?>
                                    <div class="course-information position-relative d-flex">
                                        <?php if ($money_only_show_product == false) { ?>
                                            <?php if (empty(get_post_meta(get_the_ID(), 'level_course', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">سطح</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'level_course', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                            <?php if (empty(get_post_meta(get_the_ID(), 'pro_type', true)) == false) { ?>
                                                <div class="col p-0 text-right">
                                                    <span class="level-course">دوره</span>
                                                    <span class="name-level-course"><?php echo get_post_meta(get_the_ID(), 'pro_type', true) ?></span>
                                                </div>
                                            <?php } else {
                                            } ?>
                                        <?php } ?>
                                        <div class="col p-0 text-center price-course m-auto">
                                            <?php
                                            global $product;
                                            echo $product->get_price_html();
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php $i++; endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php esc_html_e('متاسفانه محصولی پیدا نشد'); ?></p>
            <?php endif; ?>
        </div>
        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev btn-prev-related op-blog-btn"></div>
        <div class="swiper-button-next btn-next-related op-blog-btn"></div>
    </div>
    <!--    <div class="swiper-pagination w-100"></div>-->
<?php } ?>

<?php wp_reset_postdata(); ?>
