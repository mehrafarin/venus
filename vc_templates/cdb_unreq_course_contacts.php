<?php

// Atts
if ( function_exists( 'vc_map_get_attributes' ) ) {
    $atts = vc_map_get_attributes( $this->getShortcode(), $atts );
}

extract( $atts );
$switch_req = '';
if($switch_unreq == true){
    $switch_req = 'unreq';
}else{
    $switch_req = 'req';
}
?>
<div class="<?php echo $switch_req; ?>">
    <div class="<?php echo $switch_req;?>-title"><?php echo $title_unreq ?></div>
    <?php echo do_shortcode($content); ?>
</div>
