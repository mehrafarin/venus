<?php

// Atts
if (function_exists('vc_map_get_attributes')) {
    $atts = vc_map_get_attributes($this->getShortcode(), $atts);
}

extract($atts);


?>
<div id="course-lessons" class="<?php echo vc_shortcode_custom_css_class($css, ''); ?> rounded mt-5">
    <?php if (!empty($title)): ?>
        <h2 class="section-title"><?php echo esc_attr($title); ?></h2>
    <?php endif; ?>
    <div>
        <?php echo wpb_js_remove_wpautop($content); ?>
    </div>
</div>
