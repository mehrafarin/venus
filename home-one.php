<?php get_header(); ?>
<?php /* Template Name: صفحه اصلی */ ?>

<?php if( iwp_Shield::is_activated() === true ) : ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="container">
        <?php the_content(); ?>
    </div>
<?php endwhile; else : ?>
    <p><?php esc_html_e('متاسفانه محتوایی برای نمایش وجود ندارد. لطفا به صفحه اصلی بروید یا از کادر جستجو در بالای سایت استفاده نمایید'); ?></p>
<?php endif; ?>
<?php endif; ?>
<?php get_footer(); ?>
