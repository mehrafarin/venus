<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if (post_password_required())
    return;
?>
<?php if (have_comments()) : ?>
<ol class="comment-list m-0 p-0">
    <?php
    wp_list_comments(array(
        'type'		  => 'comment',
        'style'       => 'ul',
        'avatar_size' => 80,
        'callback' => 'mytheme_comment'
    ));
    ?>
</ol>
    <?php
    // Are there comments to navigate through?
    if (get_comment_pages_count() > 1 && get_option('page_comments')) :
        ?>
        <nav class="navigation comment-navigation" role="navigation">
            <h1 class="screen-reader-text section-heading"><?php _e('صفحات دیدگاه', 'twentythirteen'); ?></h1>
            <div class="nav-previous"><?php previous_comments_link(__('&larr; قدیمی تر', 'twentythirteen')); ?></div>
            <div class="nav-next"><?php next_comments_link(__('جدید تر &rarr;', 'twentythirteen')); ?></div>
        </nav><!-- .comment-navigation -->
    <?php endif; // Check for comment navigation ?>
<?php endif; // have_comments() ?>
<?php if (comments_open()) : ?>
    <?php
    $args = array(
        'id_form' => 'commentform',
        'id_submit' => 'submit',
        'class_submit' => 'w-100 btn-com',
        'title_reply' => __(''),
        'title_reply_to' => __('ارسال پاسخ به %s'),
        'cancel_reply_link' => __('لغو پاسخ'),
        'label_submit' => __('Post Comment'),

        'comment_field' => '<div class="text_area">' . '<textarea id="comment" placeholder="متن دیدگاه" name="comment" cols="58" rows="3" aria-required="true">' . '</textarea>' . '</div>',

        'must_log_in' => '<div class="comment-respond-a"><h3 style="font-size: 20px">پاسخی بگذارید</h3><p class="m-0">برای نوشتن دیدگاه باید <span style="color: #007bff; cursor: pointer;" data-toggle="modal" data-target="#popup-login">وارد بشوید</span>.</p></div>',

        'logged_in_as' => '<p class="logged-in-as">' .
            sprintf(
                __( 'شما با حساب کاربری  <a href="%1$s">%2$s</a> وارد شده‌اید. <a href="%3$s" title="خروج از این حساب کاربری ">خارج می‌شوید؟</a>' ),
                admin_url( 'profile.php' ),
                $user_identity,
                wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
            ) . '</p>',

        'comment_notes_before' => '',
        'comment_notes_after' => '',

        'fields' => apply_filters('comment_form_default_fields', array(

                'author' => '<div class="field_comm">' . '<input id="author" placeholder="نام" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '"/>',

                'email' => '<input id="email" name="email" placeholder="ایمیل" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30"/>' . '</div>',
            )
        ),
    );

    comment_form($args, get_the_ID());
    ?>

<?php endif; ?>
