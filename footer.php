<?php
global $venus_options;
$footer = $venus_options['select_footer'];
switch ($footer) {
    case 'venus_default_footer':
        get_template_part('template/footer/footer');
        break;
    case 'no_footer':
        break;
    default:
        $footerPost = $footer;
        if ($footerPost != null) {
            echo \Elementor\Plugin::$instance->frontend->get_builder_content($footerPost);
        }
}
?>
<!-- End Footer -->
<?php wp_footer(); ?>
</body>
</html>

