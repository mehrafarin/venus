<?php get_header();

global $venus_options;

if ( isset( $venus_options['img_404'] ) && strlen( $venus_options['img_404']['url'] ) > 0 ) {
    $img_404 = $venus_options['img_404']['url'];
} else {
    $img_404 = get_template_directory_uri().'/images/source.gif';
}
?>

<div class="error404-inner container">
    <div class="image-404">
        <img src="<?php echo $img_404; ?>" class="d-block m-auto" alt="<?php wp_title(''); ?>">
    </div>
    <h2 class="text-center mt-2 mb-5 notfound">متاسفانه صفحه مورد نظر یافت نشد :(</h2>
    <p class="mt-5 pt-3" style="text-align: center;font-size: 22px;margin-bottom: 30px;">به دنبال مطلب خاصی می گردید؟<br>
        چرا جستجو نمی کنید؟</p>
    <form role="search" method="get" class="notfound-search position-relative d-flex text-left" action="<?php echo home_url( '/' ); ?>" style="width: 43%;margin: 0 auto;margin-bottom: 100px;">
        <input type="search" class="search-field vstr_isHover" placeholder="عنوان مورد نظر خود را وارد نمایید..." value="<?php echo get_search_query() ?>" name="s">
        <button class="btn">
            <span>جستجو</span>
        </button>
    </form>
</div>

<?php get_footer(); ?>
