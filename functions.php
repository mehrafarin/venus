<?php

/*
Theme Designed By: AmirMohammad Mirlohi
Email: amir.mirlohi79@gmail.com
Demo Website: Bigersoft.com/venus
Author Website: BigerSoft.com
*/

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

//Theme's constants
define('Venus_URI', get_template_directory_uri());
define('Venus_DIR', get_template_directory());

load_theme_textdomain('venusshop', trailingslashit(get_template_directory()) . 'languages');

require_once(get_template_directory() . '/inc/admin/venus-config.php');

global $venus_options;

$venus_options = get_option('venus_options');

add_action('wp_enqueue_scripts', 'loadfiles');

function loadfiles()
{
    global $venus_options;

    $theme_version = wp_get_theme()->get('Version');

    wp_enqueue_style('style', Venus_URI . '/style.css', false, $theme_version);

    wp_enqueue_style('swiper-min', Venus_URI . '/css/swiper.min.css', false, $theme_version);

    wp_enqueue_style('responsive', Venus_URI . '/css/responsive.css', false, $theme_version);

    wp_enqueue_style('venus-font', Venus_URI . '/css/fonts/' . $venus_options['ve_main_font'] . '.css', false, $theme_version);

    wp_enqueue_style('digikala', Venus_URI . '/css/digikala.css', false, $theme_version);

    if (is_plugin_active('dokan-lite/dokan.php')) :
        wp_enqueue_style('Dokan', Venus_URI . '/css/plugin/dokan.css', false, $theme_version);
    endif;

    wp_enqueue_style('LearnPress', Venus_URI . '/css/learn-press.css', false, $theme_version);

    if ($venus_options['style_plugin_ticket']):
        wp_enqueue_style('Plugin-Ticket', Venus_URI . '/css/plugin/wp-ticket.css', false, $theme_version);
    endif;

    if ($venus_options['style_plugin_yith_woocommerce_deposits']):
        wp_enqueue_style('Plugin-Yith-Woo-Deposits', Venus_URI . '/css/plugin/yith-woo-deposits.css', false, $theme_version);
    endif;

    wp_enqueue_script('jquery', Venus_URI . '/js/jquery.min.js', array(), '1.0');

//    wp_enqueue_script('bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array(), $theme_version, 'false');

    wp_enqueue_script('bootstrap.bundle.min', Venus_URI . '/js/bootstrap.bundle.min.js', array(), $theme_version);

    wp_enqueue_script('jquery.sidr', Venus_URI . '/js/jquery.sidr.min.js', array(), $theme_version);

    wp_enqueue_script('carousel', Venus_URI . '/js/owl.carousel.min.js', array(), $theme_version);

    wp_enqueue_script('jquery.appear', Venus_URI . '/js/jquery.appear.js', array(), $theme_version);

    wp_enqueue_script('jquery.countTo', Venus_URI . '/js/jquery.countTo.js', array(), $theme_version);

    wp_enqueue_script('sticky-sidebar.min', Venus_URI . '/js/theia-sticky-sidebar.min.js', array(), $theme_version);

    wp_enqueue_script('countdown', Venus_URI . '/js/countdown.js', array(), $theme_version);

    wp_enqueue_script('plugin', Venus_URI . '/js/plugin.js', array(), $theme_version);

    $translations = array(
        'countdown_days' => esc_html__('روز', 'venus'),
        'countdown_hours' => esc_html__('ساعت', 'venus'),
        'countdown_mins' => esc_html__('دقیقه', 'venus'),
        'countdown_sec' => esc_html__('ثانیه', 'venus'),
    );

    wp_localize_script('plugin', 'venus_sell_name', $translations);
}

add_action('after_setup_theme', 'venus_setup');
function venus_setup()
{

    add_theme_support('title-tag');

    add_theme_support('post-formats', array('audio', 'video'));

    add_theme_support('post-thumbnails');

    add_image_size('post-size', 720, 508, true);

    add_image_size('post-size-two', 360, 239, true);

    add_image_size('post-size-related', 340, 214, true);

//    add_image_size('product-size', 400, 250, false);

    add_theme_support('woocommerce');

    add_theme_support('wc-product-gallery-zoom');

    add_theme_support('wc-product-gallery-lightbox');

    add_theme_support('wc-product-gallery-slider');

    $venusmenu = array(
        'top_menu' => 'منو اصلی در بالا',
        'mobile_menu' => 'منو اصلی موبایل',
        'footer_menu' => 'منو فوتر سایت'
    );

    register_nav_menus($venusmenu);
}

//breadcrumb
function the_breadcrumb()
{
    $sep = ' <i class="fas fa-chevron-left"></i> ';
    if (!is_front_page()) {
        // Start the breadcrumb with a link to your homepage
        echo '<div class="breadcrumb_venus">';
        echo '<a href="';
        echo get_option('home');
        echo '">';
        bloginfo('name');
        echo '</a>' . $sep;
        // Check if the current page is a category, an archive or a single page. If so show the category or archive name.
        if (is_category() || is_single()) {
            the_category(' <i class="fas fa-chevron-left"></i> ');
        } elseif (is_archive() || is_single()) {
            if (is_day()) {
                printf(__('%s', 'text_domain'), get_the_date());
            } elseif (is_month()) {
                printf(__('%s', 'text_domain'), get_the_date(_x('F Y', 'monthly archives date format', 'text_domain')));
            } elseif (is_year()) {
                printf(__('%s', 'text_domain'), get_the_date(_x('Y', 'yearly archives date format', 'text_domain')));
            } else {
                _e('Blog Archives', 'text_domain');
            }
        }
        // If the current page is a single post, show its title with the separator
        if (is_single()) {
            echo $sep;
            the_title();
        }
        // If the current page is a static page, show its title.
        if (is_page()) {
            echo the_title();
        }
        // if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
        if (is_home()) {
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ($page_for_posts_id) {
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                the_title();
                rewind_posts();
            }
        }
        echo '</div>';
    }
}

// include files
require_once Venus_DIR . '/inc/meta-box.php';
require_once get_parent_theme_file_path('/inc/venus_vc.php');

if (did_action('elementor/loaded')) {
    require_once 'elementor/venus-widgets.php';
}

//require_once(get_template_directory() . '/inc/custom-ajax-auth.php');
require_once Venus_DIR . '/inc/comments.php';
require_once Venus_DIR . '/inc/tgm/example.php';
require_once Venus_DIR . '/inc/codebean_woo.php';
require_once Venus_DIR . '/inc/jdf.php';
require_once Venus_DIR . '/venus-post-type/venus-header.php';
require_once Venus_DIR . '/venus-post-type/venus-footer.php';
require_once Venus_DIR . '/venus-post-type/iron-willed.php';
require_once Venus_DIR . '/venus-post-type/notifications.php';
require_once Venus_DIR . '/inc/course/tab-course.php';
require_once Venus_DIR . '/inc/custom-functions.php';
require_once Venus_DIR . '/inc/dokan.php';

if ($venus_options['switch_digikala_venus']):
    require_once Venus_DIR . '/inc/product/physical/cmb2-new-product.php';
endif;

/**
 * Code that requires CMB2
 */
if (!defined('CMB2_LOADED')) {
    require_once Venus_DIR . '/inc/cmb2/init.php';
}

/**
 * Start Count Cart
 */
add_filter('woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1);
function iconic_cart_count_fragments($fragments)
{
    $fragments['span.number'] = '<span class="number">' . WC()->cart->get_cart_contents_count() . '</span>';
    $fragments['span.wm-cart'] = '<span class="wm-cart">' . WC()->cart->get_cart_contents_count() . '</span>';
    return $fragments;
}

#remove
function remove_editor_menu()
{
    remove_action('admin_menu', '_add_themes_utility_last', 101);
}

add_action('_admin_menu', 'remove_editor_menu', 1);

/**
 * Start Custom Admin
 */
function customAdmin()
{
    global $venus_options;
    $theme_version = wp_get_theme()->get('Version');
    wp_enqueue_style('venus-font', Venus_URI . '/css/fonts/' . $venus_options['ve_main_font'] . '.css', false, $theme_version);
    wp_enqueue_style('style-admin', Venus_URI . '/css/admin.css', false);
    wp_enqueue_style('style-admin-icon', Venus_URI . '/css/all.css', false);

}

add_action('admin_enqueue_scripts', 'customAdmin');

#comment-reply
function theme_queue_js()
{
    if ((!is_admin()) && is_singular() && comments_open() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');
}

add_action('wp_print_scripts', 'theme_queue_js');

// get video address ajax

function get_video_ajax()
{

    $post_id = $_POST['post_id'];
    $url = $_POST['url'];
    if (empty($post_id)) {
        echo $url;
    } else {
        echo $link_show_course_intro_video = get_post_meta($post_id, 'cor_intro_video', true);
    }
    wp_die();
}

add_action('wp_ajax_get_video_ajax', 'get_video_ajax');
add_action('wp_ajax_nopriv_get_video_ajax', 'get_video_ajax');

/**
 * Get Lesson Video Address Ajax
 */
function get_lesson_video_ajax()
{

    $post_id = $_POST['post_id'];
    $url = $_POST['url'];
    if (empty($post_id)) {
        echo $url;
    } else {
        echo $preview_video;
    }
    wp_die();
}

add_action('wp_ajax_get_lesson_video_ajax', 'get_lesson_video_ajax');
add_action('wp_ajax_nopriv_get_lesson_video_ajax', 'get_lesson_video_ajax');

/**
 * Add Footer
 */

function add_main_footer()
{
    global $venus_options;

    if (isset($venus_options['modal_login']) && $venus_options['modal_login']) :
        get_template_part('template/popup-form');
    endif;
}

add_action('wp_footer', 'add_main_footer');

/**
 * User Registered
 */

function hs_user_registered()
{
    global $current_user;
    $registered = $current_user->user_registered;
    $message = date("Y-m-d H:i:s", strtotime($registered));
    $x = new DateTime($message, new DateTimeZone('UTC'));
    $x->setTimeZone(new DateTimeZone('Asia/Tehran'));
    $hs_register = gregorian_to_jalali($x->format('Y'), $x->format('m'), $x->format('d'), ' / ');

    return $hs_register;
}