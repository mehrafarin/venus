<?php
add_action( 'init', 'venus_theme_register_header_post_type' );
function venus_theme_register_header_post_type() {
    $labels = array(
        'name'               => __( 'هدر ساز ونوس', 'venus-theme' ),
        'singular_name'      => __( 'هدر ساز ونوس', 'venus-theme' ),
        'menu_name'          => __( 'هدر ساز ونوس', 'venus-theme' ),
        'name_admin_bar'     => __( 'هدر ساز ونوس', 'venus-theme' ),
        'add_new'            => __( 'افزودن هدر جدید', 'venus-theme' ),
        'add_new_item'       => __( 'افزودن هدر جدید', 'venus-theme' ),
        'new_item'           => __( 'هدر جدید', 'venus-theme' ),
        'edit_item'          => __( 'ویرایش هدر', 'venus-theme' ),
        'view_item'          => __( 'نمایش هدر', 'venus-theme' ),
        'all_items'          => __( 'همه هدر ها', 'venus-theme' ),
        'search_items'       => __( 'جستجو هدر', 'venus-theme' ),
        'parent_item_colon'  => __( 'Parent headers:', 'venus-theme' ),
        'not_found'          => __( 'هدری یافت نشد.', 'venus-theme' ),
        'not_found_in_trash' => __( 'هدری یافت نشد.', 'venus-theme' )
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __( 'هدر ساز ونوس', 'venus-theme' ),
        'supports'           => array( 'title', 'editor'),
        'public'            => true,
        'has_archive'       => false,
        'show_in_nav_menus' => false,
        'menu_position'      => 5,
        'menu_icon'         => 'dashicons-admin-post'
    );
    register_post_type( 'venus-header', $args );
}
