<?php
function Notifications()
{

    $labels = array(
        'name' => _x('اطلاعیه ها', 'Post Type General Name', 'Notifications'),
        'singular_name' => _x('اطلاعیه ها', 'Post Type Singular Name', 'Notifications'),
        'menu_name' => __('اطلاعیه ها', 'Notifications'),
        'name_admin_bar' => __('اطلاعیه ها', 'Notifications'),
        'archives' => __('بایگانی موارد', 'Notifications'),
        'attributes' => __('ویژگی های مورد', 'Notifications'),
        'parent_item_colon' => __('مورد والدین:', 'Notifications'),
        'all_items' => __('همه اطلاعیه ها', 'Notifications'),
        'add_new_item' => __('افزودن اطلاعیه جدید', 'Notifications'),
        'add_new' => __('افزودن', 'Notifications'),
        'new_item' => __('آیتم جدید', 'Notifications'),
        'edit_item' => __('ویرایش', 'Notifications'),
        'update_item' => __('بروزرسانی', 'Notifications'),
        'view_item' => __('نمایش', 'Notifications'),
        'view_items' => __('مشاهده موارد', 'Notifications'),
        'search_items' => __('جستجوی مورد', 'Notifications'),
        'not_found' => __('اطلاعیه پیدا نشد :(', 'Notifications'),
        'not_found_in_trash' => __('در سطل زباله یافت نشد', 'Notifications'),
        'featured_image' => __('تصویر شاخص', 'Notifications'),
        'set_featured_image' => __('تنظیم تصویر برجسته', 'Notifications'),
        'remove_featured_image' => __('تصویر برجسته را حذف کنید', 'Notifications'),
        'use_featured_image' => __('به عنوان تصویر برجسته استفاده کنید', 'Notifications'),
        'insert_into_item' => __('Insert into item', 'Notifications'),
        'uploaded_to_this_item' => __('در این مورد بارگذاری شد', 'Notifications'),
        'items_list' => __('لیست موارد', 'Notifications'),
        'items_list_navigation' => __('پیمایش لیست موارد', 'Notifications'),
        'filter_items_list' => __('لیست موارد فیلتر کنید', 'Notifications'),
    );

    $args = array(
        'label' => __('اطلاعیه ها', 'Notifications'),
        'labels' => $labels,
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 9,
        'menu_icon' => 'dashicons-megaphone',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'rewrite' => array('slug' => 'notifications'),
        'capability_type' => 'page',
        'supports' => array('title', 'editor', 'thumbnail'),

    );

    register_post_type('Notifications', $args);

}

add_action('init', 'Notifications', 0);


/**
 * Add Notifications Woo
 */

add_filter('woocommerce_account_menu_items', 'venus_notify', 40);

function venus_notify($menu_links)
{

    $menu_links = array_slice($menu_links, 0, 3, true)
        + array('notify' => 'اطلاعیه ها')
        + array_slice($menu_links, 3, NULL, true);

    return $menu_links;

}

/**
 * Step 2. Register Permalink Endpoint
 */

add_action('init', 'venus_notify_add_endpoint');

function venus_notify_add_endpoint()
{

    // WP_Rewrite is my Achilles' heel, so please do not ask me for detailed explanation
    add_rewrite_endpoint('notify', EP_PAGES);
}

/**
 * Step 3. Content for the new page in My Account, woocommerce_account_{ENDPOINT NAME}_endpoint
 */

add_action('woocommerce_account_notify_endpoint', 'venus_notify_my_account_endpoint_content');

function venus_notify_my_account_endpoint_content()
{
    global $venus_options;
    ?>
    <div class="ve-loader-js" id="ve-loader-js" style="display: none;">
        <img src="<?php echo Venus_URI ?>/images/loader.gif" alt="">
    </div>
    <h3 class="notify-page-title ve-notify-title">اطلاعیه ها</h3>
    <div class="holder-download">
        <ul class="dl-list">
            <?php
            $args = array(
                'post_type' => 'notifications',
            );

            $my_query = new WP_Query($args);

            if ($my_query->have_posts()) :

                while ($my_query->have_posts()) : $my_query->the_post();

                    $thum_notify = get_the_post_thumbnail_url(get_the_ID());

                    if (empty($thum_notify)) {
                        $thum_notify = Venus_URI . '/images/Message-Icon.jpg';
                    }
                    ?>
                    <li>
                        <a class="title d-sm-flex justify-content-between ve-main-product-dashboard"
                           data-toggle="collapse"
                           href="#Collapse<?php echo get_the_ID(); ?>" role="button" aria-expanded="false"
                           aria-controls="multiCollapseExample1">
                            <span class="ve-download-product-dashboard"
                                  style="background: url(<?php echo $thum_notify; ?>)center no-repeat;width: 120px;height: 120px;background-size: cover;border-radius: 7px;margin-left: 10px;"></span>
                            <span class="tt-name-course col">
                            <?php the_title(); ?>
                            <span class="notify-date float-left"><?php echo get_the_date('j F Y'); ?></span>
                        </span>
                        </a>
                    </li>
                <?php

                endwhile;

            else :
                echo '<p class="not-find-notify">هیچ اطلاعیه ای یافت نشد</p>';
            endif;
            ?>
        </ul>
        <?php
        $args = array(
            'post_type' => 'notifications',
        );

        $my_query = new WP_Query($args);

        if ($my_query->have_posts()) :

            while ($my_query->have_posts()) : $my_query->the_post();
                ?>
                <div class="sub_items collapse multi-collapse" id="Collapse<?php echo get_the_ID(); ?>">
                    <div class="head-inner-box-download mb-5 d-flex justify-content-between">
                        <h2 class="my-auto ml-auto mr-0 notify-page-title"><?php the_title() ?></h2>
                        <a class="btn--back" data-toggle="collapse" href="#Collapse<?php echo get_the_ID(); ?>"
                           role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
                            <i class="far fa-chevron-left"></i>
                        </a>
                    </div>
                    <div class="accordion" id="accordion<?php echo get_the_ID(); ?>">
                        <div id="collapse<?php echo get_the_ID(); ?>" class="collapse show ve-notify-content"
                             aria-labelledby="headingOne" data-parent="#accordion<?php echo get_the_ID(); ?>">
                            <?php the_content(); ?>
                        </div>
                    </div>

                </div>
            <?php endwhile;
        endif;
        ?>
    </div>
    <?php
    wp_reset_postdata();
}