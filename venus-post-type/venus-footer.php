<?php
add_action( 'init', 'venus_register_footer' );
function venus_register_footer() {
    $labels = array(
        'name'               => __( 'فوتر ساز ونوس', 'venus-theme' ),
        'singular_name'      => __( 'فوتر ساز ونوس', 'venus-theme' ),
        'menu_name'          => __( 'فوتر ساز ونوس', 'venus-theme' ),
        'name_admin_bar'     => __( 'فوتر ساز ونوس', 'venus-theme' ),
        'add_new'            => __( 'افزودن فوتر جدید', 'venus-theme' ),
        'add_new_item'       => __( 'افزودن فوتر جدید', 'venus-theme' ),
        'new_item'           => __( 'فوتر جدید', 'venus-theme' ),
        'edit_item'          => __( 'ویرایش فوتر', 'venus-theme' ),
        'view_item'          => __( 'نمایش فوتر', 'venus-theme' ),
        'all_items'          => __( 'همه فوتر ها', 'venus-theme' ),
        'search_items'       => __( 'جستجو فوتر', 'venus-theme' ),
        'parent_item_colon'  => __( 'Parent headers:', 'venus-theme' ),
        'not_found'          => __( 'فوتری یافت نشد.', 'venus-theme' ),
        'not_found_in_trash' => __( 'فوتری یافت نشد.', 'venus-theme' )
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __( 'فوتر ساز ونوس', 'venus-theme' ),
        'supports'           => array( 'title', 'editor'),
        'public'            => true,
        'has_archive'       => false,
        'show_in_nav_menus' => false,
        'menu_position'      => 6,
        'menu_icon'         => 'dashicons-admin-post'
    );
    register_post_type( 'venus-footer', $args );
}
