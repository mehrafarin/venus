<?php
/* Start Post */
add_action('init', 'iron_willed');
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function iron_willed()
{

    $labels = array(
        'name' => 'نظرات دانشجویان',
        'singular_name' => 'نظرات دانشجویان',
        'menu_name' => 'نظرات دانشجویان',
        'name_admin_bar' => 'افزودن نظر',
        'add_new' => 'افزودن نظر',
        'add_new_item' => 'افزودن نظر',
        'new_item' => 'افزودن نظر',
        'edit_item' => 'ویرایش نظر',
        'view_item' => 'نمایش نظر',
        'all_items' => 'همه نظرات',
        'search_items' => 'جستجوی نظر',
        'parent_item_colon' => 'نظر مادرها:',
        'not_found' => 'نظری یافت نشد',
        'not_found_in_trash' => 'نظر در زباله دان یافت نشد'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'نظرات دانشجویان',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-format-video',
        'query_var' => true,
        'rewrite' => array('slug' => 'student_comments'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 7,
        'supports' => array('title', 'editor', 'thumbnail', 'comments')
    );

    register_post_type('student_comments', $args);
}

add_action('cmb2_admin_init', 've_iron_willed');

function ve_iron_willed()
{
    $prefix = '_venus_';

    $student_comments_metaboxes = new_cmb2_box(array(
        'id' => 've_student_comments_metabox',
        'title' => esc_html__('فیلد های نظر', 'venus'),
        'object_types' => array('student_comments'),
        'context' => 'normal',
        'priority' => 'high',
        'tab_group' => 'venus_main_options',
        'show_names' => true,
    ));

    $student_comments_metaboxes->add_field(array(
        'name' => esc_html__('نام دوره', 'venus'),
        'desc' => esc_html__('دوره ای که دانشجو شرکت کرده است را وارد کنید', 'venus'),
        'id' => $prefix . 'title_course',
        'type' => 'text',
    ));

    $student_comments_metaboxes->add_field(array(
        'name' => esc_html__('خلاصه نظر', 'venus'),
        'desc' => esc_html__('این بخش باید کوتاه بوده زیرا در صفحه اصلی نمایش داده می شود', 'venus'),
        'id' => $prefix . 'excerpt_comment',
        'type' => 'textarea_small'
    ));
}
